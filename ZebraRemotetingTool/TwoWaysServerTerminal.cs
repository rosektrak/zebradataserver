using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;

namespace ZebraRemotetingTool
{
    public class TwoWaysServerTerminal
    {
        public static void StartListening(int port, string tcpChannelName,
          MarshalByRefObject remoteObject, string remoteObjectUri)
        {
            TcpChannel channel = CreateTcpChannel(port, tcpChannelName);

            ChannelServices.RegisterChannel(channel, false);

            RemotingServices.Marshal(remoteObject, remoteObjectUri);
        }

        private static TcpChannel CreateTcpChannel(int port, string tcpChannelName)
        {
            BinaryServerFormatterSinkProvider serverFormatter =
                new BinaryServerFormatterSinkProvider();

            serverFormatter.TypeFilterLevel = TypeFilterLevel.Full;

            BinaryClientFormatterSinkProvider clientProv =
                new BinaryClientFormatterSinkProvider();

            Hashtable props = new Hashtable();
            props["port"] = port;
            props["name"] = tcpChannelName;

            return new TcpChannel(
                props, clientProv, serverFormatter);
        }
    }
}
