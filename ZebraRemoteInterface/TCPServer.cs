using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Collections;
using ZebraGatewayInterface;
using System.IO;

namespace ZebraRemoteInterface
{
    public class TCPServer
    {
        private System.Threading.Semaphore Semaphore = new System.Threading.Semaphore(1, 1);

        public event TcpReceiveMsg OnRemoteMsg;

        public Boolean bIsRun = true;
        private TcpListener tcpListener;
        private Thread listenThread;
        private int gTcpServerPort = -1;
  
        private Hashtable _gHsClientList = new Hashtable();
        public Hashtable gHsClientList
        {
            get
            {
                return _gHsClientList;
            }
            set
            {
                _gHsClientList = value;
            }
        }

        public TCPServer(int port)
        {
            gTcpServerPort = port;
        }
        public void ServerStart()
        {
            this.tcpListener = new TcpListener(IPAddress.Any, gTcpServerPort);
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();
        }
        private void ListenForClients()
        {
            this.tcpListener.Start();
            while (bIsRun)
            {
                try
                {
                    TcpClient client = this.tcpListener.AcceptTcpClient();
                    MangerServer mServer = new MangerServer(client);
                    mServer.OnServerReceiveMsg += new TcpReceiveMsg(mServer_OnServerReceiveMsg);
                    mServer.OnServerDisconnectedMsg += new TcpDisconnectedMsg(mServer_OnServerDisconnectedMsg);

                    IPEndPoint ep = (IPEndPoint)client.Client.RemoteEndPoint; //10.0.100.211:1625
                    string key = ep.ToString();
                    if (!gHsClientList.ContainsKey(key))
                    {
                        gHsClientList.Add(key, mServer);
                        WriteNetFile(DateTime.Now.ToString("HH:mm:ss") + " ADD : " + key + " <Port name - " + gTcpServerPort + ">");
                    }
                }
                catch { }
            }
            Terminate();
        }
        private void mServer_OnServerReceiveMsg(TcpClient tcpServer, int veh_id, string cmd, string data)
        {
            if (OnRemoteMsg != null)
            {
                OnRemoteMsg(tcpServer, veh_id, cmd, data);
            }
        }
        private void mServer_OnServerDisconnectedMsg(TcpClient tcp, string data)
        {
            try
            {
                ArrayList arDis = new ArrayList();
                foreach (DictionaryEntry entry in gHsClientList)
                {
                    try
                    {
                        if (gHsClientList.ContainsKey(entry.Key))
                        {
                            arDis.Add(entry.Key.ToString());
                        }
                    }
                    catch
                    { }
                } 
                foreach (string key in arDis)
                {
                    if (gHsClientList.ContainsKey(key))
                    {
                        MangerServer msServer = (MangerServer)gHsClientList[key];
                        if (msServer.tcpClient == null || msServer.tcpClient.Client == null || !msServer.tcpClient.Client.Connected)
                        {
                            string ip = msServer.gIpPortClient;
                            msServer.OnServerReceiveMsg -= new TcpReceiveMsg(mServer_OnServerReceiveMsg);
                            msServer.OnServerDisconnectedMsg -= new TcpDisconnectedMsg(mServer_OnServerDisconnectedMsg);
                            msServer = null;
                            gHsClientList.Remove(key);

                            WriteNetFile(DateTime.Now.ToString("HH:mm:ss") + " Remove : " + key + " : " + ip + " <Port name - " + gTcpServerPort + ">");
                        }
                    }
                }
            }
            catch { }
        }
        public bool Terminate()
        {
            try
            {
                ArrayList arDis = new ArrayList();
                foreach (DictionaryEntry entry in gHsClientList)
                {
                    try
                    {
                        if (gHsClientList.ContainsKey(entry.Key))
                        {
                            arDis.Add(entry.Key.ToString());
                        }
                    }
                    catch
                    { }
                }
                foreach (string key in arDis)
                {
                    if (gHsClientList.ContainsKey(key))
                    {
                        MangerServer msServer = (MangerServer)gHsClientList[key];
                        msServer.tcpClient.Close();
                        msServer = null;
                    }
                }
            }
            catch { }
            return true;
        }
        public void ApplicationExit()
        {
            try
            {
                tcpListener.Stop();
                listenThread.Abort();
                Terminate();
            }
            catch { }
        }
        public bool SendData(string key,string msg)
        {
            Semaphore.WaitOne();
            bool isSuccess = false;
            try
            {
                if (gHsClientList.Count < 1) 
                {
                    Semaphore.Release();
                    return false; 
                }

                foreach (DictionaryEntry entry in gHsClientList)
                {
                    try
                    {
                        MangerServer mServer = (MangerServer)gHsClientList[entry.Key];
                        if (mServer.gIpPortClient == key)
                        {
                            isSuccess = mServer.SendData(msg);
                            break;
                        }
                    }
                    catch
                    { }
                }
            }
            catch
            { }
            Semaphore.Release();
            return isSuccess;
        }
        public void SendDataBroadCast(string msg)
        {
            try
            {
                if (gHsClientList.Count < 1) { return; }
                foreach (DictionaryEntry entry in gHsClientList)
                {
                    try
                    {
                        if (gHsClientList.ContainsKey(entry.Key))
                        {
                            ((MangerServer)gHsClientList[entry.Key]).SendData(msg);
                        }
                    }
                    catch
                    { }
                }
            }
            catch
            { }
        }
     
        private void WriteNetFile(String msg)
        {
            try
            {
                String logPath = Directory.GetCurrentDirectory() + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                String fileName = logPath + "\\" + "DataLog_Connection_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch
            { }
        }
    }
    class MangerServer
    {
        public event TcpReceiveMsg OnServerReceiveMsg;
        public event TcpDisconnectedMsg OnServerDisconnectedMsg;
        public TcpClient tcpClient;
        public string gIpPortClient = string.Empty;

        ~MangerServer()
        { }

        public MangerServer(object client)
        {
             tcpClient = (TcpClient)client;
             tcpClient.Client.ReceiveTimeout = 1000 * 60 * 10;
             tcpClient.Client.SendTimeout = 1000 * 5;

             if (tcpClient != null && tcpClient.Client.Connected)
             {
                 Thread clientThread = new Thread(HandleClientComm);
                 clientThread.Start();
             }
        }

        private void HandleClientComm()
        {
            if (tcpClient == null) { return; }
         
            try
            {
                NetworkStream clientStream = tcpClient.GetStream();
                byte[] message = new byte[4096];
                int bytesRead;

                while (true)
                {
                    bytesRead = 0;
                    message = new byte[4096];

                    try
                    {
                        bytesRead = clientStream.Read(message, 0, 4096);
                    }
                    catch
                    {
                        break;
                    }

                    if (bytesRead == 0)
                    {
                        break;
                    }
                    OnRecive(ASCIIEncoding.ASCII.GetString(message, 0, bytesRead));
                    Thread.Sleep(1);
                }
                clientStream.Close();
                tcpClient.Close();
                Thread.Sleep(500);
                OnServerDisconnected("");
            }
            catch { }
        }
        private void OnRecive(string zMsg)
        {
            try
            {
                if (OnServerReceiveMsg != null)
                {
                    if (zMsg.Trim() != "#T")
                    {
                        if (zMsg.StartsWith("SVIP:"))
                        {
                            string[] str = zMsg.Split(new char[] { ':', '\r' });
                            if (str.Length >= 3)
                            {
                                gIpPortClient = str[1] + ":" + str[2];
                            }
                        }
                        else
                        {
                            string[] str = zMsg.Split(',');
                            if (str.Length == 3)
                            {
                                OnServerReceiveMsg(tcpClient, Convert.ToInt32(str[0]), str[1], str[2]);
                            }
                        }
                    }
                    else if (zMsg == "#T" || zMsg == "#T\r")
                    {
                        SendData(zMsg.Trim());
                    }
                }
            }
            catch { }
        }
        private void OnServerDisconnected(string data)
        {
            try
            {
                if (OnServerDisconnectedMsg != null)
                {
                    OnServerDisconnectedMsg(tcpClient, data);
                }
            }
            catch { }
        }
       
        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        public bool SendData(string msg)
        {
            semaphor.WaitOne();
            try
            {
                NetworkStream clientStream = tcpClient.GetStream();
                ASCIIEncoding encoder = new ASCIIEncoding();

                byte[] buffer = null;
                buffer = encoder.GetBytes(msg + '\r');
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch
            {
                semaphor.Release();
                return false;
            }
            semaphor.Release();
            return true;
        }
    }
}
