using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using ZebraGatewayInterface;

namespace ZebraRemoteInterface
{
    public class RemoteClientInterface
    {
        public event RemoteMsgHandler    OnRemoteMsg ;
        public event ConnectedHandler    OnConnected ;
        public event DisconnectedHandler OnDisconnected ;

        #region properties
        private ArrayList   connList = new ArrayList();
        #endregion

        public RemoteClientInterface()
        {
            
        }
        public void Dispose()
        {
            if (connList != null)
            {
                foreach (ServerList s in connList)
                {
                    s.Dispose();
                }
            }
            connList = null;
        }

        public void Connect(IPEndPoint ep, String name)
        {
            bool found = false;
            if (connList != null)
            {
                foreach (ServerList s in connList)
                {
                    if (s.IsExist(ep))
                    {
                        found = true;
                        break;
                    }
                }
            }

            if (!found)
            {
                ServerList srv = new ServerList(ep, name);
                srv.OnClientReceiveMsg  +=  new ClientReceiveMsgHandler(srv_OnClientReceiveMsg);
                srv.OnConnected         +=  new ConnectedHandler(srv_OnConnected);
                srv.OnDisconnected      +=  new DisconnectedHandler(srv_OnDisconnected);
                connList.Add(srv);
            }
        }

        void srv_OnDisconnected(Socket socket, String name)
        {
            if (OnDisconnected != null)
            {
                OnDisconnected(socket, name);
            }
        }
        void srv_OnConnected(Socket socket, String name)
        {
            if (OnConnected != null) 
            {
                OnConnected(socket, name);
            }
        }

        void srv_OnClientReceiveMsg(Socket socket, string data)
        {
            try
            {
                if (OnRemoteMsg != null)
                {
                    int vid = -1;
                    string cmd = "";
                    string param = "";

                    string[] vidStr = data.Split(new string[] { "VID" }, StringSplitOptions.None);
                    foreach (string vidSunStr in vidStr)
                    {
                        if (vidSunStr.Trim().Length < 1) {
                            continue;
                        }

                        string[] str = vidSunStr.Split(',');

                        if (str.Length > 2) {
                            vid = Convert.ToInt32(str[0]);
                            cmd = str[1];
                            param = str[2];
                            string[] lnStr = param.Split(new char[] { '\n' });
                            foreach (string sl in lnStr)
                            {
                                if (sl.Trim().Length < 1) continue;
                                OnRemoteMsg(socket, vid, cmd, sl);
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }
        public void SendCmd(int veh_id, string cmd, string data)
        {
            RECEIVESOCKETINFO info = new RECEIVESOCKETINFO();
            info.veh_id = veh_id;
            info.cmd = cmd;
            info.data = data.Replace(',','@');

            string msg = info.veh_id.ToString() + "," + info.cmd + "," + info.data;
            if (connList != null)
            {
                foreach (ServerList s in connList)
                {
                    s.Send(msg);
                }
            }
        }
        public void SendCmd(IPEndPoint remoteEP, int veh_id, string cmd, string data)
        {
            RECEIVESOCKETINFO info = new RECEIVESOCKETINFO();
            info.veh_id = veh_id;
            info.cmd = cmd;
            info.data = data.Replace(',','@');

            string msg = info.veh_id.ToString() + "," + info.cmd + "," + info.data;
            if (connList != null)
            {
                foreach (ServerList s in connList)
                {
                    if (s.IsExist(remoteEP))
                    {
                        s.Send(msg);
                        break;
                    }
                }
            }
        }
        public void SendCmdAsyncCallback(IAsyncResult ar)
        {
            Socket sck = (Socket)ar.AsyncState;
            sck.EndSend(ar);
        }
    }

    public class ServerList
    {
        public struct SERVERLISTINFO
        {
            public Socket socket;
            public byte[] buf;
        }

        public event ClientReceiveMsgHandler OnClientReceiveMsg ;
        public event ConnectedHandler        OnConnected ;
        public event DisconnectedHandler     OnDisconnected ;

        private Socket      gNetObj;
        private IPEndPoint  curRemoteEP;
        private bool        IsRun  = true ;
        private Timer       gTimer = null ;
        private string      gName  = "" ;

        public ServerList(IPEndPoint ep, string name)
        {
            try
            {
                gName = name;
                curRemoteEP = ep;

                OpenSocket();

                if (gNetObj == null)
                {
                    return;
                }
                gTimer = new Timer(new TimerCallback(gTimer_Callback), null, 0, 30 * 1000); 
            }
            catch
            {
            }
        }
        private void OpenSocket()
        {
            try
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Any, 0);

                gNetObj = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //gNetObj.SendTimeout = 1000 * 60 * 30;
                //gNetObj.ReceiveTimeout = 1000 * 60 * 30;
                gNetObj.Bind((EndPoint)ep);
            }
            catch
            {
            }
        }
        public void Dispose()
        {
            try
            {
                IsRun = false;
                if (gNetObj != null)
                {
                    if (gNetObj.Connected)
                    {
                        gNetObj.Disconnect(false);
                    }
                    gNetObj.Close();
                }
            }
            catch
            {
            }
        }

        private void gTimer_Callback(object obj)
        {
            try
            {
                if (!gNetObj.Connected)
                {
                    gNetObj.Connect(curRemoteEP);
                    BeginRcv(gNetObj);

                    if (OnConnected != null)
                    {
                        OnConnected(gNetObj, gName);
                    }
                }
                else
                { }
            }
            catch
            {
                try
                {
                    string remStr = gNetObj.RemoteEndPoint.ToString();
                    gNetObj.Close();
                    OpenSocket();
                }
                catch
                {
                    if (OnDisconnected != null)
                    {
                        OnDisconnected(gNetObj, gName);
                    }
                }
            }
        }
        private void BeginRcv(Socket sck)
        {
            try
            {
                SERVERLISTINFO info = new SERVERLISTINFO();
                info.buf = new byte[10240];
                info.socket = sck;
                sck.BeginReceive(info.buf, 0, info.buf.Length, SocketFlags.None, new AsyncCallback(RcvCallback), info);
            }
            catch
            {
            }
        }
        private void RcvCallback(IAsyncResult ar)
        {
            String msg = "";
            SERVERLISTINFO info = (SERVERLISTINFO)ar.AsyncState;

            try {                
                info.socket.EndReceive(ar);
                if (OnClientReceiveMsg != null) {
                    msg = Encoding.ASCII.GetString(info.buf);
                    OnClientReceiveMsg(info.socket, msg);
                }
            }
            catch {
            }

            if (IsRun) {
                BeginRcv(info.socket);
            }
        }
        public void Send(string msg)
        {
            try
            {
                if (gNetObj.Connected)
                {
                    byte[] buf = Encoding.ASCII.GetBytes(msg.ToCharArray());
                    NetworkStream strm = new NetworkStream(gNetObj);
                    strm.BeginWrite(buf, 0, buf.Length, new AsyncCallback(BeginWriteCallBack), strm);
                }
            }
            catch
            {
            }
        }
        private void BeginWriteCallBack(IAsyncResult ar)
        {
            try
            {
                NetworkStream strm = (NetworkStream)ar.AsyncState;
                strm.EndWrite(ar);
            }
            catch
            {
            }
        }
        public bool IsExist(IPEndPoint ep)
        {
            try
            {
                if (gNetObj == null) return false;
                if (gNetObj.RemoteEndPoint == null) return false;

                if (((IPEndPoint)gNetObj.RemoteEndPoint).ToString() == ep.ToString()) return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
