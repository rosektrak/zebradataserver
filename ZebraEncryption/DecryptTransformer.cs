using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace ZebraEncryption
{
    class DecryptTransformer
    {
        private EncryptionAlgorithm algorithmID;
        private byte[] initVec;
        private byte[] initKey;

        public DecryptTransformer(EncryptionAlgorithm deCryptId, byte[] deKey, byte[] deIV)
        {
            algorithmID = deCryptId;
            initKey = deKey;
            initVec = deIV;
        }

        public String GetCryptoServiceProvider(byte[] cipherText, int leng)
        {
            // Pick the provider.
            switch (algorithmID)
            {
                case EncryptionAlgorithm.None:
                    {
                        return Encoding.ASCII.GetString(cipherText, 0, leng);
                    }
                case EncryptionAlgorithm.Des:
                    {
                        //DES des = new DESCryptoServiceProvider();
                        //des.Mode = CipherMode.CBC;
                        //des.Key = bytesKey;
                        //des.IV = initVec;
                        //return des.CreateDecryptor();
                        return null;
                    }
                case EncryptionAlgorithm.TripleDes:
                    {
                        //TripleDES des3 = new TripleDESCryptoServiceProvider();
                        //des3.Mode = CipherMode.CBC;
                        //return des3.CreateDecryptor(bytesKey, initVec);
                        return null;
                    }
                case EncryptionAlgorithm.Rc2:
                    {
                        //RC2 rc2 = new RC2CryptoServiceProvider();
                        //rc2.Mode = CipherMode.CBC;
                        //return rc2.CreateDecryptor(bytesKey, initVec);
                        return null;
                    }
                case EncryptionAlgorithm.Rijndael:
                    {
                        return decryptStringFromBytes_AES(cipherText);
                    }
                default:
                    {
                        throw new CryptographicException("Algorithm ID '" + algorithmID +
                            "' not supported.");
                    }
            }
        } //end GetCryptoServiceProvider

        private String decryptStringFromBytes_AES(byte[] cipherText)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (initKey == null || initKey.Length <= 0)
                throw new ArgumentNullException("Key");
            if (initVec == null || initVec.Length <= 0)
                throw new ArgumentNullException("Key");

            // TDeclare the streams used
            // to decrypt to an in memory
            // array of bytes.
            MemoryStream msDecrypt = null;
            CryptoStream csDecrypt = null;
            StreamReader srDecrypt = null;

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            try
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                aesAlg = new RijndaelManaged();
                aesAlg.Key = initKey;
                aesAlg.IV = initVec;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                msDecrypt = new MemoryStream(cipherText);
                csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
                srDecrypt = new StreamReader(csDecrypt);

                // Read the decrypted bytes from the decrypting stream
                // and place them in a string.
                plaintext = srDecrypt.ReadToEnd();
            }
            finally
            {
                // Clean things up.

                // Close the streams.
                if (srDecrypt != null)
                    srDecrypt.Close();
                if (csDecrypt != null)
                    csDecrypt.Close();
                if (msDecrypt != null)
                    msDecrypt.Close();

                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;

        }
    }
}
