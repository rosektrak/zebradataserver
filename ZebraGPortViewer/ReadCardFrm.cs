using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraGPortViewer
{
    public partial class ReadCardFrm : Form
    {
        private DataTable vidTable = new DataTable();
        public DataTable VidTable
        {
            get { return vidTable; }
            set { vidTable = value; }
        }

        private DataTable cmdTable = new DataTable();
        public ReadCardFrm()
        {
            InitializeComponent();            
        }

        private String selectedEP = "?";
        public String SelectedEP
        {
            get { return selectedEP; }
            set { selectedEP = value; }
        }

        private String selectedCmd = "?";
        public String SelectedCmd
        {
            get { return selectedCmd; }
            set { selectedCmd = value; }
        }

        private String vid = "?";
        public String Vid
        {
            get { return vid; }
            set { vid = value; }
        }


        private void IntObj()
        {
            InitCmdTable();

            cbVIDList.DataSource    =   vidTable ;
            cbVIDList.DisplayMember =   "registration" ;
            cbVIDList.ValueMember   =   "ip_endpoint" ;

            cbCmdList.DataSource    =   cmdTable ;
            cbCmdList.DisplayMember =   "cmd_display" ;
            cbCmdList.ValueMember   =   "cmd_message" ;
            cbCmdList.SelectedIndex =   0 ;
        }

        private void InitCmdTable()
        {
            cmdTable.Columns.Add("cmd_display", typeof(String));
            cmdTable.Columns.Add("cmd_message", typeof(String));

            cmdTable.Rows.Add(new object[] { "Reads Memory Block #0", "*>BR0"});
            cmdTable.Rows.Add(new object[] { "Reads Memory Block #1", "*>BR1" });
            cmdTable.Rows.Add(new object[] { "Reads Memory Block #2", "*>BR2" });
            cmdTable.Rows.Add(new object[] { "Reads Memory Block #3", "*>BR3" });
            cmdTable.Rows.Add(new object[] { "Reads Memory Block #4", "*>BR4" });
            cmdTable.Rows.Add(new object[] { "Reads Memory Block #5", "*>BR5" });
        }

        private void ReadCardFrm_Load(object sender, EventArgs e)
        {
            IntObj();
            DataRow[] row = vidTable.Select(String.Format("ip_endpoint = '{0}'",SelectedEP));
            if (row.Length > 0)
            {
                cbVIDList.Text = row[0]["registration"].ToString();
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (cbVIDList.SelectedValue != null)
            {
                SelectedEP = cbVIDList.SelectedValue.ToString();
            }

            if (cbCmdList.SelectedValue != null)
            {
                SelectedCmd = cbCmdList.SelectedValue.ToString();
            }

            if (cbVIDList.Text != null)
            {
                Vid = cbVIDList.Text.ToString().Trim();
            }
        }
    }
}