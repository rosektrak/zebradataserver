using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.Collections;
using System.IO;

namespace ZebraNet
{
    public delegate void ReceivedMsgHandler(Object Sender, ReceivedMsgEvent e);    
    public delegate void SockHandler();

    public class ReceivedMsgEvent : EventArgs
    {
        public byte[]   data ;
        public EndPoint remoteIP ;
        public int      port ;
    }    

    public class ComNet
    {
        private int netPort = 1;
        public  int NetPort
        {
            get { return netPort; }
            set { netPort = value; }
        }

        private Thread netThread ;

        private Socket netSocket ;
        public  Socket NetSocket
        {
            get { return netSocket; }
            set { netSocket = value; }
        }

        public ComNet(AddressFamily addrFamaly, SocketType sckType, ProtocolType ptcType, int port, Boolean listen)
        {
            try
            {
                if (netSocket != null && netSocket.IsBound)
                {
                    netSocket.Close();
                }

                netPort = port;
                netSocket = new Socket(addrFamaly, sckType, ptcType);

                if (listen)
                {
                    netThread = new Thread(new ThreadStart(ListeningProcThread));
                    if (netThread != null)
                    {
                        netThread.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("ComNet Error #1:" + ex.Message);
            }
        }
        ~ComNet()
        {
            try
            {
                netThread.Abort();
            }
            catch { }
        }

        protected virtual void ListeningProcThread()
        {
        }

        protected void CloseCom()
        {
            if (netThread != null && netThread.IsAlive)
            {
                netThread.Abort();
            }
        }

        protected void WriteLogEvent(String msg)
        {
           
            msg = DateTime.Now.ToString() + " : " + msg;
            String fileName = "ComNetLogEvent.txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
        }
    }
}
