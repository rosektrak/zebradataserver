using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.ComponentModel;
using System.Collections;
using System.Timers;

namespace ZebraNet
{
    public class TcpNet : ComNet
    {
        private const int MAXIMUM_LISTEN = 200 ;
        private const int RESETCONNECTION_TIMER = 1000 * 60 * 3 ;

        Boolean bRunThread = false;
        Boolean bDisconnected = false;

        public bool IsClientListBusy = false;
        System.Threading.Semaphore clientListBusy = new System.Threading.Semaphore(1, 1);

        private List<ClientManager> _clientList;
        public List<ClientManager> ClientList
        {
            get 
            {
                return _clientList; 
            }
            set 
            {
                _clientList = value;
            }
        }

        public  event ReceivedMsgHandler        OnReceivedMsg ;
        public  event NetDisconnetedHandler     OnNetDisconnected ;

        public TcpNet(int port, Boolean listen)
            : base(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp, port, listen)
        {
            try
            {
                ClientList = new List<ClientManager>();

                IPEndPoint localEP = new IPEndPoint(IPAddress.Any, port);
                NetSocket.Bind(localEP);
                NetSocket.Listen( MAXIMUM_LISTEN );

                bRunThread = true;
            }
            catch (SocketException ex)
            {
                WriteLogEvent("TcpNet Error #1:" + ex.StackTrace + ',' + ex.Message);
            }
        }

        ~TcpNet()
        {            
            CloseAll();
        }
        
        private void Shutdown()
        {
            if (this.NetSocket.Connected) this.NetSocket.Disconnect(false);
            this.NetSocket.Close();
            this.bDisconnected = true;
        }
        public void CloseAll()
        {
            CloseSocket() ;
            GC.Collect() ;
        }
        public void CloseSocket()
        {
            try
            {
                if (ClientList != null)
                {
                    int cnt = 0;
                    do 
                    {
                        try
                        {
                            for (int i=0; i<ClientList.Count; i++)
                            {
                                ClientManager mngr = (ClientManager)ClientList[i];                                
                                mngr.ShutdowConnection() ;
                                Thread.Sleep(10);
                            }

                            ClientList.Clear();
                        }
                        catch { }
                        cnt = ClientList.Count ;
                    } while (cnt > 0);
                }

                base.CloseCom();
                NetSocket.Close();

                Shutdown();
            }
            catch (Exception ex)
            {
                WriteLogEvent("TcpNet Error #1.1:" + ex.StackTrace + ',' + ex.Message);
            }
        }

        protected override void ListeningProcThread()
        {
            IPHostEntry localHostEntry;
            try
            {
                try
                {
                    localHostEntry = Dns.GetHostByName(Dns.GetHostName());
                }
                catch (Exception ex)
                {
                    WriteLogEvent("TcpNet Error #2:" + ex.Message);
                    return;
                }

                while (true)
                {
                    if (!bRunThread)
                    {
                        Thread.Sleep(2000);
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (!NetSocket.IsBound)
                {
                    try
                    {
                        IPEndPoint localIpEndPoint = new IPEndPoint(IPAddress.Any, NetPort);
                        NetSocket.Bind(localIpEndPoint);
                        NetSocket.Listen( MAXIMUM_LISTEN );
                    }
                    catch (Exception e)
                    {
                        WriteLogEvent("TcpNet Error #5:" + e.StackTrace + ',' + e.Message);
                    }
                }

                Thread.Sleep(1000);
                while (!bDisconnected)
                {
                    Socket tcpSocket = NetSocket.Accept() ;
                    CreateNewClientManager(tcpSocket) ;
                }                
            }
            catch (SocketException ex)
            {
                WriteLogEvent("TcpNet Error #3:" + ex.StackTrace + ',' + ex.Message);
            }
        }

        private void CreateNewClientManager(Socket socket)
        {
            clientListBusy.WaitOne();
            try
            {
                ClientManager client = new ClientManager(socket);
                client.ReceiveMsg += new NetMsgReceivedEventHandler(client_ReceivedMsg);
                client.ThreadExpired += new ThreadExpiredHandler(client_ThreadExpired);

                int idx = GetFreeBlock();
                if (idx > -1)
                {
                    ClientList[idx] = client;
                }
                else
                {
                    ClientList.Add(client);
                }
            }
            catch (Exception e)
            {
                WriteLogEvent("TcpNet Error #7:" + e.StackTrace +','+ e.Message);
            }
            clientListBusy.Release();
        }

        private int GetFreeBlock()
        {
            int idx = -1;
            for (int i = 0; i < ClientList.Count; i++)
            {
                try  
                { 
                    if (ClientList[i] == null) 
                    { 
                        idx = i; break; 
                    } 
                } 
                catch
                {
                    idx = i; 
                }
            }
            return idx;
        }
        private void client_ThreadExpired(object obj)
        {
            clientListBusy.WaitOne();
            try
            {
                ClientManager client     =  (ClientManager)obj ;
                client.ReceiveMsg       -=  new NetMsgReceivedEventHandler(client_ReceivedMsg) ;
                client.ThreadExpired    -=  new ThreadExpiredHandler(client_ThreadExpired) ;

               // client_OnDisconectMsg(client.socket);
                CheckConnectionAlive();

                client.TerminateObj();
                
                int idx = ClientList.IndexOf(client);
                if (idx >= 0) { ClientList[idx] = null; }                 
            }
            catch (Exception ex)
            {
                WriteLogEvent("TcpNet Error #7.5:" + ex.Message);
            }
            clientListBusy.Release();
        }

        private void client_ReceivedMsg(object sender, ReceivedMsgEvent e)
        {
            try { if (OnReceivedMsg != null) { OnReceivedMsg(this, e); } }
            catch { }
        }

        private void client_OnDisconectMsg(Socket sk)
        {
           // NetDisConnectedEvent info = new NetDisConnectedEvent();
           // info.ep = (IPEndPoint)sk.RemoteEndPoint;
         //   OnDisconectMsg(this, info);
        }
        private void OnDisconectMsg(object sender, NetDisConnectedEvent e)
        {
            try { if (OnNetDisconnected != null) { OnNetDisconnected(this, e); } }
            catch { }
        }
        private void CheckConnectionAlive()
        {
            try
            {
                for (int i = 0; i < ClientList.Count; i++)
                {
                    try
                    {
                        ClientManager mngr = (ClientManager)ClientList[i];
                        if (!mngr.IsThreadRunning)
                        {
                            ClientList.Remove(mngr);
                        }
                    }
                    catch (Exception e)
                    {
                        WriteLogEvent("TcpNet Error #6:" + e.Message);
                        continue;
                    }
                }
            }
            catch { }
        }

        public void Connect(String remoteHost, int port, byte[] data)
        {
            try
            {
                #region test code
                //if (NetSocket != null && !NetSocket.Connected)
                //{
                //    IPHostEntry remoteHostEntry = Dns.GetHostByName(remoteHost) ;
                //    IPEndPoint remoteEP = new IPEndPoint(remoteHostEntry.AddressList[0], port) ;
                //    NetSocket.Connect(remoteEP) ;
                //    foo = NetSocket.Send(data) ;
                //}
                #endregion

                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                IPHostEntry local               =   Dns.GetHostByName(Dns.GetHostName()) ;
                IPEndPoint  localIP             =   new IPEndPoint(local.AddressList[0], 0) ;

                socket.Bind(localIP);

                IPHostEntry remoteHostEntry     =   Dns.GetHostByName(remoteHost) ;
                IPEndPoint remoteEP             =   new IPEndPoint(remoteHostEntry.AddressList[0], port) ;
                socket.Connect(remoteEP) ;

                socket.Send(data) ;
            }
            catch(SocketException ex)
            {
                WriteLogEvent("TcpNet Error #4:" + ex.Message);
            }
        }
        public bool CheckConnectionAlive(IPEndPoint enp)
        {            
            if (ClientList != null)
            {
                bool found = false;
                foreach (ClientManager mngr in ClientList)
                {
                    try
                    {
                        if (mngr.socket.RemoteEndPoint.ToString() == enp.ToString())
                        {
                            found = true;
                            break;
                        }
                    }
                    catch
                    {
                        continue ;
                    }
                }
                if (!found) { return false ; }

                for(int i=0; i<ClientList.Count; i++)
                {                    
                    try
                    {
                        ClientManager mngr = (ClientManager)ClientList[i];
                        if (!mngr.IsThreadRunning)
                        {                                                    
                            ClientList.Remove(mngr) ;
                            return false;
                        }
                    }
                    catch (Exception e)
                    {
                        WriteLogEvent("TcpNet Error #6:" + e.Message);
                        continue;
                    }
                }
            }
            return true;
        }
    }
}
