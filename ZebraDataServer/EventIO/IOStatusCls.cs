using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;
using System.IO;

namespace ZebraDataServer
{
    class IOStatusCls
    {
        public enum STATUS_INPUT { ON = 1, OFF = 0 }

        public struct EXTERNAL_INPUT
        {
            public STATUS_INPUT I01;
            public STATUS_INPUT I02;
            public STATUS_INPUT I03;
            public STATUS_INPUT I04;
            public STATUS_INPUT I05;
            public STATUS_INPUT I06;
            public STATUS_INPUT I07;
            public STATUS_INPUT I08;
            public STATUS_INPUT I09;
            public STATUS_INPUT I10;
        }

        public enum IOPORT
        {
            USERINPUT1 = 2,     ///in2  =   userIn1
            USERINPUT2 = 3,     ///in3  =   userIn2
            USEROUTPUT1 = 2,    ///out2 =   userOut1  
            USEROUTPUT2 = 3,    ///out3 =   userOut2
            USEROUTPUT3 = 4     ///out4 =   userOut3          //not use
        }
        public enum IOTYPE
        {
            ENGINEOFF   = 0x00,
            ENGINEON    = 0x10,

            USERINPUT1  = 0x20,
            USERINPUT2  = 0x40,
            USERINPUT3  = 0x80,
            //POWER       = 0x80,

            IGNITION    = 0x01,     ///out1
            USEROUTPUT1 = 0x02,     ///out2 =   user1  
            USEROUTPUT2 = 0x04,     ///out3 =   user2
            USEROUTPUT3 = 0x08,      ///out4 =   user3          //not use
                                    
            EXTERNAL_INPUT01 = 0x01,
            EXTERNAL_INPUT02 = 0x02,
            EXTERNAL_INPUT03 = 0x04,
            EXTERNAL_INPUT04 = 0x08,
            EXTERNAL_INPUT05 = 0x10,
            EXTERNAL_INPUT06 = 0x20,
            EXTERNAL_INPUT07 = 0x40,
            EXTERNAL_INPUT08 = 0x80,
            EXTERNAL_INPUT09 = 0x100,
            EXTERNAL_INPUT10 = 0x200
        }

        public struct VEHINFO
        {
            public int      ref_idx ;
            public decimal log_index;
            public int      veh_id ;
            public int      value ;
            public bool     enable_q ;
            public int      speed ;
            public string   timestamp;
            public double   lat ; 
            public double   lon ;
            public CommonQeue SMSQ;
            public CommonQeue EMAILQ;
        }

        SQL gSql;
        SQL2 gSql2;
        LocationCls oLoc;
        GeneralCls oGeneral;

        public IOStatusCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
            oLoc = new LocationCls(dbServer);
            oGeneral = new GeneralCls(dbServer);
        }

        private string GetHexToDex(string zHexStr)
        {
            string retDec = zHexStr;
            try
            {
                int decValue = 0;
                if (!(int.TryParse(zHexStr, out decValue)))
                {
                    decValue = Convert.ToInt32(zHexStr, 16);
                    retDec = decValue.ToString("0");
                }
            }
            catch
            {

            }
            return retDec;
        }
        private string GetHexToDex2(string zHexStr)
        {
            string retDec = "0";
            try
            {
                int decValue = Convert.ToInt32(zHexStr, 16);
                retDec = decValue.ToString("0");
            }
            catch
            {
            }
            return retDec;
        }

        public void DetectStatus(decimal log_index, int veh_id, int value, string g_type_of_msg, bool enableQ, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ, string tag_msg, double internal_power, double external_power)
        {
            VEHINFO info = new VEHINFO();
            info.log_index = log_index;
            info.veh_id = veh_id;
            info.value = value;
            info.enable_q = enableQ;
            info.speed = speed;
            info.timestamp = timestamp;
            info.lat = lat;
            info.lon = lon;
            info.SMSQ = SMSQ;
            info.EMAILQ = EMAILQ;

            DateTime dtTimeStamp = Convert.ToDateTime(timestamp);
            //#defind of k-box
            if (tag_msg.Length > 0 && gSql2.GetVehTypeOfBox("K", veh_id))
            {
                switch (g_type_of_msg)
                {
                    case "1": KBoxIOEventMessage(log_index, veh_id, dtTimeStamp, tag_msg, info); break;
                    case "2": break;
                    case "3": KBoxInternalBatteryMessage(log_index, veh_id, internal_power, external_power, tag_msg, info); break;
                    case "4": break;
                    case "5": break;
                    case "6": break;
                    case "7": break;
                    case "8": KBoxMagneticEventMessage(log_index, veh_id, tag_msg, info); break;
                }
            }
            else
            {
                #region INTERNAL
                /*********SQL1**********/
                //int retIO = GetLastIOStatus(veh_id);
                /*********SQL2**********/
                int retIOi = GetLastIOStatus_Log(veh_id);

                if (g_type_of_msg == "i" || g_type_of_msg == "I" || g_type_of_msg == "+" || g_type_of_msg == "C" || g_type_of_msg == "c" || g_type_of_msg == "d" || g_type_of_msg == "D")
                {
                    #region
                    //--- The Old Process
                    if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.ENGINEON))
                    {
                        if ((value & (int)IOTYPE.ENGINEON) == (int)IOTYPE.ENGINEON)
                        {
                            InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.ENGINE_ON, value, info);
                        }
                        else if ((value & (int)IOTYPE.ENGINEOFF) == (int)IOTYPE.ENGINEOFF)
                        {
                            InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.ENGINE_OFF, value, info);
                        }
                    }

                    if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.USERINPUT1))
                    {
                        if ((value & (int)IOTYPE.USERINPUT1) == (int)IOTYPE.USERINPUT1)
                         {
                            InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_ON, value, info, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                            InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED1_ON);
                        }
                        else
                        {
                            InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_OFF, value, info, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                            InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED1_OFF);
                        }
                    }

                    if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.USERINPUT2))
                    {
                        if ((value & (int)IOTYPE.USERINPUT2) == (int)IOTYPE.USERINPUT2)
                        {
                            InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_ON, value, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                            InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED2_ON);
                        }
                        else
                        {
                            InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_OFF, value, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                            InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED2_OFF);
                        }
                    }

                    //if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.POWER))
                    //{
                    //    if ((value & (int)IOTYPE.POWER) != (int)IOTYPE.POWER)
                    //    {
                    //        InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, value, info);
                    //    }
                    //    else
                    //    {
                    //        InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_POWER_LINE_CONNECTED, value, info);
                    //    }
                    //}

                    if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.USERINPUT3))
                    {
                        if ((value & (int)IOTYPE.USERINPUT3) == (int)IOTYPE.USERINPUT3)
                        {
                            InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED3_ON, value, info, PORT.Port4, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                            InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED3_ON);
                        }
                        else
                        {
                            InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED3_OFF, value, info, PORT.Port4, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                            InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED3_OFF);
                        }
                    }
                    #endregion
                }
                else
                {
                    #region
                    if (g_type_of_msg == "B" || g_type_of_msg == "b")
                    {
                        if (g_type_of_msg == "B")
                        {
                            InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, value, info);
                        }
                        else
                        {
                            InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_POWER_LINE_CONNECTED, value, info);
                        }
                    }
                    else if (((retIOi & (int)IOTYPE.USERINPUT3) != (int)IOTYPE.USERINPUT3) && ((value & (int)IOTYPE.USERINPUT3) != (int)IOTYPE.USERINPUT3))
                    {
                        int pw_delay_time = GetDisconnectedPowerParams(veh_id);

                        if (pw_delay_time > 0)
                        {

                            decimal pw_ref_idx = -1.0M;
                            DateTime pw_timestamp = DateTime.Now;

                            /********SQL1**********/
                            //gSql.GetLastIOStatusTime(veh_id, out pw_prv_state, out pw_ref_idx, out pw_timestamp);

                            /********SQL2**********/
                            gSql2.GetLastEventStatus(veh_id, (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, out pw_ref_idx, out pw_timestamp);

                            TimeSpan sp = DateTime.Now - pw_timestamp;
                            if (sp.TotalMinutes >= pw_delay_time)
                            {
                                if ((value & (int)IOTYPE.USERINPUT3) != (int)IOTYPE.USERINPUT3)
                                {
                                    InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, retIOi, info);
                                }
                            }
                        }
                    }
                    #endregion
                }


                //Output 1  Port: 2 >> 0x02
                if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.USEROUTPUT1))
                {
                    if ((value & (int)IOTYPE.USEROUTPUT1) == (int)IOTYPE.USEROUTPUT1)
                    {
                        InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON, value, info, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                    else
                    {
                        InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF, value, info, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                }
                //Output 2  Port: 3 >> 0x04
                if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.USEROUTPUT2))
                {
                    if ((value & (int)IOTYPE.USEROUTPUT2) == (int)IOTYPE.USEROUTPUT2)
                    {
                        InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON, value, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                    else
                    {
                        InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF, value, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                }
                //Output 3  Port: 3 >> 0x08 ,Select use
                if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.USEROUTPUT3))
                {
                    if ((value & (int)IOTYPE.USEROUTPUT3) == (int)IOTYPE.USEROUTPUT3)
                    {
                        InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED3_OUT_ON, value, info, PORT.Port4, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                    else
                    {
                        InsertNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED3_OUT_OFF, value, info, PORT.Port4, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                }
                /// End user Defined 1,2,3 Output
                /// IGNITION Control      


                if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.IGNITION))
                {
                    if ((value & (int)IOTYPE.IGNITION) == (int)IOTYPE.IGNITION)
                    {
                        InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_IGNITION_LINE_ON, value, info);
                    }
                    else
                    {
                        InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_IGNITION_LINE_OFF, value, info);
                    }
                }
                #endregion

                //value = ��������ش ���Ţ�ҹ 16
                //retIOi = in_out_status


                #region EXTERNAL_INPUT
                if (g_type_of_msg == "D" && tag_msg.Length > 0 && (tag_msg[0] == 'I' || tag_msg[0] == 'i'))
                {                    
                    #region External_Input 
                    if (tag_msg[1] == '0' || tag_msg[1] == '1')
                    {
                        string[] external = tag_msg.Split(new char[] { ',' });
                        value = Convert.ToInt32(external[5], 16);
                        retIOi = GetLastIOStatus_External(veh_id);
                        string Input_type = external[1];
                        EXTERNAL_INPUT exInput = new EXTERNAL_INPUT();

                        #region External Input 1 - 8
                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT01))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT01) == (int)IOTYPE.EXTERNAL_INPUT01)
                            {
                                exInput.I01 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT01_ON, PROFILEMENUID.EXTERNAL_INPUT01_ON, value, info, Input_type, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I01 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT01_OFF, PROFILEMENUID.EXTERNAL_INPUT01_OFF, value, info, Input_type, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }

                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT02))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT02) == (int)IOTYPE.EXTERNAL_INPUT02)
                            {
                                exInput.I02 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT02_ON, PROFILEMENUID.EXTERNAL_INPUT02_ON, value, info, Input_type, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I02 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT02_OFF, PROFILEMENUID.EXTERNAL_INPUT02_OFF, value, info, Input_type, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }

                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT03))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT03) == (int)IOTYPE.EXTERNAL_INPUT03)
                            {
                                exInput.I03 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT03_ON, PROFILEMENUID.EXTERNAL_INPUT03_ON, value, info, Input_type, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I03 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT03_OFF, PROFILEMENUID.EXTERNAL_INPUT03_OFF, value, info, Input_type, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }

                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT04))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT04) == (int)IOTYPE.EXTERNAL_INPUT04)
                            {
                                exInput.I04 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT04_ON, PROFILEMENUID.EXTERNAL_INPUT04_ON, value, info, Input_type, PORT.Port4, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I04 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT04_OFF, PROFILEMENUID.EXTERNAL_INPUT04_OFF, value, info, Input_type, PORT.Port4, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }

                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT05))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT05) == (int)IOTYPE.EXTERNAL_INPUT05)
                            {
                                exInput.I05 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT05_ON, PROFILEMENUID.EXTERNAL_INPUT05_ON, value, info, Input_type, PORT.Port5, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I05 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT05_OFF, PROFILEMENUID.EXTERNAL_INPUT05_OFF, value, info, Input_type, PORT.Port5, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }

                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT06))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT06) == (int)IOTYPE.EXTERNAL_INPUT06)
                            {
                                exInput.I06 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT06_ON, PROFILEMENUID.EXTERNAL_INPUT06_ON, value, info, Input_type, PORT.Port6, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I06 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT06_OFF, PROFILEMENUID.EXTERNAL_INPUT06_OFF, value, info, Input_type, PORT.Port6, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }

                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT07))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT07) == (int)IOTYPE.EXTERNAL_INPUT07)
                            {
                                exInput.I07 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT07_ON, PROFILEMENUID.EXTERNAL_INPUT07_ON, value, info, Input_type, PORT.Port7, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I07 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT07_OFF, PROFILEMENUID.EXTERNAL_INPUT07_OFF, value, info, Input_type, PORT.Port7, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                               // InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }
                        if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT08))
                        {
                            if ((value & (int)IOTYPE.EXTERNAL_INPUT08) == (int)IOTYPE.EXTERNAL_INPUT08)
                            {
                                exInput.I08 = STATUS_INPUT.ON;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT08_ON, PROFILEMENUID.EXTERNAL_INPUT08_ON, value, info, Input_type, PORT.Port8, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                            else
                            {
                                exInput.I08 = STATUS_INPUT.OFF;
                                InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT08_OFF, PROFILEMENUID.EXTERNAL_INPUT08_OFF, value, info, Input_type, PORT.Port8, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                            }
                        }
                        #endregion

                        #region External Input 9 - 10
                        if (tag_msg[1] == '1')
                        {
                            if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT09))
                            {
                                if ((value & (int)IOTYPE.EXTERNAL_INPUT09) == (int)IOTYPE.EXTERNAL_INPUT09)
                                {
                                    exInput.I09 = STATUS_INPUT.ON;
                                    InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT09_ON, PROFILEMENUID.EXTERNAL_INPUT09_ON, value, info, Input_type, PORT.Port9, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                    //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                                }
                                else
                                {
                                    exInput.I09 = STATUS_INPUT.OFF;
                                    InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT09_OFF, PROFILEMENUID.EXTERNAL_INPUT09_OFF, value, info, Input_type, PORT.Port9, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                    //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                                }
                            }

                            if (!IsDoubleEvent(value, retIOi, (int)IOTYPE.EXTERNAL_INPUT10))
                            {
                                if ((value & (int)IOTYPE.EXTERNAL_INPUT06) == (int)IOTYPE.EXTERNAL_INPUT06)
                                {
                                    exInput.I10 = STATUS_INPUT.ON;
                                    InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT10_ON, PROFILEMENUID.EXTERNAL_INPUT10_ON, value, info, Input_type, PORT.Port10, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                    //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                                }
                                else
                                {
                                    exInput.I10 = STATUS_INPUT.OFF;
                                    InsertNewRecordEventExternalIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EXTERNAL_INPUT10_OFF, PROFILEMENUID.EXTERNAL_INPUT10_OFF, value, info, Input_type, PORT.Port10, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                                    //InsertNewInputRecord_Log(veh_id, log_index, exInput);
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion

                if (g_type_of_msg == "X")
                {
                    InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_GPS_DISCONNECTED, value, info);
                }
                if (g_type_of_msg == "x")
                {
                    InsertNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_GPS_CONNECTED, value, info);
                }

                if (internal_power > 0 || external_power > 0)
                {
                    oGeneral.InserPowerLevel(log_index, veh_id, (int)internal_power, (int)external_power);
                }
            }
        }

        private void KBoxIOEventMessage(decimal log_index, int veh_id, DateTime dtTimeStamp, string tag_msg, VEHINFO info)
        {
            try {
                string[] st = tag_msg.Split(new char[] { ',' });
                if (st.Length > 0)
                {
                    switch (st[0])
                    {
                        case "PB": break;
                        case "TP": break;
                        case "SP": 
                            {
                                if (st[1] == "0")
                                {
                                    //speed down
                                    //InsertKBoxNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.SPEEDING, info);
                                }
                                else if (st[1] == "1")
                                {
                                    //speed up
                                    InsertKBoxNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.SPEEDING, info);
                                }
                            }
                            break;
                        case "IG"://Ignition
                            {
                                if (st[1] == "1")
                                {
                                    InsertKBoxNewRecord( log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.ENGINE_ON, info);
                                }
                                else if (st[1] == "0")
                                {
                                    InsertKBoxNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.ENGINE_OFF, info);
                                }
                            }
                            break;
                        case "ID": break; // IDle
                        case "PS"://Car Battery Present.
                            {
                                if (st[1] == "0")
                                {
                                    InsertKBoxNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, info);
                                }
                                else if (st[1] == "1")
                                {
                                    InsertKBoxNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_POWER_LINE_CONNECTED, info);
                                }
                            }
                            break;
                        case "ANTDETECT"://ANTDET
                            {
                                if (st[1] == "0")
                                {
                                    InsertKBoxNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_GPS_DISCONNECTED, info);
                                }
                                else if (st[1] == "1")
                                {
                                    InsertKBoxNewRecord(log_index, veh_id, dtTimeStamp, (int)EVT_TYPE.EVENT_GPS_CONNECTED, info);
                                }
                            }
                            break;
                        case "CM":
                            { }
                            break;
                        case "HB":
                            { }
                            break;
                        case "HC":
                            { }
                            break;
                        default:
                            {
                                #region INPUT / OutPut
                                if (st[0] == "IN1")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_ON, info, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED1_ON);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_OFF, info, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED1_OFF);
                                    }
                                }
                                else if (st[0] == "IN2")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_ON, info, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED2_ON);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_OFF, info, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED2_OFF);
                                    }
                                }
                                else if (st[0] == "IN3")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED3_ON, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED3_ON);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED3_OFF, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED3_OFF);
                                    }
                                }
                                else if (st[0] == "IN4")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED4_ON, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED4_ON);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED4_OFF, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED4_OFF);
                                    }
                                }
                                else if (st[0] == "OUT0")//Output Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON, info, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF, info, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF);
                                    }
                                }
                                else if (st[0] == "OUT1")//Output Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON, info, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertKBoxNewRecordEventIO(log_index, veh_id, dtTimeStamp, EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF, info, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                        InsertLogUserDefined(veh_id, log_index, (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF);
                                    }
                                }
                                #endregion
                            }
                            break;
                    }
                }
            }
            catch { }
        }
        private void KBoxInternalBatteryMessage(decimal log_index, int veh_id, double _internal_power, double _external_power, string tag_msg, VEHINFO info)
        {
            try
            {
               string[] st = tag_msg.Split(new char[] { ',' });
               if (st.Length > 0)
               {
                   int internal_power = (Convert.ToInt32(st[2]) / 10);
                   //Event
                   oGeneral.InserPowerLevelKBox(log_index, veh_id, internal_power, (int)_external_power, st[0].Trim(), st[1].Trim());
               }
            }
            catch { }
        }
  
        private void KBoxMagneticEventMessage(decimal log_index, int veh_id, string tag_msg, VEHINFO info)
        {
            //edit magnetic evt.
            try
            {
                string[] st = tag_msg.Split(new char[] { ',' });
              
                string driver_type = st[3];
                string driver_sex = st[4];
                string driver_number = st[5];
                string province_code = st[6].Substring(0,3);
                string distic_code = st[6].Substring(3,2);
                string driver_name = st[0];
                string id_country = st[1].Substring(0,6);
                string id_card = st[1].Substring(6,13);
                string expiry_date = st[2].Substring(0,4);
                string birthday = st[2].Substring(4,8);
                string License = "";
                string driverid = driver_type.PadLeft(2, '0') + driver_sex.PadLeft(1, '0') + driver_number.PadLeft(7, '0') + province_code.PadLeft(5, '0');

                /****SQL1***/
                //gSql.InsertNewDriverLicence(ref_idx,
                //    veh_id,
                //    driver_type,
                //    driver_sex,
                //    driver_number,
                //    province_code,
                //    distic_code,
                //    driver_name,
                //    id_country,
                //    id_card,
                //    expiry_date,
                //    birthday);

                /****SQL2****/
                gSql2.InsertNewDriverLicence(log_index,
                 veh_id,
                 driver_type,
                 driver_sex,
                 driver_number,
                 province_code,
                 distic_code,
                 driver_name,
                 id_country,
                 id_card,
                 expiry_date,
                 birthday);



                gSql2.InsertID_DLTESRI(veh_id, driverid, License);

            }
            catch { }
        }
  
        private EVT_TYPE GetEvtExternalInputSeting(int veh_id, PORT port,STATUSOF status_onoff,IOSOURCETYPE io_type,IOSOURCE internal_port)
        {
            return gSql.GetIOSettingEvent(veh_id, (int)port, (int)status_onoff, Convert.ToChar((int)io_type), (int)internal_port);
        }

        private void InsertNewInputRecord(int veh_id, int ref_idx, EXTERNAL_INPUT inp)
        {
            gSql.InsertExternalAllInput(veh_id, ref_idx, (int)inp.I01, (int)inp.I02, (int)inp.I03, (int)inp.I04, (int)inp.I05, (int)inp.I06);
        }

        private void InsertNewInputRecord_Log(int veh_id, decimal log_index, EXTERNAL_INPUT inp)
        {  
            gSql2.InsertExternalAllInput(veh_id, log_index, (int)inp.I01, (int)inp.I02, (int)inp.I03, (int)inp.I04, (int)inp.I05, (int)inp.I06);
        }

        private int GetDisconnectedPowerParams(int veh_id)
        {
            return gSql.GetDisconnectedPowerEvtParam(veh_id);           
        }
        private int GetDisconnectedPowerParams_new(int veh_id)
        {
            return gSql.GetDisconnectedPowerEvtParam_new(veh_id);
        }
        public void InsertNewRecord(decimal log_index, int veh_id, DateTime timestamp, int evt, int value, VEHINFO info)
        {
            /********SQL1*********/
            //gSql.InsertMsgEvt(ref_idx, evt);

            /********SQL2*********/
            gSql2.InsertMsgEvt(log_index,veh_id, timestamp, evt,-1, -1);

            if (!(evt == (int)EVT_TYPE.EVENT_GPS_CONNECTED || evt == (int)EVT_TYPE.EVENT_GPS_DISCONNECTED))
            {
                //gSql.InsertPrvIOState(veh_id, value, ref_idx);

                /*****SQL2*******/
                gSql2.InsertPrvIOState(veh_id, value, log_index);
            }

            Broadcast(info.enable_q, info.ref_idx, (int)evt, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
        }

        public void InsertNewRecordEventIO(decimal log_index, int vid, DateTime dtTimeStamp,EVT_TYPE evt, int value, VEHINFO info, PORT port, STATUSOF status_onoff, IOSOURCETYPE io_type, IOSOURCE internal_port)
        {
            EVT_TYPE evt_seting = GetEvtExternalInputSeting(vid, port, status_onoff, io_type, internal_port);
            if (evt_seting != EVT_TYPE.NONE)
            {
                //gSql.InsertMsgEvt(ref_idx, (int)evt_seting);

                /*****SQL2*****/
                gSql2.InsertMsgEvt(log_index, vid, dtTimeStamp, (int)evt_seting, -1, (int)evt);
            }
            else
            {
                evt_seting = evt;
                //gSql.InsertMsgEvt(ref_idx, (int)evt);

                /*****SQL2*****/
                gSql2.InsertMsgEvt(log_index, vid, dtTimeStamp, (int)evt, -1, -1);
            }
            /*****SQL1*****/
            //gSql.InsertPrvIOState(vid, value, ref_idx);

            /*****SQL2*****/
            gSql2.InsertPrvIOState(vid, value, log_index);
            Broadcast(info.enable_q, info.ref_idx, (int)evt_seting, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
        }

        public void InsertKBoxNewRecord(decimal log_index, int vid, DateTime dtTimeStamp, int evt, VEHINFO info)
        {
            if (evt == (int)EVT_TYPE.ENGINE_ON || evt == (int)EVT_TYPE.ENGINE_OFF)
            {
                int a = gSql2.GetLastEngineEventStatus(vid);
                /********SQL2 OK********/
                if (gSql2.GetLastEngineEventStatus(vid) != evt)
                {
                    /******SQL1****/
                    //gSql.InsertMsgEvt(ref_idx, evt);

                    /******SQL2*****/
                    gSql2.InsertMsgEvt(log_index, vid, dtTimeStamp, (int)evt, -1, -1);

                    Broadcast(info.enable_q, info.ref_idx, evt, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
                }
            }
            else
            {
                /******SQL1****/
                //gSql.InsertMsgEvt(ref_idx, evt);

                /******SQL2*****/
                gSql2.InsertMsgEvt(log_index, vid, dtTimeStamp, evt, -1, -1);

                Broadcast(info.enable_q, info.ref_idx, evt, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
            }
        }

        public void InsertKBoxNewRecordEventIO(decimal log_index, int vid,DateTime dtTimeStamp, EVT_TYPE evt, VEHINFO info, PORT port, STATUSOF status_onoff, IOSOURCETYPE io_type, IOSOURCE internal_port)
        {
            EVT_TYPE evt_seting = GetEvtExternalInputSeting(vid, port, status_onoff, io_type, internal_port);
            if (evt_seting != EVT_TYPE.NONE)
            {
                /********SQL1********/
                //gSql.InsertMsgEvt(ref_idx, (int)evt_seting);
                //gSql.InsertExternalInputEvt(vid, ref_idx, evt);

                /********SQL2********/
                gSql2.InsertMsgEvt(log_index,vid, dtTimeStamp, (int)evt_seting, -1, (int)evt);
                //gSql2.InsertExternalInputEvt(vid, log_index, evt);
            }
            else
            {
                evt_seting = evt;

                /********SQL1********/
                //gSql.InsertMsgEvt(ref_idx, (int)evt_seting);
                //gSql.InsertExternalInputEvt(vid, ref_idx, evt_seting);

                /********SQL2********/
                gSql2.InsertMsgEvt(log_index,vid,dtTimeStamp, (int)evt_seting, -1, -1);
                //gSql2.InsertExternalInputEvt(vid, log_index, evt);
            }

            Broadcast(info.enable_q, info.ref_idx, (int)evt_seting, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
        }

        public void InsertNewRecordEventExternalIO(decimal log_index, int vid,DateTime dtTimeStamp, EVT_TYPE evt, PROFILEMENUID feature, int value, VEHINFO info, string msg_type,PORT port,STATUSOF status_onoff,IOSOURCETYPE io_type,IOSOURCE internal_port)
        {
            if (msg_type == "C")
            {
                EVT_TYPE evt_seting = GetEvtExternalInputSeting(vid, port, status_onoff, io_type, internal_port);
                if (evt_seting != EVT_TYPE.NONE)
                {  
                    /***SQL1*****/
                    //gSql.InsertMsgEvt(ref_idx, (int)evt_seting);
                    //gSql.InsertExternalInputEvt(vid, ref_idx, evt);

                    /***SQL2*****/
                    gSql2.InsertMsgEvt(log_index,vid,dtTimeStamp, (int)evt_seting, -1, (int)evt);
                    gSql2.InsertExternalInputEvt(vid, log_index, evt);
                }
                else
                {
                    evt_seting = evt;
                    /***SQL1*****/
                    //gSql.InsertMsgEvt(ref_idx, (int)evt_seting);
                    //gSql.InsertExternalInputEvt(vid, ref_idx, evt);

                    /***SQL2*****/
                    gSql2.InsertMsgEvt(log_index,vid,dtTimeStamp, (int)evt_seting, -1,-1);
                    gSql2.InsertExternalInputEvt(vid, log_index, evt);
                }

                /***SQL1*****/
                //gSql.InsertPrvIOState_External(vid,value,ref_idx);

                /***SQL2*****/
                gSql2.InsertPrvIOState_External(vid, value, log_index);

                Broadcast(info.enable_q, info.ref_idx, (int)evt_seting, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
            }
        }

        public void InsertLogUserDefined(int vehID, decimal log_index, int evtID)
        {
            /****SQL1****/ 
            //gSql.InsertLogUserDefined1(vehID, refIdx, evtID);
            
            /****SQL2 -  Not use****/
            if (evtID == (int)EVT_TYPE.EVENT_USER_DEFINED1_ON || evtID == (int)EVT_TYPE.EVENT_USER_DEFINED1_OFF)
            {
                gSql2.InsertLogUserDefined(vehID, log_index, evtID, (int)IOTYPE.USERINPUT1);
            }
            else if (evtID == (int)EVT_TYPE.EVENT_USER_DEFINED2_ON || evtID == (int)EVT_TYPE.EVENT_USER_DEFINED2_OFF)
            {
                gSql2.InsertLogUserDefined(vehID, log_index, evtID, (int)IOTYPE.USERINPUT2);
            }
            else if (evtID == (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON || evtID == (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF)
            {
                gSql2.InsertLogUserDefined(vehID, log_index, evtID, (int)IOTYPE.USEROUTPUT1);
            }
            else if (evtID == (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON || evtID == (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF)
            {
                gSql2.InsertLogUserDefined(vehID, log_index, evtID, (int)IOTYPE.USEROUTPUT2);
            }
            else if (evtID == (int)EVT_TYPE.EVENT_USER_DEFINED3_OUT_ON || evtID == (int)EVT_TYPE.EVENT_USER_DEFINED3_OUT_OFF)
            {
                gSql2.InsertLogUserDefined(vehID, log_index, evtID, (int)IOTYPE.USEROUTPUT3);
            }
        }

        public int GetLastIOStatus(int veh_id)
        {
            int status = 0;
            int ref_idx = 0;
            gSql.GetLastIOStatus(veh_id, out status, out ref_idx);
            return status;
        }
        public int GetLastIOStatus_Log(int veh_id)
        {
            int status = 0;
            Decimal ref_idx = 0.0m;
            gSql2.GetLastIOStatus(veh_id, out status, out ref_idx);
            return status;
        }

        private int GetLastIOStatus_External(int veh_id)
        {
            int status = 0;
            decimal ref_idx = -1.0m;
            gSql.GetLastIOStatus_External(veh_id, out status, out ref_idx);
            return status;
        }
        private int GetLastIOStatus_External_Log(int veh_id)
        {
            int status = 0;
            Decimal ref_idx = 0.0m;
            gSql2.GetLastIOStatus_External(veh_id, out status, out ref_idx);
            return status;
        }
        private bool IsDoubleEvent(int value, int prv_state, int type)
        {
            value       =   value & type ;
            prv_state   =   prv_state & type ;
            if (value == prv_state)
            {
                return true;
            }
            return false;
        }

        private void Broadcast(bool enableQ, int ref_idx, int type, int veh_id, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ, int value)
        {
            if ( enableQ )
            {
                ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                info.idx = ref_idx;
                info.type = type;
                info.veh_id = veh_id;
                info.msg = Convert.ToString(value,16);
                info.timestamp = timestamp;
                info.location_t = GetLocation((float)lon, (float)lat);
                info.lat = lat;
                info.lon = lon;
                info.speed = speed;
                
                if (SMSQ != null || EMAILQ != null) {
                   // SMSQ.PushQ(info);
                  //  EMAILQ.PushQ(info);
                }
            }
        }

        public string GetLocation(float lon, float lat)
        {
            if (oLoc == null){return "?";}
            string location_e = "No GPS.";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    location_e = oLoc.GetCustomLandmarkName(id, 1);
                }
                else
                {
                    id = oLoc.DetectLandmark(lon, lat);
                    location_e = oLoc.GetLandmarkName(id, 1);
                }
                id = oLoc.DetectAdminPoly(lon, lat);
                location_e += " " + oLoc.GetPolyName(id, 1);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_e = location_e.TrimStart(new char[] { ' ' });
            }
            if (location_e == "") { location_e = "No GPS."; }
            return location_e;
        }


        public bool isVehFromDFM_TOR(int zVehID)
        {
            return gSql.IsVehFromDFM_TOR(zVehID);
        }
    }
}
