﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZebraDataServer
{
    class ExEventIO
    {
        #region --- local variables ---------------------------------------------------------------
        //decimal g_log_index;
        //int g_veh_id;
        //String g_dtTimestampe;
        //float g_lat;
        //float g_lon;
        //int g_speed;
        //int g_course;
        //int g_type_of_fix;
        //int g_no_of_satellite;
        //int g_in_out_status;
        //int g_analog_level;
        //string g_type_of_msg;
        //String g_tag_msg;
        //bool g_df_vehicle;

        //DateTime g_TimeStamp;
        //double g_internal_power;
        //double g_external_power;
        #endregion

        bool IsEnableBroadcastQ = true;
        bool IsEnableBroadcastQTRUE = true;

        IOStatusCls oIOStatus;

        CommonQeue SMSQ;
        CommonQeue EMAILQ;
        public ExEventIO()
        {
            Properties.Settings pf = new ZebraDataServer.Properties.Settings();
            IsEnableBroadcastQ = pf.EnableBroadcastQ;
            IsEnableBroadcastQTRUE = pf.EnableBroadcastQ_TRUE;
            SMSQ = new CommonQeue(pf.Broadcast_SMS_Q);
            EMAILQ = new CommonQeue(pf.Broadcast_EMAIL_Q);
            oIOStatus = new IOStatusCls(pf.DbSever);
        }
        public void DetectEventIO(decimal log_index, int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg, string tag_msg, double internal_power, double external_power)
        {
            //#region To initialize variables
            //g_log_index = log_index;
            //g_veh_id = veh_id;
            //g_dtTimestampe = dtTimestampe;
            //g_lat = lat;
            //g_lon = lon;
            //g_speed = speed;
            //g_course = course;
            //g_type_of_fix = type_of_fix;
            //g_no_of_satellite = no_of_satellite;
            //g_in_out_status = in_out_status;
            //g_analog_level = analog_level;
            //g_type_of_msg = type_of_msg;
            //g_tag_msg = tag_msg;
            //g_df_vehicle = false;
            //g_internal_power = internal_power;
            //g_external_power = external_power;
            //#endregion


            // Boss TEST 2017-02-15 =====================
            string ab = tag_msg;
            
                if (tag_msg != "")
                {
                    string a = tag_msg;
                }

            
            // Boss TEST 2017-02-15 =====================


                oIOStatus.DetectStatus(log_index, veh_id, in_out_status, type_of_msg,
                IsEnableBroadcastQ,
                speed, dtTimestampe, lat, lon, SMSQ, EMAILQ, tag_msg, 
                internal_power, external_power);


           // oIOStatus.DetectStatus(g_log_index, g_veh_id, g_in_out_status, g_type_of_msg, IsEnableBroadcastQ, g_speed, g_dtTimestampe, g_lat, g_lon, SMSQ, EMAILQ, g_tag_msg, g_internal_power, g_external_power);
        }
    }
}
