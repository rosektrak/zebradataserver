using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Collections;

namespace ZebraDataServer
{
    class DFMIOStatusCls
    {
        public enum INPUT_IO_TYPE
        {
            ENGINEOFF   = 0x00,
            ENGINEON    = 0x10,
        }

        //--- MAIN INPUT
        public enum DFM_ENGINE          { ON = 1, OFF = 0 }
        public enum DFM_MAIN_LIGHT      { ON = 0, OFF = 1 }
        public enum DFM_PTO_SWITCH      { ON = 1, OFF = 0 }
        //--- EXTERNAL INPUT
        public enum DFM_HORN            { ON = 1, OFF = 0 }
        public enum DFM_MINOR_LIGHT     { ON = 1, OFF = 0 }
        public enum DFM_LEFT_SIGNAL     { ON = 0, OFF = 1 }
        public enum DFM_RIGHT_SIGNAL    { ON = 0, OFF = 1 }
        public enum DFM_WIPER           { ON = 1, OFF = 0 }
        public enum DFM_REVERSE_LIGHT   { ON = 1, OFF = 0 }

        public struct DFM_MAIN_INPUT
        {
            public DFM_ENGINE       DFM_ENGINE;
            public DFM_MAIN_LIGHT   DFM_MAIN_LIGHT;
            public DFM_PTO_SWITCH   DFM_PTO_SWITCH;
        }

        public struct DFM_EXTERNAL_INPUT
        {
            public DFM_HORN             DFM_HORN;
            public DFM_MINOR_LIGHT      DFM_MINOR_LIGHT;
            public DFM_LEFT_SIGNAL      DFM_LEFT_SIGNAL;
            public DFM_RIGHT_SIGNAL     DFM_RIGHT_SIGNAL;
            public DFM_WIPER            DFM_WIPER;
            public DFM_REVERSE_LIGHT    DFM_REVERSE_LIGHT;
        }

        public struct VEHINFO
        {
            public int ref_idx;
            public int veh_id;
            public int value;
            public bool enable_q;
            public int speed;
            public string timestamp;
            public double lat;
            public double lon;
            //public CommonQeue SMSQ;
            //public CommonQeue EMAILQ;
        }

        SQL gSql;
        public DFMIOStatusCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public void DetectStatus(int ref_idx, int veh_id, int value, string g_type_of_msg, bool enableQ, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ, string external_msg)
        {
            VEHINFO info = new VEHINFO();
            info.ref_idx = ref_idx;
            info.veh_id = veh_id;
            info.value = value;
            info.enable_q = enableQ;
            info.speed = speed;
            info.timestamp = timestamp;
            info.lat = lat;
            info.lon = lon;
            //info.SMSQ = SMSQ;
            //info.EMAILQ = EMAILQ;

            //int retIO = 0;
            int input_value = 0;
            int external_input_value = 0;

            //--- Message From EXTERNAL BOX INPUT ---------
            if (external_msg.Length > 0 && (external_msg[0] == 'I' || external_msg[0] == 'i'))
            {
                input_value = value;
                external_input_value = GetExternalIOValue(ref_idx, veh_id);

                DFM_MAIN_INPUT mInput = GetDFM_MainInput(input_value);
                DFM_EXTERNAL_INPUT exInput = GetDFM_ExternalInput(external_input_value);

                InsertNewInputRecord(ref_idx, veh_id, mInput, exInput);
            }
            //--- Message From MAIN BOX INPUT --------
            #region
            // else
           // {
                //if (g_type_of_msg == 'i' || g_type_of_msg == 'I' || g_type_of_msg == '+')
                //{
                //    input_value = value;
                //    external_input_value = GetExternalIOValue2(ref_idx, veh_id);

                //    DFM_MAIN_INPUT mInput = GetDFM_MainInput(input_value);
                //    DFM_EXTERNAL_INPUT exInput = GetDFM_ExternalInput(external_input_value);

                //    //--- Detect Engine ON/OFF
                //    retIO = GetLastIOStatus(veh_id);

                //    if (!IsDoubleEvent(value, retIO, INPUT_IO_TYPE.ENGINEON))
                //    {
                //        if ((value & (int)INPUT_IO_TYPE.ENGINEON) == (int)INPUT_IO_TYPE.ENGINEON)
                //        {
                //            InsertNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_ON, PROFILEMENUID.EVENT_ENGINE_ON, value, info);
                //        }
                //        else if ((value & (int)INPUT_IO_TYPE.ENGINEON) == (int)INPUT_IO_TYPE.ENGINEOFF)
                //        {
                //            InsertNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_OFF, PROFILEMENUID.EVENT_ENGINE_OFF, value, info);
                //        }
                //    }

                //    InsertNewInputRecord(ref_idx, veh_id, mInput, exInput);
                //}

                //if (g_type_of_msg == 'X')
                //{
                //    InsertNewRecord(ref_idx, veh_id, EVT_TYPE.EVENT_GPS_DISCONNECTED, PROFILEMENUID.EVENT_GPS_DISCONNECTED, value, info);
                //}
                //if (g_type_of_msg == 'x')
                //{
                //    InsertNewRecord(ref_idx, veh_id, EVT_TYPE.EVENT_GPS_CONNECTED, PROFILEMENUID.EVENT_GPS_CONNECTED, value, info);
                //}
            //}
            #endregion
        }

        private DFM_MAIN_INPUT GetDFM_MainInput(int value)
        {
            //--- Initial Input that value = 0 only
            DFM_MAIN_INPUT st = new DFM_MAIN_INPUT();
            st.DFM_ENGINE     = DFM_ENGINE.OFF;
            st.DFM_MAIN_LIGHT = DFM_MAIN_LIGHT.ON;
            st.DFM_PTO_SWITCH = DFM_PTO_SWITCH.OFF;

            int iNum4 = 8;
            int iNum3 = 4;
            int iNum2 = 2;
            int iNum1 = 1;

            try
            {
                value = GetInputValue(value);

                if (value / iNum4 >= 1)
                {
                    value = value - iNum4;
                }

                if (value / iNum3 >= 1)
                {
                    st.DFM_PTO_SWITCH = DFM_PTO_SWITCH.ON;
                    value = value - iNum3;
                }

                if (value / iNum2 >= 1)
                {
                    st.DFM_MAIN_LIGHT = DFM_MAIN_LIGHT.OFF;
                    value = value - iNum2;
                }

                if (value / iNum1 >= 1)
                {
                    st.DFM_ENGINE = DFM_ENGINE.ON;
                }
            }
            catch
            {
            }

            return st;
        }

        private int GetInputValue(int value)
        {
            int iRet = 0;
            try
            {
                string retHex = string.Format("{0:X}", value);
                retHex = retHex.Substring(0, 1);
                iRet = Convert.ToInt32(retHex, 16);
            }
            catch
            {
            }
            return iRet;
        }

        private DFM_EXTERNAL_INPUT GetDFM_ExternalInput(int value)
        {
            //--- Initial Input that value = 0 only
            DFM_EXTERNAL_INPUT st = new DFM_EXTERNAL_INPUT();
            st.DFM_HORN             = DFM_HORN.OFF;
            st.DFM_LEFT_SIGNAL      = DFM_LEFT_SIGNAL.ON;
            st.DFM_MINOR_LIGHT      = DFM_MINOR_LIGHT.OFF;
            st.DFM_REVERSE_LIGHT    = DFM_REVERSE_LIGHT.OFF;
            st.DFM_RIGHT_SIGNAL     = DFM_RIGHT_SIGNAL.ON;
            st.DFM_WIPER            = DFM_WIPER.OFF;

            int iNum6 = 32;
            int iNum5 = 16;
            int iNum4 = 8;
            int iNum3 = 4;
            int iNum2 = 2;
            int iNum1 = 1;

            try
            {
                if (value / iNum6 >= 1)
                {
                    st.DFM_REVERSE_LIGHT = DFM_REVERSE_LIGHT.ON;
                    value = value - iNum6;
                }

                if (value / iNum5 >= 1)
                {
                    st.DFM_WIPER = DFM_WIPER.ON;
                    value = value - iNum5;
                }

                if (value / iNum4 >= 1)
                {
                    st.DFM_RIGHT_SIGNAL = DFM_RIGHT_SIGNAL.OFF;
                    value = value - iNum4;
                }

                if (value / iNum3 >= 1)
                {
                    st.DFM_LEFT_SIGNAL = DFM_LEFT_SIGNAL.OFF;
                    value = value - iNum3;
                }

                if (value / iNum2 >= 1)
                {
                    st.DFM_MINOR_LIGHT = DFM_MINOR_LIGHT.ON;
                    value = value - iNum2;
                }

                if (value / iNum1 >= 1)
                {
                    st.DFM_HORN = DFM_HORN.ON;
                }
            }
            catch
            {
            }

            return st;
        }

        private int GetExternalIOValue(int zRefIdx, int zVehID)
        {
            return gSql.GetExternalInput(zRefIdx, zVehID);
        }
        private int GetExternalIOValue2(int zRefIdx, int zVehID)
        {
            return gSql.GetExternalInput2(zRefIdx, zVehID);
        }

        private int GetLastIOStatus(int veh_id)
        {
            int status = 0;
            int ref_idx = 0;
            gSql.GetLastIOStatus(veh_id, out status, out ref_idx);
            return status;
        }

        private int GetLastIOStatus_External(int veh_id)
        {
            int status = 0;
            decimal ref_idx = -1.0m;
            gSql.GetLastIOStatus_External(veh_id, out status, out ref_idx);
            return status;
        }

        private bool IsDoubleEvent(int value, int prv_state, INPUT_IO_TYPE type)
        {
            value = value & (int)type;
            prv_state = prv_state & (int)type;

            if (value == prv_state)
            {
                return true;
            }

            return false;
        }

        private void InsertNewRecord(int ref_idx, int vid, int evt, PROFILEMENUID feature, int value, VEHINFO info)
        {
            gSql.InsertMsgEvt(ref_idx, evt);
            gSql.InsertPrvIOState(vid, value, ref_idx);
        }

        private void InsertNewInputRecord(int ref_idx, int vid, DFM_MAIN_INPUT mInput, DFM_EXTERNAL_INPUT exInput)
        {
            try
            {
                //--- Reverse INPUT ---------
                mInput.DFM_MAIN_LIGHT       = (mInput.DFM_MAIN_LIGHT    == DFM_MAIN_LIGHT.ON)   ? DFM_MAIN_LIGHT.OFF    : DFM_MAIN_LIGHT.ON;
                exInput.DFM_LEFT_SIGNAL     = (exInput.DFM_LEFT_SIGNAL  == DFM_LEFT_SIGNAL.ON)  ? DFM_LEFT_SIGNAL.OFF   : DFM_LEFT_SIGNAL.ON;
                exInput.DFM_RIGHT_SIGNAL    = (exInput.DFM_RIGHT_SIGNAL == DFM_RIGHT_SIGNAL.ON) ? DFM_RIGHT_SIGNAL.OFF  : DFM_RIGHT_SIGNAL.ON;
                //---------------------------

                gSql.InsertDFM_AllInput(vid, ref_idx, (int)mInput.DFM_ENGINE, (int)mInput.DFM_MAIN_LIGHT, (int)mInput.DFM_PTO_SWITCH,
                    (int)exInput.DFM_HORN, (int)exInput.DFM_MINOR_LIGHT, (int)exInput.DFM_LEFT_SIGNAL, (int)exInput.DFM_RIGHT_SIGNAL,
                    (int)exInput.DFM_WIPER, (int)exInput.DFM_REVERSE_LIGHT);
            }
            catch
            {
            }
        }
    }
}
