using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;

namespace ZebraDataServer
{
    class IdlingCls
    {
        SQL gSql;
        SQL2 gSql2;
        public IdlingCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public DataTable DetectIdleVeh(int veh_id)
        {
            return gSql.DetectIdleVeh(veh_id);
        }

        public Boolean DetectIdle(int veh_id)
        {
            Boolean bRet = gSql.GetVehIdleTimeLimit(veh_id);
            return bRet;
        }

        public Boolean DetectIdle_Log(int veh_id)
        {
            Boolean bRet = gSql2.GetVehIdleTimeLimit(veh_id);
            return bRet;
        }

        public Boolean DetectEngineOn(int veh_id)
        {
            Boolean bRet = gSql.GetVehEngineOn(veh_id);
            return bRet;
        }

        public void InsertNewRecord(int ref_idx)
        {
            gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.IDLE);
        }

        public void InsertNewRecord(decimal ref_idx ,int veh_id, DateTime timestamp)
        {
            gSql2.InsertMsgEvt(ref_idx,veh_id,timestamp, (int)EVT_TYPE.IDLE, -1,-1);
        }
    }
}
