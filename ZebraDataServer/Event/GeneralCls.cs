﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ZebraDataServer
{
    public class GeneralCls
    {
       // SQL sql;
        SQL2 sql2;
        public GeneralCls(String dbServer)
        {
            //sql = new SQL(dbServer);
            sql2 = new SQL2(dbServer);
        }

        public void ProcessEvent()
        {}

        public void InsertFwVersion(int veh_id, Decimal last_log_index, string tag)
        {
            //sql.InsertFwVersion(veh_id, lasted_index, tag);
            /**SQL2***/
            sql2.InsertFwVersion(veh_id, last_log_index, tag);
        }
        public void InsertOdometer( Decimal last_log_index, double odometer)
        {
            //sql.InsertOdometer(lasted_index, odometer);

            /**SQL2***/
            //sql2.InsertLogDistance(last_log_index, 0.0, odometer);
        }
        public void InsertRPM(Decimal last_log_index, int raw_data, int rpm)
        {
            //sql.InsertLogRPM(lasted_index, raw_data, rpm);

            /**SQL2***/
            sql2.InsertLogRPM2(last_log_index, raw_data, rpm);
        }
        public void InsertTagMsg(Decimal last_log_index, int veh_id, string tag)
        {
            //sql.InsertLogTagMsg(lasted_index, veh_id, tag);
            /**SQL2***/
            sql2.InsertLogTagMsg(last_log_index, veh_id, tag);
        }

        public void InserPowerLevel(Decimal last_log_index, int veh_id, int internal_power, int external_power)
        {
            /**SQL1***/
            //sql.InsertPowerLevel(lasted_index, veh_id, internal_power, external_power);
            /**SQL2***/
            sql2.InsertPowerLevel(last_log_index, veh_id, internal_power, external_power);

        }
        public void InserPowerLevelKBox(Decimal last_log_index, int veh_id, int internal_power, int external_power, string charging, string capacity)
        {
            /**SQL2***/
            sql2.InsertPowerLevelKBox(last_log_index, veh_id, internal_power, external_power, charging, capacity);
        }
    }
}
