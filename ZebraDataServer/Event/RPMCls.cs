﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ZebraDataServer
{
    public class RPMCls
    {
        SQL gSql;
        public RPMCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }
        public DataTable GetRPMCalibration(int veh_id,int value)
        {
            return gSql.GetRPM_Config(veh_id,value);
        }
    }
}
