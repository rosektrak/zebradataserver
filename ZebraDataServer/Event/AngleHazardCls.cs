using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class AngleHazardCls
    {
        SQL gSql;
        SQL2 gSql2;
        public AngleHazardCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public Boolean DetectAngleHazard(int veh_id, int speed, int type_of_msg)
        {
            if (gSql.GetVehSpeedLimit(veh_id) < speed)
            {
                return true;
            }
            return false;
        }

        public void InsertNewRecord(int ref_idx)
        {
            gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.HAZARD_ANGLE);
        }
        public void InsertNewRecord_Log(decimal ref_idx ,int veh_id, DateTime timestamp)
        {
            gSql2.InsertMsgEvt(ref_idx,veh_id, timestamp ,(int)EVT_TYPE.HAZARD_ANGLE, -1, -1);
        }

        public bool IsFeatureExisted(int veh_id)
        {
            if (gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_HAZARD_ANGLE) < 1)
            {
                return false;
            }
            return true;
        }
    }
}
