using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class ZoneCurfewWithSpeedCls : FencingCls
    {
        SQL gSql;
        SQL2 gSql2;
        public ZoneCurfewWithSpeedCls(String dbServer): base(dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public DataTable GetFleetZoneCurfewWithSpeedByVeh(int veh_id)
        {
            return gSql.GetFleetZoneCurfewWithSpeedByVeh(veh_id);
        }
        public DataTable GetFleetZoneCurfewWithSpeedByVeh_new(int veh_id)
        {
            return gSql.GetFleetZoneCurfewWithSpeedByVeh_new(veh_id);
        }
        public void GetLastZoneCurfewWithSpeedStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark, out int speed)
        {
            gSql.GetLastZoneCurfewWithSpeedStatus(veh_id, last_zone_id, out evt_id, out zone_id, out local_timestamp, out mark, out speed);
        }


        public void GetLastZoneCurfewWithSpeedStatus_Log(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark, out int speed)
        {
            gSql2.GetLastStatusZoneCurfewWithSpeed(veh_id, last_zone_id, out evt_id, out zone_id, out local_timestamp, out mark, out speed);
        }

        public void InsertMsgZoneStatus_Log(int veh_id, Decimal ref_idx, int zone_id, int mark, int speed)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.UpSertStatusZoneCurfewWithSpeed(veh_id, ref_idx, zone_id, (int)EVT_TYPE.IN_ZONE, mark, speed);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.UpSertStatusZoneCurfewWithSpeed(veh_id, ref_idx, zone_id, (int)EVT_TYPE.OUT_ZONE, mark, speed);
            }
        }
        public void InsertNewRecordCurfewWithSpeed_Log(decimal ref_idx, int veh_id, DateTime timestamp, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED, zone_id, -1);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED, zone_id, -1);
            }
        }


        public void InsertMsgZone(int veh_id, int ref_idx, int zone_id, int mark, int speed)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.InsertMsgZoneCurfewWithSpeed(veh_id, ref_idx, zone_id, (int)EVT_TYPE.IN_ZONE, mark, speed);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.InsertMsgZoneCurfewWithSpeed(veh_id, ref_idx, zone_id, (int)EVT_TYPE.OUT_ZONE, mark, speed);
            }
        }
        public void InsertNewRecordCurfewWithSpeed(int ref_idx)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED);
            }
        }

        public bool IsFeatureExisted(int veh_id)
        {
            if (gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_NO_INZONE_CURFEW_WITH_SPEED) < 1 &&
                gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_NO_OUTZONE_CURFEW_WITH_SPEED) < 1)
            {
                return false;
            }
            return true;
        }
    }
}
