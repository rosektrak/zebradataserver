using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    public class EngineOnOffInOutZoneCls
    {
        public enum ZONETYPE
        { 
            IN = 00,
            OUT = 01 
        };

        public enum ZONEEVT 
        { 
            IN = 17,
            OUT = 18 
        };

        SQL gSql;
        SQL2 gSql2;
        LocationCls oLoc;

        public EngineOnOffInOutZoneCls(String dbServer)
        {
            gSql = new SQL(dbServer); 
            gSql2 = new SQL2(dbServer);
            oLoc = new LocationCls(dbServer);
        }
        public DataTable GetFeatureEvtParam(int veh_id)
        {
            return gSql.GetFeatureEvtParam(veh_id, (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE);
        }
        public DataTable GetFeatureEvtParam_new(int veh_id)
        {
            return gSql.GetFeatureEvtParam_new(veh_id, (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE);
        }
        public bool IsEngineOn(int veh_id)
        {
            return gSql.IsEngineOn(veh_id);
        }

        public void GetLastZoneStatus(int veh_id, int zone_id, out int evt_id, out int last_zone_id)
        {
            gSql.GetLastZoneStatus(veh_id, zone_id, out evt_id, out last_zone_id);
        }

        public bool IsZonePortOutOfPolicy(int veh_id,int zone_id,int zone_evt_id,int io_port, int delay)
        {
            return gSql.IsZonePortOutOfPolicy(veh_id, Convert.ToInt32(zone_id), zone_evt_id, io_port, delay);
        }
        public void InsertNewRecord(int ref_idx,int evt)
        {
            gSql.InsertMsgEvt(ref_idx, evt);
        }

        public bool IsEngineOn_Log(int veh_id)
        {
            return gSql2.IsEngineOn(veh_id);
        }

        public void GetLastZoneStatus_Log(int veh_id, int zone_id, out int evt_id, out int last_zone_id)
        {
            gSql2.GetLastZoneStatus(veh_id, zone_id, out evt_id, out last_zone_id);
        }

        public bool IsZonePortOutOfPolicy_Log(int veh_id, int zone_id, int zone_evt_id, int io_port, int delay)
        {
            return gSql2.IsZonePortOutOfPolicy(veh_id, Convert.ToInt32(zone_id), zone_evt_id, io_port, delay);
        }
        public void InsertNewRecord_Log(decimal ref_idx, int veh_id, DateTime timestamp, int evt,int zone_id, int evt_black_side)
        {
            gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, evt, zone_id, evt_black_side);
        }

        public void DetectEvent(int ref_idx, decimal log_index, int veh_id, string g_type_of_msg, bool enableQ, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ)
        {            
            try
            {
                DataTable newweb = gSql.GetFeatureEvtParam_new(veh_id, (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE) ;
                DataTable dt = new DataTable();
                if (newweb.Rows.Count > 0)
                {
                    dt = gSql.GetFeatureEvtParam_new(veh_id, (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE);
                }
                else
                {
                    dt = gSql.GetFeatureEvtParam(veh_id, (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE);
                }
                foreach (DataRow r in dt.Rows)
                {                    
                    string zone_name     =  r["zone_desc"]  as string ;
                    string zone_id       =  r["zone_id"]    as string ;
                    string zone_type     =  r["zone_type"]  as string ;
                    string day           =  r["day_name"]   as string ;
                    string start_time    =  r["start_time"] as string ;
                    string end_time      =  r["end_time"]   as string ;
                    string engine_status =  r["engine_status"] as string ; 
                    string delay         =  r["delay_time"] as string ;

                    /*********SQL1*********/
                    bool IsEngineOn = gSql.IsEngineOn(veh_id) ;

                    /*********SQL2*********/
                   // bool IsEngineOn = gSql2.IsEngineOn(veh_id);

                    int evt_id = -1;
                    int last_zone_id = -1;

                    /*********SQL1*********/
                    gSql.GetLastZoneStatus(veh_id, Convert.ToInt32(zone_id), out evt_id, out last_zone_id);
                    /*********SQL2*********/
                   // gSql2.GetLastZoneStatus(veh_id, Convert.ToInt32(zone_id), out evt_id, out last_zone_id);                    

                    int iDay = Convert.ToInt32(day) - 1;
                    if (iDay < 1) iDay = 7;
                    if ((int)DateTime.Now.DayOfWeek != iDay) continue;

                    DateTime dn = Convert.ToDateTime(DateTime.Now);
                    DateTime st = Convert.ToDateTime( start_time );
                    DateTime en = Convert.ToDateTime( end_time );
                    
                    TimeSpan spStart = new TimeSpan(st.Hour, st.Minute, st.Second);
                    TimeSpan spEnd   = new TimeSpan(en.Hour, en.Minute, en.Second);
                    TimeSpan spNow   = new TimeSpan(dn.Hour, dn.Minute, dn.Second);                    

                    if (spStart.TotalMinutes > spNow.TotalMinutes) continue ;                    
                    if (spNow.TotalMinutes > spEnd.TotalMinutes)   continue ;                    

                    int zone_evt_id = Convert.ToInt32(zone_type) == 0 ? (int)ZONEEVT.IN : (int)ZONEEVT.OUT ;
                    int io_port = Convert.ToInt32(engine_status) == 0 ? 0x00 : 0x10 ;

                    if (gSql.IsZonePortOutOfPolicy(veh_id, Convert.ToInt32(zone_id), zone_evt_id, io_port, Convert.ToInt32(delay)))
                    {                        
                        //if (io_port == 0x10 && zone_evt_id == (int)ZONEEVT.IN)
                        //{
                        //    /*********SQL1*********/
                        //    InsertNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_ON_IN_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name);
                        //    /*********SQL2*********/
                        //    InsertNewRecord_Log(log_index, veh_id, EVT_TYPE.ENGINE_ON_IN_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name, int.Parse(zone_id));
                        //}
                        //else if (io_port == 0x00 && zone_evt_id == (int)ZONEEVT.IN)
                        //{
                        //    /*********SQL1*********/
                        //    InsertNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_OFF_IN_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name);
                          
                        //    /*********SQL2*********/
                        //    InsertNewRecord_Log(log_index, veh_id, EVT_TYPE.ENGINE_OFF_IN_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name, int.Parse(zone_id));
                        //}
                        //else if (io_port == 0x10 && zone_evt_id == (int)ZONEEVT.OUT)
                        //{
                        //    /*********SQL1*********/
                        //    InsertNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_ON_OUT_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name);
                         
                        //    /*********SQL2*********/
                        //    InsertNewRecord_Log(log_index, veh_id, EVT_TYPE.ENGINE_ON_OUT_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name, int.Parse(zone_id));
                        //}
                        //else if (io_port == 0x00 && zone_evt_id == (int)ZONEEVT.OUT)
                        //{
                        //    /*********SQL1*********/
                        //    InsertNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_OFF_OUT_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name);
                            
                        //    /*********SQL2*********/
                        //    InsertNewRecord_Log(log_index, veh_id, EVT_TYPE.ENGINE_OFF_OUT_ZONE, PROFILEMENUID.EVENT_ENGINE_ON_OFF_IN_OUTZONE, speed, timestamp.ToString(), lat, lon, enableQ, SMSQ, EMAILQ, zone_name, int.Parse(zone_id));
                        //}                                                
                    }
                }
            }
            catch
            {
            }
        }

        //public void InsertNewRecord(int ref_idx, int vid, EVT_TYPE evt, PROFILEMENUID feature, int speed, string timestamp, double lat, double lon, bool enable_q, CommonQeue SMSQ, CommonQeue EMAILQ, string value)
        //{
        //    if (gSql.GetFeatureExistence(vid, (int)feature) == 1)
        //    {
        //        gSql.InsertMsgEvt(ref_idx, evt);
        //        Broadcast(enable_q, ref_idx, (int)evt, vid, speed, timestamp, lat, lon, SMSQ, EMAILQ, value);
        //    }
        //}

        //public void InsertNewRecord_Log(decimal ref_idx, int vid, EVT_TYPE evt, PROFILEMENUID feature, int speed, string timestamp, double lat, double lon, bool enable_q, CommonQeue SMSQ, CommonQeue EMAILQ, string value,int zone_id)
        //{
        //    if (gSql.GetFeatureExistence(vid, (int)feature) == 1)
        //    {
        //        gSql2.InsertMsgEvt(ref_idx, evt, zone_id);
        //        //Broadcast(enable_q, -1, (int)evt, vid, speed, timestamp, lat, lon, SMSQ, EMAILQ, value);
        //    }
        //}

        private void Broadcast(bool enableQ, int ref_idx, int type, int veh_id, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ, string value)
        {
            if (enableQ)
            {
                ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                info.idx = ref_idx;
                info.type = type;
                info.veh_id = veh_id;
                info.msg = value;
                info.timestamp = timestamp;
                info.location_t = GetLocation((float)lon, (float)lat);
                info.lat = lat;
                info.lon = lon;
                if (SMSQ != null || EMAILQ != null)
                {
                    SMSQ.PushQ(info);
                    EMAILQ.PushQ(info);
                }
            }
        }

        public string GetLocation(float lon, float lat)
        {
            if (oLoc == null)
            {
                return "?";
            }

            string location_e = "NO LOCATION";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    location_e = oLoc.GetCustomLandmarkName(id, 0);
                }
                else
                {
                    id = oLoc.DetectLandmark(lon, lat);
                    location_e = oLoc.GetLandmarkName(id, 0);
                }

                id = oLoc.DetectAdminPoly(lon, lat);
                location_e += " " + oLoc.GetPolyName(id, 0);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_e = location_e.TrimStart(new char[] { ' ' });
            }

            return location_e;
        }
    }
}
