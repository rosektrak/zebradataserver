using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class EngineOnEventCls 
    {
        SQL gSql;
        SQL2 gSql2;
        public EngineOnEventCls(String dbServer)            
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public DataTable GetEngineOnEventByVeh(int veh_id)
        {
            return gSql.GetEngineOnEventByVeh(veh_id);
        }
        public DataTable GetEngineOnEventByVeh_new(int veh_id)
        {
            return gSql.GetEngineOnEventByVeh_new(veh_id);
        }

        ///1 Engine On or Off
        public Boolean GetEngineOnOnByTime(int veh_id)
        {
            return gSql.GetEngineOnByTime(veh_id);
        }
        ///2 last event 
        public void GetLastEngineOnCurfewEventStatus(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            gSql.GetLastEngineOnCurfewEventStatus(veh_id, out evt_id, out local_timestamp);
        }
        //3 last engine on
        public void GetLastEngineOnEventStatus(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            gSql.GetLastEngineOnEventStatus(veh_id, out evt_id, out local_timestamp);
        }


        ///1 Engine On or Off
        public Boolean GetEngineOnByTime_Log(int veh_id)
        {
            return gSql2.GetEngineOnByTime(veh_id);
        }
        ///2 
        public void GetLastStatusEventEngineOn_Log(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            gSql2.GetLastStatusEngineOnEvent(veh_id, out evt_id, out local_timestamp);
        }
        ///3
        public void GetLastEngineOnStatus_Log(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            gSql2.GetLastEngineOnStatus(veh_id, out evt_id, out local_timestamp);
        }

        public void InsertMsgEngineOnEvent(int ref_idx)
        {
            gSql.InsertMsgEngineOnEvent(ref_idx, (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME);
        }
        public void InsertMsgEngineOnEvent_Log(decimal ref_idx ,int veh_id, DateTime timestamp)
        {
            gSql2.InsertMsgEvt(ref_idx,veh_id,timestamp, (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME, -1, -1);
        }

        public bool IsFeatureExisted(int veh_id)
        {
            if (gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_ENGINE_ON)  < 1 &&
                gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_ENGINE_OFF) < 1)
            {
                return false;
            }
            return true;
        }
    }
}
