using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class TemperatureEvtCls 
    {
        SQL gSql;
        SQL2 gSql2;
        public TemperatureEvtCls(String dbServer)            
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public DataTable GetTemperatureByVeh(int veh_id)
        {
            return gSql.GetTemperatureByVeh(veh_id);
        }
        public DataTable GetTemperatureByVeh_new(int veh_id)
        {
            return gSql.GetTemperatureByVeh_new(veh_id);
        }
        public void GetLastTemperatureEventStatus(int veh_id, out int evt_id, out int last_temperature, out int cur_temperature, out DateTime local_timestamp)
        {
            gSql.GetLastTemperatureEventStatus(veh_id, out evt_id, out last_temperature, out cur_temperature, out local_timestamp);
        }

        public void GetLastTemperatureEventStatus_Log(int veh_id, out int evt_id, out int last_temperature, out int cur_temperature, out DateTime local_timestamp)
        {
            gSql2.GetLastTemperatureEventStatus(veh_id, out evt_id, out last_temperature, out cur_temperature, out local_timestamp);
        }

        public void InsertMsgTemperatureEvent(decimal ref_idx)
        {
            gSql.InsertMsgTemperatureEvent(ref_idx, (int)EVT_TYPE.TEMPERATURE);
        }

        public void InsertMsgEvt_Log(decimal ref_idx ,int veh_id, DateTime timestamp)
        {
            gSql2.InsertMsgEvt(ref_idx,veh_id,timestamp, (int)EVT_TYPE.TEMPERATURE, -1, -1);
        }

        public bool IsFeatureExisted(int veh_id)
        {
            if (gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_TEMPERATURE) < 1)
            {
                return false;
            }
            return true;
        }
    }
}
