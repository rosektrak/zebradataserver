using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
//using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Silver.UI;
using System.Collections;
using System.Reflection;

namespace ZebraManagementGUI
{
    enum eGroupMenu { Machines = 1, Application = 2, Control = 3 ,Service = 4}

    public partial class frmMain : Form
    {
        #region Static Helper Methods
        public static Stream GetResource(string resourceName)
        {
            Stream stream = null;
            try
            {
                Assembly asm = Assembly.GetExecutingAssembly();
                stream = asm.GetManifestResourceStream("ZebraManagementGUI.Resources." + resourceName);
            }
            catch (Exception)
            {
                stream = null;
            }
            return stream;
        }

        public static Image GetImage(string resouceName)
        {
            Image image = null;
            Stream stream = null;
            try
            {
                stream = GetResource("Images." + resouceName);
                image = Image.FromStream(stream);
            }
            catch (Exception)
            {
                image = null;
            }
            return image;
        }
        #endregion //Static Helper Methods

        private frmDefault objDefault = null;
        private frmMachine objMachine = null;
        private frmMachindMenu objMachineBar = null;

        private SQL gSql = new SQL();

        private Hashtable hsDataTable = new Hashtable();
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
      //      toolBox.SetImageList(GetImage("ToolBox_Small.bmp"), new Size(16, 16), Color.Magenta, true);
      //      toolBox.SetImageList(GetImage("ToolBox_Large.bmp"), new Size(32, 32), Color.Magenta, false);

            //this.CreateToolWindow();
            //InitializeInternal();
            toolBox.RenameFinished += new RenameFinishedHandler(toolBox_RenameFinished);
            toolBox.TabSelectionChanged += new TabSelectionChangedHandler(toolBox_TabSelectionChanged);
            toolBox.ItemSelectionChanged += new ItemSelectionChangedHandler(toolBox_ItemSelectionChanged);
            toolBox.TabMouseUp += new TabMouseEventHandler(toolBox_TabMouseUp);
            toolBox.ItemMouseUp += new ItemMouseEventHandler(toolBox_ItemMouseUp);
            toolBox.OnDeSerializeObject += new XmlSerializerHandler(toolBox_OnDeSerializeObject);
            toolBox.OnSerializeObject += new XmlSerializerHandler(toolBox_OnSerializeObject);
            toolBox.ItemKeyPress += new ItemKeyPressEventHandler(toolBox_ItemKeyPress);

            foreach (eGroupMenu suit in Enum.GetValues(typeof(eGroupMenu)))
            {
                AddToolBox(suit);
            }
        }

        void toolBox_RenameFinished(ToolBoxItem sender, RenameFinishedEventArgs e)
        {
        }

        void toolBox_TabSelectionChanged(ToolBoxTab sender, EventArgs e)
        {
            if (this.Focus())
            { }
        }

        void toolBox_ItemSelectionChanged(ToolBoxItem sender, EventArgs e)
        {

           
        }

        void toolBox_TabMouseUp(ToolBoxTab sender, MouseEventArgs e)
        {
            //Tap
            if (sender.Selected)
            {
                ClearObj();
                if (sender.Caption == eGroupMenu.Machines.ToString())
                {
                    this.objMachine = new frmMachine();
                    this.objMachine.TopLevel = false;
                    this.objMachine.Parent = this.pnMain;
                    this.objMachine.OnUpdateDataCompleted += new UpdateDataCompletedHandler(Obj_OnUpdateDataCompleted);
                    this.objMachine.Show();

                    this.objMachineBar = new frmMachindMenu();
                    this.objMachineBar.TopLevel = false;
                    this.objMachineBar.Parent = this.pnMenu;
                    this.objMachineBar.OnUpdateDataCompleted += new UpdateDataCompletedHandler(Obj_OnUpdateDataCompleted);
                    this.objMachineBar.Show();
                }
            }
        }

        void toolBox_ItemMouseUp(ToolBoxItem sender, MouseEventArgs e)
        {
            //Item
            if (sender.Selected)
            {
                ClearObj();

                this.objMachine = new frmMachine();
                this.objMachine.TopLevel = false;
                this.objMachine.Parent = this.pnMain;
                this.objMachine.OnUpdateDataCompleted += new UpdateDataCompletedHandler(Obj_OnUpdateDataCompleted);
                this.objMachine.Show();

            }
        }

        void toolBox_OnDeSerializeObject(ToolBoxItem sender, XmlSerializationEventArgs e)
        {
        }

        void toolBox_OnSerializeObject(ToolBoxItem sender, XmlSerializationEventArgs e)
        {
        }

        void toolBox_ItemKeyPress(ToolBoxItem sender, KeyPressEventArgs e)
        {
        }

        private void Obj_OnUpdateDataCompleted()
        {
        }
    
        private void AddToolBox(eGroupMenu menu)
        {
            try
            {
                toolBox.AddTab(menu.ToString(), -1);
                
                //ToolGroup group = new ToolGroup(menu.ToString(), true);
                //toolBoxMenu.ToolGroups.Add(group);
                DataTable dt = GetAndAddToolItem(menu);
                
                int tab = (int)menu - 1;
                if (!hsDataTable.ContainsKey(tab))
                {
                    hsDataTable.Add(tab, dt);
                }

                for (int i = 0; i < dt.Rows.Count;i++ )
                {
                    ////    AddToolItem((int)menu - 1, r["display"].ToString(), null);
                    toolBox[tab].AddItem((string)dt.Rows[i]["display"]);
                }
            }
            catch
            { }
        }

        private DataTable GetAndAddToolItem(eGroupMenu menu)
        {
            try
            {
                switch (menu)
                {
                    case eGroupMenu.Application:
                        return gSql.GetApplication();
                    case eGroupMenu.Machines:
                        return gSql.GetMachine();
                }
            }
            catch { }
            return new DataTable();
        }

        private void AddToolItem(int group_index, string item_name, Image img)
        {
            try
            {
                //ToolItem item = new ToolItem();
                //item.Text = item_name;
                //item.Type = group_index;

                //if (img != null)
                //{
                //    //item.Image = Image.FromFile(textBox3.Text);
                //    item.Image = img;
                //}
                //if (group_index < 0)
                //{
                //    return;
                //}

                //item.Group = toolBoxMenu.ToolGroups[group_index];
                //toolBoxMenu.ToolItems.Add(item);
            }
            catch { }
        }

        private void ClearObj()
        {
            if (this.objDefault != null)
            {
                this.objDefault.Dispose();
                this.objDefault.Close();
                this.objDefault = null;
            }
            if (this.objMachine != null)
            {
                this.objMachine.Dispose();
                this.objMachine.Close();
                this.objMachine = null;
            }
            if (this.objMachineBar != null)
            {
                this.objMachineBar.Dispose();
                this.objMachineBar.Close();
                this.objMachineBar = null;
            }

        }

        private void InitializeInternal()
        {

           // this.cmbPropType.SelectedIndex = 0;
            this.CreateToolWindow();

            //this.itemTabIndex.ValueChanged += new EventHandler(this.itemTabIndex_ValueChanged);
            //this.selItemEnabled.CheckedChanged += new EventHandler(this.selItemEnabled_CheckedChanged);
            //this.addItem.Click += new EventHandler(this.addItem_Click);
            //this.addItemEx.Click += new EventHandler(this.addItemEx_Click);
            //this.editSelItem.Click += new EventHandler(this.editSelItem_Click);
            //this.delSelItem.Click += new EventHandler(this.delSelItem_Click);
            //this.itemAllowDrag.CheckedChanged += new EventHandler(this.itemAllowDrag_CheckedChanged);
            //this.btnLoad.Click += new EventHandler(this.btnLoad_Click);
            //this.btnSave.Click += new EventHandler(this.btnSave_Click);
            //this.selTabEnabled.CheckedChanged += new EventHandler(this.selTabEnabled_CheckedChanged);
            //this.addTabEx.Click += new EventHandler(this.addTabEx_Click);
            //this.addTab.Click += new EventHandler(this.addTab_Click);
            //this.delSelTab.Click += new EventHandler(this.delSelTab_Click);
            //this.editSelTab.Click += new EventHandler(this.editSelTab_Click);
            //this.dragContainer.DragDrop += new DragEventHandler(this.dragContainer_DragDrop);
            //this.dragContainer.DragOver += new DragEventHandler(this.dragContainer_DragOver);
            //this.cmbPropType.SelectedIndexChanged += new EventHandler(this.cmbPropType_SelectedIndexChanged);

        }
        private void CreateToolWindow()
        {
            Point ptLocation = this.Location;
            TreeView treeView = null;


            //  if (null == toolWindow)
            {


                toolBox.SetImageList(GetImage("ToolBox_Small.bmp"), new Size(16, 16), Color.Magenta, true);
                toolBox.SetImageList(GetImage("ToolBox_Large.bmp"), new Size(32, 32), Color.Magenta, false);

                //toolBox1.RenameFinished += new RenameFinishedHandler(ToolBox_RenameFinished);
                //toolBox1.TabSelectionChanged += new TabSelectionChangedHandler(ToolBox_TabSelectionChanged);
                //toolBox1.ItemSelectionChanged += new ItemSelectionChangedHandler(ToolBox_ItemSelectionChanged);
                //toolBox1.TabMouseUp += new TabMouseEventHandler(ToolBox_TabMouseUp);
                //toolBox1.ItemMouseUp += new ItemMouseEventHandler(ToolBox_ItemMouseUp);
                //toolBox1.OnDeSerializeObject += new XmlSerializerHandler(ToolBox_OnDeSerializeObject);
                //toolBox1.OnSerializeObject += new XmlSerializerHandler(ToolBox_OnSerializeObject);
                //toolBox1.ItemKeyPress += new ItemKeyPressEventHandler(ToolBox_ItemKeyPress);
            }
            toolBox.DeleteAllTabs(false);

            toolBox.AddTab("Tab 1", -1);
            toolBox.AddTab("Tab 2 (TreeView)", -1);
            toolBox.AddTab("Tab 3", -1);
            toolBox.AddTab("Tab 4", -1);
            toolBox.AddTab("Tab 5 (TreeView)", -1);

            treeView = new TreeView();
            treeView.BorderStyle = BorderStyle.None;

            FillTreeView(treeView);
            toolBox[1].Control = treeView;

            treeView = new TreeView();
            treeView.BorderStyle = BorderStyle.None;
            treeView.Dock = DockStyle.Fill;

            FillTreeView(treeView);

            toolBox[4].Control = treeView;

            toolBox[0].View = ViewMode.LargeIcons;
            toolBox[0].AddItem("Pointer", 0, 0, false, null);
            toolBox[0].AddItem("Tab Item 1", 1, 1, true, 1);
            toolBox[0].AddItem("Tab Item 2", 2, 2, true, 2);

            //toolBox1[0].AddItem("Pointer",0,0,false,null);
            toolBox[2].AddItem("Pointer", 0, 0, false, null);
            toolBox[2].AddItem("Tab Item 1", 3, 3, true, 3);
            toolBox[2].AddItem("Tab Item 2", 4, 4, true, 4);

            toolBox[3].AddItem("Pointer", 0, 0, false, null);
            toolBox[3].AddItem("Tab Item 1", 5, 5, true, 5);
            toolBox[3].AddItem("Tab Item 2", 6, 6, true, 6);

            toolBox[0].Deletable = false;
            toolBox[0].Renamable = false;
            toolBox[0].Movable = false;

            toolBox[0][0].Renamable = false;
            //toolBox1[1][0].Renamable = false;
            toolBox[2][0].Renamable = false;
            toolBox[3][0].Renamable = false;

            toolBox[0][0].Deletable = false;
            //toolBox1[1][0].Deletable = false;
            toolBox[2][0].Deletable = false;
            toolBox[3][0].Deletable = false;

            toolBox[0][0].Movable = false;
            //toolBox1[1][0].Movable = false;
            toolBox[2][0].Movable = false;
            toolBox[3][0].Movable = false;

            toolBox[0].Selected = true;

            //tabIndex.Value = toolBox1.SelectedTabIndex;
           // itemTabIndex.Value = toolBox1.SelectedTabIndex;

            ptLocation.X = this.Right;
            //toolWindow.Visible = true;
            //toolWindow.Height = Height;
            //toolWindow.Location = ptLocation;

            //this.tabImgUpDn.Minimum = -1;
            //this.itemImgUpDn.Minimum = -1;
            //this.tabImgUpDn.Maximum = toolBox1.SmallImageList.Images.Count - 1;
            //this.itemImgUpDn.Maximum = toolBox1.SmallImageList.Images.Count - 1;

        }

        private void FillTreeView(TreeView t)
        {
            t.ImageList = toolBox.SmallImageList;

            t.Nodes.Add("Musical Instruments");

            /*t.Nodes[0].ImageIndex = 1;
            t.Nodes[0].SelectedImageIndex = 1;*/

            t.Nodes[0].Nodes.Add("Piano");

            /*t.Nodes[0].Nodes[0].ImageIndex = 2;
            t.Nodes[0].Nodes[0].SelectedImageIndex = 2;*/

            t.Nodes[0].Nodes[0].Nodes.Add("Acoustic grand piano");
            t.Nodes[0].Nodes[0].Nodes.Add("Bright acoustic piano");
            t.Nodes[0].Nodes[0].Nodes.Add("Electric grand piano");
            t.Nodes[0].Nodes[0].Nodes.Add("Honky-tonk piano");
            t.Nodes[0].Nodes[0].Nodes.Add("Rhodes piano");
            t.Nodes[0].Nodes[0].Nodes.Add("Chorused piano");
            t.Nodes[0].Nodes[0].Nodes.Add("Harpsichord");
            t.Nodes[0].Nodes[0].Nodes.Add("Clavinet");

            t.Nodes[0].Nodes.Add("Chromatic Percussion");

            t.Nodes[0].Nodes[1].Nodes.Add("Celesta");
            t.Nodes[0].Nodes[1].Nodes.Add("Glockenspiel");
            t.Nodes[0].Nodes[1].Nodes.Add("Music box");
            t.Nodes[0].Nodes[1].Nodes.Add("Vibraphone");
            t.Nodes[0].Nodes[1].Nodes.Add("Marimba");
            t.Nodes[0].Nodes[1].Nodes.Add("Xylophone");
            t.Nodes[0].Nodes[1].Nodes.Add("Tubular bells");
            t.Nodes[0].Nodes[1].Nodes.Add("Dulcimer");

            t.Nodes[0].Nodes.Add("Organ");
            t.Nodes[0].Nodes[2].Nodes.Add("Hammond organ");
            t.Nodes[0].Nodes[2].Nodes.Add("Percussive organ");
            t.Nodes[0].Nodes[2].Nodes.Add("Rock organ");
            t.Nodes[0].Nodes[2].Nodes.Add("Church organ");
            t.Nodes[0].Nodes[2].Nodes.Add("Reed organ");
            t.Nodes[0].Nodes[2].Nodes.Add("Accordion");
            t.Nodes[0].Nodes[2].Nodes.Add("Harmonica");
            t.Nodes[0].Nodes[2].Nodes.Add("Tango accordion");

            t.Nodes[0].Nodes.Add("Guitar");
            t.Nodes[0].Nodes[3].Nodes.Add("Acoustic guitar (nylon)");
            t.Nodes[0].Nodes[3].Nodes.Add("Acoustic guitar (steel)");
            t.Nodes[0].Nodes[3].Nodes.Add("Electric guitar (jazz)");
            t.Nodes[0].Nodes[3].Nodes.Add("Electric guitar (clean)");
            t.Nodes[0].Nodes[3].Nodes.Add("Electric guitar (muted)");
            t.Nodes[0].Nodes[3].Nodes.Add("Overdriven guitar");
            t.Nodes[0].Nodes[3].Nodes.Add("Distortion guitar");
            t.Nodes[0].Nodes[3].Nodes.Add("Guitar harmonics");

            t.Nodes[0].Nodes.Add("Bass");
            t.Nodes[0].Nodes[4].Nodes.Add("Acoustic bass");
            t.Nodes[0].Nodes[4].Nodes.Add("Electric bass (finger)");
            t.Nodes[0].Nodes[4].Nodes.Add("Electric bass (pick)");
            t.Nodes[0].Nodes[4].Nodes.Add("Fretless bass");
            t.Nodes[0].Nodes[4].Nodes.Add("Slap bass 1");
            t.Nodes[0].Nodes[4].Nodes.Add("Slap bass 2");
            t.Nodes[0].Nodes[4].Nodes.Add("Synth bass 1");
            t.Nodes[0].Nodes[4].Nodes.Add("Synth bass 2");

            t.Nodes[0].Nodes.Add("Strings");
            t.Nodes[0].Nodes[5].Nodes.Add("Violin");
            t.Nodes[0].Nodes[5].Nodes.Add("Viola");
            t.Nodes[0].Nodes[5].Nodes.Add("Cello");
            t.Nodes[0].Nodes[5].Nodes.Add("Contrabass");
            t.Nodes[0].Nodes[5].Nodes.Add("Tremolo strings");
            t.Nodes[0].Nodes[5].Nodes.Add("Pizzicato strings");
            t.Nodes[0].Nodes[5].Nodes.Add("Orchestral harp");
            t.Nodes[0].Nodes[5].Nodes.Add("Timpani");

            t.Nodes[0].Nodes.Add("Ensemble");
            t.Nodes[0].Nodes[6].Nodes.Add("String ensemble 1");
            t.Nodes[0].Nodes[6].Nodes.Add("String ensemble 2");
            t.Nodes[0].Nodes[6].Nodes.Add("Synth. strings 1");
            t.Nodes[0].Nodes[6].Nodes.Add("Synth. strings 2");
            t.Nodes[0].Nodes[6].Nodes.Add("Choir Aahs");
            t.Nodes[0].Nodes[6].Nodes.Add("Voice Oohs");
            t.Nodes[0].Nodes[6].Nodes.Add("Synth voice");
            t.Nodes[0].Nodes[6].Nodes.Add("Orchestra hit");

            t.Nodes[0].Nodes.Add("Brass");
            t.Nodes[0].Nodes[7].Nodes.Add("Trumpet");
            t.Nodes[0].Nodes[7].Nodes.Add("Trombone");
            t.Nodes[0].Nodes[7].Nodes.Add("Tuba");
            t.Nodes[0].Nodes[7].Nodes.Add("Muted trumpet");
            t.Nodes[0].Nodes[7].Nodes.Add("French horn");
            t.Nodes[0].Nodes[7].Nodes.Add("Brass section");
            t.Nodes[0].Nodes[7].Nodes.Add("Synth. brass 1");
            t.Nodes[0].Nodes[7].Nodes.Add("Synth. brass 2");

            t.Nodes[0].Nodes.Add("Reed");
            t.Nodes[0].Nodes[8].Nodes.Add("Soprano sax");
            t.Nodes[0].Nodes[8].Nodes.Add("Alto sax");
            t.Nodes[0].Nodes[8].Nodes.Add("Tenor sax");
            t.Nodes[0].Nodes[8].Nodes.Add("Baritone sax");
            t.Nodes[0].Nodes[8].Nodes.Add("Oboe");
            t.Nodes[0].Nodes[8].Nodes.Add("English horn");
            t.Nodes[0].Nodes[8].Nodes.Add("Bassoon");
            t.Nodes[0].Nodes[8].Nodes.Add("Clarinet");

            t.Nodes[0].Nodes.Add("Pipe");
            t.Nodes[0].Nodes[9].Nodes.Add("Piccolo");
            t.Nodes[0].Nodes[9].Nodes.Add("Flute");
            t.Nodes[0].Nodes[9].Nodes.Add("Recorder");
            t.Nodes[0].Nodes[9].Nodes.Add("Pan flute");
            t.Nodes[0].Nodes[9].Nodes.Add("Bottle blow");
            t.Nodes[0].Nodes[9].Nodes.Add("Shakuhachi");
            t.Nodes[0].Nodes[9].Nodes.Add("Whistle");
            t.Nodes[0].Nodes[9].Nodes.Add("Ocarina");

            t.Nodes[0].Nodes.Add("Synth Lead");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 1 (square)");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 2 (sawtooth)");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 3 (calliope lead)");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 4 (chiff lead)");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 5 (charang)");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 6 (voice)");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 7 (fifths)");
            t.Nodes[0].Nodes[10].Nodes.Add("Lead 8 (brass + lead)");

            t.Nodes[0].Nodes.Add("Synth Pad");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 1 (new age)");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 2 (warm)");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 3 (polysynth)");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 4 (choir)");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 5 (bowed)");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 6 (metallic)");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 7 (halo)");
            t.Nodes[0].Nodes[11].Nodes.Add("Pad 8 (sweep)");

            t.Nodes[0].Nodes.Add("Sound Effects");
            t.Nodes[0].Nodes[12].Nodes.Add("Guitar fret noise");
            t.Nodes[0].Nodes[12].Nodes.Add("Breath noise");
            t.Nodes[0].Nodes[12].Nodes.Add("Seashore");
            t.Nodes[0].Nodes[12].Nodes.Add("Bird tweet");
            t.Nodes[0].Nodes[12].Nodes.Add("Telephone ring");
            t.Nodes[0].Nodes[12].Nodes.Add("Helicopter");
            t.Nodes[0].Nodes[12].Nodes.Add("Applause");
            t.Nodes[0].Nodes[12].Nodes.Add("Gunshot");

        }

        /*
        void toolBoxMenu_SelectionChanged(object sender, EventArgs e)
        {
            ToolItem sd = (ToolItem)sender;
            switch ((int)sd.Type)
            {
                case 0:
                    {
                        ClearObj();
                        this.objMachine = new frmMachine();
                        this.objMachine.TopLevel = false;
                        this.objMachine.Parent = this.pnMain;
                        this.objMachine.OnUpdateDataCompleted += new UpdateDataCompletedHandler(Obj_OnUpdateDataCompleted);
                        this.objMachine.Show();
                    }
                    break;
                case 1:
                    { };
                    break;
                case 2:
                    { };
                    break;
                case 3:
                    { }
                    break;
                default :
                    break;
            }
        }

        private void Obj_OnUpdateDataCompleted()
        {

        }
        private void frmMain_Load(object sender, EventArgs e)
        {
            foreach (eGroupMenu suit in Enum.GetValues(typeof(eGroupMenu)))
            {
                AddToolBox(suit);
            }
        }

        private void AddToolBox(eGroupMenu menu)
        {
            try
            {
                ToolGroup group = new ToolGroup(menu.ToString(), true);
                toolBoxMenu.ToolGroups.Add(group);
                DataTable dt = GetAndAddToolItem(menu);
                toolBoxMenu.Tag = dt;

                foreach (DataRow r in dt.Rows)
                {
                    AddToolItem((int)menu-1, r["display"].ToString(), null);
                }
            }
            catch
            { }
        }

        private DataTable GetAndAddToolItem(eGroupMenu menu)
        {
            try
            {
                switch (menu)
                {
                    case eGroupMenu.Application:
                        return gSql.GetApplication();
                    case eGroupMenu.Machines:
                        return gSql.GetMachine();
                }
            }
            catch { }
            return new DataTable();
        }

        private void AddToolItem(int group_index, string item_name, Image img)
        {
            try
            {
                ToolItem item = new ToolItem();
                item.Text = item_name;
                item.Type = group_index;

                if (img != null)
                {
                    //item.Image = Image.FromFile(textBox3.Text);
                    item.Image = img;
                }
                if (group_index < 0)
                {
                    return;
                }

                item.Group = toolBoxMenu.ToolGroups[group_index];
                toolBoxMenu.ToolItems.Add(item);
            

            }
            catch { }
        }

        private void ClearObj()
        {
            if (this.objDefault != null)
            {
                this.objDefault.Dispose();
                this.objDefault.Close();
                this.objDefault = null;
            } 
            if (this.objMachine != null)
            {
                this.objMachine.Dispose();
                this.objMachine.Close();
                this.objMachine = null;
            }
       
        }
        */
    }
}