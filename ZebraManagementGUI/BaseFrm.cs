using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraManagementGUI
{
    public delegate void UpdateDataCompletedHandler();
    public partial class BaseFrm : Form
    {
        public event UpdateDataCompletedHandler OnUpdateDataCompleted;
        public BaseFrm()
        {
            InitializeComponent();
        }
        protected void UpdateDataCompleted()
        {
            if (OnUpdateDataCompleted != null)
            {
                OnUpdateDataCompleted();
            }
        }

        private void BaseFrm_Load(object sender, EventArgs e)
        {

        }
    }
}