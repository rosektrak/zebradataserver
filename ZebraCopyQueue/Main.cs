using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ZrebraFilterInterface;
using System.Collections;

namespace ZebraCopyQueue
{
    public partial class Main : Form
    {
        struct QInfo
        {
            public int gId;
            public CommonQeue gQueue;
        }
        private CommonQeue MQ;
        private ArrayList arPushQueue = new ArrayList();

        public Main()
        {
            InitializeComponent();
            this.Text = "ZebreCopyQueue V 1.0";
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                ZebraCopyQueue.Properties.Settings st = new ZebraCopyQueue.Properties.Settings();
                string szSystemQ = st.SystemQueue;
                MQ = new CommonQeue(szSystemQ);
                MQ.OnReceiveMQ += new ReceiveMQ(MQ_OnReceiveMQ);
                MQ.InitReceiveQMsg();

                richTextBox1.Text = "SystemQueue = " + szSystemQ + '\n' + "FordwardQueue : ";

                int id = 0;
                string fwQueue = st.PushQueue;
                foreach (string qName in fwQueue.Split(','))
                {
                    if (qName.Length > 0)
                    {
                        QInfo info = new QInfo();
                        info.gId = id;
                        info.gQueue = new CommonQeue(qName);
                        arPushQueue.Add(info);

                        ListViewItem itm = listViewQueue.Items.Add(info.gId.ToString(), info.gId.ToString(), null);
                        itm.SubItems.Add(qName);
                        itm.SubItems.Add("0");
                        itm.Tag = 0;
                        richTextBox1.Text += '\n' + qName;

                        id++;
                    }
                }

                if (arPushQueue.Count > 0)
                {
                    MQ.BeginReceiveQMsg();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        void MQ_OnReceiveMQ(object owner, XmlReader xmlData, Stream stmData)
        {
            try
            {
                if (arPushQueue.Count > 0)
                {
                    for (int i = 0; i < arPushQueue.Count; i++)
                    {
                        QInfo info = (QInfo)arPushQueue[i];
                        info.gQueue.ForwardQ(stmData);
                       // int row = info.gId + 1;

                        //ListViewItem item = (ListViewItem)listViewQueue.Items[0];
                        //item.Tag = ((int)item.Tag + 1);
                        //listViewQueue.Items[row].SubItems[2].Text = item.Tag.ToString();
                        //listViewQueue.Items[row] = item;
                    }

                }
            }
            catch { }
            MQ.BeginReceiveQMsg();
            System.Threading.Thread.Sleep(10);
        }


    }
}