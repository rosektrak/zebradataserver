using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.IO;
using System.Messaging;
using ZebraPresentationLayer;

namespace ZebraDataCentre
{
    public delegate void ReceiveMQ(object owner, XmlReader xmlData);
    public delegate void ReceiveMQStream(object owner, XmlReader xmlData, Stream stmData);

    public class CommonQeue
    {
        public event ReceiveMQ OnReceiveMQ;
        String GlobalQPath = @".\Private$\ZRQ";
        //const String GlobalQPath = @".\Private$\ZRQ";
        //const String GlobalQPath = @"FormatName:DIRECT=TCP:10.0.100.78\private$\ZebraQ";
        protected MessageQueue MQ;
        public CommonQeue()
        {
            if (MessageQueue.Exists(GlobalQPath)) {
                MQ = new MessageQueue(GlobalQPath);
            } else {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public CommonQeue(String QPathStr)
        {
            GlobalQPath = String.Format(".\\Private$\\{0}", QPathStr);

            if (MessageQueue.Exists(GlobalQPath)) {
                MQ = new MessageQueue(GlobalQPath);
            } else {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public void InitReceiveQMsg()
        {
            if (MQ != null)
            {
                MQ.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                MQ.ReceiveCompleted += new ReceiveCompletedEventHandler(MQ_ReceiveCompleted);                
            }
        }
        public void BeginReceiveQMsg()
        {
            MQ.BeginReceive();
        }
        private void MQ_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                StreamReader so = new StreamReader(e.Message.BodyStream);
                String s = so.ReadToEnd();

                StringReader txtRd = new StringReader(s);
                XmlReader xmlRd = XmlReader.Create(txtRd);
                if (OnReceiveMQ != null) {
                    OnReceiveMQ(this, xmlRd);
                }

                xmlRd.Close();
                txtRd.Close();
                txtRd.Dispose();                
            }
            catch
            {
                MQ.BeginReceive();
            }
            finally
            {                
            }
        }
        public void PushQ(object txtMsg)
        {
            try
            {
                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(txtMsg.GetType());
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, txtMsg);
                txtWr.Close();

                byte[] buff = Encoding.UTF8.GetBytes(txtWr.ToString());
                Stream stm = new MemoryStream(buff);

                Message qMsg = new Message();
                qMsg.BodyStream = stm;
                qMsg.Recoverable = true;
                MQ.Send(qMsg);

                qMsg.Dispose();
                stm.Close();
                stm.Dispose();
                txtWr.Dispose();
                //xmlWriter.Close();       
            }
            catch(Exception ex)
            { }
        }
        public String GetQ()
        {
            Message msg = MQ.Receive();
            msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });

            String foo = msg.Body.ToString();
            return foo;
        }
        public void ClearAllQ()
        {
            try
            {
                MQ.Purge();
            }
            catch 
            {

            }
        }
    }

    public class CommonQueueStream
    {
        public event ReceiveMQStream OnReceiveMQ;

        String GlobalQPath = @".\Private$\ZRQ";

        //const String GlobalQPath = @".\Private$\ZRQ";
        //const String GlobalQPath = @"FormatName:DIRECT=TCP:10.0.100.78\private$\ZebraQ";

        protected MessageQueue MQ;

        public CommonQueueStream()
        {
            if (MessageQueue.Exists(GlobalQPath))
            {
                MQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public CommonQueueStream(String QPathStr)
        {
            GlobalQPath = String.Format(".\\Private$\\{0}", QPathStr);

            if (MessageQueue.Exists(GlobalQPath))
            {
                MQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public CommonQueueStream(string QFwdIpStr, string QFwdPathStr)
        {
            try
            {
                GlobalQPath = String.Format(@"FormatName:Direct=TCP:{0}\private$\{1}", QFwdIpStr, QFwdPathStr);
                MQ = new MessageQueue(GlobalQPath);
            }
            catch 
            {}
        }

        public void InitReceiveQMsg()
        {
            if (MQ != null)
            {
                MQ.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                MQ.ReceiveCompleted += new ReceiveCompletedEventHandler(MQ_ReceiveCompleted);
            }
        }

        public void BeginReceiveQMsg()
        {
            MQ.BeginReceive();
        }
        private void MQ_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                StreamReader so = new StreamReader(e.Message.BodyStream);
                String s = so.ReadToEnd();

                StringReader txtRd = new StringReader(s);
                XmlReader xmlRd = XmlReader.Create(txtRd);
                OnReceiveMQ(this, xmlRd, e.Message.BodyStream);
            }
            catch
            {
            }
            finally
            {
            }
        }
        public void ForwardQ(Stream data)
        {
            try
            {
                Message msg = new Message();
                msg.BodyStream = data;
                MQ.Send(msg);
            }
            catch
            {}
        }
        public void PushQ(object txtMsg)
        {
            XmlWriterSettings wrSettings = new XmlWriterSettings();
            wrSettings.Indent = true;

            XmlSerializer s = new XmlSerializer(txtMsg.GetType());
            TextWriter txtWr = new StringWriter();

            XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
            xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

            s.Serialize(xmlWriter, txtMsg);
            txtWr.Close();

            byte[] buff = Encoding.UTF8.GetBytes(txtWr.ToString());
            Stream stm = new MemoryStream(buff);

            Message qMsg = new Message();
            qMsg.BodyStream = stm;
            qMsg.Recoverable = true;
            MQ.Send(qMsg);
        }
        public String GetQ()
        {
            Message msg = MQ.Receive();
            msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });

            String foo = msg.Body.ToString();
            return foo;
        }

        public int GetAllMessage()
        {
            int total = 0;
            if (MQ != null)
            {
                MessageEnumerator messageEnumerator1 = MQ.GetMessageEnumerator2();

                while (messageEnumerator1.MoveNext())
                {
                    total++;
                }
            }
            return total;
        }
    }

    #region GBox
    [XmlRoot("GGateMessage")]
    public struct GGateResetMessage
    {
        string _product_name;
        [XmlElement("product_name")]
        public string Product_name
        {
            get { return _product_name; }
            set { _product_name = value; }
        }

        string _major_version;
        [XmlElement("major_version")]
        public string Major_version
        {
            get { return _major_version; }
            set { _major_version = value; }
        }

        string _minor_version;
        [XmlElement("minor_version")]
        public string Minor_version
        {
            get { return _minor_version; }
            set { _minor_version = value; }
        }

        string _release_date;
        [XmlElement("release_date")]
        public string Release_date
        {
            get { return _release_date; }
            set { _release_date = value; }
        }

        string _password;
        [XmlElement("password")]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        string _utc_time;
        [XmlElement("utc_time")]
        public string utc_time
        {
            get { return _utc_time; }
            set { _utc_time = value; }
        }

        string _lat;
        [XmlElement("lat")]
        public string lat
        {
            get { return _lat; }
            set { _lat = value; }
        }

        string _lon;
        [XmlElement("lon")]
        public string lon
        {
            get { return _lon; }
            set { _lon = value; }
        }

        string _alt;
        [XmlElement("alt")]
        public string alt
        {
            get { return _alt; }
            set { _alt = value; }
        }

        string _speed;
        [XmlElement("speed")]
        public string speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        string _true_course;
        [XmlElement("true_course")]
        public string True_course
        {
            get { return _true_course; }
            set { _true_course = value; }
        }

        string _HDOP;
        [XmlElement("HDOP")]
        public string HDOP
        {
            get { return _HDOP; }
            set { _HDOP = value; }
        }

        string _status;
        [XmlElement("status")]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        string _mesg_id;
        [XmlElement("mesg_id")]
        public string Mesg_id
        {
            get { return _mesg_id; }
            set { _mesg_id = value; }
        }

        string _timestamp;
        [XmlElement("timestamp")]
        public string Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }

        string _veh_id;
        [XmlElement("veh_id")]
        public string Veh_id
        {
            get { return _veh_id; }
            set { _veh_id = value; }
        }

        string _idle_interval;
        [XmlElement("idle_interval")]
        public string Idle_interval
        {
            get { return _idle_interval; }
            set { _idle_interval = value; }
        }

        string _speed_limit;
        [XmlElement("speed_limit")]
        public string Speed_limit
        {
            get { return _speed_limit; }
            set { _speed_limit = value; }
        }

        string _speed_interval;
        [XmlElement("speed_interval")]
        public string Speed_interval
        {
            get { return _speed_interval; }
            set { _speed_interval = value; }
        }

        string _ref_lat;
        [XmlElement("ref_lat")]
        public string Ref_lat
        {
            get { return _ref_lat; }
            set { _ref_lat = value; }
        }

        string _ref_lon;
        [XmlElement("ref_lon")]
        public string Ref_lon
        {
            get { return _ref_lon; }
            set { _ref_lon = value; }
        }

        string _milage;
        [XmlElement("mileage")]
        public string Milage
        {
            get { return _milage; }
            set { _milage = value; }
        }

        string _port;
        [XmlElement("port")]
        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        string _port_value;
        [XmlElement("port_value")]
        public string Port_value
        {
            get { return _port_value; }
            set { _port_value = value; }
        }

        string _driver_id;
        [XmlElement("driver_id")]
        public string Driver_id
        {
            get { return _driver_id; }
            set { _driver_id = value; }
        }

        string _trip_id;
        [XmlElement("trip_id")]
        public string Trip_id
        {
            get { return _trip_id; }
            set { _trip_id = value; }
        }

        string _waypt_id;
        [XmlElement("waypt_id")]
        public string Waypt_id
        {
            get { return _waypt_id; }
            set { _waypt_id = value; }
        }

        string _zone_id;
        [XmlElement("zone_id")]
        public string Zone_id
        {
            get { return _zone_id; }
            set { _zone_id = value; }
        }

        string _passenger_count;
        [XmlElement("passenger_count")]
        public string Passenger_count
        {
            get { return _passenger_count; }
            set { _passenger_count = value; }
        }

        string _sent_msg_id;
        [XmlElement("sent_mesg_id")]
        public string Sent_msg_id
        {
            get { return _sent_msg_id; }
            set { _sent_msg_id = value; }
        }

        string _action_code;
        [XmlElement("action_code")]
        public string Action_code
        {
            get { return _action_code; }
            set { _action_code = value; }
        }

        string _cm_id;
        [XmlElement("cm_id")]
        public string Cm_id
        {
            get { return _cm_id; }
            set { _cm_id = value; }
        }

        string _job_assign_id;
        [XmlElement("job_assign_id")]
        public string Job_assign_id
        {
            get { return _job_assign_id; }
            set { _job_assign_id = value; }
        }

        string _actual_qty;
        [XmlElement("actual_qty")]
        public string Actual_qty
        {
            get { return _actual_qty; }
            set { _actual_qty = value; }
        }

        string _actual_vol;
        [XmlElement("actual_vol")]
        public string Actual_vol
        {
            get { return _actual_vol; }
            set { _actual_vol = value; }
        }

        string _actual_weight;
        [XmlElement("actual_weight")]
        public string Actual_weight
        {
            get { return _actual_weight; }
            set { _actual_weight = value; }
        }

        string _job_status_id;
        [XmlElement("job_status_id")]
        public string Job_status_id
        {
            get { return _job_status_id; }
            set { _job_status_id = value; }
        }

        string _sched_id;
        [XmlElement("sched_id")]
        public string Sched_id
        {
            get { return _sched_id; }
            set { _sched_id = value; }
        }

        string _job_assign_id_cnt;
        [XmlElement("job_assign_id_cnt")]
        public string Job_assign_id_cnt
        {
            get { return _job_assign_id_cnt; }
            set { _job_assign_id_cnt = value; }
        }

        string _rfid_tag_no;
        [XmlElement("rfid_tag_no")]
        public string Rfid_tag_no
        {
            get { return _rfid_tag_no; }
            set { _rfid_tag_no = value; }
        }

        string _avg_rssi;
        [XmlElement("avg_rssi")]
        public string Avg_rssi
        {
            get { return _avg_rssi; }
            set { _avg_rssi = value; }
        }

        string _min_rssi;
        [XmlElement("min_rssi")]
        public string Min_rssi
        {
            get { return _min_rssi; }
            set { _min_rssi = value; }
        }

        string _max_rssi;
        [XmlElement("max_rssi")]
        public string Max_rssi
        {
            get { return _max_rssi; }
            set { _max_rssi = value; }
        }

        string _satelite;
        [XmlElement("satelite")]
        public string Satelite
        {
            get { return _satelite; }
            set { _satelite = value; }
        }

        string _odometer_sensor;
        [XmlElement("odometer_sensor")]
        public string Odometer_sensor
        {
            get { return _odometer_sensor; }
            set { _odometer_sensor = value; }
        }

        string _gps_odometer;
        [XmlElement("gps_odometer")]
        public string Gps_odometer
        {
            get { return _gps_odometer; }
            set { _gps_odometer = value; }
        }

        string _gsm_cell_id;
        [XmlElement("gsm_cell_id")]
        public string Gsm_cell_id
        {
            get { return _gsm_cell_id; }
            set { _gsm_cell_id = value; }
        }

        string _gsm_location_id;
        [XmlElement("gsm_location_id")]
        public string Gsm_location_id
        {
            get { return _gsm_location_id; }
            set { _gsm_location_id = value; }
        }

        string _gsm_rssi_level;
        [XmlElement("gsm_rssi_level")]
        public string Gsm_rssi_level
        {
            get { return _gsm_rssi_level; }
            set { _gsm_rssi_level = value; }
        }

        string _battery_level;
        [XmlElement("battery_level")]
        public string Battery_level
        {
            get { return _battery_level; }
            set { _battery_level = value; }
        }

        string _power_level;
        [XmlElement("power_level")]
        public string Power_level
        {
            get { return _power_level; }
            set { _power_level = value; }
        }

        string _sms_pa_command_id;
        [XmlElement("sms_pa_command_id")]
        public string Sms_pa_command_id
        {
            get { return _sms_pa_command_id; }
            set { _sms_pa_command_id = value; }
        }

        string _sms_pa_ack;
        [XmlElement("sms_pa_ack")]
        public string Sms_pa_ack
        {
            get { return _sms_pa_ack; }
            set { _sms_pa_ack = value; }
        }

        string _low_power_reason;
        [XmlElement("low_power_reason")]
        public string Low_power_reason
        {
            get { return _low_power_reason; }
            set { _low_power_reason = value; }
        }

        string _chanel;
        [XmlElement("Channel")]
        public string Chanel
        {
            get { return _chanel; }
            set { _chanel = value; }
        }

        string _gpio_type;
        [XmlElement("GPIO_Type")]
        public string Gpio_type
        {
            get { return _gpio_type; }
            set { _gpio_type = value; }
        }

        string _isdata_logging;
        [XmlElement("isdatalogging")]
        public string Isdata_logging
        {
            get { return _isdata_logging; }
            set { _isdata_logging = value; }
        }

        string _zone_lat;
        [XmlElement("zone_lat")]
        public string Zone_lat
        {
            get { return _zone_lat; }
            set { _zone_lat = value; }
        }

        string _zone_lon;
        [XmlElement("zone_lon")]
        public string Zone_lon
        {
            get { return _zone_lon; }
            set { _zone_lon = value; }
        }

        string _zone_fixed_status;
        [XmlElement("zone_fix_status")]
        public string Zone_fixed_status
        {
            get { return _zone_fixed_status; }
            set { _zone_fixed_status = value; }
        }

        string _zone_radius;
        [XmlElement("zone_radius")]
        public string Zone_radius
        {
            get { return _zone_radius; }
            set { _zone_radius = value; }
        }

        string _zone_status;
        [XmlElement("zone_status")]
        public string Zone_status
        {
            get { return _zone_status; }
            set { _zone_status = value; }
        }
    }

    [XmlRoot("GGateMessage")]
    public struct GGateMessage
    {
        string _utc_time;
        [XmlElement("utc_time")]
        public string utc_time
        {
            get { return _utc_time; }
            set { _utc_time = value; }
        }

        string _lat;
        [XmlElement("lat")]
        public string lat
        {
            get { return _lat; }
            set { _lat = value; }
        }

        string _lon;
        [XmlElement("lon")]
        public string lon
        {
            get { return _lon; }
            set { _lon = value; }
        }

        string _alt;
        [XmlElement("alt")]
        public string alt
        {
            get { return _alt; }
            set { _alt = value; }
        }

        string _speed;
        [XmlElement("speed")]
        public string speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        string _true_course;
        [XmlElement("true_course")]
        public string True_course
        {
            get { return _true_course; }
            set { _true_course = value; }
        }

        string _HDOP;
        [XmlElement("HDOP")]
        public string HDOP
        {
            get { return _HDOP; }
            set { _HDOP = value; }
        }

        string _status;
        [XmlElement("status")]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        string _mesg_id;
        [XmlElement("mesg_id")]
        public string Mesg_id
        {
            get { return _mesg_id; }
            set { _mesg_id = value; }
        }

        string _timestamp;
        [XmlElement("timestamp")]
        public string Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }

        string _veh_id;
        [XmlElement("veh_id")]
        public string Veh_id
        {
            get { return _veh_id; }
            set { _veh_id = value; }
        }

        string _idle_interval;
        [XmlElement("idle_interval")]
        public string Idle_interval
        {
            get { return _idle_interval; }
            set { _idle_interval = value; }
        }

        string _speed_limit;
        [XmlElement("speed_limit")]
        public string Speed_limit
        {
            get { return _speed_limit; }
            set { _speed_limit = value; }
        }

        string _speed_interval;
        [XmlElement("speed_interval")]
        public string Speed_interval
        {
            get { return _speed_interval; }
            set { _speed_interval = value; }
        }

        string _ref_lat;
        [XmlElement("ref_lat")]
        public string Ref_lat
        {
            get { return _ref_lat; }
            set { _ref_lat = value; }
        }

        string _ref_lon;
        [XmlElement("ref_lon")]
        public string Ref_lon
        {
            get { return _ref_lon; }
            set { _ref_lon = value; }
        }

        string _milage;
        [XmlElement("mileage")]
        public string Milage
        {
            get { return _milage; }
            set { _milage = value; }
        }

        string _port;
        [XmlElement("port")]
        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        string _port_value;
        [XmlElement("port_value")]
        public string Port_value
        {
            get { return _port_value; }
            set { _port_value = value; }
        }

        string _driver_id;
        [XmlElement("driver_id")]
        public string Driver_id
        {
            get { return _driver_id; }
            set { _driver_id = value; }
        }

        string _trip_id;
        [XmlElement("trip_id")]
        public string Trip_id
        {
            get { return _trip_id; }
            set { _trip_id = value; }
        }

        string _waypt_id;
        [XmlElement("waypt_id")]
        public string Waypt_id
        {
            get { return _waypt_id; }
            set { _waypt_id = value; }
        }

        string _zone_id;
        [XmlElement("zone_id")]
        public string Zone_id
        {
            get { return _zone_id; }
            set { _zone_id = value; }
        }

        string _passenger_count;
        [XmlElement("passenger_count")]
        public string Passenger_count
        {
            get { return _passenger_count; }
            set { _passenger_count = value; }
        }

        string _sent_msg_id;
        [XmlElement("sent_mesg_id")]
        public string Sent_msg_id
        {
            get { return _sent_msg_id; }
            set { _sent_msg_id = value; }
        }

        string _action_code;
        [XmlElement("action_code")]
        public string Action_code
        {
            get { return _action_code; }
            set { _action_code = value; }
        }

        string _cm_id;
        [XmlElement("cm_id")]
        public string Cm_id
        {
            get { return _cm_id; }
            set { _cm_id = value; }
        }

        string _job_assign_id;
        [XmlElement("job_assign_id")]
        public string Job_assign_id
        {
            get { return _job_assign_id; }
            set { _job_assign_id = value; }
        }

        string _actual_qty;
        [XmlElement("actual_qty")]
        public string Actual_qty
        {
            get { return _actual_qty; }
            set { _actual_qty = value; }
        }

        string _actual_vol;
        [XmlElement("actual_vol")]
        public string Actual_vol
        {
            get { return _actual_vol; }
            set { _actual_vol = value; }
        }

        string _actual_weight;
        [XmlElement("actual_weight")]
        public string Actual_weight
        {
            get { return _actual_weight; }
            set { _actual_weight = value; }
        }

        string _job_status_id;
        [XmlElement("job_status_id")]
        public string Job_status_id
        {
            get { return _job_status_id; }
            set { _job_status_id = value; }
        }

        string _sched_id;
        [XmlElement("sched_id")]
        public string Sched_id
        {
            get { return _sched_id; }
            set { _sched_id = value; }
        }

        string _job_assign_id_cnt;
        [XmlElement("job_assign_id_cnt")]
        public string Job_assign_id_cnt
        {
            get { return _job_assign_id_cnt; }
            set { _job_assign_id_cnt = value; }
        }

        string _rfid_tag_no;
        [XmlElement("rfid_tag_no")]
        public string Rfid_tag_no
        {
            get { return _rfid_tag_no; }
            set { _rfid_tag_no = value; }
        }

        string _avg_rssi;
        [XmlElement("avg_rssi")]
        public string Avg_rssi
        {
            get { return _avg_rssi; }
            set { _avg_rssi = value; }
        }

        string _min_rssi;
        [XmlElement("min_rssi")]
        public string Min_rssi
        {
            get { return _min_rssi; }
            set { _min_rssi = value; }
        }

        string _max_rssi;
        [XmlElement("max_rssi")]
        public string Max_rssi
        {
            get { return _max_rssi; }
            set { _max_rssi = value; }
        }

        string _satelite;
        [XmlElement("satelite")]
        public string Satelite
        {
            get { return _satelite; }
            set { _satelite = value; }
        }

        string _odometer_sensor;
        [XmlElement("odometer_sensor")]
        public string Odometer_sensor
        {
            get { return _odometer_sensor; }
            set { _odometer_sensor = value; }
        }

        string _gps_odometer;
        [XmlElement("gps_odometer")]
        public string Gps_odometer
        {
            get { return _gps_odometer; }
            set { _gps_odometer = value; }
        }

        string _gsm_cell_id;
        [XmlElement("gsm_cell_id")]
        public string Gsm_cell_id
        {
            get { return _gsm_cell_id; }
            set { _gsm_cell_id = value; }
        }

        string _gsm_location_id;
        [XmlElement("gsm_location_id")]
        public string Gsm_location_id
        {
            get { return _gsm_location_id; }
            set { _gsm_location_id = value; }
        }

        string _gsm_rssi_level;
        [XmlElement("gsm_rssi_level")]
        public string Gsm_rssi_level
        {
            get { return _gsm_rssi_level; }
            set { _gsm_rssi_level = value; }
        }

        string _battery_level;
        [XmlElement("battery_level")]
        public string Battery_level
        {
            get { return _battery_level; }
            set { _battery_level = value; }
        }

        string _power_level;
        [XmlElement("power_level")]
        public string Power_level
        {
            get { return _power_level; }
            set { _power_level = value; }
        }

        string _sms_pa_command_id;
        [XmlElement("sms_pa_command_id")]
        public string Sms_pa_command_id
        {
            get { return _sms_pa_command_id; }
            set { _sms_pa_command_id = value; }
        }

        string _sms_pa_ack;
        [XmlElement("sms_pa_ack")]
        public string Sms_pa_ack
        {
            get { return _sms_pa_ack; }
            set { _sms_pa_ack = value; }
        }

        string _low_power_reason;
        [XmlElement("low_power_reason")]
        public string Low_power_reason
        {
            get { return _low_power_reason; }
            set { _low_power_reason = value; }
        }

        string _chanel;
        [XmlElement("Channel")]
        public string Chanel
        {
            get { return _chanel; }
            set { _chanel = value; }
        }

        string _gpio_type;
        [XmlElement("GPIO_Type")]
        public string Gpio_type
        {
            get { return _gpio_type; }
            set { _gpio_type = value; }
        }

        string _isdata_logging;
        [XmlElement("isdatalogging")]
        public string Isdata_logging
        {
            get { return _isdata_logging; }
            set { _isdata_logging = value; }
        }

        string _zone_lat;
        [XmlElement("zone_lat")]
        public string Zone_lat
        {
            get { return _zone_lat; }
            set { _zone_lat = value; }
        }

        string _zone_lon;
        [XmlElement("zone_lon")]
        public string Zone_lon
        {
            get { return _zone_lon; }
            set { _zone_lon = value; }
        }

        string _zone_fixed_status;
        [XmlElement("zone_fix_status")]
        public string Zone_fixed_status
        {
            get { return _zone_fixed_status; }
            set { _zone_fixed_status = value; }
        }

        string _zone_radius;
        [XmlElement("zone_radius")]
        public string Zone_radius
        {
            get { return _zone_radius; }
            set { _zone_radius = value; }
        }

        string _zone_status;
        [XmlElement("zone_status")]
        public string Zone_status
        {
            get { return _zone_status; }
            set { _zone_status = value; }
        }
    }

    [XmlRoot("GGateMessage")]
    public struct GGateMessage6
    {
        string _utc_time;
        [XmlElement("utc_time")]
        public string utc_time
        {
            get { return _utc_time; }
            set { _utc_time = value; }
        }

        string _lat;
        [XmlElement("lat")]
        public string lat
        {
            get { return _lat; }
            set { _lat = value; }
        }

        string _lon;
        [XmlElement("lon")]
        public string lon
        {
            get { return _lon; }
            set { _lon = value; }
        }

        string _alt;
        [XmlElement("alt")]
        public string alt
        {
            get { return _alt; }
            set { _alt = value; }
        }

        string _speed;
        [XmlElement("speed")]
        public string speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        string _true_course;
        [XmlElement("true_course")]
        public string True_course
        {
            get { return _true_course; }
            set { _true_course = value; }
        }

        string _HDOP;
        [XmlElement("HDOP")]
        public string HDOP
        {
            get { return _HDOP; }
            set { _HDOP = value; }
        }

        string _status;
        [XmlElement("status")]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        string _mesg_id;
        [XmlElement("mesg_id")]
        public string Mesg_id
        {
            get { return _mesg_id; }
            set { _mesg_id = value; }
        }

        string _timestamp;
        [XmlElement("timestamp")]
        public string Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }

        string _veh_id;
        [XmlElement("veh_id")]
        public string Veh_id
        {
            get { return _veh_id; }
            set { _veh_id = value; }
        }

        string _idle_interval;
        [XmlElement("idle_interval")]
        public string Idle_interval
        {
            get { return _idle_interval; }
            set { _idle_interval = value; }
        }

        string _speed_limit;
        [XmlElement("speed_limit")]
        public string Speed_limit
        {
            get { return _speed_limit; }
            set { _speed_limit = value; }
        }

        string _speed_interval;
        [XmlElement("speed_interval")]
        public string Speed_interval
        {
            get { return _speed_interval; }
            set { _speed_interval = value; }
        }

        string _ref_lat;
        [XmlElement("ref_lat")]
        public string Ref_lat
        {
            get { return _ref_lat; }
            set { _ref_lat = value; }
        }

        string _ref_lon;
        [XmlElement("ref_lon")]
        public string Ref_lon
        {
            get { return _ref_lon; }
            set { _ref_lon = value; }
        }

        string _milage;
        [XmlElement("mileage")]
        public string Milage
        {
            get { return _milage; }
            set { _milage = value; }
        }

        string _port;
        [XmlElement("port")]
        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        string _port_value;
        [XmlElement("port_value")]
        public string Port_value
        {
            get { return _port_value; }
            set { _port_value = value; }
        }

        string _driver_id;
        [XmlElement("driver_id")]
        public string Driver_id
        {
            get { return _driver_id; }
            set { _driver_id = value; }
        }

        string _trip_id;
        [XmlElement("trip_id")]
        public string Trip_id
        {
            get { return _trip_id; }
            set { _trip_id = value; }
        }

        string _waypt_id;
        [XmlElement("waypt_id")]
        public string Waypt_id
        {
            get { return _waypt_id; }
            set { _waypt_id = value; }
        }

        string _zone_id;
        [XmlElement("zone_id")]
        public string Zone_id
        {
            get { return _zone_id; }
            set { _zone_id = value; }
        }

        string _passenger_count;
        [XmlElement("passenger_count")]
        public string Passenger_count
        {
            get { return _passenger_count; }
            set { _passenger_count = value; }
        }

        string _sent_msg_id;
        [XmlElement("sent_mesg_id")]
        public string Sent_msg_id
        {
            get { return _sent_msg_id; }
            set { _sent_msg_id = value; }
        }

        string _action_code;
        [XmlElement("action_code")]
        public string Action_code
        {
            get { return _action_code; }
            set { _action_code = value; }
        }

        string _cm_id;
        [XmlElement("cm_id")]
        public string Cm_id
        {
            get { return _cm_id; }
            set { _cm_id = value; }
        }

        string _job_assign_id;
        [XmlElement("job_assign_id")]
        public string Job_assign_id
        {
            get { return _job_assign_id; }
            set { _job_assign_id = value; }
        }

        string _actual_qty;
        [XmlElement("actual_qty")]
        public string Actual_qty
        {
            get { return _actual_qty; }
            set { _actual_qty = value; }
        }

        string _actual_vol;
        [XmlElement("actual_vol")]
        public string Actual_vol
        {
            get { return _actual_vol; }
            set { _actual_vol = value; }
        }

        string _actual_weight;
        [XmlElement("actual_weight")]
        public string Actual_weight
        {
            get { return _actual_weight; }
            set { _actual_weight = value; }
        }

        string _job_status_id;
        [XmlElement("job_status_id")]
        public string Job_status_id
        {
            get { return _job_status_id; }
            set { _job_status_id = value; }
        }

        string _sched_id;
        [XmlElement("sched_id")]
        public string Sched_id
        {
            get { return _sched_id; }
            set { _sched_id = value; }
        }

        string _job_assign_id_cnt;
        [XmlElement("job_assign_id_cnt")]
        public string Job_assign_id_cnt
        {
            get { return _job_assign_id_cnt; }
            set { _job_assign_id_cnt = value; }
        }

        string _rfid_tag_no;
        [XmlElement("rfid_tag_no")]
        public string Rfid_tag_no
        {
            get { return _rfid_tag_no; }
            set { _rfid_tag_no = value; }
        }

        string _avg_rssi;
        [XmlElement("avg_rssi")]
        public string Avg_rssi
        {
            get { return _avg_rssi; }
            set { _avg_rssi = value; }
        }

        string _min_rssi;
        [XmlElement("min_rssi")]
        public string Min_rssi
        {
            get { return _min_rssi; }
            set { _min_rssi = value; }
        }

        string _max_rssi;
        [XmlElement("max_rssi")]
        public string Max_rssi
        {
            get { return _max_rssi; }
            set { _max_rssi = value; }
        }

        string _satelite;
        [XmlElement("satelite")]
        public string Satelite
        {
            get { return _satelite; }
            set { _satelite = value; }
        }

        string _odometer_sensor;
        [XmlElement("odometer_sensor")]
        public string Odometer_sensor
        {
            get { return _odometer_sensor; }
            set { _odometer_sensor = value; }
        }

        string _gps_odometer;
        [XmlElement("gps_odometer")]
        public string Gps_odometer
        {
            get { return _gps_odometer; }
            set { _gps_odometer = value; }
        }

        string _gsm_cell_id;
        [XmlElement("gsm_cell_id")]
        public string Gsm_cell_id
        {
            get { return _gsm_cell_id; }
            set { _gsm_cell_id = value; }
        }

        string _gsm_location_id;
        [XmlElement("gsm_location_id")]
        public string Gsm_location_id
        {
            get { return _gsm_location_id; }
            set { _gsm_location_id = value; }
        }

        string _gsm_rssi_level;
        [XmlElement("gsm_rssi_level")]
        public string Gsm_rssi_level
        {
            get { return _gsm_rssi_level; }
            set { _gsm_rssi_level = value; }
        }

        string _battery_level;
        [XmlElement("battery_level")]
        public string Battery_level
        {
            get { return _battery_level; }
            set { _battery_level = value; }
        }

        string _power_level;
        [XmlElement("power_level")]
        public string Power_level
        {
            get { return _power_level; }
            set { _power_level = value; }
        }

        string _sms_pa_command_id;
        [XmlElement("sms_pa_command_id")]
        public string Sms_pa_command_id
        {
            get { return _sms_pa_command_id; }
            set { _sms_pa_command_id = value; }
        }

        string _sms_pa_ack;
        [XmlElement("sms_pa_ack")]
        public string Sms_pa_ack
        {
            get { return _sms_pa_ack; }
            set { _sms_pa_ack = value; }
        }

        string _low_power_reason;
        [XmlElement("low_power_reason")]
        public string Low_power_reason
        {
            get { return _low_power_reason; }
            set { _low_power_reason = value; }
        }

        string _chanel;
        [XmlElement("Channel")]
        public string Chanel
        {
            get { return _chanel; }
            set { _chanel = value; }
        }

        string _gpio_type;
        [XmlElement("GPIO_Type")]
        public string Gpio_type
        {
            get { return _gpio_type; }
            set { _gpio_type = value; }
        }

        string _isdata_logging;
        [XmlElement("isdatalogging")]
        public string Isdata_logging
        {
            get { return _isdata_logging; }
            set { _isdata_logging = value; }
        }

        string _zone_lat;
        [XmlElement("zone_lat")]
        public string Zone_lat
        {
            get { return _zone_lat; }
            set { _zone_lat = value; }
        }

        string _zone_lon;
        [XmlElement("zone_lon")]
        public string Zone_lon
        {
            get { return _zone_lon; }
            set { _zone_lon = value; }
        }

        string _zone_fixed_status;
        [XmlElement("zone_fix_status")]
        public string Zone_fixed_status
        {
            get { return _zone_fixed_status; }
            set { _zone_fixed_status = value; }
        }

        string _zone_radius;
        [XmlElement("zone_radius")]
        public string Zone_radius
        {
            get { return _zone_radius; }
            set { _zone_radius = value; }
        }

        string _zone_status;
        [XmlElement("zone_status")]
        public string Zone_status
        {
            get { return _zone_status; }
            set { _zone_status = value; }
        }
    }

    [XmlRoot("GGateMessage")]
    public struct GGateMessage7
    {
        string _utc_time;// 1
        [XmlElement("utc_time")]
        public string utc_time
        {
            get { return _utc_time; }
            set { _utc_time = value; }
        }

        string _lat;// 2
        [XmlElement("lat")]
        public string lat
        {
            get { return _lat; }
            set { _lat = value; }
        }

        string _lon;// 3
        [XmlElement("lon")]
        public string lon
        {
            get { return _lon; }
            set { _lon = value; }
        }

        string _alt; //4
        [XmlElement("alt")]
        public string alt
        {
            get { return _alt; }
            set { _alt = value; }
        }

        string _speed; //5
        [XmlElement("speed")]
        public string speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        string _true_course; //6
        [XmlElement("true_course")]
        public string True_course
        {
            get { return _true_course; }
            set { _true_course = value; }
        }

        string _HDOP; //7
        [XmlElement("HDOP")]
        public string HDOP
        {
            get { return _HDOP; }
            set { _HDOP = value; }
        }

        string _status; //8
        [XmlElement("status")]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        string _mesg_id; //9
        [XmlElement("mesg_id")]
        public string Mesg_id
        {
            get { return _mesg_id; }
            set { _mesg_id = value; }
        }

        string _timestamp; //10
        [XmlElement("timestamp")]
        public string Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }

        string _veh_id; //11
        [XmlElement("veh_id")]
        public string Veh_id
        {
            get { return _veh_id; }
            set { _veh_id = value; }
        }

        string _idle_interval;  //12
        [XmlElement("idle_interval")]
        public string Idle_interval
        {
            get { return _idle_interval; }
            set { _idle_interval = value; }
        }

        string _speed_limit; //13
        [XmlElement("speed_limit")]
        public string Speed_limit
        {
            get { return _speed_limit; }
            set { _speed_limit = value; }
        }

        string _speed_interval; //14
        [XmlElement("speed_interval")]
        public string Speed_interval
        {
            get { return _speed_interval; }
            set { _speed_interval = value; }
        }

        string _ref_lat; //15
        [XmlElement("ref_lat")]
        public string Ref_lat
        {
            get { return _ref_lat; }
            set { _ref_lat = value; }
        }

        string _ref_lon; //16
        [XmlElement("ref_lon")]
        public string Ref_lon
        {
            get { return _ref_lon; }
            set { _ref_lon = value; }
        }

        string _milage; //17
        [XmlElement("mileage")]
        public string Milage
        {
            get { return _milage; }
            set { _milage = value; }
        }

        string _port; //18
        [XmlElement("port")]
        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        string _port_value; //19
        [XmlElement("port_value")]
        public string Port_value
        {
            get { return _port_value; }
            set { _port_value = value; }
        }

        string _driver_id; //20
        [XmlElement("driver_id")]
        public string Driver_id
        {
            get { return _driver_id; }
            set { _driver_id = value; }
        }

        string _trip_id; //21
        [XmlElement("trip_id")]
        public string Trip_id
        {
            get { return _trip_id; }
            set { _trip_id = value; }
        }

        string _waypt_id; //22
        [XmlElement("waypt_id")]
        public string Waypt_id
        {
            get { return _waypt_id; }
            set { _waypt_id = value; }
        }

        string _zone_id; //23
        [XmlElement("zone_id")]
        public string Zone_id
        {
            get { return _zone_id; }
            set { _zone_id = value; }
        }

        string _passenger_count; //24
        [XmlElement("passenger_count")]
        public string Passenger_count
        {
            get { return _passenger_count; }
            set { _passenger_count = value; }
        }

        string _sent_msg_id; //25
        [XmlElement("sent_mesg_id")]
        public string Sent_msg_id
        {
            get { return _sent_msg_id; }
            set { _sent_msg_id = value; }
        }

        string _action_code; //26
        [XmlElement("action_code")]
        public string Action_code
        {
            get { return _action_code; }
            set { _action_code = value; }
        }

        string _cm_id; //27
        [XmlElement("cm_id")]
        public string Cm_id
        {
            get { return _cm_id; }
            set { _cm_id = value; }
        }

        string _job_assign_id; //28
        [XmlElement("job_assign_id")]
        public string Job_assign_id
        {
            get { return _job_assign_id; }
            set { _job_assign_id = value; }
        }

        string _actual_qty; //29
        [XmlElement("actual_qty")]
        public string Actual_qty
        {
            get { return _actual_qty; }
            set { _actual_qty = value; }
        }

        string _actual_vol; //30
        [XmlElement("actual_vol")]
        public string Actual_vol
        {
            get { return _actual_vol; }
            set { _actual_vol = value; }
        }

        string _actual_weight; //31
        [XmlElement("actual_weight")]
        public string Actual_weight
        {
            get { return _actual_weight; }
            set { _actual_weight = value; }
        }

        string _job_status_id; //32
        [XmlElement("job_status_id")]
        public string Job_status_id
        {
            get { return _job_status_id; }
            set { _job_status_id = value; }
        }

        string _sched_id; //33
        [XmlElement("sched_id")]
        public string Sched_id
        {
            get { return _sched_id; }
            set { _sched_id = value; }
        }

        string _job_assign_id_cnt; //34
        [XmlElement("job_assign_id_cnt")]
        public string Job_assign_id_cnt
        {
            get { return _job_assign_id_cnt; }
            set { _job_assign_id_cnt = value; }
        }

        string _rfid_tag_no; //35
        [XmlElement("rfid_tag_no")]
        public string Rfid_tag_no
        {
            get { return _rfid_tag_no; }
            set { _rfid_tag_no = value; }
        }

        string _avg_rssi; //36
        [XmlElement("avg_rssi")]
        public string Avg_rssi
        {
            get { return _avg_rssi; }
            set { _avg_rssi = value; }
        }

        string _min_rssi; //37
        [XmlElement("min_rssi")]
        public string Min_rssi
        {
            get { return _min_rssi; }
            set { _min_rssi = value; }
        }

        string _max_rssi; //38
        [XmlElement("max_rssi")]
        public string Max_rssi
        {
            get { return _max_rssi; }
            set { _max_rssi = value; }
        }

        string _temperature; //39
        [XmlElement("temperature")]
        public string Temperature
        {
            get { return _temperature; }
            set { _temperature = value; }
        }

        string _huminity; //40
        [XmlElement("huminity")]
        public string Huminity
        {
            get { return _huminity; }
            set { _huminity = value; }
        }

        string _seal_status; //41
        [XmlElement("seal_status")]
        public string Seal_status
        {
            get { return _seal_status; }
            set { _seal_status = value; }
        }

        string _age; //42
        [XmlElement("age")]
        public string Age
        {
            get { return _age; }
            set { _age = value; }
        }

        string _timer; //43
        [XmlElement("timer")]
        public string Timer
        {
            get { return _timer; }
            set { _timer = value; }
        }

        string _satelite; //44
        [XmlElement("satelite")]
        public string Satelite
        {
            get { return _satelite; }
            set { _satelite = value; }
        }

        string _odometer_sensor; //45
        [XmlElement("odometer_sensor")]
        public string Odometer_sensor
        {
            get { return _odometer_sensor; }
            set { _odometer_sensor = value; }
        }

        string _gps_odometer; //46
        [XmlElement("gps_odometer")]
        public string Gps_odometer
        {
            get { return _gps_odometer; }
            set { _gps_odometer = value; }
        }

        string _gsm_cell_id; //47
        [XmlElement("gsm_cell_id")]
        public string Gsm_cell_id
        {
            get { return _gsm_cell_id; }
            set { _gsm_cell_id = value; }
        }

        string _gsm_location_id; //48
        [XmlElement("gsm_location_id")]
        public string Gsm_location_id
        {
            get { return _gsm_location_id; }
            set { _gsm_location_id = value; }
        }

        string _gsm_rssi_level; //49
        [XmlElement("gsm_rssi_level")]
        public string Gsm_rssi_level
        {
            get { return _gsm_rssi_level; }
            set { _gsm_rssi_level = value; }
        }

        string _battery_level; //50
        [XmlElement("battery_level")]
        public string Battery_level
        {
            get { return _battery_level; }
            set { _battery_level = value; }
        }

        string _power_level; //51
        [XmlElement("power_level")]
        public string Power_level
        {
            get { return _power_level; }
            set { _power_level = value; }
        }

        string _sms_pa_command_id; //52
        [XmlElement("sms_pa_command_id")]
        public string Sms_pa_command_id
        {
            get { return _sms_pa_command_id; }
            set { _sms_pa_command_id = value; }
        }

        string _sms_pa_ack; //53
        [XmlElement("sms_pa_ack")]
        public string Sms_pa_ack
        {
            get { return _sms_pa_ack; }
            set { _sms_pa_ack = value; }
        }

        string _low_power_reason; //54
        [XmlElement("low_power_reason")]
        public string Low_power_reason
        {
            get { return _low_power_reason; }
            set { _low_power_reason = value; }
        }

        string _chanel; //55
        [XmlElement("Channel")]
        public string Chanel
        {
            get { return _chanel; }
            set { _chanel = value; }
        }

        string _gpio_type; //56
        [XmlElement("GPIO_Type")]
        public string Gpio_type
        {
            get { return _gpio_type; }
            set { _gpio_type = value; }
        }

        string _isdata_logging; //57
        [XmlElement("isdatalogging")]
        public string Isdata_logging
        {
            get { return _isdata_logging; }
            set { _isdata_logging = value; }
        }

        string _zone_lat; //58
        [XmlElement("zone_lat")]
        public string Zone_lat
        {
            get { return _zone_lat; }
            set { _zone_lat = value; }
        }

        string _zone_lon; //59
        [XmlElement("zone_lon")]
        public string Zone_lon
        {
            get { return _zone_lon; }
            set { _zone_lon = value; }
        }

        string _zone_fixed_status; //60
        [XmlElement("zone_fix_status")]
        public string Zone_fixed_status
        {
            get { return _zone_fixed_status; }
            set { _zone_fixed_status = value; }
        }

        string _zone_radius; //61
        [XmlElement("zone_radius")]
        public string Zone_radius
        {
            get { return _zone_radius; }
            set { _zone_radius = value; }
        }

        string _zone_status; //62
        [XmlElement("zone_status")]
        public string Zone_status
        {
            get { return _zone_status; }
            set { _zone_status = value; }
        }

        string _datalog_reason; //63
        [XmlElement("datalog_reason")]
        public string Datalog_reason
        {
            get { return _datalog_reason; }
            set { _datalog_reason = value; }
        }

        string _event_type; //64
        [XmlElement("event_type")]
        public string Event_type
        {
            get { return _event_type; }
            set { _event_type = value; }
        }

        string _event_type_status; //65
        [XmlElement("event_type_status")]
        public string Event_type_status
        {
            get { return _event_type_status; }
            set { _event_type_status = value; }
        }

        string _fuel_gauge; //66
        [XmlElement("fuel_gauge")]
        public string Fuel_gauge
        {
            get { return _fuel_gauge; }
            set { _fuel_gauge = value; }
        }

        string _RecvLength; //67
        [XmlElement("RecvLength")]
        public string RecvLength
        {
            get { return _RecvLength; }
            set { _RecvLength = value; }
        }

        string _raw_define_gpio; //68
        [XmlElement("raw_define_gpio")]
        public string Raw_define_gpio
        {
            get { return _raw_define_gpio; }
            set { _raw_define_gpio = value; }
        }

        string _Antena_Power; //69
        [XmlElement("Antena_Power")]
        public string Antena_Power
        {
            get { return _Antena_Power; }
            set { _Antena_Power = value; }
        }

        string _Antena_Status; //70
        [XmlElement("Antena_Status")]
        public string Antena_Status
        {
            get { return _Antena_Status; }
            set { _Antena_Status = value; }
        }

        string _type; //71
        [XmlElement("type")]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        string _dt_utc; //72
        [XmlElement("dt_utc")]
        public string Dt_utc
        {
            get { return _dt_utc; }
            set { _dt_utc = value; }
        }

        string _dt_local; //73
        [XmlElement("dt_local")]
        public string Dt_local
        {
            get { return _dt_local; }
            set { _dt_local = value; }
        }

        string _Total; //74
        [XmlElement("Total")]
        public string Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }

    [XmlRoot("Max2FmtMsg")]
    public struct Max2FmtMsgForward
    {
        string _vid;// 1
        [XmlElement("vid")]
        public string vid
        {
            get { return _vid; }
            set { _vid = value; }
        }

        string _gps_date_time;// 2
        [XmlElement("gps_date_time")]
        public string gps_date_time
        {
            get { return _gps_date_time; }
            set { _gps_date_time = value; }
        }

        string _utc;// 3
        [XmlElement("utc")]
        public string utc
        {
            get { return _utc; }
            set { _utc = value; }
        }

        string _lat;// 4
        [XmlElement("lat")]
        public string lat
        {
            get { return _lat; }
            set { _lat = value; }
        }

        string _lon;// 5
        [XmlElement("lon")]
        public string lon
        {
            get { return _lon; }
            set { _lon = value; }
        }

        string _speed;// 6
        [XmlElement("speed")]
        public string speed
        {
            get { return _speed; }
            set { _speed = value; }
        }
        string _course;// 7
        [XmlElement("course")]
        public string course
        {
            get { return _course; }
            set { _course = value; }
        }
        string _type_of_fix;// 8
        [XmlElement("type_of_fix")]
        public string type_of_fix
        {
            get { return _type_of_fix; }
            set { _type_of_fix = value; }
        }
        string _no_of_satellite;// 9
        [XmlElement("no_of_satellite")]
        public string no_of_satellite
        {
            get { return _no_of_satellite; }
            set { _no_of_satellite = value; }
        }
        string _gpio_status;// 10
        [XmlElement("gpio_status")]
        public string gpio_status
        {
            get { return _gpio_status; }
            set { _gpio_status = value; }
        }
        string _analog_level;// 11
        [XmlElement("analog_level")]
        public string analog_level
        {
            get { return _analog_level; }
            set { _analog_level = value; }
        }
        string _type_of_message;// 12
        [XmlElement("type_of_message")]
        public string type_of_message
        {
            get { return _type_of_message; }
            set { _type_of_message = value; }
        }
        string _tag;// 13
        [XmlElement("tag")]
        public string tag
        {
            get { return _tag; }
            set { _tag = value; }
        }
    }

    public class GBoxQueue : CommonQueueStream
    {
        public GBoxQueue()
            : base()
        {
        }

        public GBoxQueue(String QPathStr)
            : base(QPathStr)
        {
        }

        public void PushResetQ
                    (
                    String product_name, String major_vesion, String minor_version, String release_date, String password,
                    String utc_time, String lat, String lon, String alt, String speed,
                    String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
                    String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
                    String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
                    String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
                    String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
                    String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
                    String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
                    String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
                    String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
                    String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            XmlMSG xmlMsg = new XmlMSG();
            String xmlStr = xmlMsg.WriteResetString
                    (
                     product_name, major_vesion, minor_version, release_date, password,
                     utc_time, lat, lon, alt, speed,
                     true_course, HDOP, status, mesg_id, timestamp, veh_id,
                     idle_interval, speed_limit, speed_interval, ref_lat, ref_lon,
                     milage, port, port_value, driver_id, trip_id, waypt_id,
                     zone_id, passenger_count, sent_msg_id, action_code, cm_id,
                     job_assign_id, actual_qty, actual_vol, actual_weight, job_status_id,
                     sched_id, job_assign_id_cnt, rfid_tag_no, agv_rssi, min_rssi,
                     max_rssi, satelite, odometer_sensor, gps_odometer, gsm_cell_id,
                     gsm_location_id, gsm_rssi_level, battery_level, power_level, sms_pa_command_id,
                     sms_pa_ack, low_power_reason, chanel, gpio_type, isdata_logging,
                     zone_lat, zone_lon, zone_fixed_status, zone_radius, zone_status);

            byte[] buff = Encoding.UTF8.GetBytes(xmlStr);
            Stream stm = new MemoryStream(buff);

            Message msg = new Message();
            msg.BodyStream = stm;
            MQ.Send(msg);

            msg.Dispose();
            stm.Close();
            stm.Dispose();
        }

        public void PushLogQ
                    (
                    String utc_time, String lat, String lon, String alt, String speed,
                    String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
                    String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
                    String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
                    String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
                    String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
                    String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
                    String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
                    String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
                    String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
                    String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            XmlMSG xmlMsg = new XmlMSG();
            String xmlStr = xmlMsg.WriteLogString
                    (
                     utc_time, lat, lon, alt, speed,
                     true_course, HDOP, status, mesg_id, timestamp, veh_id,
                     idle_interval, speed_limit, speed_interval, ref_lat, ref_lon,
                     milage, port, port_value, driver_id, trip_id, waypt_id,
                     zone_id, passenger_count, sent_msg_id, action_code, cm_id,
                     job_assign_id, actual_qty, actual_vol, actual_weight, job_status_id,
                     sched_id, job_assign_id_cnt, rfid_tag_no, agv_rssi, min_rssi,
                     max_rssi, satelite, odometer_sensor, gps_odometer, gsm_cell_id,
                     gsm_location_id, gsm_rssi_level, battery_level, power_level, sms_pa_command_id,
                     sms_pa_ack, low_power_reason, chanel, gpio_type, isdata_logging,
                     zone_lat, zone_lon, zone_fixed_status, zone_radius, zone_status);

            byte[] buff = Encoding.UTF8.GetBytes(xmlStr);
            Stream stm = new MemoryStream(buff);

            Message msg = new Message();
            msg.BodyStream = stm;
            MQ.Send(msg);

            msg.Dispose();
            stm.Close();
            stm.Dispose();
        }

        public void PushLogQ6
                    (
                    String utc_time, String lat, String lon, String alt, String speed,
                    String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
                    String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
                    String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
                    String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
                    String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
                    String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
                    String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
                    String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
                    String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
                    String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            XmlMSG xmlMsg = new XmlMSG();
            String xmlStr = xmlMsg.WriteLogString6
                    (
                     utc_time, lat, lon, alt, speed,
                     true_course, HDOP, status, mesg_id, timestamp, veh_id,
                     idle_interval, speed_limit, speed_interval, ref_lat, ref_lon,
                     milage, port, port_value, driver_id, trip_id, waypt_id,
                     zone_id, passenger_count, sent_msg_id, action_code, cm_id,
                     job_assign_id, actual_qty, actual_vol, actual_weight, job_status_id,
                     sched_id, job_assign_id_cnt, rfid_tag_no, agv_rssi, min_rssi,
                     max_rssi, satelite, odometer_sensor, gps_odometer, gsm_cell_id,
                     gsm_location_id, gsm_rssi_level, battery_level, power_level, sms_pa_command_id,
                     sms_pa_ack, low_power_reason, chanel, gpio_type, isdata_logging,
                     zone_lat, zone_lon, zone_fixed_status, zone_radius, zone_status);

            byte[] buff = Encoding.UTF8.GetBytes(xmlStr);
            Stream stm = new MemoryStream(buff);

            Message msg = new Message();
            msg.BodyStream = stm;
            MQ.Send(msg);

            msg.Dispose();
            stm.Close();
            stm.Dispose();
        }

        public void PushLogQ7
                    (
                    String utc_time, String lat, String lon, String alt, String speed,
                    String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
                    String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
                    String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
                    String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
                    String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
                    String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
                    String max_rssi,
                    String temperature, String huminity, String seal_status, String age, String timer,
                    String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
                    String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
                    String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
                    String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status,
                    String datalog_reason, String event_type, String event_type_status, String fuel_gauge, String RecvLength,
                    String raw_define_gpio, String Antena_Power, String Antena_Status, String type, String dt_utc, String dt_local, String Total)
        {
            XmlMSG xmlMsg = new XmlMSG();
            String xmlStr = xmlMsg.WriteLogString7
                    (
                     utc_time, lat, lon, alt, speed,
                     true_course, HDOP, status, mesg_id, timestamp, veh_id,
                     idle_interval, speed_limit, speed_interval, ref_lat, ref_lon,
                     milage, port, port_value, driver_id, trip_id, waypt_id,
                     zone_id, passenger_count, sent_msg_id, action_code, cm_id,
                     job_assign_id, actual_qty, actual_vol, actual_weight, job_status_id,
                     sched_id, job_assign_id_cnt, rfid_tag_no, agv_rssi, min_rssi,
                     max_rssi,
                     temperature, huminity, seal_status, age, timer,
                     satelite, odometer_sensor, gps_odometer, gsm_cell_id,
                     gsm_location_id, gsm_rssi_level, battery_level, power_level, sms_pa_command_id,
                     sms_pa_ack, low_power_reason, chanel, gpio_type, isdata_logging,
                     zone_lat, zone_lon, zone_fixed_status, zone_radius, zone_status,
                     datalog_reason, event_type, event_type_status, fuel_gauge, RecvLength,
                     raw_define_gpio, Antena_Power, Antena_Status, type, dt_utc, dt_local, Total);

            byte[] buff = Encoding.UTF8.GetBytes(xmlStr);
            Stream stm = new MemoryStream(buff);

            Message msg = new Message();
            msg.BodyStream = stm;
            MQ.Send(msg);

            msg.Dispose();
            stm.Dispose();
            stm.Close();
        }
    }

    public class XmlMSG
    {
        public XmlMSG()
        {
        }

        public String WriteResetString
            (
            String product_name, String major_version, String minor_version, String release_date, String password,
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            try
            {

                GGateResetMessage msg = new GGateResetMessage();

                msg.Product_name = product_name;
                msg.Major_version = major_version;
                msg.Minor_version = minor_version;
                msg.Release_date = release_date;
                msg.Password = password;
                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateResetMessage));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }

        public String WriteLogString
            (
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            try
            {

                GGateMessage msg = new GGateMessage();

                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateMessage));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }

        public String WriteLogString6
            (
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            try
            {

                GGateMessage msg = new GGateMessage();

                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateMessage));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }

        public String WriteLogString7
            (
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi,
            String temperature, String huminity, String seal_status, String age, String timer,
            String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status,
            String datalog_reason, String event_type, String event_type_status, String fuel_gauge, String RecvLength,
            String raw_define_gpio, String Antena_Power, String Antena_Status, String type, String dt_utc, String dt_local, String Total)
        {
            try
            {

                GGateMessage7 msg = new GGateMessage7();

                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Temperature = temperature;
                msg.Huminity = huminity;
                msg.Seal_status = seal_status;
                msg.Age = age;
                msg.Timer = timer;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;
                msg.Datalog_reason = datalog_reason;
                msg.Event_type = event_type;
                msg.Event_type_status = event_type_status;
                msg.Fuel_gauge = fuel_gauge;
                msg.RecvLength = RecvLength;
                msg.Raw_define_gpio = raw_define_gpio;
                msg.Antena_Power = Antena_Power;
                msg.Antena_Status = Antena_Status;
                msg.Type = type;
                msg.Dt_utc = dt_utc;
                msg.Dt_local = dt_local;
                msg.Total = Total;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateMessage7));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }
    }

    public class XmlMSG6
    {
        public XmlMSG6()
        {
        }

        public String WriteResetString
            (
            String product_name, String major_version, String minor_version, String release_date, String password,
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            try
            {

                GGateResetMessage msg = new GGateResetMessage();

                msg.Product_name = product_name;
                msg.Major_version = major_version;
                msg.Minor_version = minor_version;
                msg.Release_date = release_date;
                msg.Password = password;
                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateResetMessage));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }

        public String WriteLogString
            (
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            try
            {

                GGateMessage msg = new GGateMessage();

                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateMessage));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }
    }

    public class XmlMSG7
    {
        public XmlMSG7()
        {
        }

        public String WriteResetString
            (
            String product_name, String major_version, String minor_version, String release_date, String password,
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            try
            {

                GGateResetMessage msg = new GGateResetMessage();

                msg.Product_name = product_name;
                msg.Major_version = major_version;
                msg.Minor_version = minor_version;
                msg.Release_date = release_date;
                msg.Password = password;
                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateResetMessage));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }

        public String WriteLogString
            (
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status)
        {
            try
            {

                GGateMessage msg = new GGateMessage();

                msg.utc_time = utc_time;
                msg.lat = lat;
                msg.lon = lon;
                msg.alt = alt;
                msg.speed = speed;
                msg.True_course = true_course;
                msg.HDOP = HDOP;
                msg.Status = status;
                msg.Mesg_id = mesg_id;
                msg.Timestamp = timestamp;
                msg.Veh_id = veh_id;
                msg.Idle_interval = idle_interval;
                msg.Speed_limit = speed_limit;
                msg.Speed_interval = speed_interval;
                msg.Ref_lat = ref_lat;
                msg.Ref_lon = ref_lon;
                msg.Milage = milage;
                msg.Port = port;
                msg.Port_value = port_value;
                msg.Driver_id = driver_id;
                msg.Trip_id = trip_id;
                msg.Waypt_id = waypt_id;
                msg.Zone_id = zone_id;
                msg.Passenger_count = passenger_count;
                msg.Sent_msg_id = sent_msg_id;
                msg.Action_code = action_code;
                msg.Cm_id = cm_id;
                msg.Job_assign_id = job_assign_id;
                msg.Actual_qty = actual_qty;
                msg.Actual_vol = actual_vol;
                msg.Actual_weight = actual_weight;
                msg.Job_status_id = job_status_id;
                msg.Sched_id = sched_id;
                msg.Job_assign_id_cnt = job_assign_id_cnt;
                msg.Rfid_tag_no = rfid_tag_no;
                msg.Avg_rssi = agv_rssi;
                msg.Min_rssi = min_rssi;
                msg.Max_rssi = max_rssi;
                msg.Satelite = satelite;
                msg.Odometer_sensor = odometer_sensor;
                msg.Gps_odometer = gps_odometer;
                msg.Gsm_cell_id = gsm_cell_id;
                msg.Gsm_location_id = gsm_location_id;
                msg.Gsm_rssi_level = gsm_rssi_level;
                msg.Battery_level = battery_level;
                msg.Power_level = power_level;
                msg.Sms_pa_command_id = sms_pa_command_id;
                msg.Sms_pa_ack = sms_pa_ack;
                msg.Low_power_reason = low_power_reason;
                msg.Chanel = chanel;
                msg.Gpio_type = gpio_type;
                msg.Isdata_logging = isdata_logging;
                msg.Zone_lat = zone_lat;
                msg.Zone_lon = zone_lon;
                msg.Zone_fixed_status = zone_fixed_status;
                msg.Zone_radius = zone_radius;
                msg.Zone_status = zone_status;

                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(GGateMessage));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }
    }

    public class XmlMSG8
    {
        public XmlMSG8()
        {
        }

        public String WriteMax2MsgString
            (
            String product_name, String major_version, String minor_version, String release_date, String password,
            String utc_time, String lat, String lon, String alt, String speed,
            String true_course, String HDOP, String status, String mesg_id, String timestamp, String veh_id,
            String idle_interval, String speed_limit, String speed_interval, String ref_lat, String ref_lon,
            String milage, String port, String port_value, String driver_id, String trip_id, String waypt_id,
            String zone_id, String passenger_count, String sent_msg_id, String action_code, String cm_id,
            String job_assign_id, String actual_qty, String actual_vol, String actual_weight, String job_status_id,
            String sched_id, String job_assign_id_cnt, String rfid_tag_no, String agv_rssi, String min_rssi,
            String max_rssi, String satelite, String odometer_sensor, String gps_odometer, String gsm_cell_id,
            String gsm_location_id, String gsm_rssi_level, String battery_level, String power_level, String sms_pa_command_id,
            String sms_pa_ack, String low_power_reason, String chanel, String gpio_type, String isdata_logging,
            String zone_lat, String zone_lon, String zone_fixed_status, String zone_radius, String zone_status
            )
        {
            try
            {

                Max2FmtMsgForward msg = new Max2FmtMsgForward();



                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(typeof(Max2FmtMsgForward));
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, msg);
                txtWr.Close();
                return txtWr.ToString();
            }
            catch
            {
                return null;
            }
        }
    }
    
    #endregion
}
