using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace ZebraDataCentre
{
    [XmlRoot("MESSAGE")]
    public struct MESSAGE
    {
        string _id;
        [XmlAttribute("ID")]
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        #region for using in another format, comment only, don't delete
        /*
        string _lon_lat;
        [XmlElement("lon_lat")]
        public string lon_lat
        {
            get { return _lon_lat; }
            set { _lon_lat = value; }
        }

        string _device_id;
        [XmlElement("device_id")]
        public string device_id
        {
            get { return _device_id; }
            set { _device_id = value; }
        }

        string _date_time;
        [XmlElement("date_time")]
        public string date_time
        {
            get { return _date_time; }
            set { _date_time = value; }
        }

        string _speed;
        [XmlElement("speed")]
        public string speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        string _direction;
        [XmlElement("direction")]
        public string direction
        {
            get { return _direction; }
            set { _direction = value; }
        }

        string _gps_status;
        [XmlElement("gps_status")]
        public string gps_status
        {
            get { return _gps_status; }
            set { _gps_status = value; }
        }
        */
        #endregion

        string[] _field;
        [XmlElement("FIELD")]
        public string[] field
        {
            get { return _field; }
            set { _field = value; }
        }
    }

    public class ESRI
    {
        public ESRI()
        {
        }

        public String WriteString(String id, String lat_lon, String device_id, String date_time, String direction, String speed, String gps_status)
        {
            try
            {
                MESSAGE xmlMsg = new MESSAGE();

                #region for using in another format, comment only, don't delete
                /*
                xmlMsg.ID = id;
                xmlMsg.lon_lat = lat_lon;
                xmlMsg.device_id = device_id;
                xmlMsg.date_time = date_time;
                xmlMsg.direction = direction;
                xmlMsg.speed = speed;
                xmlMsg.gps_status = gps_status;
                */
                #endregion

                xmlMsg.ID = id;
                xmlMsg.field = new string[] { lat_lon, device_id, date_time, speed, direction, gps_status, "0" };

                XmlWriterSettings wrSettings    =   new XmlWriterSettings() ;
                wrSettings.Indent               =   false ;
                wrSettings.OmitXmlDeclaration   =   true ;

                XmlSerializer s                 =   new XmlSerializer(typeof(MESSAGE)) ;
                TextWriter txtWr                =   new StringWriter() ;

                XmlWriter xmlWriter             =   XmlWriter.Create(txtWr, wrSettings) ;

                System.Xml.Serialization.XmlSerializerNamespaces xs = new XmlSerializerNamespaces() ;
                xs.Add("", "");

                s.Serialize(xmlWriter, xmlMsg, xs);
                txtWr.Close();

                String retStr = txtWr.ToString();
                return retStr;
            }
            catch
            {
                return null;
            }
        }
    }
}
