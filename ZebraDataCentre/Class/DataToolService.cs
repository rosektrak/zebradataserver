﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading;
using System.ServiceProcess;
using ZebraLogTools;

namespace ZebraDataCentre
{
    public class DataToolService
    {
        public void GetServiceState(string ServiceName, out bool pass, out string message, out ServiceStatus status)
        {
            pass = true;
            message = string.Empty;
            status = ServiceStatus.None;
            ServiceController controller = null;
            controller = this.FindService(ServiceName);
            if (controller != null)
            {
                if (controller.Status == ServiceControllerStatus.Stopped)
                {
                    message = ServiceName + " Services fail. " + ServiceName + " Services not start.";
                    TextCls txt = new TextCls();
                    txt.WriteSystemLogEvent(message);

                    int i = 0;
                    while (i < 100)
                    {
                        Thread.Sleep(100);
                        i++;
                    }
                    pass = false;
                    status = ServiceStatus.Stop;
                   // Environment.Exit(0);
                }

                int count = 0;
                while (controller.Status != ServiceControllerStatus.Running)
                {
                    controller = this.FindService(ServiceName);
                    Thread.Sleep(0x2710);
                    if (count > 100)
                    {
                        message = ServiceName + " Waiting of Time Out. " + ServiceName + " Services not start.";
                        status = ServiceStatus.Stop_TimeOut;
                        pass = false;
                        break; 
                    }
                }
            }
            else
            {
                message = ServiceName +" Services fail!!! " + ServiceName + "Services no found.";
                status = ServiceStatus.Not_Install;
                pass = false;
                //Environment.Exit(0);
            }
        }
        private ServiceController FindService(string ServiceName)
        {
            try
            {
                ServiceController[] services = ServiceController.GetServices();
                for (int i = 0; i < services.Length; i++)
                {
                    if (services[i].ServiceName.Trim().ToUpper() == ServiceName.Trim().ToUpper())
                    {
                        return services[i];
                    }
                }
                return null;
            }
            catch //(Exception exception)
            {
                // Global.ErrorHandler(Thread.CurrentThread.Name, "FindService: " + ServiceName, exception);
                return null;
            }
        }
    }
}
