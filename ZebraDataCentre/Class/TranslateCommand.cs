using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Net;

namespace ZebraDataCentre
{
    public enum TypeOfBox { K, M, T};
    public enum TypeCommand { R, W };

    public struct COMMANDSYNTAX
    {
        public string cmd_name;
        public string type;
        public string condition;
    }
    public class TranslateCommand
    {
        public TranslateCommand()
        { }
        public DataTable GetKBoxCommand(DataTable dtTemp)
        {
            DataTable dtRet = CreateTempTable();
            try 
            {
                foreach (DataRow dr in dtTemp.Rows)
                {
                    if ((int)dr[0] == 125) { }
                    object obj = new object();
                    string cmd_message = string.Empty;
                    string cmd_display = (string)dr["cmd_function"];
                    string cmd = (string)dr["cmd_command"];
                    string cmd_type = (string)dr["cmd_type"];
                    string cmd_ex = "";

                    switch (cmd_type)
                    {
                        case "R":
                            {
                                cmd_message = (string)dr["cmd_syntax"];
                                cmd_ex = (string)dr["cmd_command"];
                            }
                            break;
                        case "W":
                            {
                                obj = CalcurateSyntax((string)dr["cmd_syntax"]);
                                cmd_ex = (string)dr["cmd_command"];
                                cmd_message = ((COMMANDSYNTAX)((ArrayList)obj)[0]).cmd_name;
                            }
                            break;
                    }
                    dtRet.Rows.Add(cmd_display, cmd_message, cmd_ex, obj);
                }
            }
            catch
            { }
            return dtRet;
        }
        private object CalcurateSyntax(string cmd)
        {
            object obj = new object();
            try 
            {
                ArrayList ar = new ArrayList();
                string[] data = cmd.Split(',');
                foreach (string st in data)
                {
                    COMMANDSYNTAX info = new COMMANDSYNTAX();
                    info.cmd_name = string.Empty;
                    info.condition = string.Empty;
                    info.type = string.Empty;

                    string[] dataDetail = st.Split('^');

                    for (int i = 0; i < dataDetail.Length; i++)
                    {
                        switch (i)
                        {
                            case 0: 
                                info.cmd_name = dataDetail[i];
                                break;
                            case 1:
                                info.type = dataDetail[i];
                                break;
                            case 2:
                                info.condition = dataDetail[i];
                                break;
                            default :
                                if (i > 2) { info.condition += '^' + dataDetail[i]; }
                                break;
                        }
                    }
                    ar.Add(info);
                }
                obj = ar;
            }
            catch { }
            return obj;
        }
        private object getType(string type)
        {
            object obj = new object();
            switch (type.ToUpper())
            {
                case "STRING": obj = string.Empty; break;
            }
            return obj;
        }
        private DataTable CreateTempTable()
        {
            DataTable dtRet = new DataTable();
            dtRet.Columns.Add("cmd_display", typeof(string));
            dtRet.Columns.Add("cmd_message", typeof(string));
            dtRet.Columns.Add("cmd_example", typeof(string));
            dtRet.Columns.Add("obj", typeof(object));
            return dtRet;
        }

        public bool CheckSyntax(string type, string condition,string value, out string output)
        {
            bool isOK = false;
            output = "";
            try
            {
                switch (type)
                {
                    case "INT":
                        {
                            int number = -1;
                            if (int.TryParse(value, out number))
                            {
                                isOK = true;
                                if (condition.Length > 0)
                                {
                                    string[] _condition = condition.Split('^');
                                    foreach (string st in _condition)
                                    {
                                        isOK = checkCondition(type, st, value);
                                        if (!isOK)
                                        { break; }
                                    }
                                }
                                else
                                    isOK = true;
                            }
                        }
                        break;
                    case "STRING":
                        {
                            if (condition.Length > 0)
                            {
                                string[] _condition = condition.Split('^');
                                foreach (string st in _condition)
                                {
                                    isOK = checkCondition(type, st, value);
                                }
                            }
                            else
                            {
                                isOK = true;
                            }
                        }
                        break;
                    case "TEL":
                        {
                            if (condition.Length > 0)
                            {
                                string[] _condition = condition.Split('^');
                                foreach (string st in _condition)
                                {
                                    isOK = checkCondition(type, st, value);
                                }
                            }
                            else
                            {
                                isOK = true;
                            }
                        }
                        break;
                    case "IPENDPOINT":
                        {
                            string[] _dot = value.Split('.');
                            
                            for (int i = 0; i < _dot.Length; i++)
                            {
                                char[] ch = _dot[i].ToCharArray();
                                for (int j = 0; j < ch.Length; j++)
                                {
                                    if (!char.IsDigit(ch[j])) { return false; }
                                }
                                if (_dot[i].Length > 0 && !checkCondition("INT","between0-255",_dot[i]))
                                {
                                     return  false;
                                }
                            }
                            isOK = true;
                        }
                        break;
                }
            }
            catch 
            { }
            return isOK;
        }
        private bool checkCondition(string type, string condiotion, string value)
        {
            if (condiotion.StartsWith("between"))
            {
                if (type == "INT")
                {
                    if (condiotion.IndexOf('-') < 0) return false;
                    string number = condiotion.Substring(7, condiotion.Length - 7);
                    int numA = Convert.ToInt32((number.Split('-'))[0]);
                    int numB = Convert.ToInt32((number.Split('-'))[1]);
                    int numC = -1;
                    if (int.TryParse(value, out numC) && (numC >= numA && numC <= numB))
                    {
                        return true;
                    }
                }
                else if (type == "STRING")
                {
                    return true;
                }
            }
            else if (condiotion.StartsWith("start"))
            {
                if (type == "INT")
                {
                    return true;
                }
                else if (type == "STRING" || type == "TEL")
                {
                    string msg = condiotion.Substring(5, condiotion.Length - 5);
                    char[] stTemp = value.ToCharArray();
                    char[] stCheck = msg.ToCharArray();

                    int max =Math.Min(stTemp.Length , stCheck.Length);
                    for (int i = 0; i < max; i++)
                    {
                        if (stTemp[i] != stCheck[i])
                        {
                            return false;
                        }
                    }
                    if (type == "STRING")
                    {
                        return true;
                    }
                    else if (type == "TEL")
                    {
                        char[] ch = value.ToCharArray();
                        if (ch.Length > 12) return false;
                        for (int i = 1; i < ch.Length; i++)
                        {
                            if (!char.IsDigit(ch[i])) { return false; }
                        }
                        return true;
                    }
                }
            }
            else if (condiotion.StartsWith("leng"))
            {
                string number = condiotion.Substring(4, condiotion.Length - 4);
                int numA = Convert.ToInt32(number);
                if (value.Length <= numA)
                {
                    return true;
                }
            }
            else if (condiotion.StartsWith("Port"))//Port^between0-66556
            {
                //Port^between0-66556
                if (condiotion.IndexOf('-') < 0) return false;
                string number = condiotion.Substring(12, condiotion.Length - 12);
                int numA = Convert.ToInt32((number.Split('-'))[0]);
                int numB = Convert.ToInt32((number.Split('-'))[1]);
                int numC = -1;
                if (int.TryParse(value, out numC) && (numC >= numA && numC <= numB))
                {
                    return true;
                }
            }
            return false;
        }
    }
   
    public class TranslateCommandSQL
    {
        private string dbConn = string.Empty;
        public TranslateCommandSQL(string db)
        {
            dbConn = string.Format(@"Data Source = {0}; Initial Catalog = ZebraDB; uid = sa; pwd = aab-gps-gtt", db); 
        }
        public DataTable GetCommand(TypeOfBox typeBox, TypeCommand typeCommand)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spZM_GetBoxCommand";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60;

                    SqlParameter param1 = new SqlParameter("@cmd_type", typeCommand.ToString());
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@cmd_box", typeBox.ToString());
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);

                    cn.Close();
                }
            }
            catch
            {
            }

            return dtRet;
        }

    }
}
