namespace ZebraDataCentre
{
    partial class KBoxWriteControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbVehID = new System.Windows.Forms.Label();
            this.comboVehList = new System.Windows.Forms.ComboBox();
            this.comboCommandList = new System.Windows.Forms.ComboBox();
            this.tb1 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tb5 = new System.Windows.Forms.TextBox();
            this.lbPara5 = new System.Windows.Forms.Label();
            this.lbPara4 = new System.Windows.Forms.Label();
            this.lbPara3 = new System.Windows.Forms.Label();
            this.lbPara2 = new System.Windows.Forms.Label();
            this.lbPara1 = new System.Windows.Forms.Label();
            this.lbCommand = new System.Windows.Forms.Label();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.tb3 = new System.Windows.Forms.TextBox();
            this.tb4 = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbVehID
            // 
            this.lbVehID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbVehID.Location = new System.Drawing.Point(3, 0);
            this.lbVehID.Name = "lbVehID";
            this.lbVehID.Size = new System.Drawing.Size(104, 25);
            this.lbVehID.TabIndex = 6;
            this.lbVehID.Text = "Veh ID :";
            // 
            // comboVehList
            // 
            this.comboVehList.FormattingEnabled = true;
            this.comboVehList.Location = new System.Drawing.Point(113, 3);
            this.comboVehList.Name = "comboVehList";
            this.comboVehList.Size = new System.Drawing.Size(171, 21);
            this.comboVehList.TabIndex = 5;
            this.comboVehList.SelectedIndexChanged += new System.EventHandler(this.comboVehList_SelectedIndexChanged);
            // 
            // comboCommandList
            // 
            this.comboCommandList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCommandList.FormattingEnabled = true;
            this.comboCommandList.Location = new System.Drawing.Point(113, 28);
            this.comboCommandList.Name = "comboCommandList";
            this.comboCommandList.Size = new System.Drawing.Size(171, 21);
            this.comboCommandList.TabIndex = 4;
            this.comboCommandList.SelectedIndexChanged += new System.EventHandler(this.comboCommandList_SelectedIndexChanged);
            // 
            // tb1
            // 
            this.tb1.Location = new System.Drawing.Point(113, 53);
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(171, 20);
            this.tb1.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 177F));
            this.tableLayoutPanel1.Controls.Add(this.tb5, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lbPara5, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lbPara4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbPara3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbPara2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbPara1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbVehID, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.comboVehList, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tb1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbCommand, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.comboCommandList, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tb2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tb3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tb4, 1, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(287, 175);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // tb5
            // 
            this.tb5.Location = new System.Drawing.Point(113, 153);
            this.tb5.Name = "tb5";
            this.tb5.Size = new System.Drawing.Size(171, 20);
            this.tb5.TabIndex = 13;
            // 
            // lbPara5
            // 
            this.lbPara5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPara5.Location = new System.Drawing.Point(3, 150);
            this.lbPara5.Name = "lbPara5";
            this.lbPara5.Size = new System.Drawing.Size(104, 25);
            this.lbPara5.TabIndex = 17;
            this.lbPara5.Text = "parameter5 :";
            // 
            // lbPara4
            // 
            this.lbPara4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPara4.Location = new System.Drawing.Point(3, 125);
            this.lbPara4.Name = "lbPara4";
            this.lbPara4.Size = new System.Drawing.Size(104, 25);
            this.lbPara4.TabIndex = 16;
            this.lbPara4.Text = "parameter4 :";
            // 
            // lbPara3
            // 
            this.lbPara3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPara3.Location = new System.Drawing.Point(3, 100);
            this.lbPara3.Name = "lbPara3";
            this.lbPara3.Size = new System.Drawing.Size(104, 25);
            this.lbPara3.TabIndex = 15;
            this.lbPara3.Text = "parameter3 :";
            // 
            // lbPara2
            // 
            this.lbPara2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPara2.Location = new System.Drawing.Point(3, 75);
            this.lbPara2.Name = "lbPara2";
            this.lbPara2.Size = new System.Drawing.Size(104, 25);
            this.lbPara2.TabIndex = 14;
            this.lbPara2.Text = "parameter2 :";
            // 
            // lbPara1
            // 
            this.lbPara1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPara1.Location = new System.Drawing.Point(3, 50);
            this.lbPara1.Name = "lbPara1";
            this.lbPara1.Size = new System.Drawing.Size(104, 25);
            this.lbPara1.TabIndex = 13;
            this.lbPara1.Text = "parameter1 :";
            // 
            // lbCommand
            // 
            this.lbCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCommand.Location = new System.Drawing.Point(3, 25);
            this.lbCommand.Name = "lbCommand";
            this.lbCommand.Size = new System.Drawing.Size(104, 25);
            this.lbCommand.TabIndex = 7;
            this.lbCommand.Text = "Command :";
            // 
            // tb2
            // 
            this.tb2.Location = new System.Drawing.Point(113, 78);
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(171, 20);
            this.tb2.TabIndex = 10;
            // 
            // tb3
            // 
            this.tb3.Location = new System.Drawing.Point(113, 103);
            this.tb3.Name = "tb3";
            this.tb3.Size = new System.Drawing.Size(171, 20);
            this.tb3.TabIndex = 11;
            // 
            // tb4
            // 
            this.tb4.Location = new System.Drawing.Point(113, 128);
            this.tb4.Name = "tb4";
            this.tb4.Size = new System.Drawing.Size(171, 20);
            this.tb4.TabIndex = 12;
            // 
            // KBoxWriteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "KBoxWriteControl";
            this.Size = new System.Drawing.Size(293, 184);
            this.Load += new System.EventHandler(this.KBoxWriteControl_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbVehID;
        private System.Windows.Forms.ComboBox comboVehList;
        private System.Windows.Forms.ComboBox comboCommandList;
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lbCommand;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.TextBox tb3;
        private System.Windows.Forms.TextBox tb4;
        private System.Windows.Forms.Label lbPara4;
        private System.Windows.Forms.Label lbPara3;
        private System.Windows.Forms.Label lbPara2;
        private System.Windows.Forms.Label lbPara1;
        private System.Windows.Forms.Label lbPara5;
        private System.Windows.Forms.TextBox tb5;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
