using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ZebraDataCentre
{
    public partial class IntegerControl : UserControl
    {
        public IntegerControl()
        {
            InitializeComponent();
        }

        private void mkTb_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            int number = 0;
            if (Int32.TryParse(mkTb.Text, out number)){ return; }
        }

        private void mkTb_TextChanged(object sender, System.EventArgs e)
        {
            MaskedTextBox mtb = (MaskedTextBox)sender;
            int number = 0;
            if (Int32.TryParse(mtb.Text, out number)) { return; }
            else
            {
                string stNumber = mtb.Text;
                if (Int32.TryParse(stNumber.Replace(" ", ""), out number))
                {
                    mtb.Text = number.ToString();
                }
            }
        }
    }
}
