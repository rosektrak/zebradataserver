namespace ZebraDataCentre
{
    partial class KBoxWrite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KBoxWrite));
            this.btWrite = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.kBoxWriteControl = new ZebraDataCentre.KBoxWriteControl();
            this.rtExample = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btWrite
            // 
            this.btWrite.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btWrite.Location = new System.Drawing.Point(353, 218);
            this.btWrite.Name = "btWrite";
            this.btWrite.Size = new System.Drawing.Size(75, 23);
            this.btWrite.TabIndex = 1;
            this.btWrite.Text = "Write";
            this.btWrite.UseVisualStyleBackColor = true;
            this.btWrite.Click += new System.EventHandler(this.btWrite_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.kBoxWriteControl);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(419, 209);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Write Command";
            // 
            // kBoxWriteControl
            // 
            this.kBoxWriteControl.Location = new System.Drawing.Point(9, 19);
            this.kBoxWriteControl.Name = "kBoxWriteControl";
            this.kBoxWriteControl.Size = new System.Drawing.Size(399, 175);
            this.kBoxWriteControl.TabIndex = 0;
            // 
            // rtExample
            // 
            this.rtExample.BackColor = System.Drawing.SystemColors.Control;
            this.rtExample.Location = new System.Drawing.Point(3, 217);
            this.rtExample.Name = "rtExample";
            this.rtExample.ReadOnly = true;
            this.rtExample.Size = new System.Drawing.Size(344, 80);
            this.rtExample.TabIndex = 4;
            this.rtExample.Text = "";
            // 
            // KBoxWrite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 299);
            this.Controls.Add(this.rtExample);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btWrite);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(450, 338);
            this.MinimumSize = new System.Drawing.Size(450, 338);
            this.Name = "KBoxWrite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "KBoxWriteCommand";
            this.Load += new System.EventHandler(this.KBoxWrite_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btWrite;
        private System.Windows.Forms.GroupBox groupBox1;
        private ZebraDataCentre.KBoxWriteControl kBoxWriteControl;
        private System.Windows.Forms.RichTextBox rtExample;

    }
}