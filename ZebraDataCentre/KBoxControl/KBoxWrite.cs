using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraDataCentre
{
    public partial class KBoxWrite : Form
    {
        delegate void SetTextCallback(string obj);
        private int _cmd_veh_id = -1;
        public int gVehID
        {
            get
            {
                return _cmd_veh_id;
            }
        }

        private string _cmdCommand = "";
        public string gCommand
        {
            get
            {
                return _cmdCommand;
            }
        }

        private string _cmdParameter = "";
        public string gParameter
        {
            get
            {
                return _cmdParameter;
            }
        }

        private DataTable _dtWriteCommand = new DataTable();
        private DataTable _dtVeh_list = new DataTable();

        public KBoxWrite(DataTable VehList, DataTable Command)
        {
            InitializeComponent();
            this.kBoxWriteControl.EventMsg += new ZebraDataCentre.ExcampleHandler(kBoxWriteControl_EventMsg);
            this.kBoxWriteControl.gDatatableVehList = VehList;
            this.kBoxWriteControl.gDataTableCommand = Command;
        }

        void kBoxWriteControl_EventMsg(string msg)
        {
            UpdateExampleStatus(msg);
        }

        private void UpdateExampleStatus(string obj)
        {
            if (this.rtExample.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(UpdateExampleStatus);
                this.Invoke(d, new object[] { obj });
            }
            else
                rtExample.Text = obj.Replace('%','\n');
        }

        private void btWrite_Click(object sender, EventArgs e)
        {
            this.kBoxWriteControl.GetCommand();
            if (kBoxWriteControl.gCommand.Length > 0 && kBoxWriteControl.gVehID > -1)
            {
                string st = kBoxWriteControl.gCommand;
                _cmd_veh_id = kBoxWriteControl.gVehID;
                _cmdCommand = st.Split(',')[0];
                //speed,100,20
                _cmdParameter = st.Substring(st.IndexOf(',') + 1, st.Length - st.IndexOf(',') - 1);
                //_cmdParameter
            }
        }

        private void KBoxWrite_Load(object sender, EventArgs e)
        {

        }

    }
}