using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;

namespace ZebraPresentationLayer
{
  //enum GTRAX_MSG_ORDER { BITMASK, HEADER, UTC, LAT, HEMISPHERE, LON, LON_DIR, ALTITUDE, SPEED, COURSE, NO_OF_SATELLITE, GYRODISTANCE, DIO_INPUT, DIO_OUTPUT, ADC, ADC0, ADC1, DEVICE_ID=15, TYPE_OF_MSG };
  //enum DIGITALIO { IGT_L = 0x00, USER1=0x01, CALL=0x02, USER3=0x03, IGT_H=0x04, USER2=0x05, BATTERY_STATUS=0x06, POWER_STATUS=0x07 };    
    enum GTRAX_MSG_ORDER { BITMASK, HEADER, UTC, LAT, HEMISPHERE, LON, LON_DIR, ALTITUDE, SPEED, COURSE, NO_OF_SATELLITE, GYRODISTANCE, DIO_INPUT, DIO_OUTPUT, ADC, ADC0, ADC1, DEVICE_ID, TYPE_OF_MSG };
    enum DIGITALIO { IGT_L = 0x01, USER1 = 0x02, CALL = 0x04, USER3 = 0x08, IGT_H = 0x10, USER2 = 0x20, BATTERY_STATUS = 0x40, POWER_STATUS = 0x80 };

    public struct GTraxFmtMsg
    {
        public String header;
        public String vid;
        public String gps_date_time;
        public String utc;
        public String lat;
        public String lon;
        public String speed;
        public String course;
        public String type_of_fix;
        public String no_of_satellite;
        public String gpio_status;
        public String analog_level;
        public String type_of_message;
    }    

    public class GTrax
    {
        private const  String GTRAX_HEADER = "PUBX00";
        private const  String GTRAKIOFILE = "GTraxIO.xml";
        private static Hashtable gVehicleState = new Hashtable();

        System.Threading.Semaphore IOWAIT = new System.Threading.Semaphore(1, 1);

        public GTrax()
        {
            if (File.Exists(GTRAKIOFILE))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<string>));
                XmlReader xmlRd = XmlReader.Create(GTRAKIOFILE);
                List<string> arr = (List<string>)xml.Deserialize(xmlRd);
                xmlRd.Close();

                foreach (string s in arr)
                {
                    try
                    {
                        string[] sp = s.Split(new char[] { ':' });
                        gVehicleState.Add(sp[0], Convert.ToInt32(sp[1]));
                    }
                    catch { }
                }
            }
        }

        ~GTrax()
        {
            UpdateIOFile();
        }

        private void UpdateIOFile()
        {
            IOWAIT.WaitOne();

            List<string> arr = new List<string>();
            foreach (DictionaryEntry o in gVehicleState)
            {
                arr.Add(o.Key + ":" + o.Value);;
            }

            XmlSerializer xml = new XmlSerializer(typeof(List<string>));
            XmlWriter xmlWr = XmlWriter.Create(GTRAKIOFILE);
            xml.Serialize(xmlWr, arr);
            xmlWr.Flush();
            xmlWr.Close();

            IOWAIT.Release();
        }

        public object Translate(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length > 0)
            {
                msg = PutMax2Msg(data);

                if (msg.vid  == null)
                    return null;
            }
            else
            {
                return null;
            }

            return msg;
        }

        private Max2FmtMsg PutMax2Msg(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();

            if (data.Length <= 50)
            {
                //return (Max2FmtMsg)(new object());
                return msg;
            }

                String msgStr = Encoding.ASCII.GetString(data).Trim();
                String[] seqMsg = msgStr.Split(new char[] { ',', '\n' });

                if (seqMsg.Length >= 11)
                {
                    try
                    {
                        msg.header = seqMsg[(int)GTRAX_MSG_ORDER.HEADER];
                        //Console.WriteLine(msg.header.ToUpper());
                       // Console.WriteLine(GTRAX_HEADER.ToUpper());

                        //if (msg.header.ToUpper() != GTRAX_HEADER.ToUpper()) return null;
                        if (msg.header.ToUpper() != GTRAX_HEADER.ToUpper()) return msg;

                        DateTime utc_date = ToUTCDate(Convert.ToInt32(seqMsg[(int)GTRAX_MSG_ORDER.UTC]));

                        msg.header = "$";
                        msg.vid = seqMsg[(int)GTRAX_MSG_ORDER.DEVICE_ID];
                        msg.gps_date_time = utc_date.ToString("ddMMyy", new System.Globalization.CultureInfo("en-US"));
                        msg.utc = utc_date.ToString("HHmmss", new System.Globalization.CultureInfo("en-US"));
                        msg.lat = seqMsg[(int)GTRAX_MSG_ORDER.LAT];
                        msg.lon = seqMsg[(int)GTRAX_MSG_ORDER.LON];
                        msg.speed = seqMsg[(int)GTRAX_MSG_ORDER.SPEED];
                        msg.course = seqMsg[(int)GTRAX_MSG_ORDER.COURSE];
                        msg.type_of_fix = "1";
                        msg.no_of_satellite = seqMsg[(int)GTRAX_MSG_ORDER.NO_OF_SATELLITE];
                        msg.gpio_status = CalculateIO(seqMsg[(int)GTRAX_MSG_ORDER.DIO_INPUT]);
                        msg.analog_level = Convert.ToInt32(seqMsg[(int)GTRAX_MSG_ORDER.ADC], 16).ToString();
                        msg.type_of_message = CalculateMsg(msg.vid, seqMsg[(int)GTRAX_MSG_ORDER.DIO_INPUT] + seqMsg[(int)GTRAX_MSG_ORDER.DIO_OUTPUT]);
                    }
                    catch (Exception e)
                    {
                        WriteLogEvent("GTrax Error #1 :" + e.Message);
                    }
                }

            return msg;
        }
        private DateTime ToUTCDate(int gps_time)
        {
            System.DateTime cur_utc_date = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            cur_utc_date = cur_utc_date.AddSeconds(gps_time);

            return cur_utc_date;
        }
        private String CalculateIO(String data)
        {
            int v = Convert.ToInt32( data,16 );
            int r = 0x0;
            if ((v & (int)DIGITALIO.IGT_H) == (int)DIGITALIO.IGT_H)     { r |= 0x10; }
            if ((v & (int)DIGITALIO.POWER_STATUS) == 0)                 { r |= 0x80; }
            if ((v & (int)DIGITALIO.USER2) == (int)DIGITALIO.USER2)     { r |= 0x20; }
            if ((v & (int)DIGITALIO.USER3) == (int)DIGITALIO.USER3)     { r |= 0x40; }

            String ret = String.Format("{0:X2}", r);
            return ret ;
        }
        private String CalculateMsg(String DevID, String data)
        {
            int o = gVehicleState[DevID] == null ? 0 : (int)gVehicleState[DevID] ;
            int v = Convert.ToInt32( data,16 );
            bool r = false ;

            if (o != v) r = true;

            //if ((v & (int)DIGITALIO.IGT_H) !=  (o & (int)DIGITALIO.IGT_H))              { r = true ; }
            //if ((v & (int)DIGITALIO.POWER_STATUS) != (o & (int)DIGITALIO.POWER_STATUS)) { r = true ; }
            //if ((v & (int)DIGITALIO.USER2) != (o & (int)DIGITALIO.USER2))               { r = true ; }
            //if ((v & (int)DIGITALIO.USER3) != (o & (int)DIGITALIO.USER3))               { r = true ; }

            String msg = "G";

            if (!gVehicleState.ContainsKey(DevID)) {
                gVehicleState.Add(DevID, v);
            } else {
                gVehicleState[DevID] = v;
            }

            if (r)
            {
                msg = "I";
                UpdateIOFile();
            }
            return msg;
        }
        public String GetRTPVString(Max2FmtMsg msg)
        {
            String prnStr = "";

            prnStr = msg.vid + ",";
            prnStr += msg.gps_date_time + ",";
            prnStr += msg.utc + ",";
            prnStr += msg.lat + ",";
            prnStr += msg.lon + ",";
            prnStr += msg.speed + ",";
            prnStr += msg.course + ",";
            prnStr += msg.type_of_fix + ",";
            prnStr += msg.no_of_satellite + ",";
            prnStr += msg.gpio_status + ",";
            prnStr += msg.analog_level + ",";
            prnStr += msg.type_of_message;

            return prnStr;
        }
        private void WriteLogEvent(String msg)
        {
            /*
            msg = DateTime.Now.ToString() + " : " + msg;
            String fileName = "Max2LogEvent.txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
             */
        }
    }
}
