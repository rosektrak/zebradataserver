using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using ZebraLogTools;

namespace ZebraPresentationLayer
{
    //ODOMETOR
    enum MAX2_MSG_ORDER { VID, GPS_DATE_TIME, UTC, LAT, LON, SPEED, COURSE, TYPE_OF_FIX, NO_OF_SATELLITE, GPIO, ANALOG_LEVEL, TYPE_OF_MSG, TAG };
    enum MAX2_MSG_ORDER2 { VID, GPS_DATE_TIME, UTC, LAT, LON, SPEED, COURSE, TYPE_OF_FIX, NO_OF_SATELLITE, GPIO, ANALOG_LEVEL, TYPE_OF_MSG, TAG, ODOMETOR };

    [Serializable]
    public struct ConnectionInfo
    {
        public String source_ip;
        public String source_port;
        public String destination_ip;
        public String destination_port;
        public String type_of_box;
    }

    [Serializable]
    public struct Max2FmtMsg 
    {
        public String header;
        public String vid;
        public String gps_date_time;
        public String utc;
        public String lat;
        public String lon;
        public String speed;
        public String course;
        public String type_of_fix;
        public String no_of_satellite;
        public String gpio_status;
        public String analog_level;
        public String type_of_message;
        public String tag;
        public String odometor;
        public String internal_power;
        public String external_power;
        public ConnectionInfo conInfo;
    }

    public class MAX2
    {
        const char MAX2_HEADER = '$';

        public MAX2()
        {
        }

        public object Translate(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length > 0)
            {
                if (data[0] == MAX2_HEADER) 
                {
                    msg = PutMax2Msg(data);

                    if(msg.vid == null) return null;
                } 
                else 
                {
                    return null;
                }
            } 
            else 
            {
                return null;
            }
            return msg;
        }

        private Max2FmtMsg PutMax2Msg(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();

            if (data.Length <= 50) 
            {
                return msg;
            }
            try
            {
                String msgStr = Encoding.ASCII.GetString(data).Trim();
                String[] seqMsg = msgStr.Split(new char[] { ',', '\n' });

                if (seqMsg[0].Length >= 12)
                {
                    return msg;
                }

                if (seqMsg.Length >= 11)
                {
                    msg.vid = seqMsg[(int)MAX2_MSG_ORDER.VID].Substring(1, seqMsg[(int)MAX2_MSG_ORDER.VID].Length - 1);
                    msg.gps_date_time = seqMsg[(int)MAX2_MSG_ORDER.GPS_DATE_TIME];
                    msg.utc = seqMsg[(int)MAX2_MSG_ORDER.UTC];
                    msg.lat = seqMsg[(int)MAX2_MSG_ORDER.LAT];
                    msg.lon = seqMsg[(int)MAX2_MSG_ORDER.LON];
                    msg.speed = seqMsg[(int)MAX2_MSG_ORDER.SPEED];
                    msg.course = seqMsg[(int)MAX2_MSG_ORDER.COURSE];
                    msg.type_of_fix = seqMsg[(int)MAX2_MSG_ORDER.TYPE_OF_FIX];
                    msg.no_of_satellite = seqMsg[(int)MAX2_MSG_ORDER.NO_OF_SATELLITE];
                    msg.gpio_status = seqMsg[(int)MAX2_MSG_ORDER.GPIO];
                    msg.analog_level = seqMsg[(int)MAX2_MSG_ORDER.ANALOG_LEVEL];
                    msg.type_of_message = seqMsg[(int)MAX2_MSG_ORDER.TYPE_OF_MSG];

                    msg.tag = "";
                    msg.odometor = "";
                    msg.external_power = "";
                    msg.internal_power = "";

                    for (int i = (int)MAX2_MSG_ORDER.TAG; i < seqMsg.Length; i++)
                    {
                        msg.tag += seqMsg[i] + ",";
                    }
                    if (msg.tag.Length > 0)
                    {
                        msg.tag = msg.tag.TrimEnd(',', '\0');
                    }

                    if (msg.type_of_message.Length > 0)
                    {
                        msg.type_of_message = msg.type_of_message.Trim('\0');
                    }
                }
            }
            catch 
            {
            }
            return msg;
        }
        public  String GetRTPVString(Max2FmtMsg msg)
        {
            String prnStr = "";

            prnStr  =   msg.vid + ",";
            prnStr +=   msg.gps_date_time + ",";
            prnStr +=   msg.utc + ",";
            prnStr +=   msg.lat + ",";
            prnStr +=   msg.lon + ",";
            prnStr +=   msg.speed + ",";
            prnStr +=   msg.course + ",";
            prnStr +=   msg.type_of_fix + ",";
            prnStr +=   msg.no_of_satellite + ",";
            prnStr +=   msg.gpio_status + ",";
            prnStr +=   msg.analog_level + ",";
            prnStr +=   msg.type_of_message;

            return prnStr;
        }
    }
}
