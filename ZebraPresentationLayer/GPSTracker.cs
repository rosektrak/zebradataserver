using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace ZebraPresentationLayer
{
    public class GPSTracker
    {
        enum GPS_TRACKER_ORDER { GPS_LENGTH, VID, COMMAND };

        enum GPS_TRACKER_MSG
        {
            PACKER_FLAG_LENGTH = 0,
            IMEI = 1,
            COMMAND = 2,
            CODE = 3,
            LATITUDE = 4,
            LONGITUDE = 5,
            GPS_DATE_TIME = 6,
            GPS_STATUS = 7,
            NUMBER_OF_SATELLITES = 8,
            GSM_SIGNAL = 9,
            SPEED = 10,
            HEADING = 11,
            HDOP = 12,
            ALTITUDE = 13,
            MILEAGE = 14,
            RUNTIME = 15,
            BASE_ID = 16,
            GPIO_STATUS = 17,
            AD = 18,
            RFID = 19,
            PICTURE = 20,
            FENCE = 21,
            CHECK_SUM = 22
        }

        enum GPS_TRACKER_COMMAND
        {
            NONE = '0',
            AAA = 'G'
        }


        const char GPS_TRACKER_HEADER = '$';
        const char GPS_TRACKER_REC_HEADER = '@';

        public object Translate(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length > 0)
            {
                /// location
                if (data[0] == GPS_TRACKER_HEADER && data[1] == GPS_TRACKER_HEADER)
                {
                    msg = TranslateMax2MsgLocation(data);

                    if (msg.vid == null )
                        return null;
                }
            }
            else
            {
                return null;
            }
            return msg;
        }

        private Max2FmtMsg TranslateMax2MsgLocation(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length <= 100) { return msg; }   //check ex $020202000000,0,0.0,0.0,0.0,0.0,0.0,99.99,1
            try
            {
                String msgStr = Encoding.ASCII.GetString(data).Trim();
                String[] seqMsg = msgStr.Split(new char[] { ',', '\n' });

                if (seqMsg[0].Length < 2)
                {
                    return msg;
                }

                if (seqMsg.Length >= 11)
                {
                    msg.header = "$";
                    msg.vid = seqMsg[(int)GPS_TRACKER_MSG.IMEI];
                    string gps_date_time = seqMsg[(int)GPS_TRACKER_MSG.GPS_DATE_TIME].Substring(0, 6);
                    string yy = gps_date_time.Substring(0, 2);
                    string MM = gps_date_time.Substring(2, 2);
                    string dd = gps_date_time.Substring(4, 2);
                    msg.gps_date_time = dd + MM + yy;
                    msg.utc = seqMsg[(int)GPS_TRACKER_MSG.GPS_DATE_TIME].Substring(6, 6);
                    msg.lat = seqMsg[(int)GPS_TRACKER_MSG.LATITUDE] + "N";
                    msg.lon = seqMsg[(int)GPS_TRACKER_MSG.LONGITUDE] + "E";
                    msg.speed = seqMsg[(int)GPS_TRACKER_MSG.SPEED].Length > 0 ? seqMsg[(int)GPS_TRACKER_MSG.SPEED] : "0";
                    msg.course = seqMsg[(int)GPS_TRACKER_MSG.HEADING].Length > 0 ? seqMsg[(int)GPS_TRACKER_MSG.HEADING] : "0";
                    msg.type_of_fix = seqMsg[(int)GPS_TRACKER_MSG.GPS_STATUS] == "A" ? "1" : "0";
                    msg.no_of_satellite = seqMsg[(int)GPS_TRACKER_MSG.NUMBER_OF_SATELLITES].Length > 0? seqMsg[(int)GPS_TRACKER_MSG.NUMBER_OF_SATELLITES]: "0";

                    msg.type_of_message = GetCommandTypeMsg(seqMsg[(int)GPS_TRACKER_MSG.COMMAND]);// GetCommandTypeMsg();

                    msg.tag = "";
                    msg.odometor = "0";
                    msg.internal_power = "0";
                    msg.gpio_status = "00";

                    msg.analog_level = "0";
                    msg.external_power = "0";

                    #region timestamp
                    DateTime testDate = DateTime.Parse(GetTimeStamp(msg.gps_date_time, msg.utc));
                    DateTime validDate1 = DateTime.Now.Add(new TimeSpan(-2, 0, 0, 0));
                    DateTime validDate2 = DateTime.Now.AddMinutes(5);
                  
                    if (!(validDate1 <= testDate && testDate <= validDate2))
                    {
                        DateTime d = DateTime.Now;
                        d = d.AddHours(-7);
                        msg.gps_date_time = d.ToString("ddMMyy");
                        msg.utc = d.ToString("HHmmss");
                    }
                    #endregion
                }
            }
            catch 
            {
                return msg = new Max2FmtMsg();
            }

            return msg;
        }

        private string GetCommandTypeMsg(string cmd)
        {
            string type = string.Empty;
            GPS_TRACKER_COMMAND ty = (GPS_TRACKER_COMMAND)Enum.Parse(typeof(GPS_TRACKER_COMMAND), cmd);
            switch (ty)
            {
                case GPS_TRACKER_COMMAND.AAA:
                    type = ((char)GPS_TRACKER_COMMAND.AAA).ToString();
                    break;

                default :
                    break;
            }

            return type;
        }

        private String GetTimeStamp(String date, String time)
        {
            try
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                d = d.AddHours(7.0);
                String szTimeStamp = d.ToString("yyyy-MM-dd HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            catch
            {
            }

            return "";
        }
    }
}
