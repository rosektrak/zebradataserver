﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Collections;

namespace ZebraPresentationLayer
{
    #region MESSAGE

    enum MEITRACK_MSG_LOCATION
    {
        VID = 1,
        TYPE_OF_MESSAGE = 3,
        LAT = 4,
        LON = 5,
        GPS_DATE_TIME = 6,
        TYPE_OF_FIX = 7,
        NO_OF_SATELLITE = 8,
        SPEED = 10,
        COURSE = 11,
        GPIO_STATUS = 17,
        ANALOG_LEVEL = 18,
    };
    #endregion

    public class Meitrack
    {
        const char Meitrack_HEADER = '$';
        public Meitrack()
        {
        }

        public object Translate(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length > 0)
            {
                if (data[0] == Meitrack_HEADER && data[1] == Meitrack_HEADER)
                {
                    msg = TranslateMax2Msg(data);
                    if (msg.vid == null) return null;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            return msg;
        }

        private Max2FmtMsg TranslateMax2Msg(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            try
            {
                String msgStr = Encoding.ASCII.GetString(data).Trim();
                String[] seqMsg = msgStr.Split(new char[] { ',', '\n' });

                if (seqMsg[0].Length < 2)
                {
                    return msg;
                }

                if (seqMsg.Length == 23)
                {
                    msg.vid = seqMsg[(int)MEITRACK_MSG_LOCATION.VID];
                    string dt = seqMsg[(int)MEITRACK_MSG_LOCATION.GPS_DATE_TIME];
                    string dt2 = "20" + dt.Substring(0, 2) + "-" + dt.Substring(2, 2) + "-" + dt.Substring(4, 2) + " " + dt.Substring(6, 2) + ":" + dt.Substring(8, 2) + ":" + dt.Substring(10, 2);
                    DateTime date_time = Convert.ToDateTime(dt2);
                    date_time = date_time.AddHours(-7);
                    string day = date_time.ToString("dd");
                    string month = date_time.ToString("MM");
                    string years = date_time.ToString("yyyy");
                    string hh = date_time.ToString("HH");
                    string mm = date_time.ToString("mm");
                    string ss = date_time.ToString("ss");
                    msg.gps_date_time = day + "" + month + "" + years.Substring(2, 2);
                    msg.utc = hh + "" + mm + "" + ss;
                    msg.lat = seqMsg[(int)MEITRACK_MSG_LOCATION.LAT];
                    msg.lon = seqMsg[(int)MEITRACK_MSG_LOCATION.LON];
                    msg.speed = seqMsg[(int)MEITRACK_MSG_LOCATION.SPEED];
                    Decimal course_decimal = Convert.ToDecimal(seqMsg[(int)MEITRACK_MSG_LOCATION.COURSE]);
                    int course_int = Convert.ToInt32(course_decimal);
                    msg.course = Convert.ToString(course_int);
                    msg.analog_level = seqMsg[(int)MEITRACK_MSG_LOCATION.ANALOG_LEVEL];
                    string[] spit_AD = msg.analog_level.Split('|');
                    msg.analog_level = Convert.ToString(int.Parse(Convert.ToString(spit_AD[1]), System.Globalization.NumberStyles.HexNumber));
                    //msg.no_of_satellite = "5";
                    msg.no_of_satellite = seqMsg[(int)MEITRACK_MSG_LOCATION.NO_OF_SATELLITE];
                    //msg.type_of_fix = "1";
                    msg.type_of_fix = seqMsg[(int)MEITRACK_MSG_LOCATION.TYPE_OF_FIX];
                    switch (msg.type_of_fix)
                    {
                        case "A": msg.type_of_fix = "1"; break;
                        case "V": msg.type_of_fix = "0"; break;
                    }
                    msg.gpio_status = seqMsg[(int)MEITRACK_MSG_LOCATION.GPIO_STATUS]; //แปลงค่าเลขก่อน
                    switch (msg.gpio_status)
                    {
                        case "0400": msg.gpio_status = "10"; break; // 0400 >> ฐาน 2 = 0000 0100 0000 0000 >> Bit 3 = 1 ,Input 3 ON (Engine ON) >> Engine ON ,IO status = 10
                        case "0000": msg.gpio_status = "00"; break;
                    }
                    msg.type_of_message = seqMsg[(int)MEITRACK_MSG_LOCATION.TYPE_OF_MESSAGE];
                    switch (Convert.ToInt32(msg.type_of_message))
                    {
                        case 3: msg.type_of_message = "I"; break; //Input 3 ON
                        case 11: msg.type_of_message = "I"; break; //Input 3 OFF
                        case 35: msg.type_of_message = "G"; break; //Time Interval
                        case 37: msg.type_of_message = "D"; break; //RFID
                        default: msg.type_of_message = "G"; break;
                    }
                    msg.tag = "";
                    if (seqMsg[19] != "00000001" && seqMsg[19] != "")
                    {
                        msg.tag = "RFID" + seqMsg[19];
                    }
                    msg.odometor = "";
                    msg.external_power = "";
                    msg.internal_power = "";

                }
            }
            catch (Exception ex)
            {
                string msgex = ex.Message;
                return msg = new Max2FmtMsg();
            }

            return msg;
        }
    }
}
