﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ZebraNoSQLLib
{
    public enum ZONE_SIDE_TYPE { NONE, IN, OUT };
    public enum SOURE_EVENT { NONE = 0, USER_DEFINED_INPUT = 1, USER_DEFINED_OUTPUT = 2, EXTERNAL_INPUT = 3, SMART_CARD = 4, MAGNETIC = 5 ,ZONEING = 6}
    public enum LOGGINGMOGE { UNKNOWN = 0,REALTIME = 1,LOGGER = 2,BUFFER = 3 }
    public class KTLogMsg
    {
        public ObjectId _id { get; set; }
        public Double number { get; set; }
        public Int64 date_id { get; set; }
        public int vid { get; set; }
        public string reg { get; set; } //registration
        public DateTime timestamp { get; set; } //local_timestamp
        public double lat { get; set; }
        public double lon { get; set; }
        public int speed { get; set; }
        public int course { get; set; }
        public int fix { get; set; } //type_of_fix
        public int sat { get; set; } //no_of_satellite
        public int io_status { get; set; } //in_out_status
        public int analog { get; set; } //analog_level
        public string type_msg { get; set; } //type_of_msg
        public string tag { get; set; }
        public string loc_t { get; set; } //location_t
        public string loc_e { get; set; } //location_e
        public int log_mode { get; set; } //logging_mode
        public IList<Events> list_event { get; set; }
        public DataError log_error { get; set; }
        public Distance distance { get; set; }
        public Energy energy { get; set; }
        public GSM gsm { get; set; }
        public Power power { get; set; }
        public Firmware_version fw_vs { get; set; }
        public ETA eta { get; set; }
        public RPM eng_rpm { get; set; } //engine_rpm
        public Option option { get; set; }
    }

    #region Event
    public class Events
    {
        public int evt_id { get; set; }
        public string event_desc_e { get; set; }
        public string event_desc_t { get; set; }
        public bool is_backside { get; set; }
        public Event_Backside evt_backside { get; set; }
        public Zoning zone { get; set; }
        public Routing route { get; set; }
        public SmartCard smart_card { get; set; }
    }
    public class Event_Backside
    {
        public int type_id { set; get; }
        public int evt_id { get; set; }
        public string event_e { get; set; }
        public string event_t { get; set; }
        public Zoning zone_backside { get; set; }
    }
    public class Zoning
    {
        public int zone_id { get; set; }
        public string zone_desc { get; set; }
    }
    public class Routing
    {
        public int route_id { get; set; }
        public string route_desc { get; set; }
    }
    public class SmartCard {
        public int smart_id { get; set; }
        public string smart_desc { get; set; }
    }
    #endregion
    public class Energy
    {
        public double oil1 { get; set; }
        public double oil2 { get; set; }
        public double oil3 { get; set; }
    }
    public class Distance
    {
        public double distance { get; set; }
        public double odometor { get; set; }
    }
    public class DataError
    {
        public double lat { get; set; }
        public double lon { get; set; }
        public int sat { get; set; }
    }
    public class GSM
    {
        public string mcc { get; set; }
        public string mnc { get; set; }
        public string lac { get; set; }
        public string cell { get; set; }
        public string bsic { get; set; }
        public int arfcn { get; set; }
        public int rxlev { get; set; }
    }
    public class Power
    {
        public double inernal { get; set; }
        public double external { get; set; }
    }
    public class Firmware_version
    {
        public string fw_version { get; set; }
        public string type_of_box { get; set; }
        public string source_ip { get; set; }
        public string local_ip { get; set; }
        public int local_port { get; set; }
    }
    public class ETA
    {
        public int eta_id { get; set; }
        public double time_left { get; set; }
        public int destination_id { get; set; }
        public string destination_desc { get; set; }
    }
    public class RPM
    {
        public int rawdata { get; set; }
        public int rpm { get; set; }
    }
    public class Option
    {
        public Option_External_Input optExIn { get; set; } //optExternal_Input
        public Option_Analog_level optAnalog { get; set; }
        public Option_Temperature optTemp { get; set; }
        public Option_Magnetic optMNT { get; set; } //optMagnetic
        public Option_SmartCard optSMC { get; set; } //optSmartCard
    }
    public class Option_External_Input
    {
        public int input_id { get; set; }
        public int message_type { get; set; }
        public int status_byte { get; set; }
        public int input_mark { get; set; }
        public int report_time { get; set; }
        public int input_status { get; set; }
        public External_Input ex_input { get; set; } // external_input
    }
    public class External_Input
    {
        public int in01 { get; set; }
        public int in02 { get; set; }
        public int in03 { get; set; }
        public int in04 { get; set; }
        public int in05 { get; set; }
        public int in06 { get; set; }
    }
    public class Option_Analog_level
    {
        public int analog2 { get; set; } //analog_level2
        public int analog3 { get; set; } //analog_level3
    }
    public class Option_Temperature
    {
        public int temp_id { get; set; }
        public double temperature { set; get; }
        public double max_temp { set; get; }
        public double min_temp { set; get; }
    }
    public class Option_Magnetic
    {
        public Driver_License drive_lic { get; set; }
    }
    public class Driver_License
    {
        public string driver_type { get; set; }
        public string driver_sex { get; set; }
        public string driver_number { get; set; }
        public string country_code { get; set; }
        public string distic_code { get; set; }
        public string driver_name { get; set; }
        public string province_code { get; set; }
        public string card_id { get; set; }
        public string expiry_date { get; set; }
        public string birthday { get; set; }
    }
    public class Option_SmartCard
    {
        public int smc_evt_id { get; set; }
        public string data { get; set; }
    }
    public class KPI_LOG
    {
        public string recsts { get; set; }
        public int kpi_type { get; set; }
    }

}
