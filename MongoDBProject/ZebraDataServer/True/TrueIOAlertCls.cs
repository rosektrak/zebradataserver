using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class TrueIOAlertCls
    {
        public enum IOTYPE
        {
            ENGINEOFF   = 0x00,
            ENGINEON    = 0x10,

            USERINPUT1  = 0x20,
            USERINPUT2  = 0x40,
            POWER       = 0x80,

            IGNITION    = 0x01,
            USEROUTPUT1 = 0x02,
            USEROUTPUT2 = 0x04,
            USEROUTPUT3 = 0x08
        }

        public struct VEHINFO
        {
            public int      ref_idx ;
            public int      veh_id ;
            public int      value ;
            public bool     enable_q ;
            public int      speed ;
            public string   timestamp;
            public double   lat ; 
            public double   lon ;
            public CommonQeue SMSQ;
            public CommonQeue EMAILQ;
        }

        SQL gSql;
        LocationCls oLoc;

        public TrueIOAlertCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            oLoc = new LocationCls(dbServer);
        }

        public void DetectStatus(int ref_idx, int veh_id, int value, string g_type_of_msg, bool enableQ, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ)
        {
            VEHINFO info = new VEHINFO();
            info.ref_idx    =   ref_idx ;
            info.veh_id     =   veh_id ;
            info.value      =   value ;
            info.enable_q   =   enableQ ;
            info.speed      =   speed ;
            info.timestamp  =   timestamp ;
            info.lat        =   lat ;
            info.lon        =   lon ;
            info.SMSQ       =   SMSQ;
            info.EMAILQ     =   EMAILQ;

            if (g_type_of_msg == "i" || g_type_of_msg == "I" || g_type_of_msg == "+")
            {
                int retIO = GetLastIOStatus(veh_id);
                if (!IsDoubleEvent(value, retIO, IOTYPE.POWER))
                {                    
                    if ((value & (int)IOTYPE.POWER) == (int)IOTYPE.POWER)
                    {
                        string txt = "POWER LINE PLUG IN";
                        InsertNewRecord(ref_idx, veh_id, txt, EVT_TYPE.EVENT_POWER_LINE_CONNECTED, value, info);
                    }
                    else
                    {
                        string txt = "POWER LINE IS UNPLUGED";
                        InsertNewRecord(ref_idx, veh_id, txt, EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, value, info);
                    }
                }
            }

            if (g_type_of_msg == "X")
            {
                string txt = "GPS IS UNPLUGED";
                InsertNewRecord(ref_idx, veh_id, txt, EVT_TYPE.EVENT_GPS_DISCONNECTED, value, info);

                //gSql.True2InsertMsgIO(ref_idx, (int)EVT_TYPE.EVENT_GPS_DISCONNECTED, type_id);
               // Broadcast(enableQ, ref_idx, "GPS IS UNPLUGED", veh_id, speed, timestamp, lat, lon, SMSQ, EMAILQ, value);
            }
            else if (g_type_of_msg == "x")
            {
                string txt = "GPS PLUG IN";
                InsertNewRecord(ref_idx, veh_id, txt, EVT_TYPE.EVENT_GPS_CONNECTED, value, info);

                //gSql.True2InsertMsgIO(ref_idx, (int)EVT_TYPE.EVENT_GPS_CONNECTED, type_id);
               // Broadcast(enableQ, ref_idx, "GPS PLUG IN", veh_id, speed, timestamp, lat, lon, SMSQ, EMAILQ, value);
            }
        }

        public void InsertNewRecord(int ref_idx, int vid, string msg, EVT_TYPE feature, int value, VEHINFO info)
        {
            //Insert io status
            gSql.True2InsertMsgIO(ref_idx, (int)feature);

            //update ststus
            if (feature == EVT_TYPE.EVENT_POWER_LINE_CONNECTED || feature == EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED)
            {
                gSql.True2InsertTruePrvIOState(vid, value, ref_idx);
            }
            //evt_id,message,zone_id
            msg = ((int)feature).ToString() + "," + msg + ",-1";
            Broadcast(info.enable_q, info.ref_idx, msg, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
        }

        public int GetLastIOStatus(int veh_id)
        {
            int status = 0;
            int ref_idx = 0;
            gSql.GetLastTrueIOStatus(veh_id, out status, out ref_idx);

            return status;
        }

        private bool IsDoubleEvent(int value, int prv_state, IOTYPE type)
        {
            value       =   value & (int)type ;
            prv_state   =   prv_state & (int)type ;

            if (value == prv_state)
            {
                return true;
            }

            return false;
        }

        private void Broadcast(bool enableQ, int ref_idx, string txt, int veh_id, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ, int value)
        {
            if ( enableQ )
            {
                ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                info.idx = ref_idx;
                info.type = (int)EVT_TYPE.TRUE_IOSTATUS;
                info.veh_id = veh_id;
                info.msg = txt;
                info.timestamp = timestamp;
//info.location = GetLocation((float)lon, (float)lat);
                info.lat = lat;
                info.lon = lon;
                if (SMSQ != null || EMAILQ != null)
                {
                    SMSQ.PushQ(info);
                    EMAILQ.PushQ(info);
                }
            }
        }

        public string GetLocation(float lon, float lat)
        {
            if (oLoc == null)
            {
                return "?";
            }

            string location_e = "����Һ���˹�";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    //location_e = oLoc.GetCustomLandmarkName(id, 0);
                    location_e = oLoc.GetCustomLandmarkName(id, 1);
                }
                else
                {
                    id = oLoc.DetectLandmark(lon, lat);
                    //location_e = oLoc.GetLandmarkName(id, 0);
                    location_e = oLoc.GetLandmarkName(id, 1);
                }

                id = oLoc.DetectAdminPoly(lon, lat);
                //location_e += " " + oLoc.GetPolyName(id, 0);
                location_e += " " + oLoc.GetPolyName(id, 1);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_e = location_e.TrimStart(new char[] { ' ' });
            }
            return location_e;
        }
        public bool IsVehicleExisted(int veh_id)
        {
            return gSql.GetTrueVehicle(veh_id);
        }
    }
}
