using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class TrueZoneAlertCls2
    {
        SQL gSql;
        public TrueZoneAlertCls2(string dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public DataTable GetZoneByVehicle(int veh_id)
        {
            return gSql.GetTrueZoneAlert2(veh_id);
        }

        public void GetLastZoneStatus(int veh_id, int zone_id, out int evt_id)
        {
            gSql.GetLastTrueZoneStatus(veh_id, zone_id, out evt_id);
        }

        public void InsertNewRecord(ZebraDataServer.FencingCls.SIDE_TYPE side, int veh_id, int zone_id)
        {
            if (side == ZebraDataServer.FencingCls.SIDE_TYPE.IN)
            {
                gSql.InsertPrvTrueZoneState(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE);
            }
            else if (side == ZebraDataServer.FencingCls.SIDE_TYPE.OUT)
            {
                gSql.InsertPrvTrueZoneState(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE);
            }
        }

        public void InsertTempReport(int ref_id, int zone_id)
        {
            gSql.InsertTrue2TempReport(ref_id, zone_id);
        }
    }
}
