using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Data;
using ZebraDataInterface;
using System.Collections;
using ZebraCommonInterface;
using System.Threading;
using System.IO;
using MongoDB.Bson;
using MongoDB.Driver;
using ZebraNoSQLLib;

namespace ZebraDataServer
{
    public struct ExEventCompleteStruc
    {
        public double speeding_elapsed_time_maximum;
        public double idle_elapsed_time_maximum;
        public double fencing_elapsed_time_maximum;
        public double zone_in_curfew_elapsed_time_maximum;
        public double routing_elapsed_time_maximum;
        public double location_elapsed_time_maximum;
        public double eta1_elapsed_time_maximum;
        public double eta2_elapsed_time_maximum;
        public double io_status_elapsed_time_maximum;
        public double engine_on_elapsed_time_maximum;
        public double speed_hazard_elapsed_time_maximum;
        public double angle_hazard_elapsed_time_maximum;
        public double temperature_elapsed_time_maximum;
        public double engine_onoff_inout_zone_elapsed_time_maximun;
        public double true_fencing_elapsed_time_maximum;
        public double true_io_elapsed_time_maximum_;
        public double DFM_io_status_elapsed_time_maximum;
        public double zone_in_curfew_with_speed_elapsed_time_maximum;
        public double magnetic_elapsed_time_maximum;
    }

    public delegate void ProcessCompleted(Object sender, ExEventCompleteStruc msg);
    public delegate void ElapedTimeProcess(string zMsg);

    public class ExEvent : MarshalByRefObject, DataInterface
    {
        public event ProcessCompleted OnProcessCompleted;
        public event ElapedTimeProcess OnProcessTime;

        private KTLogMsg _ktLog;
        public KTLogMsg KtLog
        {
            set { _ktLog = value; }
            get { return _ktLog; }
        }

        #region --- local variables ---------------------------------------------------------------
        int g_ref_idx;
        int g_veh_id;
        String g_dtTimestampe;
        float g_lat;
        float g_lon;
        int g_speed;
        int g_course;
        int g_type_of_fix;
        int g_no_of_satellite;
        int g_in_out_status;
        int g_analog_level;
        string g_type_of_msg;
        String g_tag_msg;
        DateTime g_dtTimeStamp;
        #endregion

        bool IsEnableBroadcastQ = true;
        bool IsEnableBroadcastQTRUE = true;
        CommonQeue SMSQ;
        CommonQeue EMAILQ;
        Hashtable hzTrueZone;

        private int ElapedTime = 100;

        #region --- declare objects ---------------------------------------------------------------
        SQL oSql;
        SpeedingCls oSpeed;
        IdlingCls oIdling;
        FencingCls oFencing;
        ZoneCurfewCls oZoneCurfew;
        RoutingCls oRouting;
        LocationCls oLoc;
        ETACls oETACls1;
        ETACls oETACls2;
        EngineOnEventCls oEngineOnEvt;
        TemperatureEvtCls oTemperatureEvt;
        SpeedHazardCls oSpeedHazard;
        AngleHazardCls oAngleHazard;
        TrueZoneAlertCls oTrueZone;
        TrueIOAlertCls oTrueIO;
        TrueZoneAlertCls2 oTrueZone2;
        DFMIOStatusCls oDMFIO;
        ZoneCurfewWithSpeedCls oZoneCurfewWithSpeed;
        EngineOnOffInOutZoneCls oEngineOnOffInOutZoneObj;
        UpdateLog oUpdateLog;
        MagneticCard oMagneTic;
        SmartCard oSmartCard;
      //  DistanceCls oDistCls;
        GetDataMongo oMogoEvent;
      //  MongoDBDocumentOperators
        MongoDBDocumentOperators oMongoDoc;
        #endregion

        public ExEvent()
        {
            Properties.Settings pf = new ZebraDataServer.Properties.Settings();

            IsEnableBroadcastQ = pf.EnableBroadcastQ;
            IsEnableBroadcastQTRUE = pf.EnableBroadcastQ_TRUE;
            SMSQ = new CommonQeue(pf.Broadcast_SMS_Q);
            EMAILQ = new CommonQeue(pf.Broadcast_EMAIL_Q);

            oSql = new SQL(pf.DBSever);
            oSpeed = new SpeedingCls(pf.DBSever);
            oIdling = new IdlingCls(pf.DBSever);
            oFencing = new FencingCls(pf.DBSever);
            oZoneCurfew = new ZoneCurfewCls(pf.DBSever);
            oRouting = new RoutingCls(pf.DBSever);
            oLoc = new LocationCls(pf.DBSever);
            oETACls1 = new ETACls(pf.DBSever);
            oETACls2 = new ETACls(pf.DBSever);
            oEngineOnEvt = new EngineOnEventCls(pf.DBSever);
            oTemperatureEvt = new TemperatureEvtCls(pf.DBSever);
            oSpeedHazard = new SpeedHazardCls(pf.DBSever);
            oAngleHazard = new AngleHazardCls(pf.DBSever);

            oTrueZone = new TrueZoneAlertCls(pf.DBSever);
            oTrueZone2 = new TrueZoneAlertCls2(pf.DBSever);
            oTrueIO = new TrueIOAlertCls(pf.DBSever);
            oDMFIO = new DFMIOStatusCls(pf.DBSever);
           
            oEngineOnOffInOutZoneObj = new EngineOnOffInOutZoneCls(pf.DBSever);

            oZoneCurfewWithSpeed = new ZoneCurfewWithSpeedCls(pf.DBSever);
            oUpdateLog = new UpdateLog(pf.DBSever);
            oMagneTic = new MagneticCard(pf.DBSever);
           // oDistCls = new DistanceCls(pf.DbSever);

            oSmartCard = new SmartCard(pf.DBSever);
            oMogoEvent = new GetDataMongo(pf.DBSever);
            oMongoDoc = new MongoDBDocumentOperators(pf.MongoServerPort);
            //Cloen Hs
            hzTrueZone = (Hashtable)GlobalClass.hsZoneTrueList.Clone();
        }
        public void DetectEvent(int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg, string tag_msg,DateTime time_stamp)
        {
            #region To initialize variables
            g_ref_idx = -1;
            g_veh_id = veh_id;
            g_dtTimestampe = dtTimestampe;
            g_lat = lat;
            g_lon = lon;
            g_speed = speed;
            g_course = course;
            g_type_of_fix = type_of_fix;
            g_no_of_satellite = no_of_satellite;
            g_in_out_status = in_out_status;
            g_analog_level = analog_level;
            g_type_of_msg = type_of_msg;
            g_tag_msg = tag_msg;
            g_dtTimeStamp = time_stamp;

            DateTime dateTime1;
            DateTime dateTime2;
            TimeSpan totalElapedTime;

            ExEventCompleteStruc retMsg = new ExEventCompleteStruc();
            #endregion

            #region --- Event

            try
            {
                //ok
                if (g_type_of_fix == 0 || g_no_of_satellite < 3)
                {
                    dateTime1 = DateTime.Now;
                    UpdateLogError();
                    dateTime2 = DateTime.Now;
                    totalElapedTime = dateTime2 - dateTime1;
                    //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "UpdateLogError", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                    Thread.Sleep(10);
                }
                
                //ok
                dateTime1 = DateTime.Now;
                GetNPutLocation();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                ////EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "GetNPutLocation", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.location_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectIdling();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                ////EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "Idling", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.idle_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectSpeeding();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                ////EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "Speeding", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.speeding_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectFencing();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                ////EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "Fencing", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.fencing_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectZoneCurfew();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                ////EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "ZoneCurfew", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.zone_in_curfew_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectZoneCurfewWithSpeed();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                ////EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "ZoneCurfewWithSpeed", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.zone_in_curfew_with_speed_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectRouting();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                ////EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "Routing", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.routing_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);
                
                /*
                dateTime1 = DateTime.Now;
                DetectETA1();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "ETA1", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.eta1_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);
                
                dateTime1 = DateTime.Now;
                DetectETA2();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "ETA2", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.eta2_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);
                */

                //ok
                dateTime1 = DateTime.Now;
                DetectEngineOnEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "EngineOnEvent", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.engine_on_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectSpeedHazard();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "SpeedHazard", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.speed_hazard_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectAngleHazard();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "AngleHazard", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.angle_hazard_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectTemperatureEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "TemperatureEvent", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.temperature_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectEngineOnOffInOutZone();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "EngineOnOffInOutZone", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.engine_onoff_inout_zone_elapsed_time_maximun = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //ok
                dateTime1 = DateTime.Now;
                DetectMagneticEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "MagneticCard", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.magnetic_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);

                //dateTime1 = DateTime.Now;
                DetectSmartCardEvent();
                //dateTime2 = DateTime.Now;
                //Thread.Sleep(10);

                /*dateTime1 = DateTime.Now;
                DetectFencingForTrueCorp();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "TrueFencing", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.true_fencing_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);*/

                /*dateTime1 = DateTime.Now;
                DetectTrueIOStatus();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //EnQueueElapsedTime((int)totalElapedTime.TotalMilliseconds, "TrueIO", totalElapedTime.TotalMilliseconds.ToString(), veh_id);
                retMsg.true_io_elapsed_time_maximum_ = totalElapedTime.TotalMilliseconds;
                Thread.Sleep(10);*/
            }
            catch 
            {
            }
            #endregion
            OnProcessCompleted(this, retMsg);
        }

        #region Event 
        private void UpdateLogError()
        {
            double last_lat = 0.00;
            double last_lon = 0.00;
            bool lastRecord = false;
            int no_of_Satellite = 0;
            DataTable LastData = new DataTable();
            string lat_new = string.Empty;
            string lon_new = string.Empty;

            try
            {
                LastData = oUpdateLog.GetLastLatLon(g_veh_id);

                foreach (DataRow r in LastData.Rows)
                {
                    last_lat = (double)r["lat"];
                    last_lon = (double)r["lon"];
                    no_of_Satellite = (int)r["no_of_satellite"];

                    lat_new = String.Format("{0:F5}", last_lat);
                    lon_new = String.Format("{0:F5}", last_lon);
                    lastRecord = true;
                }

                if (lastRecord)
                {
                    _ktLog.log_error = new DataError();
                    _ktLog.log_error.lat = g_lat;
                    _ktLog.log_error.lon = g_lon;
                    _ktLog.log_error.sat = g_no_of_satellite;
                    _ktLog.lat = g_lat = float.Parse(lat_new);
                    _ktLog.lon = g_lon = float.Parse(lon_new);
                }
             }
            catch 
            { }
        }
        private void GenaralFunction()
        {
            _ktLog.energy = new Energy();
            _ktLog.energy.oil1 = 0.0;
            _ktLog.energy.oil2 = 0.0;
            _ktLog.energy.oil3 = 0.0;
        }
        private void GetNPutLocation()
        {
            string location_e = "NO GPS";
            string location_t = "ไม่มีสัญญาน GPS";
            try
            {
                if (_ktLog.loc_t.Length > 10 && _ktLog.loc_e.Length > 10 && g_type_of_fix > 0 && g_no_of_satellite > 3) { return; }

                if (g_lon > 0.0 && g_lat > 0.0)
                {
                    //Detail
                    int id = oLoc.DetectCustomLandmark(g_lon, g_lat);
                    if (id > -1)
                    {
                        location_e = oLoc.GetCustomLandmarkName(id, 0);
                        location_t = oLoc.GetCustomLandmarkName(id, 1);
                    }
                    else
                    {
                        id = oLoc.DetectLandmark(g_lon, g_lat);
                        location_e = oLoc.GetLandmarkName(id, 0);
                        location_t = oLoc.GetLandmarkName(id, 1);
                    }
                    //ตำบล อำเภอ จังหวัด 
                    id = oLoc.DetectAdminPoly(g_lon, g_lat);
                    location_e += " " + oLoc.GetPolyName(id, 0);
                    location_t += " " + oLoc.GetPolyName(id, 1);

                    location_e = location_e.TrimEnd(new char[] { ' ' });
                    location_t = location_t.TrimEnd(new char[] { ' ' });

                    location_e = location_e.TrimStart(new char[] { ' ' });
                    location_t = location_t.TrimStart(new char[] { ' ' });
                }
            }
            catch
            {
            }
            try
            {
                _ktLog.loc_t = location_t;
                _ktLog.loc_e = location_e;
            }
            catch
            { }
        }
        private void DetectIdling()
        {
            //--- Check Speed < 6
            if (g_speed < 6)
            {
                DataTable fzTable = oIdling.GetVehIdleTime(g_veh_id);
                bool is_idle = false;

                try
                {
                    foreach (DataRow r in fzTable.Rows)
                    {
                        double cur_lat = (double)r["lat"];//g_lat;//(double)r["lat"];
                        double cur_lon =  (double)r["lon"];// g_lon;//(double)r["lon"];
                        DateTime cur_time = (DateTime)r["current_local_timestamp"];

                        if (cur_lat > 0.0 && cur_lon > 0.0)
                        {
                            int delay = (int)r["idle_time"];
                            DateTime t_start = cur_time.AddMinutes(-(delay - 1)); //(DateTime)r["rec_timestamp"];
                            DateTime t_end = g_dtTimeStamp;
                            List<KTLogMsg> tempLog = oMongoDoc.GetLogMsg(g_veh_id, Int64.Parse(t_start.ToString("yyyyMMddHHmmss")), Int64.Parse(t_end.ToString("yyyyMMddHHmmss")));

                            double sum_lat = 0.0;
                            double sum_lon = 0.0;

                            DateTime start_time = g_dtTimeStamp.AddHours(1);
                            DateTime end_time = t_end;

                            if (tempLog.Count > 0)
                            {
                                foreach (KTLogMsg it in tempLog)
                                {
                                    sum_lat += it.lat;
                                    sum_lon += it.lon;

                                    if (start_time > it.timestamp)
                                    {
                                        start_time = it.timestamp;
                                    }
                                    if (it.speed > 6) { goto GT_Break; }
                                    if (it.list_event.Count > 0)
                                    {
                                        foreach (Events e in it.list_event)
                                        {
                                            if (e.evt_id == 22 || e.evt_id == 23 || e.evt_id == 3)
                                            {
                                                goto GT_Break;
                                            }
                                        }
                                    }
                                }

                                TimeSpan delayInLog = end_time - start_time;

                                double tmp_lat = (sum_lat / ((double)tempLog.Count)) - cur_lat;
                                double tmp_lon = (sum_lon / ((double)tempLog.Count)) - cur_lon;

                                if (tmp_lat < 0.0)
                                    tmp_lat = tmp_lat * -1;
                                if (tmp_lon < 0.0)
                                    tmp_lon = tmp_lat * -1;

                                if (tmp_lat < 0.0003 && tmp_lon < 0.0003 && delayInLog.TotalMinutes >= delay)
                                {
                                    is_idle = true;
                                }
                            }
                        }
                    GT_Break:
                        { }

                        if (is_idle)
                        {
                            InsertNewRecord(EVT_TYPE.IDLE);
                        }
                    }
                }
                catch { }
            }
        }
        private void DetectSpeeding()
        {
            try
            {
                DataTable dtTemp = oSpeed.GetSpeedSetting(g_veh_id);
                bool is_rec = false;
                foreach (DataRow r in dtTemp.Rows)
                {
                    DateTime cur_time2 = (DateTime)r["cur_time"];
                    int max_sp = (int)r["max_sp"];
                    int delay = (int)r["delay"];

                    if (g_speed >= max_sp)
                    {
                        if (delay > 0)
                        {
                            DateTime t_start = cur_time2.AddMinutes(-(delay-1));
                            DateTime t_end = g_dtTimeStamp;

                            List<KTLogMsg> tempLog = oMongoDoc.GetLogMsg(g_veh_id, Int64.Parse(t_start.ToString("yyyyMMddHHmmss")), Int64.Parse(t_end.ToString("yyyyMMddHHmmss")));

                            DateTime start_time = g_dtTimeStamp.AddHours(1);
                            DateTime end_time = t_end;

                            foreach (KTLogMsg info in tempLog)
                            {
                                if (start_time > info.timestamp)
                                {
                                    start_time = info.timestamp;
                                }

                                if (info.list_event .Count > 0)
                                {
                                    foreach (Events e in info.list_event)
                                    {
                                        if (e.evt_id == 2 || e.evt_id == 23 || e.evt_id == 22)
                                        {
                                            goto GT_Break;
                                        }
                                    }
                                }

                                if (info.speed < max_sp)
                                {
                                    goto GT_Break;
                                }
                            }

                            TimeSpan spTime = end_time - start_time;
                            if (spTime.TotalMinutes >= delay)
                            {
                                is_rec = true;
                            }
                        }
                        else
                        {
                            is_rec = true;
                            break;
                        }
                    }
                }

            GT_Break:
                { }

                if (is_rec)
                {
                    InsertNewRecord(EVT_TYPE.SPEEDING);

                    if (IsEnableBroadcastQ)
                    {
                        ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                        info.idx = g_ref_idx;
                        info.type = (int)EVT_TYPE.SPEEDING;
                        info.veh_id = g_veh_id;
                        info.msg = g_speed.ToString();
                        info.timestamp = g_dtTimestampe;
                        info.location_t = _ktLog.loc_t;
                        info.location_e = _ktLog.loc_e;
                        info.lat = g_lat;
                        info.lon = g_lon;
                        info.speed = g_speed;
                        SMSQ.PushQ(info);
                        EMAILQ.PushQ(info);
                    }
                }
            }
            catch
            { }
        }
        private void DetectFencing()
        {
            //sql ok
            DataTable fzTable = oFencing.GetFleetZoneByVeh(g_veh_id);
            try
            {
                foreach (DataRow fz_r in fzTable.Rows)
                {
                    DataTable pfTable = oFencing.GetZoneProfile((int)fz_r["zone_id"]);

                    foreach (DataRow pf_row in pfTable.Rows)
                    {
                        int zone_id = (int)fz_r["zone_id"];
                        double lat_min = (double)pf_row["lat_min"];
                        double lat_max = (double)pf_row["lat_max"];
                        double lon_min = (double)pf_row["lon_min"];
                        double lon_max = (double)pf_row["lon_max"];
                        Boolean clkdir = (Boolean)pf_row["clockwise"];
                        DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                        FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;
                        //sql ok
                        if (oFencing.DetectFencing(g_veh_id, zone_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, clkdir, lastedUpdate, out side))
                        {
                            int last_zone_id = -1;
                            int last_evt_id = -1;
                            //sql ok
                            oFencing.GetLastZoneStatus(g_veh_id, zone_id, out last_evt_id, out last_zone_id);
                            if (side == FencingCls.SIDE_TYPE.IN && last_evt_id == (int)EVT_TYPE.IN_ZONE && last_zone_id == zone_id)
                            {
                            }
                            else if (side == FencingCls.SIDE_TYPE.OUT && last_evt_id == (int)EVT_TYPE.OUT_ZONE && last_zone_id == zone_id)
                            {
                            }
                            else
                            {
                                bool bIsRecord = false;
                                if (last_evt_id != -1)
                                {
                                    bIsRecord = true;
                                }
                                else if ((last_evt_id == -1) && (side == FencingCls.SIDE_TYPE.IN))
                                {
                                    bIsRecord = true;
                                }

                                if (bIsRecord)
                                {
                                    //Detect Fencing KPI
                                     /*oFencing.KPICalculatorZoneByVeh(g_ref_idx, g_veh_id, zone_id);*/

                                    //Calculator Zone Insert
                                    /*oFencing.InsertMsgZone(g_ref_idx, zone_id);*/
                                    /*oFencing.InsertNewRecord(g_ref_idx, g_veh_id, zone_id);*/

                                    if (oFencing.GSide == FencingCls.SIDE_TYPE.IN)
                                    {
                                        InsertNewRecordZone(EVT_TYPE.IN_ZONE, zone_id);
                                        //sql ok
                                        oFencing.UpsertPrvZoneState(g_veh_id, zone_id, (int)EVT_TYPE.IN_ZONE);
                                    }
                                    else if (oFencing.GSide == FencingCls.SIDE_TYPE.OUT)
                                    { 
                                        InsertNewRecordZone(EVT_TYPE.OUT_ZONE, zone_id);
                                        //sql ok
                                        oFencing.UpsertPrvZoneState(g_veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE);
                                    }

                                    if (IsEnableBroadcastQ)
                                    {
                                        ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                                        info.idx = g_ref_idx;
                                        info.type = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.IN_ZONE : (int)EVT_TYPE.OUT_ZONE;
                                        info.veh_id = g_veh_id;
                                        info.msg = zone_id.ToString();
                                        info.timestamp = g_dtTimestampe;
                                        info.location_t = _ktLog.loc_t;
                                        info.location_e = _ktLog.loc_e;
                                        info.lat = g_lat;
                                        info.lon = g_lon;
                                        info.speed = g_speed;
                                        SMSQ.PushQ(info);
                                        EMAILQ.PushQ(info);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            { }
        }
        private void DetectZoneCurfew()
        {
            bool IsRecord = false;
            int old_zone_id = -1;
            FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;

            //sql ok
            DataTable fzTable = oZoneCurfew.GetFleetZoneCurfewByVeh(g_veh_id);
            try
            {
                foreach (DataRow fz_r in fzTable.Rows)
                {
                    IsRecord = false;

                    int r_zone_id = Convert.ToInt32((String)fz_r["zone_id"]);
                    int r_zone_type = Convert.ToInt32((String)fz_r["zone_type_id"]);
                    int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                    String[] st = fz_r["start_time"].ToString().Split(new char[] { ' ' });
                    String r_start_time = DateTime.Parse(st[st.Length -1]).ToLongTimeString();
                    String[] ed = fz_r["end_time"].ToString().Split(new char[] { ' ' });
                    String r_end_time = DateTime.Parse(ed[ed.Length - 1]).ToLongTimeString();
                    int delay_time = Convert.ToInt32((String)fz_r["delay_time"]);

                    String now = DateTime.Now.ToLongTimeString();

                    switch (r_day)
                    {
                        case 1: r_day = 7; break;
                        case 2: r_day = 1; break;
                        case 3: r_day = 2; break;
                        case 4: r_day = 3; break;
                        case 5: r_day = 4; break;
                        case 6: r_day = 5; break;
                        case 7: r_day = 6; break;
                    }

                    if (!(r_day == (int)DateTime.Now.DayOfWeek && DateTime.Parse(r_start_time) <= DateTime.Parse(now) && DateTime.Parse(now) <= DateTime.Parse(r_end_time)))
                    {
                        continue;
                    }

                    if (old_zone_id != r_zone_id)
                    {
                        //sql ok Exten class Zone  
                        DataTable pfTable = oZoneCurfew.GetZoneProfile(r_zone_id);
                        foreach (DataRow pf_row in pfTable.Rows)
                        {
                            int zone_id = r_zone_id;
                            double lat_min = (double)pf_row["lat_min"];
                            double lat_max = (double)pf_row["lat_max"];
                            double lon_min = (double)pf_row["lon_min"];
                            double lon_max = (double)pf_row["lon_max"];
                            Boolean clkdir = (Boolean)pf_row["clockwise"];
                            DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                            side = FencingCls.SIDE_TYPE.NONE;
                            //sql ok Exten class Zone  
                            if (oZoneCurfew.DetectFencing(g_veh_id, zone_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, clkdir, lastedUpdate, out side))
                            {
                                int last_zone_id = -1;
                                int last_evt_id = -1;
                                int mark = -1;
                                DateTime last_local_timestamp = new DateTime();

                                //sql ok Exten class Zone 
                                oZoneCurfew.GetLastZoneCurfewStatus(g_veh_id, zone_id, out last_evt_id, out last_zone_id, out last_local_timestamp, out mark);
                                if ((side == FencingCls.SIDE_TYPE.IN) && 
                                    (last_evt_id == (int)EVT_TYPE.NO_INZONE_CURFEW) &&
                                    (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;
                                        if ((int)sp.TotalMinutes > delay_time && mark == 0)
                                        {
                                            IsRecord = true;
                                        }
                                    }
                                }
                                else if ((side == FencingCls.SIDE_TYPE.OUT) && 
                                    (last_evt_id == (int)EVT_TYPE.NO_OUTZONE_CURFEW) && 
                                    (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;

                                        if (sp.TotalMinutes > (double)delay_time && mark == 0)
                                        {
                                            IsRecord = true;
                                        }
                                    }
                                }
                                else
                                {
                                    oZoneCurfew.UpSertZoneCurfewState(g_veh_id, r_zone_id, 0, g_dtTimeStamp);
                                }
                            }
                        }
                    }
                    else
                    {
                    }

                    if (IsRecord)
                    {
                        oZoneCurfew.UpSertZoneCurfewState(g_veh_id, r_zone_id, 1, g_dtTimeStamp);

                        if (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN)
                        {
                            InsertNewRecordEventBackSide(EVT_TYPE.NO_INZONE_CURFEW, EVT_TYPE.NONE, SOURE_EVENT.ZONEING, r_zone_id);
                        }
                        else if (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT)
                        {
                            InsertNewRecordEventBackSide(EVT_TYPE.NO_OUTZONE_CURFEW, EVT_TYPE.NONE, SOURE_EVENT.ZONEING, r_zone_id);
                        }
                    
                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = g_ref_idx;
                            info.type = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.NO_INZONE_CURFEW : (int)EVT_TYPE.NO_OUTZONE_CURFEW;
                            info.veh_id = g_veh_id;
                            info.msg = r_zone_id.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t = _ktLog.loc_t;
                            info.location_e = _ktLog.loc_e;
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }

                    IsRecord = false;
                    old_zone_id = r_zone_id;
                }
            }
            catch
            { }
        }
        private void DetectZoneCurfewWithSpeed()
        {
            bool IsRecord = false;
            int old_zone_id = -1;
            FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;

            //sql ok
            DataTable fzTable = oZoneCurfewWithSpeed.GetFleetZoneCurfewWithSpeedByVeh(g_veh_id);
            try
            {
                foreach (DataRow fz_r in fzTable.Rows)
                {
                    IsRecord = false;

                    int r_zone_id = Convert.ToInt32((String)fz_r["zone_id"]);
                    int r_zone_type = Convert.ToInt32((String)fz_r["zone_type_id"]);
                    int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                    String[] st = fz_r["start_time"].ToString().Split(new char[] { ' ' });
                    String r_start_time = DateTime.Parse(st[st.Length - 1]).ToLongTimeString();
                    String[] ed = fz_r["end_time"].ToString().Split(new char[] { ' ' });
                    String r_end_time = DateTime.Parse(ed[ed.Length -1]).ToLongTimeString();
                    int r_delay_time = Convert.ToInt32((String)fz_r["delay_time"]);
                    int r_speed = Convert.ToInt32((String)fz_r["speed"]);

                    String now = DateTime.Now.ToLongTimeString();

                    switch (r_day)
                    {
                        case 1: r_day = 7; break;
                        case 2: r_day = 1; break;
                        case 3: r_day = 2; break;
                        case 4: r_day = 3; break;
                        case 5: r_day = 4; break;
                        case 6: r_day = 5; break;
                        case 7: r_day = 6; break;
                    }

                    if (!(r_day == (int)DateTime.Now.DayOfWeek && DateTime.Parse(r_start_time) <= DateTime.Parse(now) && DateTime.Parse(now) <= DateTime.Parse(r_end_time)))
                    {
                        continue;
                    }

                    if (old_zone_id != r_zone_id)
                    {
                        DataTable pfTable = oZoneCurfewWithSpeed.GetZoneProfile(r_zone_id);
                        foreach (DataRow pf_row in pfTable.Rows)
                        {
                            int zone_id = r_zone_id;
                            double lat_min = (double)pf_row["lat_min"];
                            double lat_max = (double)pf_row["lat_max"];
                            double lon_min = (double)pf_row["lon_min"];
                            double lon_max = (double)pf_row["lon_max"];
                            Boolean clkdir = (Boolean)pf_row["clockwise"];
                            DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                            side = FencingCls.SIDE_TYPE.NONE;
                            //sql ok Exten 
                            if (oZoneCurfewWithSpeed.DetectFencing(g_veh_id, zone_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, clkdir, lastedUpdate, out side))
                            {
                                int last_zone_id = -1;
                                int last_evt_id = -1;
                                int mark = -1;
                                int last_speed = -1;
                                DateTime last_local_timestamp = new DateTime();

                                //sql ok
                                oZoneCurfewWithSpeed.GetLastZoneCurfewWithSpeedStatus(g_veh_id, zone_id, out last_evt_id, out last_zone_id, out last_local_timestamp, out mark, out last_speed);
                                if ((side == FencingCls.SIDE_TYPE.IN) && (last_evt_id == (int)EVT_TYPE.IN_ZONE) && (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;
                                        if (((int)sp.TotalMinutes > r_delay_time && mark == 0) && (g_speed >= r_speed))
                                        {
                                            IsRecord = true;
                                        }
                                    }
                                }
                                else if ((side == FencingCls.SIDE_TYPE.OUT) && (last_evt_id == (int)EVT_TYPE.OUT_ZONE) && (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;

                                        if ((sp.TotalMinutes > (double)r_delay_time && mark == 0) && (g_speed >= r_speed))
                                        {
                                            IsRecord = true;
                                        }
                                    }
                                }
                                else
                                {
                                    oZoneCurfewWithSpeed.UpSertZoneCurfewWithSpeedState(g_veh_id, r_zone_id, 0, g_speed, g_dtTimeStamp);
                                }
                            }
                        }
                    }

                    if (IsRecord)
                    {
                        //sql ok
                        oZoneCurfewWithSpeed.UpSertZoneCurfewWithSpeedState(g_veh_id, r_zone_id, 1, g_speed, g_dtTimeStamp);
                       
                        if (oZoneCurfewWithSpeed.GSide == FencingCls.SIDE_TYPE.IN)
                        {
                            InsertNewRecordEventBackSide(EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED, EVT_TYPE.NONE, SOURE_EVENT.ZONEING, r_zone_id);
                        }
                        else if (oZoneCurfewWithSpeed.GSide == FencingCls.SIDE_TYPE.OUT)
                        {
                            InsertNewRecordEventBackSide(EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED,EVT_TYPE.NONE, SOURE_EVENT.ZONEING, r_zone_id);
                        }

                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = g_ref_idx;
                            info.type = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED : (int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED;
                            info.veh_id = g_veh_id;
                            info.msg = r_zone_id.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t= _ktLog.loc_t;
                            info.location_e = _ktLog.loc_e;
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }

                    IsRecord = false;
                    old_zone_id = r_zone_id;
                }
            }
            catch
            { }
        }
        private void DetectRouting()
        {
            //sql ok
            DataTable fzTable = oRouting.GetRouteByVeh(g_veh_id);
            try
            {
                float d_min = 99999.99F;
                int rec_route_id = -1;
                bool IsOutRoute = false;

                foreach (DataRow fz_r in fzTable.Rows)
                {
                    //sql ok
                    DataTable pfTable = oRouting.GetRouteProfile((int)fz_r["route_id"]);
                    foreach (DataRow pf_row in pfTable.Rows)
                    {
                        //sql ok
                        DataTable wpTable = oRouting.GetRouteWayPoints((int)fz_r["route_id"]);

                        int route_id = (int)fz_r["route_id"];
                        double lat_min = (double)pf_row["lat_min"];
                        double lat_max = (double)pf_row["lat_max"];
                        double lon_min = (double)pf_row["lon_min"];
                        double lon_max = (double)pf_row["lon_max"];
                        int dist = (int)pf_row["distance"];
                        DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                        DataTable wp = new DataTable();
                        wp.Columns.Add("X", typeof(float));
                        wp.Columns.Add("Y", typeof(float));
                        foreach (DataRow wp_row in wpTable.Rows)
                        {
                            double wpX = (double)wp_row["lon"];
                            double wpY = (double)wp_row["lat"];
                            wp.Rows.Add(new object[] { wpX, wpY });
                        }

                        float d = 99999.0F;
                        oRouting.DetectOffRoad(g_veh_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, lastedUpdate, wp, dist, out d);
                        d_min = Math.Min(d_min, d);
                        if (d_min == d)
                        {
                            rec_route_id = route_id;
                            d *= 1000;
                            if (d <= dist)
                            {
                                IsOutRoute = false;
                            }
                            else
                            {
                                IsOutRoute = true;
                            }
                        }
                    }
                }

                bool bIsBroadcast = false;
                EVT_TYPE evtToBroadcast = EVT_TYPE.IN_ROUTE;

                if (rec_route_id > -1)
                {
                    int last_route_id = -1;
                    int last_evt_id = -1;

                    //sql ok
                    oRouting.GetLastRouteStatus(g_veh_id, rec_route_id, out last_evt_id, out last_route_id);

                    //20    IN ROUTE
                    //21    OUT ROUTE
                    if (last_evt_id == -1 && !IsOutRoute)
                    {
                        //sql ok
                        oRouting.UpSertRouteState(g_veh_id, rec_route_id, IsOutRoute ? EVT_TYPE.OUT_ROUTE : EVT_TYPE.IN_ROUTE);
                        InsertNewRecordRoute(IsOutRoute ? EVT_TYPE.OUT_ROUTE : EVT_TYPE.IN_ROUTE, rec_route_id);
                        bIsBroadcast = true;
                        evtToBroadcast = IsOutRoute ? EVT_TYPE.OUT_ROUTE : EVT_TYPE.IN_ROUTE;
                    }
                    if (IsOutRoute && last_evt_id == (int)EVT_TYPE.IN_ROUTE)//อยู่ใน && ก่อนหน้า อยู่นอก
                    {
                        //sql ok
                        oRouting.UpSertRouteState(g_veh_id, rec_route_id, EVT_TYPE.OUT_ROUTE);
                        InsertNewRecordRoute(EVT_TYPE.OUT_ROUTE, rec_route_id);
                        bIsBroadcast = true;
                        evtToBroadcast = EVT_TYPE.OUT_ROUTE;
                    }
                    else if (!IsOutRoute && last_evt_id == (int)EVT_TYPE.OUT_ROUTE) // อยู่นอก && ก่อนหน้า อยู่ใน
                    {
                        //sql ok
                        oRouting.UpSertRouteState(g_veh_id, rec_route_id, EVT_TYPE.IN_ROUTE);
                        InsertNewRecordRoute(EVT_TYPE.IN_ROUTE, rec_route_id);
                        bIsBroadcast = true;
                        evtToBroadcast = EVT_TYPE.IN_ROUTE;
                    }

                    if (bIsBroadcast)
                    {
                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = g_ref_idx;
                            info.type = (int)evtToBroadcast;
                            info.veh_id = g_veh_id;
                            info.msg = rec_route_id.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t = _ktLog.loc_t;
                            info.location_e = _ktLog.loc_e;
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }
                }
            }
            catch
            { }
        }
        private void DetectETA1()
        {
            
            float dtTimeLeft = 100.0F;
            ArrayList etaList = oETACls1.CalculateETA11(g_veh_id, g_lat, g_lon, g_speed, out dtTimeLeft);
            if (etaList == null || etaList.Count < 1)
            {
                return;
            }

            foreach (ETAINFO info in etaList)
            {
                oETACls1.InsertMsgETA(g_ref_idx, info.eta_id, info.eta_time_left, info.destination_zone_id);
                oETACls1.InsertNewRecord(g_ref_idx);
            }
             
        }
        private void DetectETA2()
        {
            /*
            float dtTimeLeft = 100.0F;
            ArrayList etaList = oETACls2.CalculateETA2(g_veh_id, g_lat, g_lon, g_speed, out dtTimeLeft);
            if (etaList == null || etaList.Count < 1)
            {
                return;
            }

            foreach (ETAINFO info in etaList)
            {
                oETACls2.InsertMsgETA(g_ref_idx, info.eta_id, info.eta_time_left, info.destination_zone_id);
                oETACls2.InsertNewRecord(g_ref_idx);
            }
             * */
        }
        private void DetectEngineOnEvent()
        {
            bool IsRecord = false;

            try
            {
                int last_evt_id = -1;
                DateTime last_local_timestamp = DateTime.Now;
                bool is_engine_on = false;
                oEngineOnEvt.GetEngineOnCurfewEvt(g_veh_id, out last_evt_id, out last_local_timestamp);
                if (last_evt_id == (int)EVT_TYPE.ENGINE_ON)
                {
                    is_engine_on = true;
                }
                else
                {
                    return;
                }

                //sql ok
                DataTable feTable = oEngineOnEvt.GetEngineOnEventByVeh(g_veh_id);
                foreach (DataRow fz_r in feTable.Rows)
                {
                    IsRecord = false;

                    int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                    String[] st = fz_r["start_time"].ToString().Split(new char[] { ' ' });
                    String r_start_time = DateTime.Parse(st[st.Length - 1]).ToLongTimeString();
                    String[] ed = fz_r["end_time"].ToString().Split(new char[] { ' ' });
                    String r_end_time = DateTime.Parse(ed[ed.Length - 1]).ToLongTimeString();
                    int delay_time = Convert.ToInt32((String)fz_r["delay_time"]);

                    String now = DateTime.Now.ToLongTimeString();

                    switch (r_day)
                    {
                        case 1: r_day = 7; break;
                        case 2: r_day = 1; break;
                        case 3: r_day = 2; break;
                        case 4: r_day = 3; break;
                        case 5: r_day = 4; break;
                        case 6: r_day = 5; break;
                        case 7: r_day = 6; break;
                    }

                    if (!(r_day == (int)DateTime.Now.DayOfWeek && DateTime.Parse(r_start_time) <= DateTime.Parse(now) && DateTime.Parse(now) <= DateTime.Parse(r_end_time)))
                    {
                        continue;
                    }
                    else
                    {
                        /// check is last event Prohitbit
                        /// sql ok
                        int last_Prohitbit_evt_id;
                        DateTime last_Prohitbit_local_timestamp;
                        oEngineOnEvt.GetLastEngineOnCurfewEventStatus(g_veh_id, (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME, out last_Prohitbit_evt_id, out last_Prohitbit_local_timestamp);
                        
                        if (last_Prohitbit_evt_id == (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME || last_Prohitbit_evt_id == -1)
                        {
                            if (last_evt_id == (int)EVT_TYPE.ENGINE_ON)
                            {
                                if (is_engine_on)
                                {
                                    IsRecord = true;
                                }
                                else if (last_Prohitbit_evt_id == -1)
                                {
                                    DateTime dt = DateTime.Now;
                                    TimeSpan sp = dt - last_local_timestamp;
                                    if (sp.TotalMinutes > delay_time)
                                    {
                                        IsRecord = true;
                                    }
                                }
                                else
                                {
                                    DateTime dt = DateTime.Now;
                                    TimeSpan sp = dt - last_Prohitbit_local_timestamp;
                                    if (sp.TotalMinutes > delay_time)
                                    {
                                        IsRecord = true;
                                    }
                                }
                            }
                        }
                    }

                    if (IsRecord)
                    {
                        InsertNewRecord(EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME);

                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = g_ref_idx;
                            info.type = (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME;
                            info.veh_id = g_veh_id;
                            info.msg = "";
                            info.timestamp = g_dtTimestampe;
                            info.location_t = _ktLog.loc_t;
                            info.location_e = _ktLog.loc_e;
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }
                    IsRecord = false;
                }
            }
            catch
            { }
        }
        private void DetectSpeedHazard()
        {
            if (g_type_of_msg[0] == 'V')
            {
                //oSpeedHazard.InsertNewRecord(g_ref_idx);
                InsertNewRecord(EVT_TYPE.HAZARD_SPEED);
            }
        }
        private void DetectAngleHazard()
        {
            if (g_type_of_msg[0] == 'A')
            {
                //oAngleHazard.InsertNewRecord(g_ref_idx);
                InsertNewRecord(EVT_TYPE.HAZARD_ANGLE);
            }
        }
        private void DetectTemperatureEvent()
        {
            if (_ktLog.option == null && _ktLog.option.optTemp == null) return;

            bool IsRecord = false;

            //sql ok
            DataTable feTable = oTemperatureEvt.GetTemperatureByVeh(g_veh_id);
            try
            {
                foreach (DataRow fz_r in feTable.Rows)
                {
                    IsRecord = false;

                    int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                    int min_temp = Convert.ToInt32(fz_r["min_temp"]);
                    int max_temp = Convert.ToInt32(fz_r["max_temp"]);
                    DateTime start_time = Convert.ToDateTime(fz_r["start_time"]);
                    DateTime end_time = Convert.ToDateTime(fz_r["end_time"]);
                    int delay_time = Convert.ToInt32(fz_r["delay_time"]);

                    int last_temperature = 0;
                    int cur_temperature = 0;

                    TimeSpan sp1 = new TimeSpan(start_time.Hour, start_time.Minute, start_time.Second);
                    TimeSpan sp2 = new TimeSpan(end_time.Hour, end_time.Minute, end_time.Second);
                    TimeSpan spNow = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                    String now = DateTime.Now.ToLongTimeString();
                    if (!(r_day == (int)DateTime.Now.DayOfWeek))
                    {
                        continue;
                    }
                    else if (spNow.TotalMinutes < sp1.TotalMinutes || spNow.TotalMinutes > sp2.TotalMinutes)
                    {
                        continue;
                    }
                    else
                    {
                        int last_evt_id;
                        DateTime last_local_timestamp;

                        oTemperatureEvt.GetLastTemperatureEventStatus(g_veh_id, out last_evt_id, out cur_temperature, out last_local_timestamp);
                       
                        if (last_evt_id == -1 && last_temperature == 0)
                        {
                            continue;
                        }

                        if (last_evt_id == (int)EVT_TYPE.TEMPERATURE || last_evt_id == -1)
                        {
                            if (cur_temperature < min_temp || cur_temperature > max_temp)
                            {
                                if (last_evt_id == -1)
                                {
                                    IsRecord = true;
                                }
                                else
                                {
                                    DateTime dt = DateTime.Now;
                                    TimeSpan sp = dt - last_local_timestamp;
                                    if (sp.TotalMinutes > delay_time)
                                    {
                                        IsRecord = true;
                                    }
                                }
                            }
                        }
                    }

                    if (IsRecord)
                    {
                        InsertNewRecord(EVT_TYPE.TEMPERATURE);

                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = g_ref_idx;
                            info.type = (int)EVT_TYPE.TEMPERATURE;
                            info.veh_id = g_veh_id;
                            info.msg = cur_temperature.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t = _ktLog.loc_t;
                            info.location_e = _ktLog.loc_e;
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }
                    IsRecord = false;
                }
            }
            catch 
            { }
        }
      
        private void DetectEngineOnOffInOutZone()
        {
            try
            {
                //sql ok
                DataTable dt = oEngineOnOffInOutZoneObj.GetFeatureEvtParam(g_veh_id);

                foreach (DataRow r in dt.Rows)
                {
                    string zone_name = r["zone_desc"] as string;
                    int zone_id = (int)r["zone_id"];
                    string zone_type = r["zone_type"] as string;
                    string day = r["day_name"] as string;
                    string start_time = r["start_time"] as string;
                    string end_time = r["end_time"] as string;
                    int engine_status = (int)r["engine_status"];
                    int delay = (int)r["delay_time"];

                    DataTable pfTable = oEngineOnOffInOutZoneObj.GetZoneProfile(zone_id);
                    FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;
                    EVT_TYPE evt = EVT_TYPE.NONE;
                    EVT_TYPE evt_bs = EVT_TYPE.NONE;
                    bool is_rec = false;

                    foreach (DataRow pf_row in pfTable.Rows)
                    {
                        double lat_min = (double)pf_row["lat_min"];
                        double lat_max = (double)pf_row["lat_max"];
                        double lon_min = (double)pf_row["lon_min"];
                        double lon_max = (double)pf_row["lon_max"];
                        Boolean clkdir = (Boolean)pf_row["clockwise"];
                        DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                        if (oEngineOnOffInOutZoneObj.DetectFencing(g_veh_id, zone_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, clkdir, lastedUpdate, out side))
                        {
                            int iDay = Convert.ToInt32(day) - 1;
                            if (iDay < 1) iDay = 7;
                            if ((int)DateTime.Now.DayOfWeek != iDay) continue;

                            DateTime dn = Convert.ToDateTime(DateTime.Now);
                            DateTime st = Convert.ToDateTime(start_time);
                            DateTime en = Convert.ToDateTime(end_time);

                            TimeSpan spStart = new TimeSpan(st.Hour, st.Minute, st.Second);
                            TimeSpan spEnd = new TimeSpan(en.Hour, en.Minute, en.Second);
                            TimeSpan spNow = new TimeSpan(dn.Hour, dn.Minute, dn.Second);

                            if (spStart.TotalMinutes > spNow.TotalMinutes) continue;
                            if (spNow.TotalMinutes > spEnd.TotalMinutes) continue;

                            int last_evt_id = -1;
                            DateTime last_local_timestamp = DateTime.Now.AddDays(-1);
                           
                            //sql ok
                            bool is_engine_on = false;
                            oEngineOnOffInOutZoneObj.GetEngineOnOffEvt(g_veh_id, out last_evt_id, out last_local_timestamp);
                            if (last_evt_id == (int)EVT_TYPE.ENGINE_ON)
                            {
                                is_engine_on = true;
                            }

                            int last_zone_id = -1;
                            int mark = -1;
                            oEngineOnOffInOutZoneObj.GetLastZoneStatus(g_veh_id, zone_id, out last_evt_id, out last_zone_id, out last_local_timestamp, out mark);
                            TimeSpan sp = DateTime.Now - last_local_timestamp;

                            if (engine_status == 1 && side == FencingCls.SIDE_TYPE.IN)
                            {
                                if (((int)sp.TotalMinutes > delay && is_engine_on))
                                {
                                    is_rec = true;
                                    evt = EVT_TYPE.ENGINE_ON_IN_ZONE;
                                    evt_bs = EVT_TYPE.IN_ZONE;
                                }
                            }
                            else if (engine_status == 0 && side == FencingCls.SIDE_TYPE.IN)
                            {
                                if (((int)sp.TotalMinutes > delay && !is_engine_on))
                                {
                                    is_rec = true;
                                    evt = EVT_TYPE.ENGINE_OFF_IN_ZONE;
                                    evt_bs = EVT_TYPE.IN_ZONE;
                                }
                            }
                            else if (engine_status == 1 && side == FencingCls.SIDE_TYPE.OUT)
                            {
                                if (((int)sp.TotalMinutes > delay && is_engine_on))
                                {
                                    is_rec = true;
                                    evt = EVT_TYPE.ENGINE_ON_OUT_ZONE;
                                    evt_bs = EVT_TYPE.OUT_ZONE;
                                }
                            }
                            else if (engine_status == 0 && side == FencingCls.SIDE_TYPE.OUT)//zone_evt_id == (int)EVT_TYPE.OUT_ZONE)
                            {
                                if (((int)sp.TotalMinutes > delay && !is_engine_on))
                                {
                                    is_rec = true;
                                    evt = EVT_TYPE.ENGINE_OFF_OUT_ZONE;
                                    evt_bs = EVT_TYPE.OUT_ZONE;
                                }
                            }

                            if (is_rec)
                            {
                                InsertNewRecordEventBackSide(evt, evt_bs, SOURE_EVENT.ZONEING, zone_id);
                                oEngineOnOffInOutZoneObj.UpSertZoneCurEngineOnOffInOutZoneState(g_veh_id, zone_id, (int)evt, (int)evt_bs, 0, g_dtTimeStamp);
                              
                                if (IsEnableBroadcastQ)
                                {
                                    ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                                    info.idx = g_ref_idx;
                                    info.type = (int)evt;
                                    info.veh_id = g_veh_id;
                                    info.msg = zone_name;
                                    info.timestamp = g_dtTimestampe;
                                    info.location_t = _ktLog.loc_t;
                                    info.location_e = _ktLog.loc_e;
                                    info.lat = g_lat;
                                    info.lon = g_lon;
                                    info.speed = g_speed;
                                    SMSQ.PushQ(info);
                                    EMAILQ.PushQ(info);
                                }
                            }
                        }
                    }

                    //sql ok
                   // oEngineOnOffInOutZoneObj.GetLastZoneStatus(g_veh_id, Convert.ToInt32(zone_id), out evt_id, out last_zone_id);
                }
            }
            catch
            {
            }
        }
        private void DetectMagneticEvent()
        {
            try
            {
                EVT_TYPE event_type = EVT_TYPE.NONE;
                //MC,24,2,0004552,00100
                if ((g_type_of_msg == "D" && g_tag_msg.Length > 0 && g_tag_msg[0] == 'M' && g_tag_msg[1] == 'C') || (g_type_of_msg == "8" && g_tag_msg.Length > 0 ))
                {
                    #region split string
                    int driver_type = -1;
                   // int driver_sex = -1;
                  //  int driver_number = -1;
                  //  int province_code = -1;

                    if (g_type_of_msg == "D")
                    {
                        string[] spit = g_tag_msg.Split(',');
                        if (spit.Length != 5) { return; }
                        driver_type = Convert.ToInt32(spit[1]);
                      //  driver_sex = Convert.ToInt32(spit[2]);
                      //  driver_number = Convert.ToInt32(spit[3]);
                      //  province_code = Convert.ToInt32(spit[4]);
                    }
                    else if (g_type_of_msg == "8")
                    {
                        string[] spit = g_tag_msg.Split(',');
                        if (spit.Length < 6) { return; }

                        driver_type = Convert.ToInt32(spit[3]);
                       // driver_sex = Convert.ToInt32(spit[4]);
                       // driver_number = Convert.ToInt32(spit[5]);
                       // province_code = Convert.ToInt32(spit[6].Substring(0, 3));

                        /*
                        string[] st = tag_msg.Split(new char[] { ',' });
                        string driver_type = st[3];
                        string driver_sex = st[4];
                        string driver_number = st[5];
                        string province_code = st[6].Substring(0,3);
                        string distic_code = st[6].Substring(3,2);
                        string driver_name = st[0];
                        string id_country = st[1].Substring(0,6);
                        string id_card = st[1].Substring(6,13);
                        string expiry_date = st[2].Substring(0,4);
                        string birthday = st[2].Substring(4,8);
                        */
                    }
                    #endregion

                    if (driver_type == 14 || driver_type == 24)
                    {
                        event_type = EVT_TYPE.VALID_CARD;
                        InsertNewRecord(event_type);
                    }
                    else if (driver_type > 0)
                    {
                        event_type = EVT_TYPE.INVALID_CARD;
                        InsertNewRecord(event_type);
                    }
                    else
                    {
                        event_type = EVT_TYPE.CARD_DATA_ERROR;
                        InsertNewRecord(event_type);
                    }
                }
                else
                {
                    if (oMagneTic.GetVehicleMagneticType(g_veh_id))
                    {
                        DateTime dtLastRec = new DateTime();
                        if (!oMagneTic.CheckLastNocard(g_veh_id, out dtLastRec))
                        {
                            int deley = 1000;
                            DataTable dtRet = oMagneTic.GetVehicleMagneticParam(g_veh_id);
                            if (dtRet.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dtRet.Rows)
                                {
                                    deley = Math.Min(deley, Convert.ToInt32(dr["delay"].ToString()));
                                }
                            }

                            if (deley > 0)
                            {
                                TimeSpan sp = Convert.ToDateTime(g_dtTimestampe) - dtLastRec;
                                if (sp.TotalMinutes >= deley && sp.TotalDays < 99)
                                {
                                    event_type = EVT_TYPE.NO_CARD;
                                    InsertNewRecord(event_type);
                                }
                            }
                            else
                            {
                                event_type = EVT_TYPE.NO_CARD;
                                InsertNewRecord(event_type);
                            }
                        }
                    }
                }

                if (IsEnableBroadcastQ && event_type != EVT_TYPE.NONE)
                {
                    ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                    info.idx = g_ref_idx;
                    info.type = (int)event_type;
                    info.veh_id = g_veh_id;
                    info.msg = g_speed.ToString();
                    info.timestamp = g_dtTimestampe;
                    info.location_t = _ktLog.loc_t;
                    info.location_e = _ktLog.loc_e;
                    info.lat = g_lat;
                    info.lon = g_lon;
                    info.speed = g_speed;
                    SMSQ.PushQ(info);
                    EMAILQ.PushQ(info);
                }
            }
            catch
            {
            }
        }
        private void DetectSmartCardEvent()
        {
            try
            {
                if (g_type_of_msg == "D" && g_tag_msg.Length > 0 && g_tag_msg[0] == 'C')
                {
                    SMARTCARDEVENT iAddress1 = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT;
                    string[] spit = g_tag_msg.Split(',');
                    string sAddress2 = "";
                    string sData = "";
                    int _evt_id = -1;
                    string _evt_desc = string.Empty;
                    switch (g_tag_msg[1])
                    {
                        case 'P':/// CP**********,AA,3460200267001   inser   Enanble,Valid
                            {
                                if (spit.Length < 2)  return; 
                                iAddress1 = SMARTCARDEVENT.VALID_SMARTCARD_INSERT;
                                sAddress2 = spit[1];    // AA
                                sData = spit[2];        //3460200267001
                                oSmartCard.GetSmartCardEvent(g_veh_id, g_dtTimeStamp, (int)iAddress1, sAddress2, sData, g_tag_msg, out _evt_id, out _evt_desc);
                            }
                            break;
                        case 'E': /// CE                              insert  Invalid
                            {
                                if (spit.Length != 1)  return; 
                                iAddress1 = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT;
                                oSmartCard.GetSmartCardEvent(g_veh_id, g_dtTimeStamp, (int)iAddress1, sAddress2, sData, g_tag_msg, out _evt_id, out _evt_desc);
                            }
                            break;
                        case 'p':/// Cp**********,AA,3460200267001   insert  Disable,Valid
                            {
                                if (spit.Length < 2) return; 
                                iAddress1 = SMARTCARDEVENT.VALID_BUT_DISABLE_SMART_CARD;
                                sAddress2 = spit[1];    //AA
                                sData = spit[2];        //3460200267001 
                                oSmartCard.GetSmartCardEvent(g_veh_id, g_dtTimeStamp, (int)iAddress1, sAddress2, sData, g_tag_msg, out _evt_id, out _evt_desc);
                            }
                            break;
                        case 'D': /// CD                              remove
                            {
                                if (spit.Length != 1) return;
                                iAddress1 = SMARTCARDEVENT.SMART_CATD_REMOVAL;
                                oSmartCard.GetSmartCardEvent(g_veh_id, g_dtTimeStamp, (int)iAddress1, sAddress2, sData, g_tag_msg, out _evt_id, out _evt_desc);
                            }
                            break;
                    }
                    if (_evt_id != -1 && _evt_desc.Length > 0)
                    {
                        InsertNewRecordSmartEvent(EVT_TYPE.SMARTCARD_DATA, _evt_id, _evt_desc);
                    }
                }
            }
            catch 
            {
            }
        }
        public string GetLocation(float lon, float lat)
        {
            if (oLoc == null)
            {
                return "?";
            }

            //string location_e = "NO LOCATION";
            string location_e = "ไม่มีสัญญาณ GPS";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    //location_e = oLoc.GetCustomLandmarkName(id, 0);
                    location_e = oLoc.GetCustomLandmarkName(id, 1);
                }
                else
                {
                    id = oLoc.DetectLandmark(g_lon, g_lat);
                    //location_e = oLoc.GetLandmarkName(id, 0);
                    location_e = oLoc.GetLandmarkName(id, 1);
                }

                id = oLoc.DetectAdminPoly(g_lon, g_lat);
                //location_e += " " + oLoc.GetPolyName(id, 0);
                location_e += " " + oLoc.GetPolyName(id, 1);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_e = location_e.TrimStart(new char[] { ' ' });
            }

            return location_e;
        }
        public string GetLocation(float lon, float lat, int lang)
        {
            if (oLoc == null)
            {
                return "?";
            }

            string location_e = "NO LOCATION";
            string location_t = "ไม่มีสัญญาณ GPS";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    location_e = oLoc.GetCustomLandmarkName(id, 0);
                    location_t = oLoc.GetCustomLandmarkName(id, 1);
                }
                else
                {
                    id = oLoc.DetectLandmark(lon, lat);
                    location_e = oLoc.GetLandmarkName(id, 0);
                    location_t = oLoc.GetLandmarkName(id, 1);
                }

                id = oLoc.DetectAdminPoly(lon, lat);
                location_e += " " + oLoc.GetPolyName(id, 0);
                location_t += " " + oLoc.GetPolyName(id, 1);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_t = location_t.TrimEnd(new char[] { ' ' });

                location_e = location_e.TrimStart(new char[] { ' ' });
                location_t = location_t.TrimStart(new char[] { ' ' });
            }

            if (lang == 1)
            {
                return location_t;
            }

            return location_e;
        }

        #endregion

        public ObjectId InsertDataToDataBase(KTLogMsg log)
        {
            return oMongoDoc.Insert_KTLogMsg(log);
        }

        public void UpSert_VehCurLocation(int zVehID, DateTime local_timestamp, int speed, Int64 date_id, double temperature,
        string smart_card, double oil01, double oil02, double oil03, string location_t, string location_e)
        {
            oSql.UpSertBox_CurrentLocation(zVehID, local_timestamp, speed, date_id, temperature, smart_card, oil01, oil02, oil03, location_t, location_e);
        }
        public void InsertNewRecord(EVT_TYPE evt)
        {
            Events info = new Events();
            try
            {
                DataTable dtEvent = oMogoEvent.GetEventDesc((int)evt);
                info.evt_id = (int)evt;
                info.event_desc_e = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc"].ToString() : evt.ToString();
                info.event_desc_t = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc_t"].ToString() : evt.ToString();
                info.is_backside = false;

                oSql.UpSertEvent_CurEventState(g_veh_id,(int)evt,_ktLog.date_id,_ktLog.timestamp);
            }
            catch
            { }
            AddEventToIListEvent(info);
        }

        public void InsertNewRecordSmartEvent(EVT_TYPE evt,int smartcard_id,string smartcard_desc)
        {
            Events info = new Events();
            try
            {
                DataTable dtEvent = oMogoEvent.GetEventDesc((int)evt);
                info.evt_id = (int)evt;
                info.event_desc_e = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc"].ToString() : evt.ToString();
                info.event_desc_t = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc_t"].ToString() : evt.ToString();
                info.is_backside = false;

                info.smart_card = new ZebraNoSQLLib.SmartCard();
                info.smart_card.smart_id = smartcard_id;
                info.smart_card.smart_desc = smartcard_desc;

                oSql.UpSertEvent_CurEventState(g_veh_id, (int)evt, _ktLog.date_id, _ktLog.timestamp);
            }
            catch
            { }
            AddEventToIListEvent(info);
        }
        public void InsertNewRecordZone(EVT_TYPE evt, int zone_id)
        {
            Events info = new Events();
            try
            {
                //info.type = side == SIDE_TYPE.IN ? 1 : 2;

                DataTable dtEvent = oMogoEvent.GetEventDesc((int)evt);
                DataTable dtZone = oMogoEvent.GetZoneDesc(zone_id);

                info.evt_id = (int)evt;
                info.event_desc_e = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc"].ToString() : evt.ToString();
                info.event_desc_t = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc_t"].ToString() : evt.ToString();
                info.is_backside = false;

                info.zone = new Zoning();
                info.zone.zone_id = zone_id;
                info.zone.zone_desc = dtZone.Rows.Count == 1 ? dtZone.Rows[0]["zone_desc"].ToString() : "UnknownZone";
                
                oSql.UpSertEvent_CurEventState(g_veh_id, (int)evt, _ktLog.date_id, _ktLog.timestamp);
            }
            catch
            { }
            AddEventToIListEvent(info);
        }
        public void InsertNewRecordRoute(EVT_TYPE evt, int route_id)
        {
            Events info = new Events();
            try
            {
                DataTable dtEvent = oMogoEvent.GetEventDesc((int)evt);
                DataTable dtRoute = oMogoEvent.GetRouteDesc(route_id);

                info.evt_id = (int)evt;
                info.event_desc_e = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc"].ToString() : evt.ToString();
                info.event_desc_t = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc_t"].ToString() : evt.ToString();
                info.is_backside = false;

                info.route = new Routing();
                info.route.route_id = route_id;
                info.route.route_desc = dtRoute.Rows.Count == 1 ? dtRoute.Rows[0]["route_desc"].ToString() : "UnknownRoute";

                oSql.UpSertEvent_CurEventState(g_veh_id, (int)evt, _ktLog.date_id, _ktLog.timestamp);
            }
            catch { }
            AddEventToIListEvent(info);
        }
        public void InsertNewRecordEventBackSide(EVT_TYPE evt, EVT_TYPE backside_evt, SOURE_EVENT soure_event, object values)
        {
            Events info = new Events();
            try
            {
                DataTable dtEvent = oMogoEvent.GetEventDesc((int)evt);
                DataTable dtEventBackside = oMogoEvent.GetEventDesc((int)backside_evt);

                info.evt_id = (int)evt;
                info.event_desc_e = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc"].ToString() : evt.ToString();
                info.event_desc_t = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc_t"].ToString() : evt.ToString();
                info.is_backside = true;

                info.evt_backside = new Event_Backside();
                info.evt_backside.type_id = (int)soure_event;
                info.evt_backside.evt_id = (int)backside_evt;
                info.evt_backside.event_t = dtEventBackside.Rows.Count == 1 ? dtEventBackside.Rows[0]["evt_desc"].ToString() : backside_evt.ToString();
                info.evt_backside.event_e = dtEventBackside.Rows.Count == 1 ? dtEventBackside.Rows[0]["evt_desc_t"].ToString() : backside_evt.ToString();

                if (evt == EVT_TYPE.NO_INZONE_CURFEW || evt == EVT_TYPE.NO_OUTZONE_CURFEW
                    || evt == EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED || evt == EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED
                    || evt == EVT_TYPE.ENGINE_OFF_IN_ZONE || evt == EVT_TYPE.ENGINE_OFF_OUT_ZONE)
                {
                    DataTable dtZone = oMogoEvent.GetZoneDesc((int)values);
                    info.evt_backside.zone_backside = new Zoning();
                    info.evt_backside.zone_backside.zone_id = (int)values;
                    info.evt_backside.zone_backside.zone_desc = dtZone.Rows.Count == 1 ? dtZone.Rows[0]["zone_desc"].ToString() : "UnknownZone";
                }

                oSql.UpSertEvent_CurEventState(g_veh_id, (int)evt, _ktLog.date_id, _ktLog.timestamp);
            }
            catch
            { }
            AddEventToIListEvent(info);
        }
        public void AddEventToIListEvent(Events info)
        {
            _ktLog.list_event.Add(info);
        }

        /// <summary>
        /// True Corp. True IO Status
        /// Modifly : Jirayu Kamsiri
        /// Last : 2012/09/12
        /// </summary>
        /// <param name="enableQ"></param>
        private void DetectTrueIOStatus()
        {
            //True Only For Check Type MSG
            if(!(g_type_of_msg == "i" || g_type_of_msg == "I" || g_type_of_msg == "+" || g_type_of_msg == "X"||g_type_of_msg == "x")){ return; }
            DataTable dtVehInGroup = oTrueZone.True2IsVehicleTrueCorp(g_veh_id);
            if (dtVehInGroup.Rows.Count < 1) { return; }

            oTrueIO.DetectStatus(g_ref_idx, g_veh_id, g_in_out_status, g_type_of_msg, IsEnableBroadcastQTRUE, g_speed, g_dtTimestampe, g_lat, g_lon, SMSQ, EMAILQ);
        }

        /// <summary>
        /// True Corp. Event New
        /// TrueFencing,TrueFencing2,True IO
        /// Modifly : Jirayu Kamsiri
        /// Last ModiFly : 2012/09/07
        /// </summary>
        /// 
        private void DetectFencingForTrueCorp()
        {
            if (g_lat == 0.0 || g_lon == 0.0 || g_no_of_satellite <= 3 || g_type_of_fix == 0) return;

            //Check and Update Veh_id and reg
            DataTable dtVehInGroup = oTrueZone.True2IsVehicleTrueCorp(g_veh_id);

            if (dtVehInGroup.Rows.Count < 1) { return; }

            try
            {
                DataTable dtVehZone = oTrueZone.True2GetVehicleGroupZone(g_veh_id);

                foreach (DataRow drZ in dtVehZone.Rows)
                {
                    int zone_id = (int)drZ["zone_id"];
                    if (zone_id == -1) { continue; }

                    GlobalClass.TrueZoneProfile data = new GlobalClass.TrueZoneProfile();

                    if (hzTrueZone.ContainsKey(zone_id))
                    {
                        data = (GlobalClass.TrueZoneProfile)hzTrueZone[zone_id];
                    }
               
                    if (data.zone_id > 0 && data.dtWp.Rows.Count > 0 && data.dtWp != null)
                    {
                        FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;
                        if (oFencing.DetectFencingTrueCorp(g_veh_id, g_lat, g_lon, (float)data.lat_min, (float)data.lat_max, (float)data.lon_min, (float)data.lon_max, data.clkdir, data.lastedUpdate, data.dtWp, out side))
                        {
                            int last_zone_id = -1;
                            int last_evt_id = -1;

                            oFencing.True2GetZoneLastStatus(g_veh_id, data.zone_id, out last_evt_id, out last_zone_id);

                            if (side == FencingCls.SIDE_TYPE.IN && last_evt_id == (int)EVT_TYPE.IN_ZONE && last_zone_id == data.zone_id)
                            {
                            }
                            else if (side == FencingCls.SIDE_TYPE.OUT && last_evt_id == (int)EVT_TYPE.OUT_ZONE && last_zone_id == data.zone_id)
                            {
                            }
                            else
                            {
                                bool bIsRecord = false;
                                if (last_evt_id != -1)
                                {
                                    bIsRecord = true;
                                }
                                else if ((last_evt_id == -1) && (side == FencingCls.SIDE_TYPE.IN))
                                {
                                    bIsRecord = true;
                                }

                                if (bIsRecord)
                                {
                                    int id_insert = oFencing.True2InsertMsgZone(g_ref_idx, data.zone_id);
                                    oFencing.True2ZoneState(g_ref_idx, g_veh_id, data.zone_id);

                                    // insert Type Group
                                    if (id_insert > -1)
                                    {
                                        DataTable dtZoneOfType = oTrueZone.True2GetVehicleGroupZoneType(g_veh_id);

                                        if (dtZoneOfType.Rows.Count > 0)
                                        {
                                            DataRow[] dr = dtZoneOfType.Select("zone_id = " + zone_id.ToString());
                                            
                                            if (dr.Length > 0)
                                            {
                                                for (int i = 0; i < dr.Length; i++)
                                                {
                                                    oFencing.True2InsertMsgZoneTypeGroup(id_insert, (int)dr[i]["group_id"], (int)dr[i]["type_id"]);
                                                }
                                            }
                                        }
                                    }

                                    if (IsEnableBroadcastQTRUE)
                                    {
                                        ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                                        info.idx = g_ref_idx;
                                        info.type = (int)EVT_TYPE.TRUE_ZONEING;
                                        info.veh_id = g_veh_id;
                                        string msg = side == FencingCls.SIDE_TYPE.IN ? "เข้าโซน" : "ออกโซน";
                                        //evt_id,message,zone_id
                                        int evt_id = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.IN_ZONE : (int)EVT_TYPE.OUT_ZONE;
                                        info.msg = evt_id.ToString() + "," + msg + data.zone_desc + "," + zone_id.ToString();
                                        info.timestamp = g_dtTimestampe;
                                        info.location_t = _ktLog.loc_t;
                                        info.location_e = _ktLog.loc_e;
                                        info.lat = g_lat;
                                        info.lon = g_lon;
                                        info.speed = g_speed;
                                        SMSQ.PushQ(info);
                                        EMAILQ.PushQ(info);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        public void GetTrueZone()
        {
            try
            {
                DataTable dtAllTrueZone = oTrueZone.True2GetAllTrueZone();

                foreach (DataRow dr in dtAllTrueZone.Rows)
                {
                    int zone_id = (int)dr["zone_id"];

                    GlobalClass.TrueZoneProfile info = GetTrueZoneInfo(zone_id);

                    if (info.zone_id > 0 && info.dtWp.Rows.Count > 0)
                    {
                        if (GlobalClass.hsZoneTrueList.ContainsKey(zone_id))
                        {
                            GlobalClass.hsZoneTrueList[zone_id] = info;
                        }
                        else
                        {
                            GlobalClass.hsZoneTrueList.Add(zone_id,info);
                        }
                    }
                    else
                    {
                        if (GlobalClass.hsZoneTrueList.ContainsKey(zone_id))
                        {
                            GlobalClass.hsZoneTrueList.Remove(zone_id);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
 
            }
        }
        private GlobalClass.TrueZoneProfile GetTrueZoneInfo(int zone_id)
        {
            GlobalClass.TrueZoneProfile info = new GlobalClass.TrueZoneProfile();
           
            try
            {
                DataTable pfTable = oFencing.GetZoneProfile(zone_id);
                if (pfTable.Rows.Count > 0)
                {
                    info.zone_id = (int)pfTable.Rows[0]["zone_id"];
                    info.zone_desc = (string)pfTable.Rows[0]["zone_desc"];
                    info.lat_min = (double)pfTable.Rows[0]["lat_min"];
                    info.lat_max = (double)pfTable.Rows[0]["lat_max"];
                    info.lon_min = (double)pfTable.Rows[0]["lon_min"];
                    info.lon_max = (double)pfTable.Rows[0]["lon_max"];
                    info.clkdir = (Boolean)pfTable.Rows[0]["clockwise"];
                    info.lastedUpdate = (DateTime)pfTable.Rows[0]["lasted_update"];

                    DataTable wpTable = oFencing.GetZoneWayPoints(zone_id);
                    DataTable wp = new DataTable();
                    wp.Columns.Add("X", typeof(float));
                    wp.Columns.Add("Y", typeof(float));
                    foreach (DataRow wp_row in wpTable.Rows)
                    {
                        double wpX = (double)wp_row["lon"];
                        double wpY = (double)wp_row["lat"];
                        wp.Rows.Add(new object[] { (float)wpX, (float)wpY });
                    }

                    info.dtWp = wp;
                    info.is_update = false;
                    info.timeExpire = DateTime.Now.AddHours(1);
                }
            }
            catch { }
            return info;
        }

        private void EnQueueElapsedTime(int totalElapedTime, String Event, String Msg, int veh_id)
        {
            try
            {
                if (totalElapedTime > ElapedTime)
                {
                    string msg = DateTime.Now.ToString("HH:mm:ss") + "," + veh_id.ToString() + "," + Event + "," + Msg + ",ms";
                    if (msg != null && msg.Length > 20 && OnProcessTime != null)
                    {
                        OnProcessTime(msg);
                    }
                }
            }
            catch
            { }
        }


        private void DetectDFMIOStatus(bool enableQ)
        {
            //if (g_df_vehicle)
            //{
            //    oDMFIO.DetectStatus(g_ref_idx, g_veh_id, g_in_out_status, g_type_of_msg, enableQ, g_speed, g_dtTimestampe, g_lat, g_lon, SMSQ, EMAILQ, g_tag_msg);
            //}
        }
    }

}
