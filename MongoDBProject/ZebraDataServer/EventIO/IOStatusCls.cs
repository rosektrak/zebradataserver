using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;
using ZebraNoSQLLib;

namespace ZebraDataServer
{
    class IOStatusCls
    {
        private KTLogMsg _objMSG = new KTLogMsg();
        public KTLogMsg objMSG
        {
            get
            {
                return _objMSG;
            }
        }
        public enum STATUS_INPUT { ON = 1, OFF = 0 }
        public struct EXTERNAL_INPUT
        {
            public STATUS_INPUT I01;
            public STATUS_INPUT I02;
            public STATUS_INPUT I03;
            public STATUS_INPUT I04;
            public STATUS_INPUT I05;
            public STATUS_INPUT I06;
        }
        public enum IOPORT
        {
            USERINPUT1 = 2,     ///in2  =   userIn1
            USERINPUT2 = 3,     ///in3  =   userIn2
            USEROUTPUT1 = 2,    ///out2 =   userOut1  
            USEROUTPUT2 = 3,    ///out3 =   userOut2
            USEROUTPUT3 = 4     ///out4 =   userOut3          //not use
        }
        public enum IOTYPE
        {
            ENGINEOFF   = 0x00,
            ENGINEON    = 0x10,

            USERINPUT1  = 0x20,
            USERINPUT2  = 0x40,
            POWER       = 0x80,

            IGNITION    = 0x01,     ///out1
            USEROUTPUT1 = 0x02,     ///out2 =   user1  
            USEROUTPUT2 = 0x04,     ///out3 =   user2
            USEROUTPUT3 = 0x08,      ///out4 =   user3          //not use
                                    
            EXTERNAL_INPUT01 = 0x01,
            EXTERNAL_INPUT02 = 0x02,
            EXTERNAL_INPUT03 = 0x04,
            EXTERNAL_INPUT04 = 0x08,
            EXTERNAL_INPUT05 = 0x10,
            EXTERNAL_INPUT06 = 0x20,
            EXTERNAL_INPUT07 = 0x40,
            EXTERNAL_INPUT08 = 0x80
        }
        public struct VEHINFO
        {
            public int      ref_idx ;
            public int      veh_id ;
            public int      value ;
            public bool     enable_q ;
            public int      speed ;
            public string   timestamp;
            public double   lat ; 
            public double   lon ;
            public CommonQeue SMSQ;
            public CommonQeue EMAILQ;
        }

        SQL gSql;
        LocationCls oLoc;
        GetDataMongo oMogoEvent;
        public IOStatusCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            oLoc = new LocationCls(dbServer);
            oMogoEvent = new GetDataMongo(dbServer);
        }
        private string GetHexToDex(string zHexStr)
        {
            string retDec = zHexStr;
            try
            {
                int decValue = 0;
                if (!(int.TryParse(zHexStr, out decValue)))
                {
                    decValue = Convert.ToInt32(zHexStr, 16);
                    retDec = decValue.ToString("0");
                }
            }
            catch
            {

            }
            return retDec;
        }
        private string GetHexToDex2(string zHexStr)
        {
            string retDec = "0";
            try
            {
                int decValue = Convert.ToInt32(zHexStr, 16);
                retDec = decValue.ToString("0");
            }
            catch
            {
            }
            return retDec;
        }
        public void DetectStatus(int veh_id, int value, string type_of_msg, bool enableQ, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ, string tag_msg, KTLogMsg objData, string type_of_box)
        {
            VEHINFO info = new VEHINFO();
            info.ref_idx = -1;
            info.veh_id = veh_id;
            info.value = value;
            info.enable_q = enableQ;
            info.speed = speed;
            info.timestamp = timestamp;
            info.lat = lat;
            info.lon = lon;
            info.SMSQ = SMSQ;
            info.EMAILQ = EMAILQ;
            int ref_idx = -1;
            _objMSG = objData;

            string g_type_of_msg = type_of_msg.Substring(type_of_msg.Length - 1, 1);
            bool is_Kbox = false;
            if (!(type_of_box == "M" || type_of_box == "G"))
            {
               is_Kbox = gSql.GetVehTypeOfBox("K", veh_id);
            }

            //#defind of k-box  //sql ok
            if (tag_msg.Length > 0 && is_Kbox)
            {
                switch (g_type_of_msg[0])
                {
                    case '1': ProcessKBoxIOEventMessage(ref_idx, veh_id, tag_msg, info); break;
                    case '2': break;
                    case '3': break;
                    case '4': break;
                    case '5': break;
                    case '6': break;
                    case '7': break;
                    case '8': break;
                }
            }
            else
            {
                #region
                if (is_Kbox) return;
                //sql ok
                int retIO = GetLastIOStatus(veh_id, IOSOURCE.INTERNAL);

                IOSOURCE iosource = IOSOURCE.INTERNAL;
                if (g_type_of_msg[0] == 'i' || g_type_of_msg[0] == 'I' || g_type_of_msg[0] == '+' || g_type_of_msg[0] == 'C' || g_type_of_msg[0] == 'c')
                {
                    //--- The Old Process
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.ENGINEON))
                    {
                        if ((value & (int)IOTYPE.ENGINEON) == (int)IOTYPE.ENGINEON)
                        {
                            InsertNewRecord(veh_id, iosource, EVT_TYPE.ENGINE_ON, value, info);
                        }
                        else if ((value & (int)IOTYPE.ENGINEOFF) == (int)IOTYPE.ENGINEOFF)
                        {
                            InsertNewRecord(veh_id, iosource, EVT_TYPE.ENGINE_OFF, value, info);
                        }
                    }

                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.USERINPUT1))
                    {
                        if ((value & (int)IOTYPE.USERINPUT1) == (int)IOTYPE.USERINPUT1)
                        {
                            InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_ON, value, info, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                        }
                        else
                        {
                            InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_OFF, value, info, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                        }
                    }

                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.USERINPUT2))
                    {
                        if ((value & (int)IOTYPE.USERINPUT2) == (int)IOTYPE.USERINPUT2)
                        {
                            InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_ON, value, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                        }
                        else
                        {
                            InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_OFF, value, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                        }
                    }

                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.POWER))
                    {
                        if ((value & (int)IOTYPE.POWER) != (int)IOTYPE.POWER)
                        {
                            InsertNewRecord(veh_id, iosource, EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, value, info);
                        }
                        else
                        {
                            InsertNewRecord(veh_id, iosource, EVT_TYPE.EVENT_POWER_LINE_CONNECTED, value, info);
                        }
                    }
                }
                else
                {
                    #region PowerLine
                    if (g_type_of_msg[0] == 'B' || g_type_of_msg[0] == 'b')
                    {
                        if (g_type_of_msg[0] == 'B')
                        {
                            InsertNewRecord(veh_id, iosource, EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, value, info);
                        }
                        else
                        {
                            InsertNewRecord(veh_id, iosource, EVT_TYPE.EVENT_POWER_LINE_CONNECTED, value, info);
                        }
                    }
                    else if (((retIO & (int)IOTYPE.POWER) != (int)IOTYPE.POWER) && ((value & (int)IOTYPE.POWER) != (int)IOTYPE.POWER))
                    {
                        //sql ok enable feture
                        int pw_delay_time = GetDisconnectedPowerParams(veh_id);
                        if (pw_delay_time > -1)
                        {
                            //if evt_id retrun -1 is not find event from db
                            int last_evt_id = -1;
                            DateTime last_timestamp = DateTime.Now;

                            //sql ok
                            gSql.GetLasEventStatus(veh_id, (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, out last_evt_id, out last_timestamp);
                          
                            TimeSpan sp = DateTime.Now - last_timestamp;
                            if (sp.TotalMinutes >= pw_delay_time)
                            {
                                if ((value & (int)IOTYPE.POWER) != (int)IOTYPE.POWER)
                                {
                                    InsertNewRecord( veh_id, iosource, EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, retIO, info);
                                }
                            }
                        }
                    }
                    #endregion PowerLine
                }
                #region Output
                //Output 1  Port: 2 >> 0x02
                if (!IsDoubleEvent(value, retIO, (int)IOTYPE.USEROUTPUT1))
                {
                    if ((value & (int)IOTYPE.USEROUTPUT1) == (int)IOTYPE.USEROUTPUT1)
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON, value, info, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                    else
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF, value, info, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                }
                //Output 2  Port: 3 >> 0x04
                if (!IsDoubleEvent(value, retIO, (int)IOTYPE.USEROUTPUT2))
                {
                    if ((value & (int)IOTYPE.USEROUTPUT2) == (int)IOTYPE.USEROUTPUT2)
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON, value, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                    else
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF, value, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                }
                //Output 3  Port: 3 >> 0x08 ,Select use
                if (!IsDoubleEvent(value, retIO, (int)IOTYPE.USEROUTPUT3))
                {
                    if ((value & (int)IOTYPE.USEROUTPUT3) == (int)IOTYPE.USEROUTPUT3)
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED3_OUT_ON, value, info, PORT.Port4, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                    else
                    {
                        InsertNewRecordEventIO( veh_id, EVT_TYPE.EVENT_USER_DEFINED3_OUT_OFF, value, info, PORT.Port4, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                    }
                }
                /// End user Defined 1,2,3 Output
                /// IGNITION Control
                if (!IsDoubleEvent(value, retIO, (int)IOTYPE.IGNITION))
                {
                    if ((value & (int)IOTYPE.IGNITION) == (int)IOTYPE.IGNITION)
                    {
                        InsertNewRecord(veh_id,iosource, EVT_TYPE.EVENT_IGNITION_LINE_ON, value, info);
                    }
                    else
                    {
                        InsertNewRecord( veh_id,iosource, EVT_TYPE.EVENT_IGNITION_LINE_OFF, value, info);
                    }
                }
                #endregion Output

                if (g_type_of_msg[0] == 'X')
                {
                    InsertNewRecord( veh_id, iosource, EVT_TYPE.EVENT_GPS_DISCONNECTED, value, info);
                }
                if (g_type_of_msg[0] == 'x')
                {
                    InsertNewRecord( veh_id, iosource , EVT_TYPE.EVENT_GPS_CONNECTED, value, info);
                }
                #endregion
            }

            if (g_type_of_msg[0] == 'D' && tag_msg.Length > 0 && (tag_msg[0] == 'I' || tag_msg[0] == 'i'))
            {
                int input_status = 0;

                string[] inputStr = tag_msg.Split(new char[] { ',' });
                if (inputStr.Length == 6)
                {
                    #region--input convert
                    //int input_id = Convert.ToInt32(inputStr[0][1].ToString());
                    //int msgType = sql.GetOptionalMsgTypeIDByMsgType(inputStr[1]);
                    //int status_byte = Convert.ToInt32(GetHexToDex2(inputStr[2]));
                    //int input_mark = Convert.ToInt32(GetHexToDex2(inputStr[3]));
                    //int report_time = Convert.ToInt32(inputStr[4]);
                    //int input_status = Convert.ToInt32(GetHexToDex2(inputStr[5]));
                    // DateTime local_timestamp = DateTime.Parse(timestamp);
                    #endregion
                    if (int.TryParse(GetHexToDex2(inputStr[5]), out input_status))
                    {

                        if (_objMSG.option == null) _objMSG.option = new Option();

                        EXTERNAL_INPUT exInput = Get_ExternalInput(ref_idx, veh_id, input_status, info, inputStr[1]);
                        _objMSG.option.optExIn.ex_input.in01 = (int)exInput.I01;
                        _objMSG.option.optExIn.ex_input.in02 = (int)exInput.I02;
                        _objMSG.option.optExIn.ex_input.in03 = (int)exInput.I03;
                        _objMSG.option.optExIn.ex_input.in04 = (int)exInput.I04;
                        _objMSG.option.optExIn.ex_input.in05 = (int)exInput.I05;
                        _objMSG.option.optExIn.ex_input.in06 = (int)exInput.I06;
                    }
                }
            }
        }
        private void ProcessKBoxIOEventMessage(int ref_idx, int veh_id, string tag_msg, VEHINFO info)
        {
            try
            {
                string[] st = tag_msg.Split(new char[] { ',' });
                if (st.Length > 0)
                {
                    switch (st[0])
                    {
                        case "PB": break;
                        case "TP": break;
                        case "SP": break;
                        case "IG"://Ignition
                            {
                                if (st[1] == "1")
                                {
                                    InsertKBoxNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_ON, info);
                                }
                                else if (st[1] == "0")
                                {
                                    InsertKBoxNewRecord(ref_idx, veh_id, EVT_TYPE.ENGINE_OFF, info);
                                }
                            }
                            break;
                        case "PS"://Car Battery Present.
                            {
                                if (st[1] == "0")
                                {
                                    InsertKBoxNewRecord(ref_idx, veh_id, EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED, info);
                                }
                                else if (st[1] == "1")
                                {
                                    InsertKBoxNewRecord(ref_idx, veh_id, EVT_TYPE.EVENT_POWER_LINE_CONNECTED, info);
                                }
                            }
                            break;
                        case "ANTDETECT":
                            {
                                if (st[1] == "0")
                                {
                                    InsertKBoxNewRecord(ref_idx, veh_id, EVT_TYPE.EVENT_GPS_DISCONNECTED, info);
                                }
                                else if (st[1] == "1")
                                {
                                    InsertKBoxNewRecord(ref_idx, veh_id, EVT_TYPE.EVENT_GPS_CONNECTED, info);
                                }
                            }
                            break;
                        default:
                            {
                                #region INPUT / OutPut
                                if (st[0] == "IN1")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_ON, -1, info, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_OFF,-1, info, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                }
                                else if (st[0] == "IN2")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_ON,-1, info, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_OFF,-1, info, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                }
                                else if (st[0] == "IN3")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED3_ON,-1, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED3_OFF,-1, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                }
                                else if (st[0] == "IN4")//Input Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED4_ON,-1, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED4_OFF,-1, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.INTERNAL);
                                    }
                                }
                                else if (st[0] == "OUT0")//Output1 Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON,-1, info, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF,-1, info, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                    }
                                }
                                else if (st[0] == "OUT1")//Output2 Event
                                {
                                    if (st[1] == "1")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON,-1, info, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                    }
                                    else if (st[1] == "0")
                                    {
                                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF,-1, info, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.OUTPUT, IOSOURCE.INTERNAL);
                                    }
                                }
                                #endregion
                            }
                            break;
                    }
                }
            }
            catch { }
        }
        private void InsertNewRecord(int vid,IOSOURCE iosource, EVT_TYPE evt, int value, VEHINFO info)
        {
            if (!(evt == EVT_TYPE.EVENT_GPS_CONNECTED || evt == EVT_TYPE.EVENT_GPS_DISCONNECTED))
            {
                gSql.UpSertBox_PrvIOState(vid, value, (iosource == IOSOURCE.INTERNAL ? "I" : "E"));
            }
            AddEventToIListEvent(evt);
            Broadcast(info.enable_q, (int)evt, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
        }
        private void InsertKBoxNewRecord(int ref_idx, int vid, EVT_TYPE evt, VEHINFO info)
        {
            if (evt == EVT_TYPE.ENGINE_ON || evt == EVT_TYPE.ENGINE_OFF)
            {
                //sql ok
                int last_evt_id = -1;
                DateTime last_local_timestamp = DateTime.Now;
                gSql.GetLastEngineEventStatus(vid, out last_evt_id,out last_local_timestamp);
                if (last_evt_id != (int)evt)
                {
                    AddEventToIListEvent(evt);
                    Broadcast(info.enable_q, (int)evt, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
                }
            }
            else
            {
                AddEventToIListEvent(evt);
                Broadcast(info.enable_q, (int)evt, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
            }
        }
        private void InsertNewRecordEventIO(int vid, EVT_TYPE evt, int value, VEHINFO info, PORT port, STATUSOF status_onoff, IOSOURCETYPE io_type, IOSOURCE internal_port)
        {
            //sql ok
            EVT_TYPE evt_seting = gSql.GetIO_Setting_Event(vid, (int)port, (int)status_onoff, Convert.ToChar((int)io_type), (int)internal_port);

            bool is_setting = false;
            if (evt_seting != EVT_TYPE.NONE) { is_setting = true; }
            else { evt_seting = evt; }

            if (io_type == IOSOURCETYPE.INPUT && internal_port == IOSOURCE.INTERNAL)
            {
                AddEventToIListEvent(evt_seting, is_setting, evt, SOURE_EVENT.USER_DEFINED_INPUT);
            }
            else if (io_type == IOSOURCETYPE.INPUT && internal_port == IOSOURCE.EXTERNAL)
            {
                AddEventToIListEvent(evt_seting, is_setting, evt, SOURE_EVENT.EXTERNAL_INPUT);
            }
            else if (io_type == IOSOURCETYPE.OUTPUT)
            {
                AddEventToIListEvent(evt_seting, is_setting, evt, SOURE_EVENT.USER_DEFINED_OUTPUT);
            }

            if (value != -1) //KBox is not have io value
            {
                //sql ok
                gSql.UpSertBox_PrvIOState(vid, value, (internal_port == IOSOURCE.INTERNAL ? "I" : "E"));
            }

            Broadcast(info.enable_q, (int)evt_seting, info.veh_id, info.speed, info.timestamp, info.lat, info.lon, info.SMSQ, info.EMAILQ, info.value);
        }
        private EXTERNAL_INPUT Get_ExternalInput(int ref_idx, int veh_id, int value, VEHINFO info, string Input_type)
        {
            EXTERNAL_INPUT inp = new EXTERNAL_INPUT();

            //sql ok
            int retIO = GetLastIOStatus(veh_id, IOSOURCE.EXTERNAL);

            try
            {
                if ((value & (int)IOTYPE.EXTERNAL_INPUT01) == (int)IOTYPE.EXTERNAL_INPUT01)
                {
                    inp.I01 = STATUS_INPUT.ON;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT01))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT01_ON, value, info, PORT.Port1, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }
                else
                {
                    inp.I01 = STATUS_INPUT.OFF;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT01))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT01_OFF, value, info, PORT.Port1, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }

                if ((value & (int)IOTYPE.EXTERNAL_INPUT02) == (int)IOTYPE.EXTERNAL_INPUT02)
                {
                    inp.I02 = STATUS_INPUT.ON;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT02))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT02_ON, value, info, PORT.Port2, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }
                else
                {
                    inp.I02 = STATUS_INPUT.OFF;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT02))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT02_OFF, value, info, PORT.Port2, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }

                if ((value & (int)IOTYPE.EXTERNAL_INPUT03) == (int)IOTYPE.EXTERNAL_INPUT03)
                {
                    inp.I03 = STATUS_INPUT.ON;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT03))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT03_ON, value, info, PORT.Port3, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }
                else
                {
                    inp.I03 = STATUS_INPUT.OFF;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT03))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT03_OFF, value, info, PORT.Port3, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }

                if ((value & (int)IOTYPE.EXTERNAL_INPUT04) == (int)IOTYPE.EXTERNAL_INPUT04)
                {
                    inp.I04 = STATUS_INPUT.ON;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT04))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT04_ON, value, info, PORT.Port4, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }
                else
                {
                    inp.I04 = STATUS_INPUT.OFF;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT04))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT04_OFF, value, info, PORT.Port4, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }

                if ((value & (int)IOTYPE.EXTERNAL_INPUT05) == (int)IOTYPE.EXTERNAL_INPUT05)
                {
                    inp.I05 = STATUS_INPUT.ON;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT05))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT05_ON, value, info, PORT.Port5, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }
                else
                {
                    inp.I05 = STATUS_INPUT.OFF;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT05))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT05_OFF, value, info, PORT.Port5, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }

                if ((value & (int)IOTYPE.EXTERNAL_INPUT06) == (int)IOTYPE.EXTERNAL_INPUT06)
                {
                    inp.I06 = STATUS_INPUT.ON;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT06))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT06_ON, value, info, PORT.Port6, STATUSOF.ON, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }
                else
                {
                    inp.I06 = STATUS_INPUT.OFF;
                    if (!IsDoubleEvent(value, retIO, (int)IOTYPE.EXTERNAL_INPUT06))
                    {
                        InsertNewRecordEventIO(veh_id, EVT_TYPE.EXTERNAL_INPUT06_OFF, value, info, PORT.Port6, STATUSOF.OFF, IOSOURCETYPE.INPUT, IOSOURCE.EXTERNAL);
                    }
                }
            }
            catch
            {
            }
            return inp;
        }
        private void AddEventToIListEvent(EVT_TYPE evt)
        {
            Events info = new Events();
            try
            {
                //sql ok
                DataTable dtEvent = oMogoEvent.GetEventDesc((int)evt);
                info.evt_id = (int)evt;
                info.event_desc_e =  dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc"].ToString() : evt.ToString();
                info.event_desc_t =  dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc_t"].ToString() : evt.ToString();
                info.is_backside = false;

                gSql.UpSertEvent_CurEventState(_objMSG.vid, (int)evt, objMSG.date_id, objMSG.timestamp);
            }
            catch { }
            _objMSG.list_event.Add(info);
        }
        private void AddEventToIListEvent(EVT_TYPE evt, bool is_backside, EVT_TYPE evt_bs, SOURE_EVENT soure_event)
        {
            Events info = new Events();
            try
            {
                //sql ok
                DataTable dtEvent = oMogoEvent.GetEventDesc((int)evt);
              
                info.evt_id = (int)evt;
                info.event_desc_e = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc"].ToString() : evt.ToString();
                info.event_desc_t = dtEvent.Rows.Count == 1 ? dtEvent.Rows[0]["evt_desc_t"].ToString() : evt.ToString();
                info.is_backside = is_backside;

                if (is_backside)
                {
                    //sql ok
                    DataTable dtEventBackside = oMogoEvent.GetEventDesc((int)evt_bs);

                    info.evt_backside = new Event_Backside();
                    info.evt_backside.type_id = (int)soure_event;
                    info.evt_backside.evt_id = (int)evt_bs;
                    info.evt_backside.event_e = dtEventBackside.Rows.Count == 1 ? dtEventBackside.Rows[0]["evt_desc"].ToString() : evt_bs.ToString();
                    info.evt_backside.event_t = dtEventBackside.Rows.Count == 1 ? dtEventBackside.Rows[0]["evt_desc_t"].ToString() : evt_bs.ToString();
                }

                gSql.UpSertEvent_CurEventState(_objMSG.vid, (int)evt, objMSG.date_id, objMSG.timestamp);
            }
            catch { }
            _objMSG.list_event.Add(info);
        }
      
        public int GetLastIOStatus(int veh_id, IOSOURCE source)
        {
            int status = 0;
            gSql.GetLastIOStatus(veh_id, (source == IOSOURCE.INTERNAL ? "I" : "E"), out status);
            return status;
        }
        private int GetDisconnectedPowerParams(int veh_id)
        {
            return gSql.GetDisconnectedPowerEvtParam(veh_id);           
        }
        private bool IsDoubleEvent(int value, int prv_state, int type)
        {
            value       =   value & type ;
            prv_state   =   prv_state & type ;

            if (value == prv_state)
            {
                return true;
            }
            return false;
        }
        private void Broadcast(bool enableQ, int type, int veh_id, int speed, string timestamp, double lat, double lon, CommonQeue SMSQ, CommonQeue EMAILQ, int value)
        {
            if (enableQ)
            {
                ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                info.idx = -1;
                info.type = type;
                info.veh_id = veh_id;
                info.msg = Convert.ToString(value, 16);
                info.timestamp = timestamp;
                info.lat = lat;
                info.lon = lon;
                info.speed = speed;

                if (_objMSG.loc_t.Length < 10 && _objMSG.loc_e.Length < 10)
                {
                    GetNPutLocation((float)lon, (float)lat);
                }

                info.location_t = _objMSG.loc_t;
                info.location_e = _objMSG.loc_e;

                if (SMSQ != null || EMAILQ != null)
                {
                    SMSQ.PushQ(info);
                    EMAILQ.PushQ(info);
                }
            }
        }
        private void GetNPutLocation(float lon, float lat)
        {
            string location_e = "NO GPS";
            string location_t = "������ѭ�ҹ GPS";
            try
            {

                if (lon > 0.0 && lat > 0.0)
                {
                    //Detail
                    int id = oLoc.DetectCustomLandmark(lon, lat);
                    if (id > -1)
                    {
                        location_e = oLoc.GetCustomLandmarkName(id, 0);
                        location_t = oLoc.GetCustomLandmarkName(id, 1);
                    }
                    else
                    {
                        id = oLoc.DetectLandmark(lon, lat);
                        location_e = oLoc.GetLandmarkName(id, 0);
                        location_t = oLoc.GetLandmarkName(id, 1);
                    }
                    //�Ӻ� ����� �ѧ��Ѵ 
                    id = oLoc.DetectAdminPoly(lon, lat);
                    location_e += " " + oLoc.GetPolyName(id, 0);
                    location_t += " " + oLoc.GetPolyName(id, 1);

                    location_e = location_e.TrimEnd(new char[] { ' ' });
                    location_t = location_t.TrimEnd(new char[] { ' ' });

                    location_e = location_e.TrimStart(new char[] { ' ' });
                    location_t = location_t.TrimStart(new char[] { ' ' });
                }
            }
            catch
            {
            }
            try
            {
                _objMSG.loc_t = location_t;
                _objMSG.loc_e = location_e;
            }
            catch
            { }
        }

        /* not use */
        public void InsertLogUserDefined1(int vehID, int refIdx, int evtID)
        {
            gSql.InsertLogUserDefined1(vehID, refIdx, evtID);
        }
        public void InsertLogUserDefined2(int vehID, int refIdx, int evtID)
        {
            gSql.InsertLogUserDefined2(vehID, refIdx, evtID);
        }
    }
}
