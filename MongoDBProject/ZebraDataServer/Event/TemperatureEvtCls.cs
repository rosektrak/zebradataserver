using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class TemperatureEvtCls 
    {
        SQL gSql;
        public TemperatureEvtCls(String dbServer)            
        {
            gSql = new SQL(dbServer);
        }

        public DataTable GetTemperatureByVeh(int veh_id)
        {
            return gSql.GetTemperatureByVeh(veh_id);
        }
        public void GetLastTemperatureEventStatus(int veh_id, out int evt_id,  out int cur_temperature, out DateTime local_timestamp)
        {
            gSql.GetLastTemperatureEventStatus(veh_id, out evt_id, out cur_temperature, out local_timestamp);
        }

    }
}
