using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;

namespace ZebraDataServer
{    
    class SpeedHazardCls
    {
        SQL gSql;

        public SpeedHazardCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        //public Boolean DetectSpeedHazard(int veh_id, int speed, int type_of_msg)
        //{
        //    if (gSql.GetVehSpeedLimit(veh_id) < speed)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        public void InsertNewRecord(int ref_idx)
        {
            gSql.InsertMsgEvt(ref_idx, EVT_TYPE.HAZARD_SPEED);
        }

     
    }
}
