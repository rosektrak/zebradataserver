using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ZebraDataServer
{
    class DistanceCls
    {
        SQL gSql;
        public DistanceCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public bool CalDistance(int veh_id,  float lat, float lon, out double distance)
        {
            bool is_chang = false;
            distance = 0.0;

            try
            {
                //sql ok
                DataTable dtData = gSql.GetCurLoc(veh_id);

                if (dtData.Rows.Count > 0)
                {
                    double tmp_lat = (double)dtData.Rows[0]["lat"];
                    double tmp_lon = (double)dtData.Rows[0]["lon"];

                    distance = Convert.ToDouble((decimal)dtData.Rows[0]["distance"]);

                    double tmp_distance = 0.0;
                    double browsing = 0.0;
                    bool is_cal = false;

                    double lat_ = lat;
                    double lon_ = lon;

                    if (!(tmp_lon == 0.00 && tmp_lon == 0.00))
                    {
                        browsing = GetDistance(tmp_lat, tmp_lon, lat_, lon_);

                        //Browsing Fillter
                        if (browsing < 100)
                        {
                            tmp_distance += browsing;
                            is_cal = true;
                        }
                    }

                    if (is_cal)
                    {
                        distance += tmp_distance;
                        is_chang = true;
                    }
                }
            }
            catch
            {
            }

            return is_chang;
        }

        private double GetDistance(double lat1, double lon1, double lat2, double lon2)
        {
            const double rad = 57.2958;
            const double R = 6371.0;
            double distance = 0.00;

            if (lat1 == lat2 && lon1 == lon2)
            {
                return distance;
            }

            //--- M.
            //distance = Math.Acos((Math.Sin(lat1 / rad) * Math.Sin((lat2) / rad)) +
            //           (Math.Cos(lat1 / rad) * Math.Cos((lat2) / rad) * Math.Cos(((lon2) / rad) - (lon1 / rad)))
            //           ) * R * 1000.0;

            //--- KM.
            distance = Math.Acos((Math.Sin(lat1 / rad) * Math.Sin((lat2) / rad)) +
                       (Math.Cos(lat1 / rad) * Math.Cos((lat2) / rad) * Math.Cos(((lon2) / rad) - (lon1 / rad)))
                       ) * R;

            return distance;
        }
    }
}
