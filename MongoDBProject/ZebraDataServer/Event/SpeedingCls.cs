using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;

namespace ZebraDataServer
{    
    class SpeedingCls
    {
        SQL gSql;

        public SpeedingCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public Boolean DetectSpeedViolate(int veh_id, int speed)
        {
            //if (gSql.GetVehSpeedLimit(veh_id) < speed)
            //{
            //    return true;
            //}
            return false;
        }
        public DataTable GetSpeedSetting(int veh_id)
        {
            return gSql.GetVehSpeedLimit(veh_id);
        }

        public void InsertNewRecord(int ref_idx)
        {
            gSql.InsertMsgEvt(ref_idx, EVT_TYPE.SPEEDING);
        }
    }
}
