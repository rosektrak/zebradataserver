using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class EngineOnOffInOutZoneCls : FencingCls
    {
        private enum ZONETYPE { IN = 00, OUT = 01 } ;
        private enum ZONEEVT  { IN = 17, OUT = 18 } ;

        SQL gSql;
        LocationCls oLoc;

        public EngineOnOffInOutZoneCls(String dbServer)
            : base(dbServer)
        {
            gSql = new SQL(dbServer);
            oLoc = new LocationCls(dbServer);
        }
    
        public DataTable GetFeatureEvtParam(int veh_id)
        {
            return gSql.GetEvtParamEngineOnOffInOutZone(veh_id, (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE);   
        }
        public void GetEngineOnOffEvt(int veh_id, out int last_evt_id, out DateTime last_local_timestamp)
        {
            last_evt_id = -1;
            last_local_timestamp = DateTime.Now.AddDays(-1);
            gSql.GetLastEngineEventStatus(veh_id, out last_evt_id, out last_local_timestamp);
        }
        public bool IsEngineOn(int veh_id)
        {
           return gSql.IsEngineOn(veh_id);
        }
        public void GetLastZoneStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark)
        {
            evt_id = -1;
            last_zone_id = -1;
            local_timestamp = DateTime.Now.AddDays(-1);
            mark = -1;
            zone_id = -1;
            gSql.GetLastEngineOnOffInOutZoneStatus(veh_id, last_zone_id, out evt_id, out last_zone_id,out local_timestamp, out mark);   
        }

        public void UpSertZoneCurEngineOnOffInOutZoneState(int veh_id, int zone_id, int evt_id, int parent_evt_id, int mark, DateTime time)
        {
            gSql.UpSertZone_CurEngineOnOffInOutZoneState(veh_id, zone_id, evt_id, parent_evt_id, mark, time);
        }
        public void GetLastZoneCurfewWithSpeedStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark, out int speed)
        {
            gSql.GetLastZoneCurfewWithSpeedStatus(veh_id, last_zone_id, out evt_id, out zone_id, out local_timestamp, out mark, out speed);
        }
      

        public string GetLocation(float lon, float lat)
        {
            if (oLoc == null)
            {
                return "?";
            }

            string location_e = "NO LOCATION";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    location_e = oLoc.GetCustomLandmarkName(id, 0);
                }
                else
                {
                    id = oLoc.DetectLandmark(lon, lat);
                    location_e = oLoc.GetLandmarkName(id, 0);
                }

                id = oLoc.DetectAdminPoly(lon, lat);
                location_e += " " + oLoc.GetPolyName(id, 0);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_e = location_e.TrimStart(new char[] { ' ' });
            }

            return location_e;
        }
    }
}
