using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;

namespace ZebraDataServer
{
    class UpdateLog
    {
        SQL gSql;

        public UpdateLog(String dbServer) 
        {
            gSql = new SQL(dbServer);
        }

        public DataTable GetLastLatLon(int veh_id)
        {
            return gSql.GetMsgLatLon(veh_id);
        }
    }
}
