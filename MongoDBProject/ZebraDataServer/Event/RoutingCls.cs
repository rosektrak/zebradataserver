using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Drawing;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class RoutingCls
    {
        public struct vector
        {
            public PointF p1;
            public PointF p2;
        }

        SQL gSql;
        public RoutingCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Route Function        
        /// 
        public DataTable GetRouteByVeh(int veh_id)
        {
            return gSql.GetRouteByVeh(veh_id);
        }
        public DataTable GetRouteProfile(int route_id)
        {
            return gSql.GetRouteProfile(route_id);
        }
        public DataTable GetRouteWayPoints(int route_id)
        {
            return gSql.GetRouteWayPoints(route_id);
        }
        public bool DetectOffRoad(int VehID, float lat, float lon, float latMin, float latMax, float lonMin, float lonMax, DateTime updatedTime, DataTable wp, int dist, out float distMin)
        {
            bool IsOffRoad = true ;
            float d_min = 99999.99F;
            distMin = d_min;

            try
            {
                //if (lat >= latMin && lat <= latMax && lon >= lonMin && lon <= lonMax)
                if( true )
                {
                    int no = -1;
                    int t_min = -1;
                    double r_min = 99999.99;

                    foreach (DataRow r in wp.Rows)
                    {
                        float wpLon = (float)r["X"];
                        float wpLat = (float)r["Y"];

                        no++;

                        if (wpLon <= 0.0 || wpLat <= 0.0)
                        {
                            continue;
                        }

                        double dr = GetODOFromTheGreatCircle(lat, lon, wpLat, wpLon);
                        r_min = Math.Min(dr, r_min);
                        if (r_min == dr)
                        {
                            t_min = no;
                        }
                    }

                    if (t_min != -1)
                    {
                        vector pv1 = new vector();
                        vector pv2 = new vector();

                        vector v1 = new vector();
                        v1.p1 = new PointF((float)wp.Rows[t_min]["X"], (float)wp.Rows[t_min]["Y"]);
                        v1.p2 = new PointF(lon, lat);

                        vector v2 = new vector();

                        if (t_min + 1 < wp.Rows.Count)
                        {
                            v2.p1 = new PointF((float)wp.Rows[t_min]["X"], (float)wp.Rows[t_min]["Y"]);
                            v2.p2 = new PointF((float)wp.Rows[t_min + 1]["X"], (float)wp.Rows[t_min + 1]["Y"]);

                          //float r1 = (float)GetODOFromTheGreatCircle(lat, lon, v2.p1.Y, v2.p1.X);
                          //float r2 = (float)GetODOFromTheGreatCircle(v2.p2.Y, v2.p2.X, v2.p1.Y, v2.p1.X);

                            double z = GetZeta(v1, v2);
                          //if ((r1<r2) && (z<Math.PI/2))
                            if (z < (Math.PI/2))
                            {
                                ProjectVector(v1, v2, out pv1);

                                float r1 = (float)GetODOFromTheGreatCircle(pv1.p2.Y, pv1.p2.X, v2.p1.Y, v2.p1.X);
                                float r2 = (float)GetODOFromTheGreatCircle(v2.p2.Y, v2.p2.X, v2.p1.Y, v2.p1.X);

                                if (r1 <= r2)
                                {
                                    double d = GetODOFromTheGreatCircle(v1.p2.Y, v1.p2.X, pv1.p2.Y, pv1.p2.X);
                                    d_min = (float)Math.Min(d_min, d);
                                }
                            }
                        }

                        vector v3 = new vector();
                        if (t_min - 1 >= 0)
                        {
                            v3.p1 = new PointF((float)wp.Rows[t_min]["X"], (float)wp.Rows[t_min]["Y"]);
                            v3.p2 = new PointF((float)wp.Rows[t_min - 1]["X"], (float)wp.Rows[t_min - 1]["Y"]);

                          //float r1 = (float)GetODOFromTheGreatCircle(lat, lon, v3.p1.Y, v3.p1.X);
                          //float r2 = (float)GetODOFromTheGreatCircle(v3.p2.Y, v3.p2.X, v3.p1.Y, v3.p1.X);

                            double z = GetZeta(v1, v3);
                          //if ((r1<r2) && (z<Math.PI/2))
                            if (z < (Math.PI/2))
                            {
                                ProjectVector(v1, v3, out pv2);

                                float r1 = (float)GetODOFromTheGreatCircle(pv2.p2.Y, pv2.p2.X, v3.p1.Y, v3.p1.X);
                                float r2 = (float)GetODOFromTheGreatCircle(v3.p2.Y, v3.p2.X, v3.p1.Y, v3.p1.X);

                                if (r1 <= r2)
                                {
                                    double d = GetODOFromTheGreatCircle(v1.p2.Y, v1.p2.X, pv2.p2.Y, pv2.p2.X);
                                    d_min = (float)Math.Min(d_min, d);
                                }
                            }
                        }
                    }

                    if ((d_min*1000.0) < dist)
                    {
                        IsOffRoad = false;
                    }
                }
                else
                {
                }
            }
            catch
            {
            }

            distMin = d_min;
            return IsOffRoad;
        }

        public void ProjectVector(vector v1, vector v2, out vector pv1)
        {
            pv1 = new vector();

            float x1 = v1.p2.X - v1.p1.X ;
            float y1 = v1.p2.Y - v1.p1.Y ;

            float x2 = v2.p2.X - v2.p1.X ;
            float y2 = v2.p2.Y - v2.p1.Y ;
            float r2 = (x2 * x2) + (y2 * y2);
            if (r2 == 0)
            {
                pv1 = v1;
            }

            float c = ((x1 * x2) + (y1 * y2))/r2;

            vector u = new vector();
            u.p1.X = 0;
            u.p1.Y = 0;
            u.p2.X = x2 * c;
            u.p2.Y = y2 * c;

            float px = u.p2.X;
            float py = u.p2.Y;

            pv1.p1 = v1.p1;
            pv1.p2.X = v1.p1.X + px;
            pv1.p2.Y = v1.p1.Y + py;
        }
        public void GetUnitVector(vector v, out vector vout)
        {
            vout = new vector();

            float x = v.p2.X - v.p1.X ;
            float y = v.p2.Y - v.p1.Y ;
            float r = (float)Math.Sqrt((x * x)+(y * y)) ;

            float oX = x / r;
            float oY = y / r;

            vout.p1.X = 0 ;
            vout.p1.Y = 0 ;

            vout.p2.X = oX;
            vout.p2.Y = oY;
        }
        public double GetMagnitude(vector v)
        {
            double x = v.p1.X - v.p2.X ;
            double y = v.p1.Y - v.p2.Y ;
            double r = Math.Sqrt((x * x) + (y * y)) ;

            return r ;
        }
        public double GetZeta(vector v1, vector v2)
        {
            double x1  = v1.p1.X - v1.p2.X;
            double y1  = v1.p1.Y - v1.p2.Y;
            double r1  = Math.Sqrt((x1 * x1) + (y1 * y1));

            double x2  = v2.p1.X - v2.p2.X;
            double y2  = v2.p1.Y - v2.p2.Y;
            double r2  = Math.Sqrt((x2 * x2) + (y2 * y2));

            double xy  = (x1 * x2) + (y1 * y2);
            double r12 = r1 * r2;
            double zeta = Math.Acos(xy / r12);

            return zeta;
        }
        public double GetODOFromTheGreatCircle(double lat1, double lon1, double lat2, double lon2)
        {
            double R = 6371.0;
            double rad = 57.2958;

            lat1 = lat1 / rad;
            lat2 = lat2 / rad;
            lon1 = lon1 / rad;
            lon2 = lon2 / rad;

            double a = Math.Sin(lat1) * Math.Sin(lat2);
            double b = Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(lon2 - lon1);
            double c = Math.Acos(a + b);
            double d = R * c;

            return d;
        }
        public void GetLastRouteStatus(int veh_id, int last_route_id, out int evt_id, out int route_id)
        {
            gSql.GetLastRouteStatus(veh_id, last_route_id, out evt_id, out route_id);
        }
        public void UpSertRouteState(int veh_id, int route_id, EVT_TYPE evt)
        {
            gSql.UpSertRoute_CurRouteState(veh_id, route_id, (int)evt);
        }
    }
}
