using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ZebraDataServer
{
    public enum LOCATION_TYPE { LANDMARK = 1, ADMIN_POLY = 2, L_TRANS = 3, EXPRESS_WAY = 4, ESRI_MAP = 5 };
    class LocationCls
    {
        SQL gSql;
        public LocationCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public int DetectAdminPoly(float lon, float lat)
        {
            int id = -1;
            try
            {
                DataTable area_id_table = gSql.GetMapAdminPolyIDs(lat, lon);

                if (area_id_table != null && area_id_table.Rows != null)
                {
                    if (area_id_table.Rows.Count == 1)
                    {
                        id = int.Parse(area_id_table.Rows[0]["id"].ToString());
                        return id;
                    }
                    else if (area_id_table.Rows.Count > 1)
                    {
                        foreach (DataRow r in area_id_table.Rows)
                        {
                            id = int.Parse(r["id"].ToString());
                            DataTable wpTable = gSql.GetMapAdminPolyWayPoints(id);
                            if (TestPointInsideArea(lon, lat, wpTable))
                            {
                                return id;
                            }
                        }
                    }
                }
            }
            catch
            {
                return -1; 
            }
            return -1;
        }
        public int DetectLandmark(float lon, float lat)
        {
            int id = -1;
            try
            {
                DataTable area_id_table = gSql.GetMapLandmark(lon, lat);
                if (area_id_table != null && area_id_table.Rows != null)
                {
                    if (area_id_table.Rows.Count == 1)
                    {
                        id = int.Parse(area_id_table.Rows[0]["id"].ToString());
                        return id;
                    }
                }
            }
            catch 
            { }
            return -1;
        }
        public void InsertMsgLoc(int ref_idx, LOCATION_TYPE loc_type, int loc_id)
        {
            gSql.InsertMsgLoc(ref_idx, loc_type, loc_id);
        }
        public int InsertMsgLoc2(string location_t, string location_e)
        {
            int loc_id = gSql.InsertLoc2(location_t, location_e);
            return loc_id;
        }
        public string GetPolyName(int id, int lang_id)
        {
            string region_name = "";
            DataTable region_table = gSql.GetMapAdminName(id);
            foreach (DataRow r in region_table.Rows)
            {
                if( lang_id == 0 )
                {
                    region_name += (string)r["tam_name"] + "," + (string)r["amp_name"] + "," + (string)r["prov_name"];
                }
                else if (lang_id == 1)
                {
                    region_name += (string)r["tam_namt"] + "," + (string)r["amp_namt"] + "," + (string)r["prov_namt"];
                }
                else
                {
                    region_name += (string)r["tam_name"] + "," + (string)r["amp_name"] + "," + (string)r["prov_name"];
                }

                break ;
            }

            return region_name;
        }
        public string GetLandmarkName(int id, int lang_id)
        {
            string location_name = "";
            try
            {
                DataTable landmark_table = gSql.GetMapLandmarkName(id);
                foreach (DataRow r in landmark_table.Rows)
                {
                    if (lang_id == 0)
                    {
                        location_name += (string)r["name"] + " " + (string)r["location_e"];
                    }
                    else if (lang_id == 1)
                    {
                        location_name += (string)r["namt"] + " " + (string)r["location_t"];
                    }
                    else
                    {
                        location_name += (string)r["name"] + " " + (string)r["location_e"];
                    }

                    break;
                }

                location_name = location_name.TrimEnd(new char[] { ' ' });
            }
            catch
            { 
            }

            return location_name;
        }
        public int DetectCustomLandmark(float lon, float lat)
        {
            int id = -1;
            try
            {
                DataTable area_id_table = gSql.GetMapCustomerLandmark(lon, lat);
                if (area_id_table != null && area_id_table.Rows != null)
                {
                    if (area_id_table.Rows.Count == 1)
                    {
                        id = int.Parse(area_id_table.Rows[0]["id"].ToString());
                        return id;
                    }
                }
            }
            catch 
            { }
            return -1;
        }
        public string GetCustomLandmarkName(int id, int lang_id)
        {
            string location_name = "";
            try
            {
                DataTable landmark_table = gSql.GetMapCustomerLandmarkName(id);
                foreach (DataRow r in landmark_table.Rows)
                {
                    if (lang_id == 0)
                    {
                        location_name += (string)r["name"] + " " + (string)r["location_e"];
                    }
                    else if (lang_id == 1)
                    {
                        location_name += (string)r["namt"] + " " + (string)r["location_t"];
                    }
                    else
                    {
                        location_name += (string)r["name"] + " " + (string)r["location_e"];
                    }

                    break;
                }

                location_name = location_name.TrimEnd(new char[] { ' ' });
            }
            catch
            { 
                return "";
            }
            return location_name;
        }

        /////////////////////////////////////////////////////////////////////////////
        // New Map 
        /*
        public int GetLocation(float lon, float lat)
        {            
            ArcData_MapServer www = new ArcData_MapServer();
            www.Url = "http://db1/arcgis/services/Map/ArcData/MapServer";

            MapServerInfo mapinfo = www.GetServerInfo(www.GetDefaultMapName());
            MapDescription mapdesc = mapinfo.DefaultMapDescription;

            ImageDisplay imgdisp = new ImageDisplay();
            imgdisp.ImageHeight = 400;
            imgdisp.ImageWidth = 400;
            imgdisp.ImageDPI = 96;

            int tolerance = 50;

            esriIdentifyOption identifyoption = esriIdentifyOption.esriIdentifyAllLayers;

            PointN inputpoint = new PointN();
            inputpoint.X = lon; // 99.8765061893098;
            inputpoint.Y = lat;  // 13.8124107041706;

            double r2 = 9999.99;
            string location_t = "";
            string location_e = "";
            Geometry shape = new Geometry();

            ((ws1.EnvelopeN)(mapdesc.MapArea.Extent)).XMin = inputpoint.X - 0.003;
            ((ws1.EnvelopeN)(mapdesc.MapArea.Extent)).YMin = inputpoint.Y + 0.003;
            ((ws1.EnvelopeN)(mapdesc.MapArea.Extent)).XMax = inputpoint.X + 0.003;
            ((ws1.EnvelopeN)(mapdesc.MapArea.Extent)).YMax = inputpoint.Y - 0.003;

            LayerDescription[] layerdescriptions = mapdesc.LayerDescriptions;
            int[] layerids = new int[2];
            int i = 0;

            layerids.SetValue(0, 0);
            layerids.SetValue(11, 1);

            MapServerIdentifyResult[] identifyresults = www.Identify(mapdesc, imgdisp, inputpoint, tolerance, identifyoption, layerids);
            foreach (MapServerIdentifyResult layer in identifyresults)
            {
                if (layer.LayerID == 0)
                {
                    double x = Math.Abs(((PointN)layer.Shape).X - inputpoint.X);
                    double y = Math.Abs(((PointN)layer.Shape).Y - inputpoint.Y);
                    double r = Math.Sqrt((x * x) + (y * y));

                    if (r < r2)
                    {
                        location_t = layer.Properties.PropertyArray[6].Value + " " + layer.Properties.PropertyArray[10].Value + " ";
                        location_e = layer.Properties.PropertyArray[7].Value + " " + layer.Properties.PropertyArray[11].Value + " ";

                        shape = layer.Shape;
                    }
                    r2 = r;
                }
            }

            foreach (MapServerIdentifyResult layer in identifyresults)
            {
                if (layer.LayerID == 11)
                {
                    double x = ((ws1.PointN)(shape)).X;
                    double y = ((ws1.PointN)(shape)).Y;

                    if (TestPointInsideArea(x, y, ((ws1.PolygonN)(layer.Shape)).RingArray[0]))
                    {
                        location_t += layer.Properties.PropertyArray[10].Value + " " + layer.Properties.PropertyArray[8].Value + " " + layer.Properties.PropertyArray[6].Value;
                        location_e += layer.Properties.PropertyArray[11].Value + " " + layer.Properties.PropertyArray[9].Value + " " + layer.Properties.PropertyArray[7].Value;

                        break;
                    }
                }
            }

            location_t = location_t.TrimEnd(new char[] { ' ' });
            location_e = location_e.TrimEnd(new char[] { ' ' });

            int id = gSql.InsertLoc2(location_t, location_e);
            return id ;
        }
        */

        public bool TestPointInsideArea(double X, double Y, DataTable ringArr)
        {
            DataTable myPts = ringArr;

            if (myPts.Rows.Count <= 0)
            {
                return false;
            }

            int sides = myPts.Rows.Count - 1;
            int j = sides - 1;
            bool status = false;

            for (int i = 0; i < sides; i++)
            {
                double pntYi = (double)Convert.ToDouble( myPts.Rows[i]["Y"].ToString() );
                double pntYj = (double)Convert.ToDouble( myPts.Rows[j]["Y"].ToString() );
                double pntXi = (double)Convert.ToDouble( myPts.Rows[i]["X"].ToString() );
                double pntXj = (double)Convert.ToDouble( myPts.Rows[j]["X"].ToString() );

                if (pntYi < Y && pntYj >= Y || pntYj < Y && pntYi >= Y)
                {
                    if (pntXi + (Y - pntYi) / (pntYj - pntYi) * (pntXj - pntXi) < X)
                    {
                        status = !status;
                    }
                }
                j = i;
            }
            return status;
        }
    }
}
