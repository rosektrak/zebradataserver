using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class ZoneCurfewWithSpeedCls : FencingCls
    {
        SQL gSql;
        public ZoneCurfewWithSpeedCls(String dbServer): base(dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public DataTable GetFleetZoneCurfewWithSpeedByVeh(int veh_id)
        {
            return gSql.GetFleetZoneCurfewWithSpeedByVeh(veh_id);
        }

        public void GetLastZoneCurfewWithSpeedStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark, out int speed)
        {
            gSql.GetLastZoneCurfewWithSpeedStatus(veh_id, last_zone_id, out evt_id, out zone_id, out local_timestamp, out mark, out speed);
        }

        public void UpSertZoneCurfewWithSpeedState(int veh_id, int zone_id, int mark, int speed, DateTime time)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.UpSertZone_CurZoneCurfewWithSpeed(veh_id, zone_id,(int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED, (int)EVT_TYPE.IN_ZONE, mark, speed, time);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.UpSertZone_CurZoneCurfewWithSpeed(veh_id, zone_id,(int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED, (int)EVT_TYPE.OUT_ZONE, mark, speed, time);
            }
        }
        public void InsertNewRecordCurfewWithSpeed(int ref_idx)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.InsertMsgEvt(ref_idx, EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.InsertMsgEvt(ref_idx, EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED);
            }
        }

       
    }
}
