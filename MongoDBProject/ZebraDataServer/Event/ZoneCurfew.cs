using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class ZoneCurfewCls : FencingCls
    {
        SQL gSql;
        public ZoneCurfewCls(String dbServer) : base(dbServer)
        {
            gSql = new SQL(dbServer);
        }
        public DataTable GetFleetZoneCurfewByVeh(int veh_id)
        {
            return gSql.GetFleetZoneCurfewByVeh(veh_id);
        }

        public void GetLastZoneCurfewStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark)
        {
            gSql.GetLastZoneCurfewStatus(veh_id, last_zone_id, out evt_id, out zone_id, out local_timestamp, out mark);
        }

        public void UpSertZoneCurfewState(int veh_id, int zone_id, int mark, DateTime time)
        {
            if (GSide == SIDE_TYPE.IN) 
            {
                gSql.UpSertZone_CurZoneCurfewState(veh_id, zone_id,(int)EVT_TYPE.NO_INZONE_CURFEW, (int)EVT_TYPE.IN_ZONE, mark, time);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.UpSertZone_CurZoneCurfewState(veh_id, zone_id,(int)EVT_TYPE.NO_OUTZONE_CURFEW, (int)EVT_TYPE.OUT_ZONE, mark, time);
            }
        }

        public void InsertNewRecordCurfew(int ref_idx)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.InsertMsgEvt(ref_idx, EVT_TYPE.NO_INZONE_CURFEW);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.InsertMsgEvt(ref_idx, EVT_TYPE.NO_OUTZONE_CURFEW);
            }
        }

    }
}
