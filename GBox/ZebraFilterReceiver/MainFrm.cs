using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Tcp;
using System.Diagnostics;
using System.Xml;
using System.Globalization;
using System.Xml.Serialization;
using System.Threading;
using System.IO;

using ZebraDataCentre;
using ZrebraFilterInterface;

namespace ZebraFilterReceiver
{
    public partial class MainFrm : Form
    {
        public struct CFG
        {
            private string _messagequeue;
            [XmlElement("MessageQueue")]
            public string MessageQueue
            {
                get { return _messagequeue; }
                set { _messagequeue = value; }
            }

            private string _report_port;
            [XmlElement("ReportPort")]
            public string ReportPort
            {
                get { return _report_port; }
                set { _report_port = value; }
            }

            private string _programHeader;
            [XmlElement("ProgramHeader")]
            public string ProgramHeader
            {
                get { return _programHeader; }
                set { _programHeader = value; }
            }
        }
        CFG gCFG;
        string pname = "";
        int pid = 0;
        public MainFrm()
        {
            InitializeComponent();
            InitLocalSystem();
        }

        private bool InitLocalSystem()
        {
            try
            {
                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                // -----------------
                ReadConfig();
                //----------------
                TcpChannel chan = new TcpChannel(Convert.ToInt32(gCFG.ReportPort));
                RemotingConfiguration.RegisterWellKnownServiceType(
                    Type.GetType("ReceiverClass.ReceiverClass,ReceiverClass"),
                    "ReceiverObj",
                    WellKnownObjectMode.Singleton);

                WriteLogEvent("Zebra Filter Receiver: Ready to work ...");
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Filter Receiver #1:" + ex.Message);
                return false;
            }
            return true;
        }
      
        private void openConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfigFrm frm = new  ConfigFrm();
            frm.CFG = gCFG;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                createXml(frm.CFG);
                if (MessageBox.Show("Restart System Now.", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
        }
        private bool createXml(CFG _cfg)
        {
            try
            {
                String path = "";
                String fullPath = this.GetType().Assembly.GetName().CodeBase.ToString().ToUpper();
                String fileName = this.GetType().Assembly.GetName().Name + ".EXE";
                fileName = fileName.ToUpper();
                int iFound = fullPath.IndexOf(fileName);
                if (iFound > -1)
                {
                    path = fullPath.Substring(0, iFound);
                }
                else
                {
                    path = fullPath;
                }
            
                XmlSerializer serializer = new XmlSerializer(typeof(CFG));
                Stream fs = new FileStream("ReceiverClassCfg.xml", FileMode.Create);
                XmlWriter writer = new XmlTextWriter(fs, Encoding.Unicode);
                serializer.Serialize(writer, _cfg);
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message");
            }
            return false;
        }
        private void ReadConfig()
        {
            try
            {
                String path = "";
                String fullPath = this.GetType().Assembly.GetName().CodeBase.ToString().ToUpper();
                String fileName = this.GetType().Assembly.GetName().Name + ".EXE";
                fileName = fileName.ToUpper();
                int iFound = fullPath.IndexOf(fileName);
                if (iFound > -1)
                {
                    path = fullPath.Substring(0, iFound);
                }
                else
                {
                    path = fullPath;
                }

                XmlSerializer s = new XmlSerializer(typeof(CFG));
                XmlTextReader xmlRd = new XmlTextReader(path + "ReceiverClassCfg.xml");
                gCFG = (CFG)s.Deserialize(xmlRd);
                xmlRd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message");
            }
        }
   
        private void MainFrm_Shown(object sender, EventArgs e)
        {
            this.Text = String.Format("Zebra Filter Receiver V {0} {1}",  System.Reflection.Assembly.GetEntryAssembly().GetName().Version, gCFG.ProgramHeader);
            richTextBox1.Text = richTextBox1.Text + "\nMSMQ : " + gCFG.MessageQueue.ToString() + "\nPort : " + gCFG.ReportPort.ToString();
        }

        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
        
        }

        //write log to windows *** ������Է�������� ***
        private void WriteLogEvent(String msg)
        {
            //if (!EventLog.SourceExists("Zebra"))
            //{
            //    EventLog.CreateEventSource("Zebra", "ServerLog");
            //}
            //EventLog myLog = new EventLog();
            //myLog.Source = "Zebra";
            //myLog.WriteEntry(msg);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tsTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            //�Ҥ�� PID ����������͹��������ʵ���
            foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcesses())
            {
                pname = proc.ProcessName;
                if (pname == "mqsvc")
                {
                    int pid2 = proc.Id;
                    if (pid == 0)
                    {
                        pid = proc.Id;
                    }
                    if (pid != pid2)
                    {
                        Application.Exit();
                    }
                    pid = pid2;
                }
            }
            //
        }
// ...............
        private void MainFrm_Load(object sender, EventArgs e)
        {

        }
// ...............
    }
}