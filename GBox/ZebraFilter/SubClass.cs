using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Data;
using ZrebraFilterInterface;

namespace ZebraFilter
{
    class SubClass
    {
        private sqlCls sql = null;
        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
       
        public SubClass(string db)
        {
            sql = new sqlCls(db);
        }
        
        public void WriteLog(object log)
        {
            semaphor.WaitOne();

            string msg = (String)log;

            String logPath = Directory.GetCurrentDirectory() + "\\Log";

            if (!Directory.Exists(logPath))
            {
                Directory.CreateDirectory(logPath);
            }

            String fileName = logPath + "\\" + "DataLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                String fileNameOld = logPath + "\\" + "DataLog_" + DateTime.Now.AddDays(-1).ToString("_yyyy_MM_dd") + ".txt";


                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();

                if (File.Exists(fileNameOld))
                {
                    AddFileArchive(fileNameOld);
                    if (File.Exists(fileNameOld + ".rar"))
                    {
                        File.Delete(fileNameOld);
                        Thread.Sleep(3000);
                    }
                }
            }

            semaphor.Release();
        }
        public void AddFileArchive(String fileNameOld)
        {
            string src_file_name = fileNameOld;
            string dst_file_name = fileNameOld + ".rar";

            System.IO.FileStream file_input = null;
            System.IO.FileStream file_output = null;
            System.IO.Compression.GZipStream rar = null;
            byte[] buffer;

            try
            {
                file_output = new System.IO.FileStream(dst_file_name, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);
                rar = new System.IO.Compression.GZipStream(file_output, System.IO.Compression.CompressionMode.Compress, true);

                file_input = new System.IO.FileStream(src_file_name, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                buffer = new byte[file_input.Length];
                int iBuff = file_input.Read(buffer, 0, buffer.Length);
                file_input.Close();
                file_input = null;

                rar.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
            finally
            {
                if (rar != null)
                {
                    rar.Close();
                    rar = null;
                }
                if (file_output != null)
                {
                    file_output.Close();
                    file_output = null;
                }
                if (file_input != null)
                {
                    file_input.Close();
                    file_input = null;
                }
            }
        }

        public void GBoxStatus(Max2FmtMsg info)
        {
            try
            {
                sql.InsertNewGBoxStatus(Convert.ToInt32(info.vid), "0.0.0.0", 0, true, 0);
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }

        public bool checkTimeinterval(string veh_id, int evt_id, out bool bZerorow)
        {
            bool status = false;
            bZerorow = false;
            try
            {
                /********SQL2 OK***********/
                DataTable dt = sql.GetDataInterval(Convert.ToInt32(veh_id));

                if (dt.Rows.Count > 1)
                {
                    for (int i = dt.Rows.Count - 1; i > 0; i--)
                    {
                        DateTime timestamp1 = (DateTime)dt.Rows[i]["local_timestamp"];
                        DateTime timestamp2 = (DateTime)dt.Rows[i - 1]["local_timestamp"];
                        TimeSpan sp = timestamp2 - timestamp1;

                        if (sp.TotalMilliseconds > 0)
                        {
                            if (evt_id == 22)
                            {
                                if (sp.TotalSeconds >= 90 && sp.TotalSeconds <= 180)
                                {
                                    status = true;
                                }
                                else
                                    status = false;
                            }
                            else if (evt_id == 23)
                            {
                                if (sp.TotalMinutes >= 25 && sp.TotalMinutes <= 35)
                                {
                                    status = true;
                                }
                                else
                                    status = false;
                            }
                        }

                    }
                }
                else
                    bZerorow = true;
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
            return status;
        }
    }
}
