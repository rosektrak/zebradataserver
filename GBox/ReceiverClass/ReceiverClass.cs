using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Diagnostics;
using System.Xml.Serialization;
using System.IO;
using ZebraDataCentre;
using ZrebraFilterInterface;
using ZebraLogTools;

namespace ReceiverClass
{
    [Serializable]
    public class ReceiverClass : MarshalByRefObject, ReceiverInterface
    {
        public struct CFG
        {
            private string _messagequeue;
            [XmlElement("MessageQueue")]
            public string MessageQueue
            {
                get { return _messagequeue; }
                set { _messagequeue = value; }
            }
        }
        private CommonQeue MQ;
        private CFG gCFG;

        public ReceiverClass()
        {
            try
            {
                String path = "";
                String fullPath = this.GetType().Assembly.GetName().CodeBase.ToString().ToUpper();
                String fileName = this.GetType().Assembly.GetName().Name + ".DLL";
                fileName = fileName.ToUpper();
                int iFound = fullPath.IndexOf(fileName);
                if (iFound > -1) {
                    path = fullPath.Substring(0, iFound);
                } else {
                    path = fullPath;
                }
                XmlSerializer s = new XmlSerializer(typeof(CFG));
                XmlTextReader xmlRd = new XmlTextReader(path + "ReceiverClassCfg.xml");
                gCFG = (CFG)s.Deserialize(xmlRd);
                xmlRd.Close();

                MQ = new CommonQeue(gCFG.MessageQueue);
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Receiver Class #1:" + ex.Message);
            }
        }

        public bool IsAlive()
        {
            return true;
        }

        public void PushMax2Q(Max2FmtMsg msg)
        {
            try
            {
                MQ.PushQ(msg);
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Receiver Class #7:" + ex.Message);
            }
        }
        public void PushBroadCastQ(ZEBRABROADCASTMQINFO msg)
        {
            try
            {
                MQ.PushQ(msg);
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Receiver Class #7:" + ex.Message);
            }
        }
        //public void PushDistanceOverQ(ZEBRASMSDISTANCE msg)
        //{
        //    try
        //    {
        //        MQ.PushQ(msg);
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLogEvent("Zebra Receiver Class #7:" + ex.Message);
        //    }
        //}
        public void PushLogInfo(ZrebraFilterInterface.LogInfo msg)
        {
            try
            {
                MQ.PushQ(msg);
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Receiver Class #7:" + ex.Message);
            }
        }
        protected void WriteLogEvent(String msg)
        {
            try
            {
                TextCls text = new TextCls();
                msg = DateTime.Now.ToString() + " : " + msg;
                text.WriteLogError(msg); 
            }
            catch
            {
            }
        }


        public interface_debug[] debug(Max2FmtMsg msg)
        {

            var items = new List<interface_debug>();
            items.Add(new interface_debug() { name = "strep1" });


            MQ.PushQ(msg);

            items.Add(new interface_debug() { name = "strep2" });

            return items.ToArray();

        }
    }
}
