using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Threading;

using ZebraDataCentre;
using ZebraLogTools;
using ZrebraFilterInterface;

namespace ZebraForwardMSMQ
{
    public partial class MainFrm : Form
    {
        #region
        string pname = "";
        int pid = 0;
        public enum IPSTATE { NONE = 0, IP1 = 1, IP2 = 2 };

        private DataTable dtXmlConfig;
        private ReceiverInterface gReceiverObjFirst;
        private ReceiverInterface gReceiverObjSecond;
        private IPSTATE ipState;
        private Q_BROADCASTTYPE qType;
        private CommonQeue MQ;

        private String szSystemQ = "";
        private String szForwardIPFirst = "";
        private String szForwardIPSecond = "";
        private String szForwardPort = "";
        private String szHeaderProgram = "";

        private String RuningStatus = "BeginReceiveQMsg";
        private String szSystemQOld = "";

        private bool szMax2Format = false;
        private bool szBroadCastFormat = false;
        private bool szLogFormat = false;
        private bool enableIPSecond = false;
        private bool IsClosing = false;
        //private bool szDistanceOver = false;

        private int timerInterval = 0;
        private int TotalData = 0;

        private DateTime tmBeginRecive = new DateTime();
        System.Windows.Forms.Timer _tmClose;
        #endregion
        public MainFrm()
        {
            InitializeComponent();
            InitCfg();
            InitRemoteObj();
        }
        private void InitCfg()
        {
            try
            {
                dtXmlConfig = createConfigTable("Config");

                dtXmlConfig.ReadXml("Config.xml");
                foreach (DataRow dr in dtXmlConfig.Rows)
                {
                    enableIPSecond = Convert.ToBoolean((string)dr["CbIPSecond"]);
                    szForwardIPFirst = (string)dr["IPFirst"];
                    szForwardIPSecond = (string)dr["IPSecond"];
                    szForwardPort = (string)dr["Port"];
                    szSystemQ = (string)dr["SystemQueue"];
                    szMax2Format = Convert.ToBoolean((string)dr["Max2Format"]);
                    szBroadCastFormat = Convert.ToBoolean((string)dr["BroadCastFormat"]);
                    szLogFormat = Convert.ToBoolean((string)dr["LogFormat"]);
                    timerInterval = Convert.ToInt16((string)dr["TmInterval"]);
                    szHeaderProgram = (string)dr["TextHead"];
                    //szDistanceOver = Convert.ToBoolean((string)dr["DistanceOver"]);
                }

                if (szMax2Format)
                {
                    qType = Q_BROADCASTTYPE.MAX2;
                }
                else if (szBroadCastFormat)
                {
                    qType = Q_BROADCASTTYPE.BROADCAST;
                }
                else if (szLogFormat)
                {
                    qType = Q_BROADCASTTYPE.Log;
                }
                //else if (szDistanceOver)
                //{
                //    qType = Q_BROADCASTTYPE.DistanceOver;
                //}

                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                //****---- Read config. from setting file ------------------------------
                this.Text = String.Format("Zebra Forward MSMQ V{0} {1}", System.Reflection.Assembly.GetEntryAssembly().GetName().Version, szHeaderProgram);
                tmCheckState.Interval = 1000 * timerInterval;

                Thread.Sleep(1000);
                //---- Init System Q -----------------------------------------------
                MQ = new CommonQeue(szSystemQ);
                MQ.OnReceiveMQ += new ReceiveMQ(mq_OnReceiveMQ);
                MQ.InitReceiveQMsg();

                szSystemQOld = szSystemQ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message");
            }
        }

        private void InitRemoteObj()
        {
            bool ip1 = false;
            bool ip2 = false;

            try
            {
                String connStr = String.Format("tcp://{0}:{1}/ReceiverObj", szForwardIPFirst.TrimEnd(), szForwardPort.TrimEnd());
                gReceiverObjFirst = (ReceiverInterface)Activator.GetObject(typeof(ReceiverInterface), connStr);
                gReceiverObjFirst.IsAlive();
                ip1 = true;
            }
            catch (Exception e)
            {
                WriteLogError(e.Message);
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (enableIPSecond)
            {
                try
                {
                    String connStr = String.Format("tcp://{0}:{1}/ReceiverObj", szForwardIPSecond.TrimEnd(), szForwardPort.TrimEnd());
                    gReceiverObjSecond = (ReceiverInterface)Activator.GetObject(typeof(ReceiverInterface), connStr);
                    gReceiverObjSecond.IsAlive();
                    ip2 = true;
                }
                catch (Exception e)
                {
                    WriteLogError(e.Message);
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (ip1)
            {
                ipState = IPSTATE.IP1;
            }
            else if (ip2)
            {
                ipState = IPSTATE.IP2;
            }
            else
            {
                ipState = IPSTATE.NONE;
            }
        }
        private void setingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Seting frm = new Seting();
            frm.dtXml = dtXmlConfig;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                bool mqChang = createXml(frm.dtXml);
                if (MessageBox.Show("Chang System Now.", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    InitCfg();
                    InitRemoteObj();

                    if (mqChang)
                    {
                        MQ.BeginReceiveQMsg();
                        tmBeginRecive = DateTime.Now;
                    }
                }
            }
        }
        private void mq_OnReceiveMQ(object owner, XmlReader data, Stream stmData)
        {
            OnProcess(data);
            BeginReceiveQMsg();
        }

        private void OnProcess(XmlReader data)
        {
            try
            {
                switch (qType)
                {
                    case Q_BROADCASTTYPE.MAX2:
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(Max2FmtMsg));
                            Max2FmtMsg msg = (Max2FmtMsg)ser.Deserialize(data);

                            #region  Forward Q
                            switch (ipState)
                            {
                                case IPSTATE.IP1:
                                    {
                                        try
                                        {
                                            gReceiverObjFirst.PushMax2Q(msg);
                                        }
                                        catch
                                        {
                                            if (enableIPSecond)
                                            {
                                                ipState = IPSTATE.IP2;
                                            }
                                            else
                                            {
                                                ipState = IPSTATE.NONE;
                                            }
                                            MQ.PushQ(msg);
                                            Thread.Sleep(3000);
                                        }
                                    }
                                    break;
                                case IPSTATE.IP2:
                                    {
                                        try
                                        {
                                            gReceiverObjSecond.PushMax2Q(msg);
                                        }
                                        catch
                                        {
                                            ipState = IPSTATE.NONE;
                                            MQ.PushQ(msg);
                                        }
                                    }
                                    break;
                                default:
                                    {
                                        MQ.PushQ(msg);
                                    }
                                    break;
                            }
                            break;
                            #endregion
                        }
                    case Q_BROADCASTTYPE.BROADCAST:
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(ZEBRABROADCASTMQINFO));
                            ZEBRABROADCASTMQINFO msg = (ZEBRABROADCASTMQINFO)ser.Deserialize(data);

                            #region Forward Q
                            switch (ipState)
                            {
                                case IPSTATE.IP1:
                                    {
                                        try
                                        {
                                            gReceiverObjFirst.PushBroadCastQ(msg);
                                        }
                                        catch
                                        {
                                            if (enableIPSecond)
                                            {
                                                ipState = IPSTATE.IP2;
                                            }
                                            else
                                            {
                                                ipState = IPSTATE.NONE;
                                            }

                                            MQ.PushQ(msg);
                                        }
                                        break;
                                    }
                                case IPSTATE.IP2:
                                    {
                                        try
                                        {
                                            gReceiverObjSecond.PushBroadCastQ(msg);
                                        }
                                        catch
                                        {
                                            ipState = IPSTATE.NONE;
                                            MQ.PushQ(msg);
                                        }
                                        break;
                                    }
                                default:
                                    { MQ.PushQ(msg); }
                                    break;
                            }
                            #endregion
                            break;
                        }
                    case Q_BROADCASTTYPE.Log:
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(ZrebraFilterInterface.LogInfo));
                            ZrebraFilterInterface.LogInfo msg = (ZrebraFilterInterface.LogInfo)ser.Deserialize(data);

                            #region Forward Q
                            switch (ipState)
                            {
                                case IPSTATE.IP1:
                                    {
                                        try
                                        {
                                            gReceiverObjFirst.PushLogInfo(msg);
                                        }
                                        catch
                                        {
                                            if (enableIPSecond)
                                            {
                                                ipState = IPSTATE.IP2;
                                            }
                                            else
                                            {
                                                ipState = IPSTATE.NONE;
                                            }

                                            MQ.PushQ(msg);
                                        }
                                        break;
                                    }
                                case IPSTATE.IP2:
                                    {
                                        try
                                        {
                                            gReceiverObjSecond.PushLogInfo(msg);
                                        }
                                        catch
                                        {
                                            ipState = IPSTATE.NONE;
                                            MQ.PushQ(msg);
                                        }
                                        break;
                                    }
                                default:
                                    { MQ.PushQ(msg); }
                                    break;
                            }
                            #endregion
                        }
                        break;
                    //case Q_BROADCASTTYPE.DistanceOver:
                    //    {
                    //        XmlSerializer ser = new XmlSerializer(typeof(ZEBRASMSDISTANCE));
                    //        ZEBRASMSDISTANCE msg = (ZEBRASMSDISTANCE)ser.Deserialize(data);

                    //        #region Forward Q
                    //        switch (ipState)
                    //        {
                    //            case IPSTATE.IP1:
                    //                {
                    //                    try
                    //                    {
                    //                        gReceiverObjFirst.PushDistanceOverQ(msg);
                    //                    }
                    //                    catch
                    //                    {
                    //                        if (enableIPSecond)
                    //                        {
                    //                            ipState = IPSTATE.IP2;
                    //                        }
                    //                        else
                    //                        {
                    //                            ipState = IPSTATE.NONE;
                    //                        }

                    //                        MQ.PushQ(msg);
                    //                    }
                    //                    break;
                    //                }
                    //            case IPSTATE.IP2:
                    //                {
                    //                    try
                    //                    {
                    //                        gReceiverObjSecond.PushDistanceOverQ(msg);
                    //                    }
                    //                    catch
                    //                    {
                    //                        ipState = IPSTATE.NONE;
                    //                        MQ.PushQ(msg);
                    //                    }
                    //                    break;
                    //                }
                    //            default:
                    //                { MQ.PushQ(msg); }
                    //                break;
                    //        }
                    //        #endregion
                    //        break;
                    //    }
                }
            }
            catch (Exception ex)
            {
                ipState = IPSTATE.NONE;
                WriteLogError(ex.Message);
            }
        }
        private void BeginReceiveQMsg()
        {
            try
            {
                bool is_BeginReceiveQ = true;
                while (is_BeginReceiveQ)
                {
                    if (IsClosing)
                    {
                        break;
                    }
                    else if (ipState != IPSTATE.NONE)
                    {
                        MQ.BeginReceiveQMsg();
                        is_BeginReceiveQ = false;
                        TotalData++;
                        Thread.Sleep(10);
                        tmBeginRecive = DateTime.Now;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogError(ex.Message);
            }
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            MQ.BeginReceiveQMsg();
            tmBeginRecive = DateTime.Now;
            tmStatus.Start();
            tmCheckState.Start();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainFrm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            e.Cancel = !IsClosing;
            if (!IsClosing)
            {
                _tmClose = new System.Windows.Forms.Timer();
                _tmClose.Interval = 1000 * 10;
                _tmClose.Tick += new EventHandler(_tmClose_Tick);
                _tmClose.Enabled = true;
            }
            IsClosing = true;
        }

        void _tmClose_Tick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tmCheckState_Tick(object sender, EventArgs e)
        {
            tmCheckState.Stop();
            Thread thread = new Thread(new ThreadStart(CheckStateQueue));
            thread.Start();
            tmCheckState.Start();
        }

        private void CheckStateQueue()
        {
            try
            {
                gReceiverObjFirst.IsAlive();
                ipState = IPSTATE.IP1;
                goto Active;
            }
            catch
            {
                ipState = IPSTATE.NONE;
            }

            if (enableIPSecond)
            {
                try
                {
                    gReceiverObjSecond.IsAlive();
                    ipState = IPSTATE.IP2;
                }
                catch
                {
                    ipState = IPSTATE.NONE;
                }
            }
        Active:
            {
            }

            if (tmBeginRecive < DateTime.Now.AddMinutes(-2))
            {
                int total = MQ.CountQueue();
                if (total > 1000 && ipState != IPSTATE.NONE)
                {
                    MQ.BeginReceiveQMsg();
                    tmBeginRecive = DateTime.Now;
                    WriteLogError("Start MQ.BeginForwardQueueTotal : " + total.ToString() + " IPSTATE." + ipState);
                }
            }
        }

        private void tmStatus_Tick(object sender, EventArgs e)
        {
            if (IsClosing)
                RuningStatus = "Not BeginForwardQueue Delay : 10 Sec";
            else
                RuningStatus = "BeginForwardQueue";

            richTextBox1.Text = "Status : " + RuningStatus +
                                "\nTotalData : " + TotalData.ToString() +
                                "\nDestination IP1 : " + szForwardIPFirst + ":" + szForwardPort +
                                "\nDestination IP2 : " + szForwardIPSecond + ":" + szForwardPort + "    Enable : " + enableIPSecond +
                                "\nForword Queue : " + szSystemQ;

            tsIP.Text = "IP State : " + ipState;
            tsTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            //�Ҥ�� PID ����������͹��������ʵ���
            try
            {
                foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcesses())
                {
                    pname = proc.ProcessName;
                    if (pname == "mqsvc")
                    {
                        int pid2 = proc.Id;
                        if (pid == 0)
                        {
                            pid = proc.Id;
                        }
                        if (pid != pid2)
                        {
                            Application.Exit();
                        }
                        pid = pid2;
                    }
                    //MessageBox.Show("pass");
                }
            }
            catch (Exception ex)
            {
            }
            //
        }
        private bool createXml(DataTable dtTemp)
        {
            string Config = "Config";
            dtXmlConfig = createConfigTable(Config);
            string tmpQ = "";

            try
            {
                foreach (DataRow dr in dtTemp.Rows)
                {
                    dtXmlConfig.Rows.Add(
                                (string)dr["CbIPSecond"],
                                (string)dr["IPFirst"],
                                (string)dr["IPSecond"],
                                (string)dr["Port"],
                                (string)dr["SystemQueue"],
                                (string)dr["Max2Format"],
                                (string)dr["BroadCastFormat"],
                                (string)dr["LogFormat"],
                                (string)dr["TmInterval"],
                                (string)dr["TextHead"]
                                //(string)dr["DistanceOver"]
                                );
                    tmpQ = (string)dtTemp.Rows[0]["SystemQueue"];
                }
                dtXmlConfig.WriteXml(Config + ".xml");

                if (szSystemQOld != tmpQ)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                WriteLogError(ex.Message);
            }
            return false;
        }
        private DataTable createConfigTable(string tableName)
        {
            DataTable dtRet = new DataTable(tableName);
            dtRet.Columns.Add("CbIPSecond", typeof(string));
            dtRet.Columns.Add("IPFirst", typeof(string));
            dtRet.Columns.Add("IPSecond", typeof(string));
            dtRet.Columns.Add("Port", typeof(string));
            dtRet.Columns.Add("SystemQueue", typeof(string));
            dtRet.Columns.Add("Max2Format", typeof(string));
            dtRet.Columns.Add("BroadCastFormat", typeof(string));
            dtRet.Columns.Add("LogFormat", typeof(string));
            dtRet.Columns.Add("TmInterval", typeof(string));
            dtRet.Columns.Add("TextHead", typeof(string));
            //dtRet.Columns.Add("DistanceOver", typeof(string));
            return dtRet;
        }

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        private void WriteLogError(String Msg)
        {

            semaphor.WaitOne();
            try
            {
                TextCls text = new TextCls();
                Msg = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + '\t' + Msg;
                text.WriteLogError(Msg);
            }
            catch { }
            semaphor.Release();
        }

    }
}