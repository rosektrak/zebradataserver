using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.IO;
using System.Messaging;

namespace ZebraForwardMSMQ
{
    public delegate void ReceiveMQ(object owner, XmlReader xmlData, Stream stmData);

    public class CommonQeue
    {
        public event ReceiveMQ OnReceiveMQ;

        String GlobalQPath = @".\Private$\ZRQ";

        //const String GlobalQPath = @".\Private$\ZRQ";
        //const String GlobalQPath = @"FormatName:DIRECT=TCP:10.0.100.78\private$\ZebraQ";

        protected MessageQueue MQ;

        public CommonQeue()
        {
            if (MessageQueue.Exists(GlobalQPath))
            {
                MQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public CommonQeue(String QPathStr)
        {
            GlobalQPath = String.Format(".\\Private$\\{0}", QPathStr);

            if (MessageQueue.Exists(GlobalQPath))
            {
                MQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public CommonQeue(string QFwdIpStr, string QFwdPathStr)
        {
            try
            {
                GlobalQPath = String.Format(@"FormatName:Direct=TCP:{0}\private$\{1}", QFwdIpStr, QFwdPathStr);
                MQ = new MessageQueue(GlobalQPath);
            }
            catch
            {
            }
        }

        public int CountQueue()
        {
            if (MQ == null) { return -1; }
            int total = 0;
            try
            {
                total = MQ.GetAllMessages().Length;
            }
            catch
            { }
            return total;
        }

        public void InitReceiveQMsg()
        {
            if (MQ != null)
            {
                MQ.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                MQ.ReceiveCompleted += new ReceiveCompletedEventHandler(MQ_ReceiveCompleted);
            }
        }
        public void BeginReceiveQMsg()
        {
            MQ.BeginReceive();
        }
        private void MQ_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                StreamReader so = new StreamReader(e.Message.BodyStream);
                String s = so.ReadToEnd();

                StringReader txtRd = new StringReader(s);
                XmlReader xmlRd = XmlReader.Create(txtRd);
                OnReceiveMQ(this, xmlRd, e.Message.BodyStream);
            }
            catch
            {
            }
        }
        public void ForwardQ(Stream data)
        {
            try
            {
                Message msg = new Message();
                msg.BodyStream = data;
                MQ.Send(msg);
            }
            catch
            {
            }
        }

        public void PushQ(object txtMsg)
        {
            XmlWriterSettings wrSettings = new XmlWriterSettings();
            wrSettings.Indent = true;

            XmlSerializer s = new XmlSerializer(txtMsg.GetType());
            TextWriter txtWr = new StringWriter();

            XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
            xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

            s.Serialize(xmlWriter, txtMsg);
            txtWr.Close();

            byte[] buff = Encoding.UTF8.GetBytes(txtWr.ToString());
            Stream stm = new MemoryStream(buff);

            Message qMsg = new Message();
            qMsg.BodyStream = stm;
            qMsg.Recoverable = true;
            MQ.Send(qMsg);
        }

        public String GetQ()
        {
            Message msg = MQ.Receive();
 
            String foo = msg.Body.ToString();
            return foo;
        }
    }
}
