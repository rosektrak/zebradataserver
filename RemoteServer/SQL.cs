using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace RemoteServer
{
    class SQL
    {
        private string connStr = "Data Source = 10.0.10.70; Initial Catalog = ZebraDB; uid = sa; pwd = aab-gps-gtt";
        public SQL()
        { 
        }
        public DataTable GetVehInAddress(int zVid)
        {
            DataTable dtRet = new DataTable();
            try
            {
                SqlConnection cn = new SqlConnection(connStr);
                cn.Open();

                string sqlCmd = string.Format(
                    @"	DECLARE @veh_id INT 
						SET @veh_id = {0};
						
						SELECT mb.veh_id
							,isConnected
                            ,rec_timestamp
                            ,local_port
                            ,local_ip
                            ,type_of_box
						FROM [ZebraDB].[dbo].[mbox_status] mb
						WHERE veh_id = @veh_id AND type_of_box in ('M')
                ", zVid);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sqlCmd;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 30 * 30;
                cmd.Connection = cn;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtRet);

                cn.Close();
            }
            catch 
            {
            }

            return dtRet;
        }

        public int InsertLogCommand(int zVid, string zCmd,bool zSucess,DateTime zTime,string zHost,string zType)
        {
            int lasted_index = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spZMTrigger_Insert_CommandLog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", zVid);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@cmd", zCmd);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@suscess", zSucess);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@timestamp", zTime);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@host", zHost);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@send_type", zType);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@lasted_index", SqlDbType.Int);
                    param.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        lasted_index = (int)cm.Parameters["@lasted_index"].Value;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
            return lasted_index;
        }
        public bool UpdateLogCommand(int zIdx, int zVid, bool zSucess)
        {
            Boolean ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spZMTrigger_Update_CommandLog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@id", zIdx);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@veh_id", zVid);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@suscess", zSucess);
                    cm.Parameters.Add(param);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }
            return ret;
        }
        public bool UpdateLogCommandReport(int zIdx, int zVid, string zReport)
        {
            Boolean ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spZMTrigger_Update_ReportLog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@id", zIdx);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@veh_id", zVid);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@report", zReport);
                    cm.Parameters.Add(param);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }
    }
}
