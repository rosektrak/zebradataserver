using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections;

using ZebraGatewayInterface;
using ZebraCommonInterface;
using ZebraNet;
using ZebraDataCentre;
using ZebraPresentationLayer;
using ZebraLogTools;

namespace ZebraPortMonitor
{
    [Serializable]
    [XmlRoot("CFG")]
    public struct CFG
    {
        string _dbHost;
        [XmlElement("DbHost")]
        public string DbHost
        {
            get { return _dbHost; }
            set { _dbHost = value; }
        }

        string _host_ip;
        [XmlElement("Host_ip")]
        public string Host_ip
        {
            get { return _host_ip; }
            set { _host_ip = value; }
        }

        int _local_port;
        [XmlElement("Local_port")]
        public int Local_port
        {
            get { return _local_port; }
            set { _local_port = value; }
        }

        int _forward_port;
        [XmlElement("Forward_Port")]
        public int Forward_Port
        {
            get { return _forward_port; }
            set { _forward_port = value; }
        }

        string _remote_ip;
        [XmlElement("Remote_ip")]
        public string Remote_ip
        {
            get { return _remote_ip; }
            set { _remote_ip = value; }
        }

        int _remote_port;
        [XmlElement("Remote_port")]
        public int Remote_port
        {
            get { return _remote_port; }
            set { _remote_port = value; }
        }

        string _RewDataQ;
        [XmlElement("RawDataQ")]
        public string RawDataQ
        {
            get { return _RewDataQ; }
            set { _RewDataQ = value; }
        }

        string _readq;
        [XmlElement("ReadQ")]
        public string ReadQ
        {
            get { return _readq; }
            set { _readq = value; }
        }

        string _msg_tag;
        [XmlElement("Msg_tag")]
        public string Msg_tag
        {
            get { return _msg_tag; }
            set { _msg_tag = value; }
        }

        string _triggerIp;
        [XmlElement("TriggerIP")]
        public string TriggerIP
        {
            get { return _triggerIp; }
            set { _triggerIp = value; }
        }

        int _triggerPort;
        [XmlElement("TriggerPort")]
        public int TriggerPort
        {
            get { return _triggerPort; }
            set { _triggerPort = value; }
        }


        string _max2_pushq;
        [XmlElement("Max2PushQ")]
        public string Max2PushQ
        {
            get { return _max2_pushq; }
            set { _max2_pushq = value; }
        }

        bool _enableRecVeh;
        [XmlElement("EnableRecVeh")]
        public bool EnableRecVeh
        {
            get { return _enableRecVeh; }
            set { _enableRecVeh = value; }
        }

        bool _enableTrigger;
        [XmlElement("EnableTrigger")]
        public bool EnableTrigger
        {
            get { return _enableTrigger; }
            set { _enableTrigger = value; }
        }

        bool _EnableRemoteObj;
        [XmlElement("EnableRemoteObj")]
        public bool EnableRemoteObj
        {
            get { return _EnableRemoteObj; }
            set { _EnableRemoteObj = value; }
        }

        bool _EnableLoqQueue;
        [XmlElement("EnableLoqQueue")]
        public bool EnableLoqQueue
        {
            get { return _EnableLoqQueue; }
            set { _EnableLoqQueue = value; }
        }
    }
    public class MonitorEvtArgs : EventArgs
    {
        public String msg;
    }

    public  delegate void WriteFileProc(String msg);
    public  delegate void SetTextCallback(String t);
    public  delegate void MonitorsMsgHandler(MonitorEvtArgs e);
    public  delegate void MonitorsMsgHandler2(MonitorEvtArgs e);
    public  delegate void ClientDisconectedHandler(NetDisConnectedEvent e);

    public  delegate String NetDispatchData(ReceivedMsgEvent e);

    public class EventWrapper : MarshalByRefObject
    {
        public event MonitorsMsgHandler         OnMonitorsMsg ;
        public void  MonitorsEventHandler(MonitorEvtArgs o)
        {
            OnMonitorsMsg(o);
        }
    }
        public class Monitors : MarshalByRefObject, GatewayInterface
        {
            private CFG _CFG = new CFG();
            public CFG CFG
            {
                set { _CFG = value; }
                get { return _CFG; }
            }

            #region Constant
            private const byte RESET_ACK = 0x03;
            private const byte GPIO_ACK = 0x80;
            private const uint GGatePort = 6005;
            #endregion

            /*
            #region Properties
            private string _remote_ip = "127.0.0.1";
            public string Remote_ip
            {
                get { return _remote_ip; }
                set { _remote_ip = value; }
            }

            private int _remote_port = 6000;
            public int Remote_port
            {
                get { return _remote_port; }
                set { _remote_port = value; }
            }

            private string _msg_tag = "???";
            public string Msg_tag
            {
                get { return _msg_tag; }
                set { _msg_tag = value; }
            }

            private string _readq = "???";
            public string ReadQ
            {
                get { return _readq; }
                set { _readq = value; }
            }

            private string _max2_pushq = "???";
            public string Max2PushQ
            {
                get { return _max2_pushq; }
                set { _max2_pushq = value; }
            }

            private string _local_ip = "127.0.0.1";
            public string Local_ip
            {
                get { return _local_ip; }
                set { _local_ip = value; }
            }

            private int _local_port = 5000;
            public int Local_port
            {
                get { return _local_port; }
                set { _local_port = value; }
            }

            private string _raw_data = "rawdata";
            public string RawData
            {
                get { return _raw_data; }
                set { _raw_data = value; }
            }
            #endregion
            */
      
            #region Variables
            public bool FstRun = true;
            public MonitorsMsgHandler MonMsgHandler = null;

            public event MonitorsMsgHandler2 OnMonitorsMsg2;
            public event ClientDisconectedHandler OnDisconnectFromClient;

            public TcpNet tcp;
            public GBoxQueue MQ;
            public CommonQeue Max2PushQueue;
            public CommonQeue MQLog;
            private MAX2 max2 = new MAX2();
            private ArrayList arMax2PushQueue = new ArrayList();
            private Queue gQueue = new Queue();
            #endregion

            public Monitors()
            {
                try
                {
                    if (!InitCfg()) return;
                  
                    foreach (string q in _CFG.Max2PushQ.Split(new char[] { ',' }))
                    {
                        try
                        {
                            if (q.Length > 0)
                            {
                                Max2PushQueue = new CommonQeue(q);
                                arMax2PushQueue.Add(Max2PushQueue);
                            }
                        }
                        catch
                        { }
                    }


                    tcp = new TcpNet(_CFG.Local_port, true);
                    MQ = new GBoxQueue(_CFG.ReadQ);
                    MQLog = new CommonQeue(_CFG.RawDataQ);

                    tcp.OnReceivedMsg += new ReceivedMsgHandler(tcp_OnReceivedMsg);
                    tcp.OnNetDisconnected += new NetDisconnetedHandler(tcp_OnNetDisconnected);
                }
                catch (Exception ex)
                {
                    WriteLogEvent("Port Monitor #1:" + ex.Message);
                }
            }
            ~Monitors()
            {
                Close();
            }

            public bool InitCfg()
            {
                try
                {
                    String path = "";
                    String fullPath = this.GetType().Assembly.GetName().CodeBase;
                    int iFound = fullPath.IndexOf(this.GetType().Assembly.GetName().Name + ".DLL");

                    if (iFound > -1)
                    {
                        path = fullPath.Substring(0, iFound);
                    }
                    else
                    {
                        path = fullPath;
                    }

                    XmlSerializer s = new XmlSerializer(typeof(CFG));
                    XmlTextReader xmlRd = new XmlTextReader(path + "PortMonitorCfg.xml");
                    _CFG = (CFG)s.Deserialize(xmlRd);

                  
                    WriteLogEvent(String.Format("Port Monitor #2: Remoted to {0}:{1}, Local Port : {2}, Tag : {3}, Read-Q : {4}", _CFG.Remote_ip, _CFG.Remote_port, _CFG.Local_port, _CFG.Msg_tag, _CFG.ReadQ));
                    xmlRd.Close();
                }
                catch (Exception ex)
                {
                    WriteLogEvent("Port Monitor #3:" + ex.Message);
                    return false;
                }
                return true;
            }
       

            #region  Remote methodes

            public void Close()
            {
                tcp.OnReceivedMsg -= new ReceivedMsgHandler(tcp_OnReceivedMsg);
                tcp.CloseAll();
            }
            public void TestPost()
            {
                PostMsg2("???");
                tcp.Connect(_CFG.Remote_ip, _CFG.Remote_port, System.Text.ASCIIEncoding.ASCII.GetBytes("???"));
            }
            public bool SendDeviceCmd(int veh_id, string cmd)
            {
                bool res = false;
                return res;
            }
            public int GetNumberOfConnection()
            {
                if (tcp == null || tcp.ClientList == null)
                {
                    return 0;
                }
                return tcp.ClientList.Count;
            }
            public String Alive()
            {
                return "OK";
            }
            public bool ChkConnAlive(IPEndPoint enp)
            {
                return tcp.CheckConnectionAlive(enp);
            }
            public GATEWAYINFO GetIPStr()
            {
                GATEWAYINFO GatewayInfo = new GATEWAYINFO();

                GatewayInfo.ipaddr = _CFG.Remote_ip;
                GatewayInfo.port = _CFG.Remote_port;
                GatewayInfo.total_vehicle = 0;

                return GatewayInfo;
            }
            public ArrayList GetActiveConnectionList()
            {
                ArrayList cnnList = new ArrayList();                
                if (tcp == null || tcp.ClientList == null)
                {
                    return cnnList;
                }

                foreach (ClientManager cln in tcp.ClientList) {
                    cnnList.Add(cln);
                }                
                return cnnList;
            }
            #endregion

            #region local functions
            public void RegisterForEvents(EventWrapper ew)
            {
                MonMsgHandler = new MonitorsMsgHandler(ew.MonitorsEventHandler);
            }
            public void UnregisterForEvents()
            {
            }

            public void udp_OnReceivedMsg(object Sender, ReceivedMsgEvent e)
            {
                DoCallBack(e);
            }
            public void tcp_OnReceivedMsg(object Sender, ReceivedMsgEvent e)
            {
                DoCallBack(e);
            }
            public void tcp_OnNetDisconnected(object Sender, NetDisConnectedEvent e)
            {
                OnDisconnectFromClient(e);
            }

            private void ForwardIP(object obj)
            {
                ESRI esri = new ESRI();
                if (obj != null)
                {
                    if (obj.GetType() == typeof(RTPVmsg))
                    {
                        RTPVmsg msg = (RTPVmsg)obj;
                        byte[] tmp;

                        tmp = new byte[] { msg.utc2, msg.utc1 };
                        String szUtcTime = BitConverter.ToUInt16(tmp, 0).ToString();
                        String szDateTime = GetLocalTimeStamp();
                        String szUtcDateTime = GetUTCTimeStamp();

                        tmp = new byte[] { msg.lat4, msg.lat3, msg.lat2, msg.lat1 };
                        String szLat = ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString();

                        tmp = new byte[] { msg.lon4, msg.lon3, msg.lon2, msg.lon1 };
                        String szLon = ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString();

                        String szAlt = msg.alt1.ToString();
                        Double iSpd = (Double.Parse(msg.spd1.ToString())) * 1.852;
                        String szSpd = iSpd.ToString("0");
                        String szCsr = msg.crs1.ToString();

                        String szHdop = msg.hdop1.ToString();
                        String szNumOfSat = msg.num_of_satellite.ToString();

                        Int16 iGpsStatus = CalStatus(Int16.Parse(szHdop), Int16.Parse(szNumOfSat));
                        String szGpsStatus = Char.ToString((char)msg.gps_status1);

                        tmp = new byte[] { msg.odo4, msg.odo3, msg.odo2, msg.odo1 };
                        String szOdo = BitConverter.ToUInt16(tmp, 0).ToString();

                        tmp = new byte[] { msg.gps_odo4, msg.gps_odo3, msg.gps_odo2, msg.gps_odo1 };
                        String szGpsOdo = BitConverter.ToUInt16(tmp, 0).ToString();

                        tmp = new byte[] { msg.cell_id2, msg.cell_id1 };
                        String szCellID = BitConverter.ToUInt16(tmp, 0).ToString();

                        tmp = new byte[] { msg.loc_id2, msg.loc_id1 };
                        String szLocID = BitConverter.ToUInt16(tmp, 0).ToString();

                        String szRssi = msg.rssi1.ToString();
                        String szVehBattery = msg.veh_battery1.ToString();
                        String szVehPower = msg.veh_power1.ToString();

                        tmp = new byte[] { msg.VID2, msg.VID1 };
                        String szVID = BitConverter.ToUInt16(tmp, 0).ToString();

                        String szTimeStamp = GetTimeStamp();

                        String dataStr = esri.WriteString(_CFG.Msg_tag, szLon + "," + szLat, szVID, szUtcDateTime, szCsr, szSpd, iGpsStatus.ToString());
                        tcp.Connect(_CFG.Remote_ip, _CFG.Remote_port, System.Text.ASCIIEncoding.ASCII.GetBytes(dataStr));


                        MQ.PushLogQ(
                            szUtcTime, szLat, szLon, szAlt, szSpd, szCsr, szHdop, szGpsStatus, "33", szTimeStamp,
                            szVID, "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
                            "0", "0", "0", "0", "0", "0", "0", "0", "0", szNumOfSat, "0", "0", szCellID, szLocID, szRssi,
                            szVehBattery, szVehPower, "0", "0", "0", "GPRS", "INPUT", "false", "0", "0", "0", "0", "0");
                    }
                }
            }

            private Int16 CalStatus(Int16 hdop, Int16 nSat)
            {
                Int16 iStatus = 1;
                if (hdop <= 2)
                {
                    iStatus = 2;
                }
                else if (hdop > 2 && hdop < 6)
                {
                    if (nSat > 4)
                    {
                        iStatus = 3;
                    }
                }

                return iStatus;
            }
            private String GetTimeStamp()
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                DateTime d = DateTime.Parse(DateTime.UtcNow.ToString());
                String szTimeStamp = d.ToString("yyyy-MM-ddTHH:mm:sssszzzz");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }

            private String GetTimeStamp(String date, String time)
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                String szTimeStamp = d.ToString("yyyy-MM-ddTHH:mm:sssszzzz");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            private String GetLocalTimeStamp()
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                DateTime d = DateTime.Parse(DateTime.Now.ToString());
                String szTimeStamp = d.ToString("MM/dd/yyyy HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            private String GetUTCTimeStamp()
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                DateTime d = DateTime.Parse(DateTime.UtcNow.ToString());
                String szTimeStamp = d.ToString("MM/dd/yyyy HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }

            private void DoCallBack(ReceivedMsgEvent e)
            {
                NetDispatchData proc = new NetDispatchData(CallBackProc);
                proc.BeginInvoke(e, CalBackCompletedProc, null);
            }

            private String CallBackProc(ReceivedMsgEvent e)
            {
                String prnStr = "";
                String retPrnStr = "";
                try
                {
                    if (e.data.Length < 50) return "";


                    String dataSeq = Encoding.ASCII.GetString(e.data);
                    String[] dataSeqStr = dataSeq.Split(new char[] { '\n' });
                    if (dataSeqStr.Length <= 0) { return retPrnStr; }

                    prnStr = Encoding.ASCII.GetString(e.data).TrimEnd(new char[] { '\0', '\n', '\r' });
                    if (_CFG.EnableLoqQueue) PostRawData(prnStr, e.remoteIP.ToString(), _CFG.Host_ip.ToString() + ":" + _CFG.Local_port.ToString());
                   
                    foreach (String msgStr in dataSeqStr)
                    {
                        if (msgStr[0] != '$' || msgStr.Length < 20)
                        {
                            if (msgStr[0] == 'R' || msgStr[0] == 'W')
                            {
                                PostMsg2(msgStr.TrimEnd(new char[] { '\0', '\n', '\r' }));
                            }
                            continue;
                        }

                        byte[] data = Encoding.ASCII.GetBytes(msgStr.ToCharArray());
                        object obj = null;
                        object obj1 = max2.Translate(data);

                        if (obj1 != null)
                        {
                            obj = obj1;
                        }
                        else
                        {
                            return retPrnStr;
                        }

                        if (obj != null)
                        {
                            if (obj.GetType() == typeof(ZebraPresentationLayer.Max2FmtMsg))
                            {
                                if (data != null && data.Length > 5)
                                {
                                    if (prnStr.Length > 11)
                                    {
                                        if (((ZebraPresentationLayer.Max2FmtMsg)obj).type_of_message == null) { continue; }

                                        Max2FmtMsg max2Frm = (Max2FmtMsg)obj;
                                        max2Frm.conInfo = new ConnectionInfo();
                                        max2Frm.conInfo.source_ip = (e.remoteIP.ToString()).Split(':')[0];
                                        max2Frm.conInfo.source_port = (e.remoteIP.ToString()).Split(':')[1];
                                        max2Frm.conInfo.destination_ip = _CFG.Host_ip;
                                        max2Frm.conInfo.destination_port = _CFG.Local_port.ToString();
                                        max2Frm.conInfo.type_of_box = "M";

                                        int iVid = -1;
                                        try { iVid = Convert.ToInt32(max2Frm.vid); }
                                        catch { continue; }

                                        PutMax2Msg(max2Frm);
                                        Thread.Sleep(200);

                                        if (max2Frm.type_of_message == "R")
                                        {
                                            string[] zMsg = msgStr.Split(',');
                                            string report = "Report@" + max2Frm.vid.Trim() + "@" + max2Frm.type_of_message.ToString() + "@" + zMsg[zMsg.Length - 1].Trim();
                                            PostMsg2(report);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    retPrnStr = prnStr + "\r\n" + ",EP:" + e.remoteIP.ToString();
                    string log = prnStr;
                    log = prnStr + ",EP:" + e.remoteIP.ToString() + " " + DateTime.Now.ToString("HH:mm:ss");
                    LogToFile(log);
                }
                catch (Exception ex)
                {
                    WriteLogEvent("Port Monitor #4:" + ex.Message);
                }
                //Console.WriteLine(retPrnStr);
                return retPrnStr;
            }
            private String CallBackProc2(ReceivedMsgEvent e)
            {
                String prnStr = "";
                String retPrnStr = "";
                String writeString = "";
                String Address = ",EP:" + e.remoteIP.ToString() + "\n";

                try
                {
                    if (e.data.Length <= 0) return "";
                    String dataSeq = Encoding.ASCII.GetString(e.data).TrimEnd('\0');

                    String[] dataSeqStr = dataSeq.Split(new char[] { '\r', '\n' });
                    if (dataSeqStr.Length <= 0) { return retPrnStr; }

                    #region  ProcessString
                    foreach (String msgStr in dataSeqStr)
                    {
                        if (msgStr.Trim().Length <= 0) continue;

                        writeString += msgStr + "\r\n";

                        if (msgStr[0] != '$' || msgStr.Length < 20)
                        {
                            if (msgStr[0] == 'R' || msgStr[0] == 'W' || msgStr[0] == 'B')
                            {
                                PostMsg2(msgStr.Trim());
                            }
                            continue;
                        }
                        //$888       ,060180,211929,00.00000N,000.00000E,000,000,0,00,00,000,R,888~~~~~~~
                        byte[] data = Encoding.ASCII.GetBytes(msgStr.ToCharArray());
                        object obj = null;
                        object obj1 = max2.Translate(data);    ///max 2 
                        object obj2 = null;//gtrax2.Translate(data);  ///gtrax2

                        if (obj1 != null)
                        {
                            obj = obj1;
                        }
                        else if (obj2 != null)
                        {
                            obj = obj2;

                            String prnStr2 = Encoding.ASCII.GetString(e.data).TrimEnd(new char[] { '\0', '\n', '\r' });
                            Max2FmtMsg msg = (Max2FmtMsg)obj;
                            prnStr = "$" +
                                     msg.vid.PadRight(11) + "," +
                                     msg.gps_date_time + "," +
                                     msg.utc + "," +
                                     msg.lat + "N," +
                                     msg.lon + "E," +
                                     msg.speed.Trim() + "," +
                                     msg.course + "," +
                                     msg.type_of_fix + "," +
                                     msg.no_of_satellite + "," +
                                     msg.gpio_status + "," +
                                     msg.analog_level + "," +
                                     msg.type_of_message + ",GTRAXMSG," +
                                     prnStr2;
                        }

                        if (obj != null)
                        {
                            if (obj.GetType() != typeof(Max2FmtMsg)) { continue; }
                            if (data != null && data.Length > 5)
                            {
                                if (((Max2FmtMsg)obj).type_of_message == null) { continue; }

                                int iVid = -1;
                                String vidStr = ((Max2FmtMsg)obj).vid;
                                if (int.TryParse(vidStr.Trim(), out iVid))
                                {
                                    PutMax2Msg((Max2FmtMsg)obj);
                                }
                                if (((Max2FmtMsg)obj).type_of_message == "R")
                                {
                                    string[] zMsg = msgStr.Split(',');
                                    string report = "Report@" + ((Max2FmtMsg)obj).vid.Trim() + "@" + ((Max2FmtMsg)obj).type_of_message.ToString() + "@" + zMsg[zMsg.Length-1].Trim();
                                    PostMsg2(report);
                                }
                            }
                            retPrnStr += msgStr + "\r\n"; ;
                        }

                        if (dataSeqStr.Length > 1) Thread.Sleep(50);
                    }
                    #endregion
                    if (writeString.Length > 2)
                    {
                        string ddsds = DateTime.Now.ToString("HH:mm:ss") + " - " + writeString.Substring(0, writeString.Length - 2) + ",EP:" + e.remoteIP.ToString();
                        LogToFile(ddsds);
                        retPrnStr += Address;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                return retPrnStr;
            }
            private void CalBackCompletedProc(IAsyncResult ar)
            {
                AsyncResult result = (AsyncResult)ar;
                NetDispatchData proc = (NetDispatchData)result.AsyncDelegate;
                String retStr = proc.EndInvoke(ar);
                if (retStr.Length > 0)
                {
                    PostMsg2(retStr);
                }
            }

            private void PutMax2Msg(ZebraPresentationLayer.Max2FmtMsg obj)
            {
                if (obj.GetType() != null)
                {
                    if (obj.GetType() == typeof(ZebraPresentationLayer.Max2FmtMsg))
                    {
                        foreach (CommonQeue q in arMax2PushQueue)
                        {
                            q.PushQ(obj);
                        }
                    }
                }
            }

            private void PostMsg(String msg)
            {
                try
                {
                    if (MonMsgHandler != null)
                    {
                        MonitorEvtArgs e = new MonitorEvtArgs();
                        e.msg = msg;
                        MonMsgHandler(e);
                    }
                }
                catch (Exception ex)
                {
                    MonMsgHandler = null;
                    WriteLogEvent("Port Monitor #5:" + ex.Message);
                }
            }

            private void PostMsg2(String msg)//enqueue msg to rich text
            {
                MonitorEvtArgs e = new MonitorEvtArgs();
                e.msg = msg;
                OnMonitorsMsg2(e);
            }
            private void PostRawData(string msg, string source, string destination)
            {
                ZrebraFilterInterface.LogInfo e = new ZrebraFilterInterface.LogInfo();
                e.data = msg;
                e.time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                e.source = source;
                e.destination = destination;
                e.data_type = ((int)BoxVersions.MBox).ToString();
                MQLog.PushQ(e);
            }
            private void LogToFile(String msg)
            {
                WriteFileProc fn = new WriteFileProc(WriteNetFile);
                fn.BeginInvoke(msg, WriteNetFileCompleted, null);
            }

            System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
            private void WriteNetFile(String msg)
            {
                semaphor.WaitOne();
                TextCls text = new TextCls();
                text.WriteTextFileLiftTime(Directory.GetCurrentDirectory(), msg, 30);
                semaphor.Release();
            }
            private void WriteNetFileCompleted(IAsyncResult ar)
            {
                AsyncResult result = (AsyncResult)ar;
                WriteFileProc proc = (WriteFileProc)result.AsyncDelegate;
                proc.EndInvoke(ar);
            }

            private void WriteLogEvent(String msg)
            {
                try
                {
                    msg = DateTime.Now.ToString() + " : " + msg;
                    String fileName = DateTime.Now.ToString("yyyyMM_") + "MonitorLogEvent.txt";
                    if (File.Exists(fileName))
                    {
                        StreamWriter sr = File.AppendText(fileName);
                        sr.WriteLine(msg);
                        sr.Close();
                        sr.Dispose();
                    }
                    else
                    {
                        StreamWriter sr = File.CreateText(fileName);
                        sr.WriteLine(msg);
                        sr.Close();
                        sr.Dispose();
                    }
                }
                catch { }
            }
            #endregion        
        }
}
