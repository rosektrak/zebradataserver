using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Tcp;
using System.Threading;
using ZebraEncryption;
using ZebraRemoteInterface;
using ZebraGatewayInterface;
using ZebraDataCentre;
using ZebraLogTools;

namespace ZebraGateWay
{
    public partial class MainFrm : Form
    {
        private enum TreeViewType { ADD = 0, UPDATE = 1 };
    
        private System.Threading.Semaphore updateMsgQSemaphore = new System.Threading.Semaphore(1, 1);
        private Queue<string> qDataLog = new Queue<string>();
        private System.Threading.Semaphore updateMsgMBoxQSemaphore = new System.Threading.Semaphore(1, 1);
        private Queue<string> qMboxLog = new Queue<string>();
        private System.Threading.Semaphore updateSystemSemaphore = new System.Threading.Semaphore(1, 1);
        private Queue<NetDisConnectedEvent> qConnecttion = new Queue<NetDisConnectedEvent>();
        private System.Threading.Semaphore updateTreeNodeSemaphore = new System.Threading.Semaphore(1, 1);

        private Version thisVersion = new Version("1.0");

        private Monitor objMonitor;
        private TextCls objText = new TextCls();
        private RemoteSeverInterface remoteServerInterface;

        private DataTable dtXmlConfig;
        private DataTable gDtVehList = null;
        private DataTable gDtReadCommand = new DataTable();
        private DataTable gDtWriteCommand = new DataTable();

        private Hashtable gHsVehExternalCommad = new Hashtable();
        private Hashtable gHsVehInternalCommad = new Hashtable();

        private bool IsCloseMainFrom = false;
        private DateTime TimeClose = new DateTime();
        private DateTime TimeDisPlayMonitor = new DateTime();

        private ArrayList regLogMsg = new ArrayList();
        private SQL gSql;

        private string gLocalIP = "";
        private int gLocalPort = -1;

        public MainFrm()
        {
            InitializeComponent();
            InitializeSystem();
        }
        private void InitializeSystem()
        {
            try
            {
                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                //-----Open TCP
                objMonitor = new Monitor();
                objMonitor.OnMonitorsReceived += new MonitorsMsgHandler(objPortMon_OnMonitorsReceived);
                objMonitor.OnMonitorsReceivedMBox += new MonitorsMsgHandler(objMonitor_OnMonitorsReceivedMBox);
                objMonitor.OnMonitorsDisconnected += new MonitorsMsgHandler2(objMonitor_OnMonitorsDisconnected);

                this.ipDataBase.Text = objMonitor.Database;
                this.tbLocalPort.Text = objMonitor.LocalPort.ToString();
                this.tbForwardPort.Text = objMonitor.ForwardPort.ToString();
                this.tbRemotePort.Text = objMonitor.RemotePort.ToString();
                this.rtSystemQueue.Text = objMonitor.SystemQueue.Replace(',', '\n');
                this.tbLocalIP.Text = objMonitor.LocalIP.ToString();
                dtXmlConfig = objMonitor.DtXmlConfig;

                gSql = new SQL(objMonitor.Database);

                remoteServerInterface = new RemoteSeverInterface(objMonitor.ForwardPort);
                remoteServerInterface.Init();
                remoteServerInterface.OnRemoteMsg += new RemoteMsgHandler(remoteInterface_OnRemoteMsg);

                if (objMonitor.EnableRemoteObj)
                    InitRemoteMonitorObj();

                CreateVehListTable();
                //gLocalIP = GetIP4Address();
                gLocalPort = objMonitor.LocalPort;

                this.Text = "ZebraGateway V." + System.Reflection.Assembly.GetEntryAssembly().GetName().Version;
            }
            catch { }
        }
        private void InitializeCommand()
        {
            try
            {
                TranslateCommand ts = new TranslateCommand();
                TranslateCommandSQL tsq = new TranslateCommandSQL(objMonitor.Database);
                DataTable dtTemp = new DataTable();
                if (objMonitor.BoxVersion == BoxVersions.KBox)
                {
                    dtTemp = tsq.GetCommand(TypeOfBox.K, TypeCommand.R);
                    gDtReadCommand = ts.GetKBoxCommand(dtTemp);
                    dtTemp = tsq.GetCommand(TypeOfBox.K, TypeCommand.W);
                    gDtWriteCommand = ts.GetKBoxCommand(dtTemp);
                }
                else if (objMonitor.BoxVersion == BoxVersions.MBox)
                {
                    gDtReadCommand = tsq.GetCommand(TypeOfBox.M, TypeCommand.R);
                    gDtWriteCommand = tsq.GetCommand(TypeOfBox.M, TypeCommand.W);
                }
            }
            catch
            { }
        }
        private void InitRemoteMonitorObj()
        {
            try
            {
                TcpChannel chan = new TcpChannel(objMonitor.RemotePort);
                //RemotingConfiguration.RegisterWellKnownServiceType(
                //    Type.GetType("ZebraGateWay.BroadcastRemoteReceiver,BroadcastRemoteReceiver"),
                //    "BroadcastInterface",
                //    WellKnownObjectMode.Singleton);

                RemotingConfiguration.RegisterWellKnownServiceType(typeof(ZebraGateWay.BroadcastRemoteReceiver),"SMSObj",WellKnownObjectMode.Singleton);

            }
            catch (Exception ex)
            {
                //WriteLogEvent("Zebra Sevice Error #3" + ex.Message);
            }
        }
        private void remoteInterface_OnRemoteMsg(Socket socket, int veh_id, string cmd, string data)
        {
            if (socket == null) return;

            try
            {
                REMOTECMDINFO info = new REMOTECMDINFO();
                info.veh_id = veh_id;
                info.cmd = cmd;
                info.data = data.Trim(new char[] { '\0' });
                info.socket = socket;

                if (cmd.Trim().ToUpper() == REMOTECMD.TOTALVEHS.ToString())
                {
                    return;
                }
                else if (cmd.Trim().ToUpper() == REMOTECMD.GETVEHSLIST.ToString())
                {
                    GetAllVehicles(info);
                    return;
                }
                else if (cmd.Trim().ToUpper() == REMOTECMD.REGISTERTOLOG.ToString())
                {
                    RegisterTologMsg(socket);
                    return;
                }
                else
                {
                    if (veh_id > -1 && cmd.Length > 0 && data.Length > -1)
                    {
                         string command = cmd;
                         if (!data.StartsWith("\0\0"))
                         {
                             command = command + ',' + data.Substring(0, data.IndexOf('\0'));
                         }

                        if (SendData(checkEp(veh_id.ToString()), veh_id, command))
                        {
                            if (!cmd.StartsWith("[VehicleCommand]"))
                            {
                                if (!gHsVehExternalCommad.ContainsKey(veh_id))
                                {
                                    gHsVehExternalCommad.Add(veh_id, info);
                                }
                                else
                                {
                                    gHsVehExternalCommad[veh_id] = info;
                                }
                            }
                        }
                    }
                }
            }
            catch
            { }
        }
        private void GetAllVehicles(REMOTECMDINFO info)
        {
            try
            {
                string vehs = "";
                if (tvEngineOn != null && tvEngineOn.Nodes != null)
                {
                    foreach (TreeNode node in tvEngineOn.Nodes)
                    {
                        CONNECTINFO nodeInfo = (CONNECTINFO)node.Tag;
                        vehs += nodeInfo.VehID.ToString() + "#";
                    }
                    if (vehs.Trim().Length > 1)
                    {
                        vehs = vehs.Substring(0, vehs.Length - 1);
                        remoteServerInterface.SendCmd(info.socket, info.veh_id, info.cmd, vehs);
                    }
                }
            }
            catch
            {
            }
        }
        private void configToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfigForm frm = new ConfigForm();
            frm.dtCMDList = dtXmlConfig;
            frm.ShowDialog();

            if (frm.btConfig.DialogResult == DialogResult.OK)
            {
                createXml(frm.dtCMDList);
                if (MessageBox.Show("Initialize System. Restart Application.", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    objMonitor.closeSecsion();
                    ProcessKill();
                }
            }
        }
        private bool createXml(DataTable dtTemp)
        {
            string Config = "Config";
            try
            {
                dtXmlConfig.WriteXml(Config + ".xml");
            }
            catch (Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }

            return false;
        }
        private void MainFrm_Load(object sender, EventArgs e)
        {
            this.tmDisplay.Start();
            this.tmSystem.Start();
            this.tvEngineOn.Nodes.Clear();
            this.tvEngineOff.Nodes.Clear();
            this.tvOffline.Nodes.Clear();
            //this.tvConnection.Nodes.Clear();
            InitializeCommand();
            gDtVehList = CreateVehListTable();
        }
        private void ShowLogMsg(string msg)
        {
            try
            {
                if (msg.Length == 0)
                {
                    return;
                }

                string[] stringSeparators = new string[] { ",EP:", ",VID:" };
                string[] result = msg.Split(stringSeparators, StringSplitOptions.None);

                #region VehCommad
                if (gHsVehInternalCommad.Count > 0 || gHsVehExternalCommad.Count > 0)
                {
                    int Vid = -1;
                    string Result = string.Empty;
                    string[] data = result[0].Split('\r');
                    foreach (string st in data)
                    {
                        if (st.IndexOf('#') == -1) continue;

                        string[] dataSplit = st.Split(',');
                        if (dataSplit.Length >= 4 && (dataSplit[1] == "5" || dataSplit[1] == "7"))
                        {
                            Vid = int.Parse(dataSplit[dataSplit.Length - 1]);
                            Result = dataSplit[2];

                            if (gHsVehInternalCommad.ContainsKey(Vid))
                            {
                                if (Result.StartsWith((string)(gHsVehInternalCommad[Vid])))
                                {
                                    MessageBox.Show(st, "Result " + Vid.ToString());
                                    gHsVehInternalCommad.Remove(Vid);
                                }
                            }
                            if (gHsVehExternalCommad.ContainsKey(Vid))
                            {
                                REMOTECMDINFO info = (REMOTECMDINFO)gHsVehExternalCommad[Vid];
                                if (Result.ToUpper().IndexOf(info.cmd.ToUpper()) > -1)
                                {
                                    ReplyRemoteBack((REMOTECMDINFO)gHsVehExternalCommad[Vid], st.Replace(',', '#'));
                                    gHsVehExternalCommad.Remove(Vid);
                                }
                            }
                        }
                    }
                }
                #endregion

                String txt = result[0];
                rtMessageLog.Text = txt + "\n" + rtMessageLog.Text;
                int nRow = ((int)(rtMessageLog.ClientSize.Height / rtMessageLog.Font.GetHeight()));
                if (rtMessageLog.Lines.Length > nRow)
                {
                    int iPos = 0;
                    int iIdx = -1;
                    for (int i = 0; i < nRow; i++)
                    {
                        if (iPos < rtMessageLog.Text.Length)
                        {
                            iIdx = rtMessageLog.Text.IndexOf('\n', iPos);
                            if (iIdx > -1)
                            {
                                iPos = iIdx + 1;
                            }
                        }
                    }
                    if (iPos > 0)
                    {
                        rtMessageLog.Text = rtMessageLog.Text.Remove(iPos - 1);
                    }
                }

                if (result.Length == 3 && result[2].Length > 0)
                {
                    UpdateTreeListView(TreeViewType.ADD, result[1], result[2],ConnectionEvent.ReceivedData);
                }
            }
            catch (Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }
        }
        private void ReplyRemoteBack(REMOTECMDINFO info, string msg)
        {
            try
            {
                remoteServerInterface.SendCmd(info.socket, info.veh_id, info.cmd, msg);
            }
            catch { }
        }

        private void ShowLogMsgMBox(object obj)
        {
            try
            {
                String txt = (string)obj;

                string Msg = string.Empty;
                string[] tmp = txt.Split('\n');
                foreach (string st in tmp)
                {
                    if(st.IndexOf('$') > -1)
                    Msg += st.Substring(st.IndexOf("$"), st.Length - st.IndexOf("$")) + '\n';
                }

                SendRemoteMsg(Msg);
            }
            catch (Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }
        }

        private void UpdateTreeListView(TreeViewType type, string ep, string vid, ConnectionEvent Event)
        {
            updateTreeNodeSemaphore.WaitOne();
            try
            {
                switch (type)
                {
                    case TreeViewType.ADD:
                        {
                            int keyIndex = this.tvEngineOn.Nodes.IndexOfKey(vid);
                            if (keyIndex > -1) // Find AND Update
                            {
                                CONNECTINFO info = (CONNECTINFO)this.tvEngineOn.Nodes[keyIndex].Tag;
                                // objMonitor.tcp.HsClientList.Count > 0 && info.Status != "Connect" && this.tvConnection.Nodes[keyIndex].ForeColor != System.Drawing.Color.LimeGreen
                                info.Status = "Connect";
                                info.Event = Event;
                                info.VehID = vid;
                                info.Ep = ep;

                                this.tvEngineOn.Nodes[keyIndex].Tag = info;
                                this.tvEngineOn.Update();

                                UpdateVehTable(info, type);
                            }
                            else
                            {
                                CONNECTINFO info = new CONNECTINFO();
                                /// Insert TreeNode
                                TreeNode n = this.tvEngineOn.Nodes.Add(vid, vid);
                                info.VehID = vid;
                                info.Status = "Connect";
                                info.Event = Event;
                                info.Ep = ep;

                                n.Tag = info;
                                this.tvEngineOn.Update();

                                UpdateVehTable(info, type);
                            }
                            break;
                        }
                    case TreeViewType.UPDATE:
                        {
                            if (vid == "0")
                            {
                                foreach (TreeNode node in tvEngineOn.Nodes)
                                {
                                    CONNECTINFO TagInfo = (CONNECTINFO)node.Tag;
                                    if (TagInfo.Ep == ep)
                                    {
                                        CONNECTINFO infoTag = (CONNECTINFO)node.Tag;
                                        infoTag.Status = "Disconnect";
                                        infoTag.Event = Event;
                                        infoTag.onDisconnect = DateTime.Now;

                                        node.Tag = infoTag;

                                        UpdateVehTable(infoTag, type);
                                    }
                                }
                            }
                            else
                            {
                                int keyIndex = this.tvEngineOn.Nodes.IndexOfKey(vid);
                                if (keyIndex > -1)
                                {
                                    CONNECTINFO info = (CONNECTINFO)this.tvEngineOn.Nodes[keyIndex].Tag;
                                    info.Status = "Disconnect";
                                    info.Event = Event;
                                    info.onDisconnect = DateTime.Now;

                                   // this.tvConnection.Nodes[keyIndex].ForeColor = System.Drawing.Color.Crimson;
                                    this.tvEngineOn.Nodes[keyIndex].Tag = info;
                                    this.tvEngineOn.Update();
                                    UpdateVehTable(info, type);
                                }
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }
            updateTreeNodeSemaphore.Release();
        }
        private void UpdateVehTable(CONNECTINFO info,TreeViewType type)
        {
            try
            {
                if (type == TreeViewType.ADD)
                {
                    DataRow[] dr = gDtVehList.Select("veh_id = " + info.VehID);
                    if (dr.Length == 0)
                    {
                        gDtVehList.Rows.Add(info.VehID, -1, true);
                    }
                }
                else if (type == TreeViewType.UPDATE)
                {
                    DataRow[] dr = gDtVehList.Select("veh_id = " + info.VehID);
                    if (dr.Length > 0)
                    {
                        dr[0][1] = -1;
                        dr[0][2] = false;

                        if (dr.Length > 1)
                        {
                            for (int i = 1; i < dr.Length; i++)
                            {
                                gDtVehList.Rows.Remove(dr[i]);
                            }
                        }
                    }
                }
            }
            catch { }
        }
        private void SendRemoteMsg(string msg)
        {
            try
            {
                if (regLogMsg.Count > 0)
                {
                    foreach (Socket sck in regLogMsg)
                    {
                        if (sck.Connected)
                        {
                            msg = msg.Replace(',', '#');
                            remoteServerInterface.SendCmd(sck, 0, REMOTECMD.REGISTERTOLOG.ToString(), msg);
                        }
                        else
                        {
                            regLogMsg.Remove(sck);
                            SendRemoteMsg(msg);
                            break;
                        }
                    }
                }
            }
            catch
            {
            }
        }
      
        private void tmDisplay_Tick(object sender, EventArgs e)
        {
            tmDisplay.Stop();
            try
            {
                while (qDataLog.Count > 0)
                {
                    string msg = (string)qDataLog.Dequeue();
                    ShowLogMsg(msg);
                    TimeDisPlayMonitor = DateTime.Now;
                }
                while (qMboxLog.Count > 0)
                {
                    string msg = (string)qMboxLog.Dequeue();
                    Thread t = new Thread(new ParameterizedThreadStart(ShowLogMsgMBox));
                    t.IsBackground = true;
                    t.Start(msg);
                    Thread.Sleep(10);
                    t.Join();
                }
            }
            catch(Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }
            tmDisplay.Start();
        }
        private void tmSystem_Tick(object sender, EventArgs e)
        {
            tmSystem.Stop();
            try
            {
                while (qConnecttion.Count > 0)
                {
                    NetDisConnectedEvent NetInfo = qConnecttion.Dequeue();

                    switch (NetInfo.Event)
                    {
                        case ConnectionEvent.Register:
                            {
                                break;
                            }
                        case ConnectionEvent.SockerError:
                            {
                                string ep = NetInfo.tcpClient.Client.RemoteEndPoint.ToString();
                                UpdateTreeListView(TreeViewType.UPDATE, ep, "0", ConnectionEvent.SockerError);
                                break;
                            }
                        case ConnectionEvent.Disconnect:
                            {
                                string ep = NetInfo.tcpClient.Client.RemoteEndPoint.ToString();
                                UpdateTreeListView(TreeViewType.UPDATE, ep, "0", ConnectionEvent.Disconnect);
                                break;
                            }
                        case ConnectionEvent.Exception:
                            {
                                if (NetInfo.tcpClient != null)
                                {
                                    objText.WriteLogError(" : hd:" + NetInfo.tcpClient.Client.Handle + ":" + NetInfo.ex);
                                }
                                else
                                {
                                    //objMonitor.WriteLogError(NetInfo.ex);
                                }
                                break;
                            }
                    }
                }

                //lbVehicle.Text = this.tvConnection.Nodes.Count.ToString() + " Vahicle";
                lbEngineOn.Text = this.tvEngineOn.Nodes.Count.ToString() + " Vehicle Online Engine ON";
                lbEngineOff.Text = this.tvEngineOff.Nodes.Count.ToString() + " Vehicle Online Engine OFF";
                lbOffline.Text = this.tvOffline.Nodes.Count.ToString() + " Vehicle OFFLINE";
                tbSection.Text = this.objMonitor.objTcp.HsClientList.Count.ToString();

                Process p = Process.GetCurrentProcess();
                tbTheard.Text = p.Threads.Count.ToString();
                int Ram = p.WorkingSet / 1024;
                tbMemory.Text = Ram.ToString();

                if ((Ram / 1024) > objMonitor.RamRestart)
                {
                    ProcessKill();
                }

                if (!IsCloseMainFrom)
                {
                    tsTimeStamp.Text = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                }
                else
                {
                    if (DateTime.Now > TimeClose)
                    {
                        ProcessKill();
                    }
                    TimeSpan sp = TimeClose - DateTime.Now;
                    tsTimeStamp.Text = "Is Close Application . . . " + sp.ToString();
                }

                if (objMonitor.objTcp.HsClientList.Count > 0)
                {
                    if (DateTime.Now.AddMinutes(-4) > TimeDisPlayMonitor && objMonitor.objTcp.HsClientList.Count > 1 && TimeDisPlayMonitor != new DateTime())
                    {
                        ProcessKill();
                    }
                    else if (DateTime.Now.AddMinutes(-7) > TimeDisPlayMonitor && TimeDisPlayMonitor != new DateTime())
                    {
                        ProcessKill();
                    }
                }
            }
            catch(Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }

            tmSystem.Start();
        }

        #region PortMon_OnMonitorsReceived
        private void objPortMon_OnMonitorsReceived(MonitorEvtArgs e)
        {
            if (e.msg != null && e.msg.Length > 0)
            {
                UpdateRichTxt(e.msg);
            }
        }
        private void objMonitor_OnMonitorsReceivedMBox(MonitorEvtArgs e)
        {
            if (e.msg != null && e.msg.Length > 0)
            {
                UpdateRichTxtMBox(e.msg);
            }
        }
        private void objMonitor_OnMonitorsDisconnected(NetDisConnectedEvent e)
        {
            UpdateOnDisconnect(e);
        }
        #endregion
        private void UpdateOnDisconnect(NetDisConnectedEvent e)
        {
            try
            {
                updateSystemSemaphore.WaitOne();
                qConnecttion.Enqueue(e);
                updateSystemSemaphore.Release();
                UpdateRichTxt(e.ex);
            }
            catch (Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }
        }
        private void UpdateRichTxt(string msg)
        {
            try
            {
                updateMsgQSemaphore.WaitOne();
                qDataLog.Enqueue(msg);
                updateMsgQSemaphore.Release();
            }
            catch (Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }
        }
        private void UpdateRichTxtMBox(string msg)
        {
            try
            {
                updateMsgMBoxQSemaphore.WaitOne();
                qMboxLog.Enqueue(msg);
                updateMsgMBoxQSemaphore.Release();
            }
            catch (Exception ex)
            {
                objText.WriteLogError(ex.TargetSite + " : " + ex.Message);
            }
        }
    
        private void disconnectedSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
           objMonitor.closeSecsion();
        }
        private void MainFrm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (!IsCloseMainFrom)
            {
                IsCloseMainFrom = true;
                objMonitor.closeSecsion();
                objMonitor.objTcp.IsClose = true;
                e.Cancel = IsCloseMainFrom;
                TimeClose = DateTime.Now.AddSeconds(15);
                tmSystem.Interval = 1000;
            }
            else
            {
                ProcessKill();
            }
        }
        private void ProcessKill()
        {
            Process p = Process.GetCurrentProcess();
            p.Kill();
        }
        private void directoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string logPath = Directory.GetCurrentDirectory() + "\\Log";
                string windir = Environment.GetEnvironmentVariable("WINDIR");
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = windir + @"\explorer.exe";
                prc.StartInfo.Arguments = logPath;
                prc.Start();
            }
            catch { }
        }
        private void stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string logPath = Directory.GetCurrentDirectory() + "\\Log\\DataLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";

            if (File.Exists(logPath))
            {
                System.Diagnostics.Process.Start("notepad.exe", logPath);
            }
            else
            {
                MessageBox.Show("Not Find. Text file", "Message",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }
        private void RegisterTologMsg(Socket socket)
        {
            bool found = false;
            foreach (Socket s in regLogMsg)
            {
                if (socket == s)
                {
                    found = true;
                }
            }
            if (!found)
            {
                regLogMsg.Add(socket);
            }
        }
        private void readCommandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                KBoxRead frmRead = new KBoxRead(gDtVehList, gDtReadCommand);
                frmRead.ShowDialog();

                if (frmRead.DialogResult == DialogResult.OK && frmRead.gCommand.Length > 0 && frmRead.gVehID > -1)
                {
                    if (MessageBox.Show("Query : " + frmRead.gCommand, frmRead.gVehID.ToString(), MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        if (SendData(checkEp(frmRead.gVehID.ToString()), frmRead.gVehID, frmRead.gCommand))
                        {
                            if (!gHsVehInternalCommad.ContainsKey(frmRead.gVehID))
                            {
                                gHsVehInternalCommad.Add(frmRead.gVehID, frmRead.gCommand);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Send Error : " + frmRead.gVehID.ToString(), "MESSAGE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch
            { }
        }
        private void writeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                KBoxWrite frmWrite = new KBoxWrite(gDtVehList, gDtWriteCommand);
                frmWrite.ShowDialog();
                if (frmWrite.DialogResult == DialogResult.OK && frmWrite.gCommand.Length > 0 && frmWrite.gVehID > -1)
                {
                    string cmd = frmWrite.gCommand + ',' + frmWrite.gParameter;
                    if (MessageBox.Show("Query : " + cmd, frmWrite.gVehID.ToString(), MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        if (SendData(checkEp(frmWrite.gVehID.ToString()), frmWrite.gVehID, cmd))
                        {
                            if (!gHsVehInternalCommad.ContainsKey(frmWrite.gVehID))
                            {
                                gHsVehInternalCommad.Add(frmWrite.gVehID, frmWrite.gCommand.Split(',')[0]);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Send Error : " + frmWrite.gVehID.ToString(), "MESSAGE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch { }
        }
        private string checkEp(string veh_id)
        {
          string  ep = string.Empty;
            try
            {
                foreach (TreeNode node in tvEngineOn.Nodes)
                {
                    CONNECTINFO TagInfo = (CONNECTINFO)node.Tag;
                    if (TagInfo.VehID == veh_id)
                    {
                        ep = TagInfo.Ep;
                        break;
                    }
                }
            }
            catch
            { }
            return ep;
        }
        private DataTable CreateVehListTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("veh_id", typeof(string));
            dt.Columns.Add("handle_id", typeof(int));
            dt.Columns.Add("status", typeof(bool));
            return dt;
        }

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        private bool SendData(string ep, int veh_id, string command)
        {
            bool is_suscee = false;
            semaphor.WaitOne();
            try
            {
                foreach (DictionaryEntry entry in objMonitor.objTcp.HsClientList)
                {
                    CONNECTINFO info = (CONNECTINFO)entry.Value;
                    if (info.VehID == veh_id.ToString() || info.Ep == ep)
                    {
                        if (info.tcpclient.Client.Connected)
                        {
                            NetworkStream clientStream = info.tcpclient.GetStream();
                            ASCIIEncoding encoder = new ASCIIEncoding();

                            byte[] buffer = null;
                            buffer = encoder.GetBytes(command + '\r');
                            clientStream.Write(buffer, 0, buffer.Length);
                            clientStream.Flush();
                            is_suscee = true;
                        }
                    }
                }
            }
            catch
            {
            }
            semaphor.Release();
            return is_suscee;
        }
        private void refreshCommandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InitializeCommand();
        }
        private string GetIP4Address()
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily == AddressFamily.InterNetwork)
                {
                    if (IPA.ToString() == objMonitor.LocalIP)
                    {
                        IP4Address = IPA.ToString();
                        break;
                    }
                }
            }

            return IP4Address;
        }
    }
}