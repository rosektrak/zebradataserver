using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.IO;
using System.Messaging;
using System.Diagnostics;

namespace ZebraBroadcast
{
    public enum Q_BROADCASTTYPE { EMAIL = 1, SMS = 2, BlukSMS = 3 }
    public delegate void ReceiveMQ(object owner, XmlReader xmlData, Q_BROADCASTTYPE qType);

    public class CommonQeue
    {
        public event ReceiveMQ OnReceiveMQ;

        String GlobalQPath = @".\Private$\ZRQ";

        //const String GlobalQPath = @".\Private$\ZRQ";
        //const String GlobalQPath = @"FormatName:DIRECT=TCP:10.0.100.78\private$\ZebraQ";

        ZebraBroadcast.Properties.Settings pf;
        protected MessageQueue MQ;
        public CommonQeue()
        {
            if (MessageQueue.Exists(GlobalQPath))
            {
                MQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public CommonQeue(String QPathStr)
        {
            GlobalQPath = String.Format(".\\Private$\\{0}", QPathStr);

            if (MessageQueue.Exists(GlobalQPath))
            {
                MQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                MQ = MessageQueue.Create(GlobalQPath);
            }
            pf = new ZebraBroadcast.Properties.Settings();
        }

        public void InitReceiveQMsg()
        {
            if (MQ != null)
            {
                MQ.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                MQ.ReceiveCompleted += new ReceiveCompletedEventHandler(MQ_ReceiveCompleted);                
            }
        }
        public void BeginReceiveQMsg()
        {
            MQ.BeginReceive();
        }
        private void MQ_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                StreamReader so = new StreamReader(e.Message.BodyStream);
                String s = so.ReadToEnd();

                StringReader txtRd = new StringReader(s);
                XmlReader xmlRd = XmlReader.Create(txtRd);
                OnReceiveMQ(this, xmlRd, GetQueueBroadcastType(MQ));                
            }
            catch (Exception ex)
            {
                WriteLogMsg(ex.ToString());
                MQ.BeginReceive();
                
            }
            finally
            {
            }
        }

        public String GetQ()
        {
            Message msg = MQ.Receive();
            msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });

            String foo = msg.Body.ToString();
            return foo;
        }

        public static void WriteLogMsg(string msg)
        {
            try
            {
                //String logPath = Directory.GetCurrentDirectory() + "\\Log";
                String logPath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Log");
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                String fileName = logPath + "\\" + "Datalog_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch { }
        }


        private Q_BROADCASTTYPE GetQueueBroadcastType(MessageQueue q)
        {
            Q_BROADCASTTYPE qType = new Q_BROADCASTTYPE();
            if (q.QueueName.ToUpper().IndexOf(pf.Broadcast_EMAILQ.ToUpper()) > -1)
            {
                qType = Q_BROADCASTTYPE.EMAIL;
            }
            else if (q.QueueName.ToUpper().IndexOf(pf.Broadcast_SMSQ.ToUpper()) > -1)
            {
                qType = Q_BROADCASTTYPE.SMS;
            }
            return qType;
        }
    }
}
