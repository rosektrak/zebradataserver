using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Messaging;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Net;
using ZebraCommonInterface;

namespace ZebraBroadcast
{
    public delegate void SMSQueueMonitorHandler(string msg);
    public delegate void SendSMSCompletedHandler(object sender, PROFILEINFO info);
    public delegate void SendBulkSMSCompletedHandler(object sender, PROFILEINFO info, int count);

    public class rs232
    {
        SerialPort ComPort;
        public rs232(String portName, int baudRate, Parity par, int dataBit, StopBits stopBit, out Boolean status)
        {
            status = false;
            try
            {
                ComPort = new SerialPort(portName, baudRate, par, dataBit, stopBit);
                ComPort.Open();
                status = true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Error!");
            }
        }
        ~rs232()
        {
            if (ComPort.IsOpen)
            {
                ComPort.Close();
            }
        }

        private void DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Console.WriteLine(ComPort.ReadExisting());
        }
        public String Read()
        {
            try
            {
                if (ComPort.IsOpen)
            {
                byte[] buf = new byte[100];
                ComPort.Read(buf, 0, buf.Length);
                Thread.Sleep(50);
                return Convert.ToString(System.Text.Encoding.ASCII.GetString(buf)).Trim('\0');
            }
            else
            {
                return "";
            }
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }
        public void Write(String buf)
        {
            if (ComPort.IsOpen)
            {
                ComPort.WriteLine(buf);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public class SMSCls : MarshalByRefObject, BroadcastInterface
    {
        public event SMSQueueMonitorHandler  OnSMSQueueMonitor;
        public event SendSMSCompletedHandler OnSendSMSCompleted;
        public event SendBulkSMSCompletedHandler OnSendBulkSMSCompleted;

        #region Variables
        private bool _IsThreadAbort = false;
        public bool IsThreadAbort
        {
            get
            {
                return _IsThreadAbort;
            }
            set
            {
                _IsThreadAbort = value;
            }
        }

        private SQLCls gSql ;
        private Queue smsQueue = new Queue();
        private Queue bulksmsQueue = new Queue();
        private MessageQueue MQ = null;

        private Boolean smsPortStatus = false;
        private rs232 smsPort;

        private String _SMSCentre = "";
        private String _PortName = "";
        private String _gBulkSmsIP = "127.0.0.1";
        private String _gBulkSmsPort = "81";
        private const int gResendCnt = 3;
        private int _BaudRate = 9600;
        private int _DataBit = 8;

        private System.IO.Ports.Parity _Parity;
        private System.IO.Ports.StopBits _StopBit;

        #endregion

        #region Internal Function
        public SMSCls()
        {
        }
        public SMSCls(String dbHost, out Boolean status)
        {
            gSql        =   new SQLCls(dbHost) ;
            //  smsQueue    =   new Queue() ;
            //  bulksmsQueue = new Queue();

            if (!smsPortStatus)
            {
                smsPortStatus = status = InitSMS();
            }
            else
            {
                status = smsPortStatus;
            }

            _IsThreadAbort = false;
            Thread smsThread = new Thread(new ThreadStart(MonitorThread));
            smsThread.Start();
        }
        ~SMSCls()
        {
            _IsThreadAbort = true;
        }

        public void Dispose()
        {
            _IsThreadAbort = true;
        }

        public bool IsAlive()
        {
            return true;
        }
        private void Clear()
        {
            String ret;
            ret = WriteATCommand("AT+CMGF=1\r");
            if (ret.IndexOf("OK") < 0) return;

            #region "Use in thefuture"
            //Immediatly ////////////////////////////
            //WriteATCommand("AT+CMGF=1");
            //WriteATCommand("AT+CNMI=1,2,0,0,0");
            //Notify ////////////////////////////////
            //WriteATCommand("AT+CMGF=1");
            //WriteATCommand("AT+CNMI=1,1,0,0,0");

            //WriteATCommand("AT+CMGR=3");            // Read SMS No.3
            #endregion

            RemoveAllSMS();
        }
        
        private void Send(String objName, String msg, String telNo)
        {
            String ret;
            ret = WriteATCommand("AT+CMGF=1\r");
            if (ret.IndexOf("OK") < 0) return;
            ret = WriteATCommand(String.Format("AT+CSCA={0}\r", _SMSCentre));   // SMS Centre No.
            if (ret.IndexOf("OK") < 0) return;
            WriteATCommand(String.Format("AT+CMGS={0}\r", telNo));              // Client No.
            WriteATCommand(objName + ":" + msg + "\x1A");                      // Ctrl-Z 
        }
        private bool SendMsg(String msg, String telNo)
        {
            bool sus = false;
            try
            {
                String ret;
                ret = WriteATCommand("AT+CMGF=1\r");   /// return OK
                if (ret.IndexOf("OK") < 0) return sus;
                ret = WriteATCommand(String.Format("AT+CSCA={0}\r", _SMSCentre));   // SMS Centre No.
                if (ret.IndexOf("OK") < 0) return sus;   
                WriteATCommand(String.Format("AT+CMGS={0}\r", telNo));              // Client No.
                WriteATCommand(msg + "\x1A");                                       // Ctrl-Z 
                sus = true;
            }
            catch 
            { }
            return sus;
        }
        private int SendInPDU(String pduMsgStr)
        {
            try
            {
                if (pduMsgStr.Trim().Length < 4)
                {
                    return -1;
                }

                String msgStr = pduMsgStr;
                String ret;
                ret = WriteATCommand("ATZ\r");
                if (ret.IndexOf("OK") < 0)
                {
                    return -2;
                }
   
                ret = WriteATCommand("AT+CMGF?\r");
                if (ret.IndexOf("0") < 0)
                {
                    ret = WriteATCommand("AT+CMGF=0\r");
                    if (ret.IndexOf("OK") < 0)
                    { 
                        return -3;
                    }
                }

                ret = WriteATCommand(String.Format("AT+CSCA={0}\r", _SMSCentre));   // SMS Centre No.
                if (ret.IndexOf("OK") < 0) 
                { 
                    return -4; 
                }
                try
                {
                    //MessageBox.Show(String.Format("AT+CMGS={0}\r", (pduMsgStr.Length / 2) - 1));
                    WriteATCommand(String.Format("AT+CMGS={0}\r", (pduMsgStr.Length / 2) - 1));
                    WriteATCommand(msgStr + "\x1A");
                }
                catch (Exception x) { MessageBox.Show("error" + x.Message); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            return 1;
        }
        private String PDUString(String msg, String telNo)
        {
            try
            {
                if (telNo.Trim().Length < 12)
                {
                    return "";
                }

                if (telNo.IndexOf('+') > -1)
                {
                    telNo = telNo.Remove(telNo.IndexOf('+'), 1);
                }
                String telNoStr1 = telNo + "F";
                byte[] telNoByte = Encoding.ASCII.GetBytes(telNoStr1);

                String telNoStr2 = "";
                for (int i = 0; i < telNoStr1.Length; )
                {
                    telNoStr2 += String.Format("{1}{0}", telNoStr1[i], telNoStr1[i + 1]);
                    i += 2;
                }

                Encoding enc = Encoding.Unicode;
                byte[] msgByte = enc.GetBytes(msg);
                String msgStr2 = "";
                for (int i = 0; i < msgByte.Length; )
                {
                    int i1 = (int)msgByte[i];
                    int i2 = (int)msgByte[i + 1];
                    msgStr2 += String.Format("{1:X2}{0:X2}", i1, i2);
                    i += 2;
                }
                String retStr = String.Format("0011000B91{0}00080B{2:X2}{1}", telNoStr2, msgStr2, msgByte.Length);
                return retStr;
                
            }
            catch (Exception e)
            {
                //GlobalClass.WriteLogEvent("ZebraBroadcast::SMSCls::PDUString() :" + e.Message);
            }
            return "";
        }

        private void InitCfg()
        {
            try
            {
                Properties.Settings cfg = new ZebraBroadcast.Properties.Settings();
                _PortName = cfg["PortName"].ToString();
                _BaudRate = int.Parse(cfg["BaudRate"].ToString());
                gSql = new SQLCls(cfg.HostDB);
                _gBulkSmsIP = cfg.BulkSms_IP;//pf.BulkSms_IP;
                _gBulkSmsPort = cfg.bulkSms_Port;//pf.bulkSms_Port;

                String p = cfg["Parity"].ToString();
                switch (p)
                {
                    case "None": _Parity = System.IO.Ports.Parity.None; break;
                    case "Odd": _Parity = System.IO.Ports.Parity.Even; break;
                    case "Even": _Parity = System.IO.Ports.Parity.Even; break;
                    default: _Parity = System.IO.Ports.Parity.None; break;
                }

                _DataBit = int.Parse(cfg["DataBit"].ToString());

                String st = cfg["StopBit"].ToString();
                switch (st)
                {
                    case "1": _StopBit = System.IO.Ports.StopBits.One; break;
                    case "2": _StopBit = System.IO.Ports.StopBits.Two; break;
                    default: _StopBit = System.IO.Ports.StopBits.One; break;
                }

                _SMSCentre = cfg["SMScentre"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private Boolean InitSMS()
        {
            String ret;

            InitCfg();
            Boolean status;
            smsPort = new rs232(_PortName, _BaudRate, _Parity, _DataBit, _StopBit, out status);

            if (!status)
            {
                return false;
            }

            int to = 5;
            do
            {
                if (to-- > 0)
                {
                    ret = WriteATCommand("AT\r");
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Cannot connect to GSM Modem!", "Error!");
                    return false;
                }
            } while (ret.IndexOf("OK") < 0);

            //WriteATCommand("AT+CPIN=\"xxxx\"");

            ret = WriteATCommand("AT+CREG?\r");
            if (ret.IndexOf("OK") < 0) return false; ;
            ret = WriteATCommand("AT+COPS=0,0\r");
            if (ret.IndexOf("OK") < 0) return false;
            ret = WriteATCommand("AT+CPIN?\r");
            if (ret.IndexOf("READY") < 0) return false;
            ret = WriteATCommand("AT+CSQ\r");             //Check signal strength
            if (ret.IndexOf("OK") < 0) return false;

            return true;
        }
        private String WriteATCommand(String atCmd)
        {
            String retStr;

            //check com port

            //uncomment for production
            smsPort.Write(atCmd);
            System.Threading.Thread.Sleep(100);
            retStr = smsPort.Read();
            return retStr;

            //////uncomment for Test. if not have GSM Module 
            //if (atCmd == "AT\r" || atCmd == "AT+CREG?\r" || atCmd == "AT+COPS=0,0\r" || atCmd == "AT+CSQ\r")
            //{
            //    retStr = "OK";
            //}
            //else
            //    retStr = "READY";
            //return retStr;
        }
        private void RemoveAllSMS()
        {
            String tk;
            String[] ln;
            String retStr = "";

            smsPort.Write("AT+CMGL=ALL\r");
            System.Threading.Thread.Sleep(1000);
            retStr = retStr + smsPort.Read();
            while (retStr.IndexOf("OK") < 0)
            {
                retStr = retStr + smsPort.Read();
            }
            String[] mailStr = retStr.Split(new String[] { "+CMGL:" }, StringSplitOptions.None);
            for (int i = 0; i < mailStr.Length; i++)
            {
                ln = mailStr[i].Split(',');
                tk = ln[0].Trim(' ');
                try
                {
                    int eno = Int32.Parse(tk);
                    String atCmd = String.Format("AT+CMGD={0}\r", eno);
                    WriteATCommand(atCmd);        // Delete SMS
                }
                catch
                {
                    continue;
                }
            }
        }
        private String ReadCfg(String keyStr)
        {
            Properties.Settings cfg = new ZebraBroadcast.Properties.Settings();
            return cfg[keyStr].ToString();
        }
        private void WriteCfg(String keyStr, String valueStr)
        {
            Properties.Settings cfg = new ZebraBroadcast.Properties.Settings();
            try
            {
                cfg[keyStr] = valueStr;
                cfg.Save();
            }
            catch
            {
            }
        }
        private void test()
        {
            smsPort.Write("ATZ\n");
        }
        private int Diag()
        {
            String ret = "";
            try
            {
                ret = WriteATCommand("ATZ\r");
            }
            catch (Exception x) { MessageBox.Show(x.Message); }
            //test
            if (ret.IndexOf("OK") < 0) return -1;
            //if (ret.IndexOf("OK") < 0) return 1;
            return 0;
        }
        #endregion

        public void TestSMS1(string telNo, string msg)
        {
            string smsMsg = PDUString(msg, telNo);
            if (smsMsg.Length > 0)
            {
                //SendInPDU(smsMsg);
                //GlobalClass.WriteLogEvent("Telephone Number:" + telNo + " " + "took sms sending...");
            }
        }
        public void TestSMS2(string telNo, string msg)
        {
            PROFILEINFO info = new PROFILEINFO();
            info.alert_type = ALERT_TYPE.SMS;
            info.veh_id = 771 ;
            info.fleet_id = 53;
            info.veh_reg = "771";
            info.staff_id = 1;
            info.driver = "test";
            info.contact_name = "test";
            info.max_speed = 100;
            info.value = msg;
            info.tel_no = telNo;
            info.email = "";
            info.timestamp = DateTime.Now.ToString();
            info.location = "test";
            info.lat = 0.0;
            info.lon = 0.0;
            info.evt_id = 2;
            info.subject = "Test SMS";

            EnQueue(info);
        }
        public void SendSMS(int idx, int VehID, int evt_type, string value, string timestamp, string location, double lat, double lon, int speed, string msg)
        {
            try
            {
                bool stataus = true;
                ArrayList contactList = new ArrayList();
                DataTable dtProfile = new DataTable();

                if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE || evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                {
                    //�������
                    DataTable checknewweb = gSql.Check_param_newweb(VehID, evt_type);
                    if (checknewweb.Rows.Count > 0)
                    {
                        dtProfile = gSql.GetFeatureProfile_New_SMS(idx, VehID, evt_type, Convert.ToInt32(msg));
                    }
                    else
                    {
                        dtProfile = gSql.GetFeatureProfile(idx, VehID, evt_type, Convert.ToInt32(msg));
                    }
                }
                else if (evt_type == (int)EVT_TYPE.SPEEDING)
                {
                    if (speed < 200)
                    {
                        //�������
                        DataTable checknewweb = gSql.Check_param_newweb(VehID, evt_type);
                        if (checknewweb.Rows.Count > 0)
                        {
                            dtProfile = gSql.GetFeatureProfileByDay_New_SMS(VehID, evt_type, (int)DateTime.Now.DayOfWeek + 1);
                        }
                        else
                        {
                            dtProfile = gSql.GetFeatureProfileByDay(VehID, evt_type, (int)DateTime.Now.DayOfWeek + 1);
                        }
                    }
                    else
                        stataus = false;
                }
                else
                {
                    //�������
                    DataTable checknewweb = gSql.Check_param_newweb(VehID, evt_type);
                    if (checknewweb.Rows.Count > 0)
                    {
                        dtProfile = gSql.GetFeatureProfile_New_SMS(idx, VehID, evt_type);
                    }
                    else
                    {
                        dtProfile = gSql.GetFeatureProfile(idx, VehID, evt_type);
                    }
                }

                if (stataus)
                {
                    foreach (DataRow r in dtProfile.Rows)
                    {
                        PROFILEINFO info = new PROFILEINFO();
                        info.alert_type = ALERT_TYPE.SMS;
                        info.veh_id = (int)r["veh_id"];
                        info.fleet_id = (int)r["fleet_id"];
                        info.veh_reg = (string)r["registration"];
                        info.staff_id = (int)r["staff_id"];
                        info.driver = (string)r["driver"];
                        info.contact_name = (string)r["contact_name"];
                        info.max_speed = (int)r["max_speed"];
                        info.value = value;
                        info.tel_no = (string)r["tel_no"];
                        info.email = (string)r["email"];
                        info.timestamp = timestamp;
                        info.location = location;
                        info.lat = lat;
                        info.lon = lon;
                        info.evt_id = (int)evt_type;
                        info.speed = speed;
                        info.evt_desc = evt_type.ToString();
                        string evt_desc = (string)r["evt_desc"];
                        info.subject = GetSubjectSMS((int)evt_type, info.veh_id);

                        EnQueue(info);
                    }
                }
            }
            catch { }
        }
        public void SendBulkSMS(int idx, int VehID, int evt_type, string value, string timestamp, string location, double lat, double lon, int speed, string msg)
        {
            DateTime dayno = Convert.ToDateTime(timestamp);
            string sdayno = "";
            if (Convert.ToString(dayno.DayOfWeek) == "Sunday") sdayno = "1";
            if (Convert.ToString(dayno.DayOfWeek) == "Monday") sdayno = "2";
            if (Convert.ToString(dayno.DayOfWeek) == "Tuesday") sdayno = "3";
            if (Convert.ToString(dayno.DayOfWeek) == "Wednesday") sdayno = "4";
            if (Convert.ToString(dayno.DayOfWeek) == "Thursday") sdayno = "5";
            if (Convert.ToString(dayno.DayOfWeek) == "Friday") sdayno = "6";
            if (Convert.ToString(dayno.DayOfWeek) == "Saturday") sdayno = "7";

            try
            {
                bool stataus = true;
                ArrayList contactList = new ArrayList();
                DataTable dtProfile = new DataTable();

                if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE || evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                {
                    dtProfile = gSql.GetFeatureProfile(idx, VehID, evt_type, Convert.ToInt32(msg));
                }
                else if (evt_type == (int)EVT_TYPE.SPEEDING || evt_type == (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME)
                {
                    if (speed < 175)
                    {
                        dtProfile = gSql.GetFeatureProfileByDay(VehID, evt_type, (int)DateTime.Now.DayOfWeek + 1);
                    }
                    else
                        stataus = false;
                }
                else if (evt_type == (int)EVT_TYPE.DISTANCE_OVER)
                {
                    //�������
                    DataTable checknewweb = gSql.Check_param_newweb(VehID, evt_type);
                    if (checknewweb.Rows.Count > 0)
                    {
                        dtProfile = gSql.GetFeatureProfile_New_SMS(idx, VehID, evt_type);
                        value = "Distance Over : " + msg;
                    }
                    else
                    {
                        dtProfile = gSql.GetFeatureProfile(idx, VehID, evt_type);
                    }
                }
                else
                {
                    dtProfile = gSql.GetFeatureProfile(idx, VehID, evt_type);
                }

                if (stataus)
                {
                    foreach (DataRow r in dtProfile.Rows)
                    {
                        PROFILEINFO info = new PROFILEINFO();
                        info.alert_type = ALERT_TYPE.BULK_SMS;
                        info.veh_id = (int)r["veh_id"];
                        info.fleet_id = (int)r["fleet_id"];
                        info.fleet_name = (string)r["fleet_desc"];
                        info.veh_reg = (string)r["registration"];
                        info.staff_id = (int)r["staff_id"];
                        info.driver = (string)r["driver"];
                        info.contact_name = (string)r["contact_name"];
                        info.max_speed = (int)r["max_speed"];
                        info.value = value;
                        info.tel_no = (string)r["tel_no"];
                        info.email = (string)r["email"];
                        info.timestamp = timestamp;
                        info.location = location;
                        info.lat = lat;
                        info.lon = lon;
                        info.evt_id = (int)evt_type;
                        info.speed = speed;
                        if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE || evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE || evt_type == (int)EVT_TYPE.SPEEDING || evt_type == (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME)
                        {
                            info.noday = (string)r["no_day"];
                            int noday = Convert.ToInt32(info.noday);
                            info.msg = gSql.GetMessageNew(info.fleet_id, VehID, evt_type, info.staff_id, noday);
                        }
                        else if (evt_type == (int)EVT_TYPE.TEMPERATURE || evt_type == (int)EVT_TYPE.IN_ZONE_CURFEW || evt_type == (int)EVT_TYPE.OUT_ZONE_CURFEW || evt_type == (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME || evt_type == (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE || evt_type == (int)EVT_TYPE.NO_ZONE_CURFEW_WITH_SPEED)
                        {
                            info.noday = (string)r["no_day"];
                            int noday = Convert.ToInt32(info.noday);
                            info.start_time = Convert.ToString((string)r["start_time"]);
                            info.stop_time = Convert.ToString((string)r["stop_time"]);
                            info.msg = gSql.GetMessageNew(info.fleet_id, VehID, evt_type, info.staff_id, noday, info.start_time, info.stop_time);
                        }
                        else
                        {
                            info.noday = (string)r["no_day"];
                            int noday = Convert.ToInt32(info.noday);
                            info.msg = gSql.GetMessageNew(info.fleet_id, VehID, evt_type, info.staff_id, noday);
                        }
                        info.evt_desc = evt_type.ToString();
                        info.subject = GetSubjectSMS((int)evt_type, VehID);

                        if (evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED1_ON || evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED1_OFF ||
                            evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED2_ON || evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED2_OFF ||
                            evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON || evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF ||
                            evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON || evt_type == (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF)
                        {
                            info.format_send = (int)SMSFORMAT.IOPORT;
                        }
                        else if (evt_type == (int)EVT_TYPE.EVENT_GPS_CONNECTED || evt_type == (int)EVT_TYPE.EVENT_GPS_DISCONNECTED)
                        {
                            info.format_send = (int)SMSFORMAT.GPSPLUG;
                        }
                        else if (evt_type == (int)EVT_TYPE.EVENT_POWER_LINE_CONNECTED || evt_type == (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED)
                        {
                            info.format_send = (int)SMSFORMAT.POWERLINE;
                        }
                        else if (evt_type == (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME)
                        {
                            info.format_send = (int)SMSFORMAT.ENGINE_ON_IN_PROHITBIT_TIME;
                        }
                        else if (evt_type == (int)EVT_TYPE.SPEEDING)
                        {
                            info.format_send = (int)SMSFORMAT.SPEEDING;
                        }
                        else
                        {
                            info.format_send = (int)SMSFORMAT.NORMAL;
                        }

                        EnQueueBulkSms(info);
                    }
                }
            }
            catch { }
        }

        private void EnQueue(PROFILEINFO info)
        {
            smsQueue.Enqueue(info);
        }

        private void EnQueueBulkSms(PROFILEINFO info)
        {
            bulksmsQueue.Enqueue(info);
        }
        public void SendTestBulkSMS(PROFILEINFO info)
        {
            EnQueueBulkSms(info);
        }
        public void SendMsgSMS(string telno, string msg)
        {
            string smsMsg = PDUString(msg, telno);
            if (smsMsg.Length > 0)
            {
                int x = SendInPDU(smsMsg);
            }
        }

         public void SendMsgSMSx(string telno, string msg)
        {
            //InitCfg();
            //PROFILEINFO info = new PROFILEINFO();
            //info.type = PROFILEINFO_TYPE.VALUE_TYPE;
            //info.value = msg;
            //info.tel_no = telno;
            //info.veh_reg = "?";
            //info.timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //info.contact_name = "?";

            //DateTime dt = DateTime.Now;
            //String dtStr = info.timestamp;
            //try
            //{
            //    dt = Convert.ToDateTime(info.timestamp);
            //    dtStr = dt.ToString("ddMMyy HHmmss", new System.Globalization.CultureInfo("en-US"));
            //}
            //catch { }

            //if (Diag() > -1)
            //{
            //    if (info.tel_no.IndexOf("+66") > -1)
            //    {
            //        info.tel_no = "0" + info.tel_no.Substring(3, info.tel_no.Length - 3);
            //    }
            //    string tel_no = info.tel_no;

            //    string body_txt = "";
            //    switch (info.type)
            //    {
            //        /// True
            //        case PROFILEINFO_TYPE.VALUE_TYPE:
            //            body_txt = info.value;
            //            break;
            //    }

            //    if (body_txt.Length > 70)
            //    {
            //        body_txt = body_txt.Substring(0, 70);
            //    }
            //    int idx = tel_no.IndexOf('0');
            //    if (idx > -1 && tel_no.Length >= 10)
            //    {
            //        tel_no = "+66" + tel_no.Substring(idx + 1);

            //        string msgx = PDUString(body_txt, tel_no);
            //        SendInPDU(msgx);

            //        if (OnSendSMSCompleted != null)
            //        {
            //            OnSendSMSCompleted(this, info);

            //            gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.SMS, info);
            //        }
            //    }
            //    else
            //    {
            //    }
            //}
            //else
            //{
            //    smsQueue.Enqueue(info);
            //}
            //Thread.Sleep(3000);
        }
        

        public void SendMsgBulkSMS(string telno, string msg)
        {
            InitCfg();
            PROFILEINFO info = new PROFILEINFO();
            info.type = PROFILEINFO_TYPE.VALUE_TYPE;
            info.value = msg;
            if (telno.StartsWith("+66"))
            {
                info.tel_no = telno.Replace("+66", "0");
            }
            else if (telno.StartsWith("0"))
            {
                info.tel_no = telno;
            }
            info.veh_reg = "?";
            info.timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            info.contact_name = "?";
            info.format_send = (int)SMSFORMAT.TESTSYNTAX;

            string tel_no = GetTelNo(info.tel_no);
            if (tel_no.StartsWith("66"))
            {
              string  body_txt = GetBodyText(info);

                info.srvResp = SendBulkSMS(tel_no, body_txt, info.srvResp, info);

                if (info.srvResp.STATUS == RESP_STATUS.OK)
                {
                    //--- Success gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.SMS, info);
                    CreateSMID(info.srvResp.SMID, tel_no);

                    foreach (Int64 smid in info.srvResp.SMID)
                    {
                        gSql.InsertBulkSmsNewBroadcastLog(info, smid, tel_no);
                    }
                    if (OnSendBulkSMSCompleted != null)
                    {
                        OnSendBulkSMSCompleted(this, info, info.srvResp.SMID.Length);
                    }
                }
                else if (info.srvResp.STATUS == RESP_STATUS.ERR && info.srvResp.resendCnt <= gResendCnt)
                {
                    ///---convert to send sms modem
                    if (info.evt_id == (int)EVT_TYPE.WATCHDOG)
                    {
                        WatchDogConvertToSMSModem(info);
                    }
                    //--- Resend
                    else
                        bulksmsQueue.Enqueue(info);
                }
                else
                {
                    CreateLogErr(info, "#1 Resend out");
                }
            }


        }

        //public bool SendMsgBulkSMS_New(string telno, string msg)
        //{
        //    bool Status = false;
        //    InitCfg();
        //    PROFILEINFO info = new PROFILEINFO();
        //    info.type = PROFILEINFO_TYPE.VALUE_TYPE;
        //    info.value = msg;
        //    if (telno.StartsWith("+66"))
        //    {
        //        info.tel_no = telno.Replace("+66", "0");
        //    }
        //    else if (telno.StartsWith("0"))
        //    {
        //        info.tel_no = telno;
        //    }
        //    info.veh_reg = "?";
        //    info.timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //    info.contact_name = "?";
        //    info.format_send = (int)SMSFORMAT.TESTSYNTAX;

        //    string tel_no = GetTelNo(info.tel_no);
        //    if (tel_no.StartsWith("66"))
        //    {
        //        string body_txt = GetBodyText(info);

        //        info.srvResp = SendBulkSMS(tel_no, body_txt, info.srvResp, info);

        //        if (info.srvResp.STATUS == RESP_STATUS.OK)
        //        {
        //            //--- Success gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.SMS, info);
        //            CreateSMID(info.srvResp.SMID, tel_no);

        //            foreach (Int64 smid in info.srvResp.SMID)
        //            {
        //                gSql.InsertBulkSmsNewBroadcastLog(info, smid, tel_no);
        //            }
        //            if (OnSendBulkSMSCompleted != null)
        //            {
        //                OnSendBulkSMSCompleted(this, info, info.srvResp.SMID.Length);
        //            }
        //            Status = true;
        //        }
        //        else if (info.srvResp.STATUS == RESP_STATUS.ERR && info.srvResp.resendCnt <= gResendCnt)
        //        {
        //            ///---convert to send sms modem
        //            if (info.evt_id == (int)EVT_TYPE.WATCHDOG)
        //            {
        //                WatchDogConvertToSMSModem(info);
        //            }
        //            //--- Resend
        //            else
        //            {
        //                bulksmsQueue.Enqueue(info);
        //            }
        //            Status = true;
        //        }
        //        else
        //        {
        //            CreateLogErr(info, "#1 Resend out");
        //        }
        //    }
        //    return Status;
        //}

        public void SendTrueMsgSMS(string telno, string msg)
        {
            if (telno.Length < 5) return;
            if (telno[0] == '0') { telno = "+66" + telno.Trim().Substring(1); }
            if (msg.Length > 70) { msg = msg.Substring(0, 70); }

            string smsMsg = PDUString(msg, telno);
            if (smsMsg.Length > 0)
            {
                SendInPDU(smsMsg);
            }
            Thread.Sleep(1000);
        }
        public void SendTrueMsgSMS(PROFILEINFO info)
        {
            smsQueue.Enqueue(info);
        }
        public void SendMsgSMS(PROFILEINFO zInfo)
        {
            EnQueue(zInfo);
        }
        public void SendMsgSMS2(PROFILEINFO zInfo)
        {
            EnQueue(zInfo);
        }
        public void SendMsgSMS3(PROFILEINFO zInfo)
        {
            EnQueue(zInfo);
        }
        public void SendTestMsgSMS(string telno, string msg)
        {
            if (telno.Length < 5) return;
            if (telno[0] == '0') { telno = "+66" + telno.Trim().Substring(1); }
            if (msg.Length > 70) { msg = msg.Substring(0, 70); }

            string smsMsg = PDUString(msg, telno);
            if (smsMsg.Length > 0)
            {
                SendInPDU(smsMsg);
            }
            Thread.Sleep(1000);
        }
        public void SendTestMsgSMS(PROFILEINFO zInfo)
        {
            EnQueue(zInfo);
        }

        public void SendTrueMsgSMS2(string telno, string msg)
        {
            if (telno.Length < 5) return;
            if (telno[0] == '0') { telno = "+66" + telno.Trim().Substring(1); }
            if (msg.Length > 70) { msg = msg.Substring(0, 70); }

            string smsMsg = PDUString(msg, telno);
            if (smsMsg.Length > 0)
            {
                SendInPDU(smsMsg);
            }
            Thread.Sleep(1000);
        }
        public void SendTrueMsgSMS2(PROFILEINFO zInfo)
        {
            EnQueue(zInfo);
        }

        public Boolean SendMsgSMS2(string telno, string msg)
        {
            Boolean sus = false;
            try
            {
                if (msg.Length > 0 && telno.Length > 11)
                {
                    int _resend = 0;

                    while (_resend < 3)
                    {
                        if (!sus)
                        {
                            sus = SendMsg(msg, telno.Trim());
                            _resend++;
                        }
                        else
                        {
                            _resend++;
                        }
                    }
                }
            }
            catch { }
            return sus;
        }

        private void MonitorThread()
        {
            while (!_IsThreadAbort)
            {
                if (smsQueue.Count > 0)
                {
                    //SendToContact();
                }
                else if (bulksmsQueue.Count > 0)
                {
                    //SendToContactBulkSms();
                }

                if (OnSMSQueueMonitor != null)
                {
                    int count = smsQueue.Count + bulksmsQueue.Count;
                    OnSMSQueueMonitor(count.ToString());
                }
                Thread.Sleep(1000);
            }
        }
        private void SendToContact()
        {
            try
            {
                Clear();
                while (smsQueue.Count > 0)                
                {
                    PROFILEINFO info = (PROFILEINFO)smsQueue.Dequeue();

                    DateTime dt = DateTime.Now ;
                    String dtStr = info.timestamp;
                    try
                    {
                        dt      =   Convert.ToDateTime(info.timestamp) ;
                        dtStr   =   dt.ToString("ddMMyy HHmmss", new System.Globalization.CultureInfo("en-US")) ;
                    }
                    catch{
                    
                    }
                    
                    if (Diag() > -1)
                    {
                        if (info.tel_no.IndexOf("+66") > -1)
                        {
                            info.tel_no = "0" + info.tel_no.Substring(3, info.tel_no.Length - 3);
                        }
                        string tel_no = info.tel_no;

                        if (tel_no == "0000000000" || tel_no == "0123456789")
                        {
                            break;
                        }

                        string body_txt = "";
                        switch (info.type)
                        {
                                /// True
                            case PROFILEINFO_TYPE.VALUE_TYPE:
                                body_txt = info.value;
                                break;
                                /// WatchDog
                            case PROFILEINFO_TYPE.ALERT:
                                body_txt = info.msg + "," + info.timestamp;
                                break;
                                /// Normal Sms
                            default:
                                body_txt = GetBodyText(info);
                                    //info.subject + ":" +
                                    //info.value + "," +
                                    //info.veh_reg + "," +
                                    //info.speed.ToString() + "," +
                                    //dtStr + "," +
                                    //string.Format("{0}", Math.Round(info.lat, 5)) + "," +
                                    //string.Format("{0}", Math.Round(info.lon, 5)) + "," +
                                    //info.location;
                                break;
                        }

                        //if (body_txt.Length > 70)
                        //{
                        //    body_txt = body_txt.Substring(0, 70);
                        //}
                        int idx = tel_no.IndexOf('0');
                        if (idx > -1 && tel_no.Length >= 10)
                        {
                            tel_no = "+66" + tel_no.Substring(idx + 1);

                            string msg = PDUString(body_txt, tel_no);
                            SendInPDU(msg);

                            if (OnSendSMSCompleted != null)
                            {
                                OnSendSMSCompleted(this, info);

                                gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.SMS, info);
                                //MessageBox.Show("OnsendSMSComplete");
                            }
                        }
                        else
                        {
                           //MessageBox.Show(idx + " >> " + tel_no.Length + " >> " + tel_no);
                           // GlobalClass.WriteLogEvent("ZebraBroadcast::MainFrm::MainFrm_Shown() : There is the wrong number!!!");
                        }                        
                    }
                    else
                    {
                        smsQueue.Enqueue(info);
                    }
                    Thread.Sleep(3000);
                }
            }
            catch (Exception ex)
            {
                //GlobalClass.WriteLogEvent("ZebraBroadcast::SMSCls::SendToContact() :" + ex.Message);
            }
        }
        private void SendToContactBulkSms()
        {
            PROFILEINFO info = new PROFILEINFO();

            try
            {
                string body_txt = "";

                if (bulksmsQueue.Count > 0)
                {
                    info = (PROFILEINFO)bulksmsQueue.Dequeue();
                   
                    string tel_no = GetTelNo(info.tel_no);
                    if (tel_no.StartsWith("66"))
                    {
                        body_txt = GetBodyText(info);

                        info.srvResp = SendBulkSMS(tel_no, body_txt, info.srvResp, info);

                        if (info.srvResp.STATUS == RESP_STATUS.OK)
                        {
                            //--- Success gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.SMS, info);
                            CreateSMID(info.srvResp.SMID, tel_no);

                            foreach (Int64 smid in info.srvResp.SMID)
                            {
                                gSql.InsertBulkSmsNewBroadcastLog(info, smid,tel_no);
                            }
                            if (OnSendBulkSMSCompleted != null)
                            {
                                OnSendBulkSMSCompleted(this, info, info.srvResp.SMID.Length);
                            }
                        }
                        else if (info.srvResp.STATUS == RESP_STATUS.ERR && info.srvResp.resendCnt <= gResendCnt)
                        {
                            ///---convert to send sms modem
                            if (info.evt_id == (int)EVT_TYPE.WATCHDOG)
                            {
                                WatchDogConvertToSMSModem(info);
                            }
                            //--- Resend
                            else  
                                bulksmsQueue.Enqueue(info);
                        }
                        else
                        {
                            CreateLogErr(info, "#1 Resend out");
                        }
                    }
                }
            }
            catch(Exception e)
            {
                CreateLogErr(info, e.Message);
            }
        }

        private SERVER_RESP SendBulkSMS(string zTelno, string zMsg, SERVER_RESP resp, PROFILEINFO info)
        {
            resp.STATUS = RESP_STATUS.ERR;

            try
            {
                String webUrl = string.Format(@"http://{0}:{1}", _gBulkSmsIP,_gBulkSmsPort);
                WebRequest request = WebRequest.Create(webUrl);

                request.Method = "POST";

                string postData = string.Format(
                //@"TRANSID=BULK&CMD=SENDMSG&FROM=66817537143&TO={0}&REPORT=Y&CHARGE=Y&CODE=K_Trak_BulkSMS&CTYPE=UNICODE&CONTENT={1}", zTelno, GetUniCode(zMsg));
                //@"TRANSID=BULK&CMD=SENDMSG&FROM=9009000&TO={0}&REPORT=Y&CHARGE=Y&CODE=TEXT&CTYPE=TEXT&CONTENT={1}", GetTelNo(zTelno), zMsg);
                //@"TRANSID=BULK&CMD=SENDMSG&FROM=9009000&TO={0}&REPORT=Y&CHARGE=Y&CODE=UNICODE&CTYPE=TEXT&CONTENT={1}", GetTelNo(zTelno), GetUniCode(zMsg)); //-- Send 2 SMID because longer than 1 message
                
                @"TRANSID=BULK&CMD=SENDMSG&FROM=66817537143&TO={0}&REPORT=Y&CHARGE=Y&CODE=K_Trak_BulkSMS&CTYPE=UNICODE&CONTENT={1}", GetTelNo(zTelno), GetUniCode(zMsg));
               
                //string postData = "TRANSID=BULK&CMD=SENDMSG&FROM=9009000&TO=6618881234&REPORT=Y&CHARGE=Y&CODE=TEXT&CTYPE=TEXT&CONTENT=test message";
                //string postData = string.Format("TRANSID=BULK&CMD=SENDMSG&FROM=9009000&TO=6618881234&REPORT=Y&CHARGE=Y&CODE=TEXT&CTYPE=TEXT&CONTENT={0}", zMsg);
               // string postData = "TRANSID=BULK&CMD=SENDMSG&FROM=9009000&TO=6618881234&REPORT=Y&CHARGE=Y&CODE=TEXT&CTYPE=TEXT&CONTENT=test message";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                Console.WriteLine(responseFromServer);

                resp = GetSrvResp(responseFromServer, resp, info);

                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                resp.resendCnt += 1;
                CreateLogErr(info, "#2 " + ex.Message);
            }
            return resp;
        }

        private SERVER_RESP GetSrvResp(string zRspStr, SERVER_RESP resp, PROFILEINFO info)
        {
            resp.STATUS = RESP_STATUS.ERR;
            resp.resendCnt += 1;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(zRspStr);

                XmlNode node = null;
                node = doc.SelectSingleNode("/XML/STATUS");
                resp.STATUS = GetRespStatus(node.InnerText);

                node = null;
                node = doc.SelectSingleNode("/XML/DETAIL");
                resp.DETAIL = node.InnerText;

                node = null;
                node = doc.SelectSingleNode("/XML/SMID");

                if (resp.STATUS == RESP_STATUS.OK)
                {
                    string[] smid_str = node.InnerText.Split(new char[] { ',' });
                    resp.SMID = new Int64[smid_str.Length];
                    int i = 0;
                    foreach (string smid in smid_str)
                    {
                        resp.SMID[i] = Convert.ToInt64(smid);
                        i++;
                    }
                }
            }
            catch
            {
                resp.STATUS = RESP_STATUS.ERR;
            }

            if (resp.STATUS == RESP_STATUS.ERR)
            {
                string erMsg = resp.DETAIL;
                if (resp.DETAIL == null) { erMsg = zRspStr; }
                CreateLogErr(info, "#3 " + erMsg.Trim());
            }
            return resp;
        }

        private RESP_STATUS GetRespStatus(string value)
        {
            RESP_STATUS ret = RESP_STATUS.ERR;
            if (value.Trim() == "OK")
            { ret = RESP_STATUS.OK; }
            return ret;
        }

        private string GetTelNo(string zTelNo)
        {
            if (zTelNo.Length == 10 && zTelNo != "0123456789" && (zTelNo.StartsWith("08") || zTelNo.StartsWith("09") || zTelNo.StartsWith("06")))
            {
                int idx;
                while (true)
                {
                    idx = zTelNo.IndexOf('-');

                    if (idx != -1)
                    {
                        zTelNo = zTelNo.Remove(idx, 1);
                    }
                    else
                        break;
                }

                if (zTelNo.Length == 10)
                {
                    zTelNo = "66" + zTelNo.Substring(1, zTelNo.Length - 1);
                }
            }
            
            return zTelNo;
        }

        private string GetUniCode(string zValueStr)
        {
            string retStr = "";
            if (zValueStr.Length > 0)
            {
                Encoding enc = Encoding.Unicode;
                byte[] msgByte = enc.GetBytes(zValueStr);
                for (int i = 0; i < msgByte.Length; )
                {
                    int i1 = (int)msgByte[i];
                    int i2 = (int)msgByte[i + 1];
                    retStr += String.Format("%{1:X2}%{0:X2}", i1, i2);
                    i += 2;
                }
            }
            return retStr;
        }

        private void CreateSMID(Int64[] zSMID, string tel)
        {
            semaphor.WaitOne();

            String logPath = Directory.GetCurrentDirectory() + "\\Log";
            if (!Directory.Exists(logPath))
            {
                Directory.CreateDirectory(logPath);
            }

            String fileName = logPath + "\\" + "DataLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
            if (File.Exists(fileName))
            {
                foreach (Int64 smid in zSMID)
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(smid.ToString() + " ," + DateTime.Now.ToString() + " ," + tel);
                    sr.Close();
                }
            }
            else
            {
                foreach (Int64 smid in zSMID)
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(smid.ToString() + " ," + DateTime.Now.ToString() + " ," + tel);
                    sr.Close();
                }
            }

            semaphor.Release();
        }

        private string GetBodyText(PROFILEINFO info)
        {
            string text = "";
            DateTime dt = DateTime.Now;
            String timeStamp = dt.ToString("ddMMyy/HHmm", new System.Globalization.CultureInfo("en-US"));
            try
            {
                dt = Convert.ToDateTime(info.timestamp);
                timeStamp = dt.ToString("ddMMyy/HHmm", new System.Globalization.CultureInfo("en-US"));
            }
            catch { }

            string location = info.location;
            if (info.location != null && info.location.Length > 18)
            { 
                location = info.location.Substring(0, 18); 
            }
            try
            {
                switch (info.format_send)
                {
                    case (int)SMSFORMAT.NORMAL:
                        {
                            text = info.subject + ":" + info.value + "," + info.veh_reg + "," + timeStamp + "," + location; break;
                        }
                    case (int)SMSFORMAT.GPSPLUG:
                        {
                            text = info.subject + "," + info.veh_reg + "," + timeStamp + "," + location; break;
                        }
                    case (int)SMSFORMAT.IOPORT:
                        {
                            text = info.subject + "," + info.veh_reg + "," + timeStamp + "," + location ; break;
                        }
                    case (int)SMSFORMAT.POWERLINE:
                        {
                            text = info.subject + "," + info.veh_reg + "," + timeStamp + "," + location; break;
                        }
                    case (int)SMSFORMAT.TRUEFORMATZONE:
                        {
                            text = info.subject + ":" + info.value + "," + info.veh_reg + "," + timeStamp + "," + location; break;
                        }
                    case (int)SMSFORMAT.TRUEFORMATIO:
                        {
                            text = info.subject + "," + info.veh_reg + "," + timeStamp + "," + location; break;
                        }
                    case (int)SMSFORMAT.TESTSYNTAX:
                        {
                            text = info.value; break;
                        }
                    case (int)SMSFORMAT.ENGINE_ON_IN_PROHITBIT_TIME:
                        {
                            string[] lction = info.location.Split(',');
                            string[] drive = info.driver.Split(' ');
                            text = info.veh_reg + "," + drive[0] +","+ info.subject  +"," + timeStamp + "," + lction[lction.Length - 1]; 
                            break;
                        }
                    case (int)SMSFORMAT.SPEEDING:
                        {
                            string[] lction = info.location.Split(',');
                            string[] drive = info.driver.Split(' ');
                            text = info.veh_reg + "," + drive[0] + "," + info.subject +","+info.speed+ "," + timeStamp + "," + lction[lction.Length - 1];
                            break;
                        }
                    case (int)SMSFORMAT.WatchDog:
                        {
                            text = info.msg + "," + info.timestamp; break;
                        }
                }

                if (!(info.format_send == (int)SMSFORMAT.WatchDog || info.format_send == (int)SMSFORMAT.TESTSYNTAX) && info.alert_type == ALERT_TYPE.BULK_SMS && info.msg.Length > 1)
                {
                    text += "," + info.msg;
                }

                //Cut Off
                if (info.format_send == (int)SMSFORMAT.IOPORT || info.format_send == (int)SMSFORMAT.GPSPLUG )
                {
                    if (text.Length > 140) { text = text.Substring(0, 140); }
                }
                else if(info.format_send == (int)SMSFORMAT.WatchDog)
                {
                    text = text.Trim();
                }
                //else
                //{
                //    if (text.Length > 70) { text = text.Substring(0, 70); }
                //}
            }
            catch { return ""; }
            return text;
        }

        private string GetSubjectSMS(int evt_type, int veh_id)
        {
            string subject = "?";
            try
            {
                subject = gSql.GetEvtAbbreviationDesc(evt_type);
            }
            catch
            { }

            return subject;
        }

        private void CreateLogErr(PROFILEINFO msg, string errMessage)
        {
            try
            {
                StreamWriter wrt = new StreamWriter("ErrLog.txt", true, Encoding.Default);

                wrt.WriteLine(
                        errMessage + ": " +
                        "SendTime:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", new System.Globalization.CultureInfo("en-US")) + " " +
                        "VID:" + msg.veh_id.ToString() + " " +
                        "Time:" + msg.timestamp + " " +
                        "Resend:" + msg.srvResp.resendCnt.ToString() + " " +
                        "Tel:" + msg.tel_no
                    );

                wrt.Dispose();
                wrt.Close();
            }
            catch
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        public void RecordLog(PROFILEINFO info)
        {
            gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.SMS, info);
        }

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        private void WriteNetFile(String msg)
        {
            semaphor.WaitOne();

            String logPath = Directory.GetCurrentDirectory() + "\\Log";
            if (!Directory.Exists(logPath))
            {
                Directory.CreateDirectory(logPath);
            }

            String fileName = logPath + "\\" + "DataLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
            }
            semaphor.Release();
        }
        private void WriteNetFileCompleted(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            WriteFileProc proc = (WriteFileProc)result.AsyncDelegate;
            proc.EndInvoke(ar);
        }

        /// <queue summary>
        /// Queue Function 
        /// </summary>
        /// <param name="QueueName"></param>
        
        public void InitQ(string QueueName)
        {
            string GlobalQPath = String.Format(".\\Private$\\{0}", QueueName);
            if (MessageQueue.Exists(GlobalQPath)) {
                MQ = new MessageQueue(GlobalQPath);
            } else {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }
        public void PushQ(object txtMsg)
        {
            try
            {
                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(txtMsg.GetType());
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, txtMsg);
                txtWr.Close();

                byte[] buff = Encoding.UTF8.GetBytes(txtWr.ToString());
                Stream stm = new MemoryStream(buff);

                System.Messaging.Message qMsg = new System.Messaging.Message();
                qMsg.BodyStream = stm;
                MQ.Send(qMsg);
            }
            catch(Exception ex)
            {
                WriteLogEvent("ZebraBroadcast::SMS Error #1 :" + ex.Message);
            }
        }

        //public bool PushQ_New(object txtMsg)
        //{
        //    bool Status = false;
        //    try
        //    {
        //        XmlWriterSettings wrSettings = new XmlWriterSettings();
        //        wrSettings.Indent = true;

        //        XmlSerializer s = new XmlSerializer(txtMsg.GetType());
        //        TextWriter txtWr = new StringWriter();

        //        XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
        //        xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

        //        s.Serialize(xmlWriter, txtMsg);
        //        txtWr.Close();

        //        byte[] buff = Encoding.UTF8.GetBytes(txtWr.ToString());
        //        Stream stm = new MemoryStream(buff);

        //        System.Messaging.Message qMsg = new System.Messaging.Message();
        //        qMsg.BodyStream = stm;
        //        MQ.Send(qMsg);
        //        Status = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLogEvent("ZebraBroadcast::SMS Error #1 :" + ex.Message);
        //    }
        //    return Status;
        //}
        private void WriteLogEvent(String msg)
        {
            /*
            msg = DateTime.Now.ToString() + " : " + msg;
            String fileName = "SMSLogEvent.txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
             */ 
        }

        private void WatchDogConvertToSMSModem(PROFILEINFO bulksms)
        {
            try
            {
                PROFILEINFO smsModem = new PROFILEINFO();
                smsModem.alert_type = ALERT_TYPE.SMS;
                smsModem.veh_id = bulksms.veh_id;
                smsModem.fleet_id = bulksms.fleet_id;
                smsModem.veh_reg = bulksms.veh_reg;
                smsModem.staff_id = bulksms.staff_id;
                smsModem.driver = bulksms.driver;
                smsModem.contact_name = bulksms.contact_name;
                smsModem.max_speed = bulksms.max_speed;
                smsModem.value = bulksms.value;
                smsModem.email = bulksms.email;
                smsModem.tel_no = bulksms.tel_no;
                smsModem.timestamp = bulksms.timestamp;
                smsModem.location = bulksms.location;
                smsModem.lat = bulksms.lat;
                smsModem.lon = bulksms.lon;
                smsModem.evt_id = bulksms.evt_id;
                smsModem.speed = bulksms.speed;
                smsModem.evt_desc = bulksms.evt_desc;
                smsModem.msg = bulksms.msg;
                smsModem.type = PROFILEINFO_TYPE.ALERT;

                EnQueue(smsModem);
            }
            catch { }
        }

        /// <summary>
        /// True Corp.
        /// Jirayu Kamsiri
        /// 2012/09/12
        /// Send Bulk Sms
        /// </summary>
        public void True2BulkSmsSendToContact(int veh_id, ArrayList usrList, string reg, string Msg, string timestamp, string location, double lat, double lon, EVT_TYPE evt_type,int speed,string zone_evt)
        {
            try
            {
                string subject = GetSubjectSMS(Convert.ToInt32(Msg), veh_id);

                #region
                for (int i = 0; i < usrList.Count; i++)
                {
                    try
                    {
                        PROFILEINFO info = new PROFILEINFO();
                        info.alert_type = ALERT_TYPE.BULK_SMS;
                        info.veh_id = veh_id;
                        info.fleet_id = -1;
                        info.fleet_name = "";
                        info.veh_reg = reg;
                        info.max_speed = -1;
                        info.value = zone_evt;
                        info.tel_no = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[i]).mobile;
                        info.email = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[i]).email;
                        info.subject = subject;
                        info.timestamp = timestamp;
                        info.location = location;
                        info.lat = lat;
                        info.lon = lon;
                        info.staff_id = -1;
                        info.driver = "";
                        info.contact_name = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[i]).name;
                        info.evt_id = (int)evt_type;
                        info.speed = speed;
                        info.evt_desc = evt_type.ToString();
                        info.msg = "";
                        info.type = PROFILEINFO_TYPE.ORIGINAL;

                        if (evt_type == EVT_TYPE.TRUE_ZONEING)
                        {
                            info.format_send = (int)SMSFORMAT.TRUEFORMATZONE;
                        }
                        else if (evt_type == EVT_TYPE.TRUE_IOSTATUS)
                        {
                            info.format_send = (int)SMSFORMAT.TRUEFORMATIO;
                        }

                        EnQueueBulkSms(info);
                    }
                    catch
                    {
                    }
                }
                #endregion
            }
            catch
            {
            }
        }



       
    }
}
